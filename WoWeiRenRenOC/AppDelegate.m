//
//  AppDelegate.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/13.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginVC.h"
#import "BaseNavigationController.h"
#import "BaseTabBarController.h"
#import "FristLaunchAppVC.h"
#define WeChatKey @"wxa864766b0014b9a7"
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
       self.window.backgroundColor = [UIColor whiteColor];
    
//    [LEUserManager deleteUserTags];
    
    
    //向微信注册
    [WXApi registerApp:WeChatKey];
    
    if ([[LEUserManager userTags] isEqualToString:@"1"]) {
        
        BaseTabBarController *baseTabBar = [[BaseTabBarController alloc]init];

        self.window.rootViewController = baseTabBar;
        
    }else {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"kIsAgreedToAgreement"]) {
              LoginVC *vc = [[LoginVC alloc]initWithNibName:@"LoginVC" bundle:nil];
              BaseNavigationController *navigationController = [[BaseNavigationController alloc] initWithRootViewController:vc];

              self.window.rootViewController = navigationController;
           }
        else {
            
               FristLaunchAppVC *vc = [[FristLaunchAppVC alloc]initWithNibName:@"FristLaunchAppVC" bundle:nil];
               BaseNavigationController *navigationController = [[BaseNavigationController alloc] initWithRootViewController:vc];

               self.window.rootViewController = navigationController;
        }
        
    }
       
  
    [self.window makeKeyAndVisible];
    
    [self configureSVP];
    
    return YES;
}

#pragma mark - 全局设置SVP
- (void)configureSVP {
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD setMinimumDismissTimeInterval:1.0];
    [IQKeyboardManager sharedManager].enable = YES;
}


// NOTE: 9.0以后使用新API接口
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options
{
   if ([url.host isEqualToString:@"pay"]){ //微信支付的回调
        NSString *result = [url absoluteString];
        NSLog(@"微信------%@",result);
        NSArray *array = [result componentsSeparatedByString:@"="];
        NSString *resultNumber = [array lastObject];
        NSLog(@"微信------%@",resultNumber);
        if ([resultNumber integerValue] == 0){ //成功
            //发送支付成功的通知
            [[NSNotificationCenter defaultCenter] postNotificationName:NoticePaySuccess object:nil];
        }else if ([resultNumber integerValue] == -1) { //错误
            //发送支付失败的通知
            [[NSNotificationCenter defaultCenter] postNotificationName:NoticePayFailure object:nil];
        }else if ([resultNumber integerValue] == -2){ //用户取消
            //发送支付取消的通知
            [[NSNotificationCenter defaultCenter] postNotificationName:NoticePayCancel object:nil];
        }
    }
    
    if ([url.host isEqualToString:@"safepay"]) {
        //跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            NSLog(@"result = %@",resultDic);
            NSString *resultStatus = [resultDic objectForKey:@"resultStatus"];
               
               if ([resultStatus intValue] == 9000) {
                   //发送支付成功的通知
                   [[NSNotificationCenter defaultCenter] postNotificationName:NoticePaySuccess object:nil];
               }else {
                  //发送支付失败的通知
                  [[NSNotificationCenter defaultCenter] postNotificationName:NoticePayFailure object:nil];
               }
        }];
    }
    
    
    return YES;
}


#pragma mark - UISceneSession lifecycle


//- (UISceneConfiguration *)application:(UIApplication *)application configurationForConnectingSceneSession:(UISceneSession *)connectingSceneSession options:(UISceneConnectionOptions *)options {
//    // Called when a new scene session is being created.
//    // Use this method to select a configuration to create the new scene with.
//    return [[UISceneConfiguration alloc] initWithName:@"Default Configuration" sessionRole:connectingSceneSession.role];
//}
//
//
//- (void)application:(UIApplication *)application didDiscardSceneSessions:(NSSet<UISceneSession *> *)sceneSessions {
//    // Called when the user discards a scene session.
//    // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
//    // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
//}


@end
