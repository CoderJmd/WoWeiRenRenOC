//
//  ShareView.m
//  SchoolApp
//
//  Created by 杨涛 on 2019/4/26.
//  Copyright © 2019 znz. All rights reserved.
//

#import "ShareView.h"
#import "UIButton+ImageTitleSpacing.h"

@implementation ShareView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
/**
 快速创建
 */
+ (instancetype)showView
{
    UIWindow *window = UIApplication.sharedApplication.keyWindow;

    ShareView *view = [[ShareView alloc]init];
    view.frame = window.bounds;
    view.backgroundColor =  [[UIColor blackColor] colorWithAlphaComponent:0.7];
    [view createUI];
    [window addSubview:view];
    
    return view;
}

+ (instancetype)showCodeView
{
    UIWindow *window = UIApplication.sharedApplication.keyWindow;

    ShareView *view = [[ShareView alloc]init];
    view.frame = window.bounds;
    view.backgroundColor =  [[UIColor blackColor] colorWithAlphaComponent:0.7];
    [view createCodeUI];
    [window addSubview:view];
    
    return view;
}




- (void)cancelView {
    
    [UIView animateWithDuration:0.5 animations:^{
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)tap {
    
}

- (void)createCodeUI {
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cancelView)];
        [self addGestureRecognizer:tap];
        
        UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, KSCREEN_HEIGHT - 280, KSCREEN_WIDTH, 220)];
        scrollView.showsHorizontalScrollIndicator = NO;
        [self addSubview:scrollView];
        scrollView.backgroundColor = WhiteColor;
        UITapGestureRecognizer *scrollTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap)];
        [scrollView addGestureRecognizer:scrollTap];


        
        
        NSArray *imgArr;
        NSArray *titleArr;
        imgArr = [NSArray arrayWithObjects:@"Group 42 Copy",@"朋友圈 copy",@"分组 15", @"erweima1",@"kaobei",nil];
        titleArr = [NSArray arrayWithObjects:@"微信好友",@"朋友圈",@"微博", @"生成海报",@"复制链接",nil];
        
    //    [scrollView setContentSize:CGSizeMake(100 * imgArr.count, scrollView.height)];
        
        CGFloat y = 0;
        CGFloat weight = kScreenW / 3;
        for (int i = 0; i < imgArr.count; i ++) {
            CGFloat newX = weight * i;
            if (i  > 2) {
                y = 100;
                weight = kScreenW / 2;
                newX = weight * (i - 3);
            }
            UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(newX, y, weight, 100)];
            [btn setImage:kImageNamed(imgArr[i]) forState:UIControlStateNormal];
            [btn setTitle:titleArr[i] forState:UIControlStateNormal];
            [btn setTitleColor:BlackColor2 forState:UIControlStateNormal];
            [btn.titleLabel setFont:kPRFont(15)];
            [btn setTag:tagValue + i];
            [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
            [btn layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleTop imageTitleSpace:10];
            [scrollView addSubview:btn];
        }
        

        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, scrollView.bottom, KSCREEN_WIDTH, 1)];
        [lineView setBackgroundColor:kColor(221)];
        [self addSubview:lineView];
        
        self.cancelBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, lineView.bottom, KSCREEN_WIDTH, 60)];
        self.cancelBtn.backgroundColor = WhiteColor;
        [self.cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
        [self.cancelBtn setTitleColor:BlackColor forState:UIControlStateNormal];
        [self.cancelBtn addTarget:self action:@selector(cancelView) forControlEvents:UIControlEventTouchUpInside];
        [self.cancelBtn.titleLabel setFont:kPRFont(18)];
        [self addSubview:self.cancelBtn];
    
}

- (void)createUI {
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cancelView)];
    [self addGestureRecognizer:tap];
    
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, KSCREEN_HEIGHT - 180, KSCREEN_WIDTH, 120)];
    scrollView.showsHorizontalScrollIndicator = NO;
    [self addSubview:scrollView];
    scrollView.backgroundColor = WhiteColor;
    UITapGestureRecognizer *scrollTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap)];
    [scrollView addGestureRecognizer:scrollTap];


    
    NSArray *imgArr;
    NSArray *titleArr;
    imgArr = [NSArray arrayWithObjects:@"Group 42 Copy",@"朋友圈 copy",@"分组 15", @"kaobei",nil];
    titleArr = [NSArray arrayWithObjects:@"微信好友",@"朋友圈",@"微博",@"复制链接",nil];
    
    [scrollView setContentSize:CGSizeMake(100 * imgArr.count, scrollView.height)];
    for (int i = 0; i < imgArr.count; i ++) {
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(100 * i, 0, 100, scrollView.height)];
        [btn setImage:kImageNamed(imgArr[i]) forState:UIControlStateNormal];
        [btn setTitle:titleArr[i] forState:UIControlStateNormal];
        [btn setTitleColor:BlackColor2 forState:UIControlStateNormal];
        [btn.titleLabel setFont:kPRFont(15)];
        [btn setTag:tagValue + i];
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [btn layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleTop imageTitleSpace:10];
        [scrollView addSubview:btn];
    }
     
      
    
    
    
    
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, scrollView.bottom, KSCREEN_WIDTH, 1)];
    [lineView setBackgroundColor:kColor(221)];
    [self addSubview:lineView];
    
    self.cancelBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, lineView.bottom, KSCREEN_WIDTH, 60)];
    self.cancelBtn.backgroundColor = WhiteColor;
    [self.cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    [self.cancelBtn setTitleColor:BlackColor forState:UIControlStateNormal];
    [self.cancelBtn addTarget:self action:@selector(cancelView) forControlEvents:UIControlEventTouchUpInside];
    [self.cancelBtn.titleLabel setFont:kPRFont(18)];
    [self addSubview:self.cancelBtn];
}

- (void)btnClick:(UIButton *)sender {
    
    if (self.buttonClick) {
        [self cancelView];
        self.buttonClick(sender.tag);
    }
}

@end
