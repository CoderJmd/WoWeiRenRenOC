//
//  MyPostersShareView.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/11/20.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "MyPostersShareView.h"
extern UserModel *userModel;

@implementation MyPostersShareView

- (void)awakeFromNib {
    
    [super awakeFromNib];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cancelView)];
    [self.deleteImageView addGestureRecognizer:tap];

    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cancelView)];
    [self.cancelBgView addGestureRecognizer:tap1];
    
    [self requestData];
    
    NSString *data = [self getShareURL];
    self.codeImageView.image = [SGQRCodeObtain generateQRCodeWithData:data size:210 logoImage:[UIImage imageNamed:@"logo2"] ratio:0.2];
    self.bgView.layer.cornerRadius = 5;
    self.bgView.clipsToBounds = YES;
    
}

-  (void)requestData{
    
    [NetRequestTool postUrl:@"sysCode/querySysCodeObject" parameters:@{@"type":@(99)} success:^(id responseObject) {
           
        
           SD_SETIMAGE_PlaceHolder(self.iconImageView, responseObject[@"object"][@"depict"], @"default_image_square");
       } failure:^(NSInteger statusCode) {
           
       } finish:^{
           
       }];
    
}

+ (instancetype)showView
{
    UIWindow *window = UIApplication.sharedApplication.keyWindow;

    MyPostersShareView *view = [[NSBundle mainBundle] loadNibNamed:@"MyPostersShareView" owner:self options:nil].lastObject;
    view.frame = window.bounds;
    view.backgroundColor =  [[UIColor blackColor] colorWithAlphaComponent:0.7];
    view.cancelBgView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.1];
    view.deleteImageView.alpha = 1;
    
    [window addSubview:view];

    
    
    return view;
}

- (void)cancelView {
    
    [UIView animateWithDuration:0.5 animations:^{
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)tap {
    
    
}
- (IBAction)saveImgClick:(id)sender {
    
    //UIGraphicsBeginImageContext(self.bgView.bounds.size);     //currentView 当前的view  创建一个基于绘图的图形上下文并指定大小为当前视图的bounds
    UIGraphicsBeginImageContextWithOptions(self.bgView.bounds.size, NO, 0.0);


    [self.bgView.layer renderInContext:UIGraphicsGetCurrentContext()];//renderInContext呈现接受者及其子范围到指定的上下文
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();//返回一个基于当前图形上下文的图片
    UIGraphicsEndImageContext();//移除栈顶的基于当前位图的图形上下文
    UIImageWriteToSavedPhotosAlbum(viewImage, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);//然后将该图片保存到图片图
    
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error  contextInfo:(void *)contextInfo{

 if (error) {

        [SVProgressHUD showErrorWithStatus:@"保存失败"];

    }else{

        [SVProgressHUD showSuccessWithStatus:@"保存成功"];
        [self cancelView];
    }

}




- (NSString *)getShareURL {
    
    
    NSString *userCode= @"";
    if ([CommonTTool isStrNull:userModel.my_code]) {
        userCode = @"";
    }else {
        userCode = [NSString stringWithFormat:@"?code=%@",userModel.my_code];
    }
    
    return [NSString stringWithFormat:@"%@%@",SHARE_HOME_URL,userCode];
    
}


@end
