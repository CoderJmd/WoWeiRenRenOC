//
//  NSString+js.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/30.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "NSString+js.h"




@implementation NSString (js)
//宽度计算
+ (instancetype)getJSWithScreentWidth:(CGFloat) width{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"webviewDeal.js" ofType:nil];
    NSString *js = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:NULL];
    return [NSString stringWithFormat:@"%@ \n autoSizeFit(%@);",js,[NSString stringWithFormat:@"%.2f",width]];
}
//高度计算
+ (instancetype)getJSWithScreentHeightWithWebView:(UIWebView *)webView{
    CGFloat height =[[webView stringByEvaluatingJavaScriptFromString:@"document.getElementsByTagName('body')[0].scrollHeight"] floatValue];
    return [NSString stringWithFormat:@"%f",height];
}
@end
