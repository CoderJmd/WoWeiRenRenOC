//
//  BaseModel.m
//  ScienceAndTechnologyCity
//
//  Created by 杨涛 on 2018/9/14.
//  Copyright © 2018年 znz. All rights reserved.
//

#import "BaseModel.h"

@implementation BaseModel

- (id)mj_newValueFromOldValue:(id)oldValue property:(MJProperty *)property {
    
    if ([oldValue isEqual:[NSNull null]] || [oldValue isKindOfClass:[NSNull class]] || oldValue == nil || [@"" isEqualToString:oldValue] || [CommonTTool isStrNull:oldValue]) { // 以字符串类型为例
        return  @"暂无数据";
    }
    
    return oldValue;
}

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"Id": @"id"};
}


@end
