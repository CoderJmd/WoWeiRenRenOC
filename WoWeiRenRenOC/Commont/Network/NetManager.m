//
//  NetManager.m
//  OriginalPassword
//
//  Created by 杨涛 on 2018/8/20.
//  Copyright © 2018年 znz. All rights reserved.
//

#import "NetManager.h"
#import "NetRequestTool.h" // 网络请求封装
#import "NSObject+AutoProperty.h" // 自动生成模型属性列表


@implementation NetManager

#pragma mark - 登录模块

/** 注册 */
+ (void)isRegisterWithParameters:(NSDictionary *)parameterDic success:(void (^)(NSDictionary *))success finish:(void (^)(void))finish {
    [NetRequestTool postUrl:Register parameters:parameterDic success:^(id responseObject) {
        
        if (success) {
            success(responseObject);
        }
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        
    }];
}


/** 忘记密码 */
+ (void)isForgetWithParameters:(NSDictionary *)parameterDic success:(void (^)(void))success {
    [NetRequestTool postUrl:Forget parameters:parameterDic success:^(id responseObject) {
        
        if (success) {
            success();
        }
        
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        
    }];
}
/** 登录 */
+ (void)isLoginWithParameters:(NSDictionary *)parameterDic success:(void (^)(void))success {
    [NetRequestTool postUrl:Login parameters:parameterDic success:^(id responseObject) {
        NSDictionary *dic = [responseObject objectForKey:@"object"];
        // 去除空值后 直接存字典
        [[NSUserDefaults standardUserDefaults] setObject:[dic deleteNull] forKey:kUserInfo];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kisLogin];
        SVP(@"登录成功");
        [LEUserManager setToken:dic[@"access_token"]];
        
        if (success) {
            success();
        }
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        
    }];
}

/** 登录 */
+ (void)isMerchantsLoginWithParameters:(NSDictionary *)parameterDic success:(void (^)(void))success {
    [NetRequestTool LoginUrl:BusinessLogin parameters:parameterDic success:^(NSInteger statusCode, id responseObject) {
         NSDictionary *dic = [responseObject objectForKey:@"object"];
               // 去除空值后 直接存字典
               [[NSUserDefaults standardUserDefaults] setObject:[dic deleteNull] forKey:kUserMerchantsInfo];
               // 记录是否是商户
               [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kisLogin];
               [LEUserManager setToken:dic[@"access_token"]];
               
               SVP(@"登录成功");
               
               if (success) {
                   success();
               }
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        
    }];
//    [NetRequestTool postUrl:BusinessLogin parameters:parameterDic success:^(id responseObject) {
//        NSDictionary *dic = [responseObject objectForKey:@"object"];
//        // 去除空值后 直接存字典
//        [[NSUserDefaults standardUserDefaults] setObject:[dic deleteNull] forKey:kUserMerchantsInfo];
//        // 记录是否是商户
//        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kisLogin];
//        [LEUserManager setToken:dic[@"access_token"]];
//        
//        SVP(@"登录成功");
//        
//        if (success) {
//            success();
//        }
//    } failure:^(NSInteger statusCode) {
//        
//    } finish:^{
//        
//    }];
}




/** banner */
+ (void)isBannerinfoListWithParameters:(NSDictionary *)parameterDic success:(void (^)(NSDictionary *))success finish:(void (^)(void))finish {
    [NetRequestTool postUrl:BannerinfoList parameters:parameterDic success:^(id responseObject) {
        NSMutableArray *array = [responseObject objectForKey:@"object"];
        
        if (array.count) {
            [NSObject printPropertyWithDict:array[0]];
        }
        
        if (success) {
            success(responseObject);
        }
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        
    }];
}

/** 更新用户n信息 */
+ (void)isUpdateUserInfoWithParameters:(NSDictionary *)parameterDic success:(void (^)(void))success {
    [NetRequestTool postUrl:updateUserInfo parameters:parameterDic success:^(id responseObject) {
        
        if (success) {
            success();
        }
        
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        
    }];
}

/** 查询平台数据 */
+ (void)isQueryTjWithParameters:(NSDictionary *)parameterDic success:(void (^)(NSDictionary *))success finish:(void (^)(void))finish {
    [NetRequestTool postUrl:QueryTj parameters:parameterDic success:^(id responseObject) {
        
        if (success) {
            success(responseObject);
        }
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        
    }];
}

/** 下单 */
+ (void)isOrderSaveOrderParameters:(NSDictionary *)parameterDic success:(void (^)(NSDictionary *))success finish:(void (^)(void))finish {
    [NetRequestTool postUrl:OrderSaveOrder parameters:parameterDic success:^(id responseObject) {
        
        if (success) {
            success(responseObject);
        }
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        
    }];
}

/** 获取支付宝的支付参数 */
+ (void)isGetAliPayInfoParameters:(NSDictionary *)parameterDic success:(void (^)(NSDictionary *))success finish:(void (^)(void))finish {
    [NetRequestTool postUrl:GetAliPayInfo parameters:parameterDic success:^(id responseObject) {
        
        if (success) {
            success(responseObject);
        }
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        
    }];
}

/** 获取微信支付参数 */
+ (void)isGetPreReqParamsModelParameters:(NSDictionary *)parameterDic success:(void (^)(NSDictionary *))success finish:(void (^)(void))finish {
    [NetRequestTool postUrl:GetPreReqParamsModel parameters:parameterDic success:^(id responseObject) {
        
        if (success) {
            success(responseObject);
        }
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        
    }];
}


#pragma mark - 上传文件
/** 上传文件 */
+ (void)isUpdateImgArr:(NSArray *)imgArr andData:(NSArray *)dataArr andFileName:(NSString *)fileName success:(void (^)(NSDictionary *))success uploadProgress:(void (^)(NSProgress *))progress {
    if (!dataArr.count && [CommonTTool isStrNull:fileName]) {
        NSDictionary *parameters = @{
                                     @"bucket":@"ofaimg", // 类型 1 图片 2 视频
                                     };
        [NetRequestTool upLoadImages:UpdateImageAliyunFile parameters:parameters image:imgArr success:^(id responseObject) {
            if (success) {
                success(responseObject);
            }
        } failure:^{
            
        } finish:^{
            
        }];
    } else {
        NSDictionary *parameters = @{
                                     @"type":@"2", // 类型 1 图片 2 视频
                                     };
        [NetRequestTool upLoadVideo:UpdateImageAliyunFile parameters:parameters dataArr:dataArr fileName:fileName success:^(id responseObject) {
            SVP(@"上传成功");
            if (success) {
                success(responseObject);
            }
        } failure:^{
            
        } finish:^{
            
        } uploadProgress:^(id completed) {
            if (progress) {
                progress(completed);
            }
        }];
    }
}




#pragma mark - 返回事件
+ (void)back {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[CommonTTool getCurrentVC].navigationController popViewControllerAnimated:YES];
    });
}

@end
