//
//  NetManager.h
//  OriginalPassword
//
//  Created by 杨涛 on 2018/8/20.
//  Copyright © 2018年 znz. All rights reserved.
//

#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN
@interface NetManager : NSObject

#pragma mark - 登录模块
/** 注册 */
+ (void)isRegisterWithParameters:(NSDictionary *)parameterDic success:(void (^)(NSDictionary *dic))success finish:(void (^)(void))finish;
/** 忘记密码 */
+ (void)isForgetWithParameters:(NSDictionary *)parameterDic success:(void (^)(void))success;
/** 登录 */
+ (void)isLoginWithParameters:(NSDictionary *)parameterDic success:(void (^)(void))success;
/** 商户登录 */
+ (void)isMerchantsLoginWithParameters:(NSDictionary *)parameterDic success:(void (^)(void))success;
/** banner */
+ (void)isBannerinfoListWithParameters:(NSDictionary *)parameterDic success:(void (^)(NSDictionary *dic))success finish:(void (^)(void))finish;
/** 更新用户信息 */
+ (void)isUpdateUserInfoWithParameters:(NSDictionary *)parameterDic success:(void (^)(void))success;
/** 查询平台数据 */
+ (void)isQueryTjWithParameters:(NSDictionary *)parameterDic success:(void (^)(NSDictionary *dic))success finish:(void (^)(void))finish;
/** 下单 */
+ (void)isOrderSaveOrderParameters:(NSDictionary *)parameterDic success:(void (^)(NSDictionary *dic))success finish:(void (^)(void))finish;
/** 获取支付宝支付参数 */
+ (void)isGetAliPayInfoParameters:(NSDictionary *)parameterDic success:(void (^)(NSDictionary *dic))success finish:(void (^)(void))finish;
/** 获取微信支付参数 */
+ (void)isGetPreReqParamsModelParameters:(NSDictionary *)parameterDic success:(void (^)(NSDictionary *dic))success finish:(void (^)(void))finish;



#pragma mark - 上传文件
/** 上传文件 */
+ (void)isUpdateImgArr:(NSArray *)imgArr andData:(NSArray *)dataArr andFileName:(NSString *)fileName success:(void (^)(NSDictionary *dic))success  uploadProgress:(void (^)(NSProgress *str))progress;

@end
NS_ASSUME_NONNULL_END



