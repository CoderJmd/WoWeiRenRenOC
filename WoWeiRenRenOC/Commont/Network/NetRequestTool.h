//
//  NetRequestTool.h
//  xiantaiApp
//
//  Created by dsy on 16/12/16.
//  Copyright © 2016年 jw. All rights reserved.
//

#import <Foundation/Foundation.h>


// 测试环境
// #define BASE_URL @"http://syapi.znzkj.net/baj_api/s/api"
// 生产环境
// #define BASE_URL @""


@interface NetRequestTool : NSObject

+ (AFHTTPSessionManager *)manager;

#pragma mark - 登录接口 无token
+ (void)LoginUrl:(NSString *)url parameters:(NSDictionary *)para success:(void (^)(NSInteger statusCode, id responseObject))success failure:(void (^)(NSInteger statusCode))failure finish:(void (^)(void))finish;

#pragma mark - 通用POST请求(以NSDictionary个是上传数据)
+ (void)postUrl:(NSString *)url parameters:(NSDictionary *)para success:(void (^)(id responseObject))success failure:(void (^)(NSInteger statusCode))failure finish:(void (^)(void))finish;

+ (void)noTokenPostUrl:(NSString *)url parameters:(NSDictionary *)para success:(void (^)(id responseObject))success failure:(void (^)(NSInteger statusCode))failure finish:(void (^)(void))finish;

#pragma mark - 通用POST请求
+ (void)postUrl:(NSString *)url idParameters:(id)para success:(void (^)(id responseObject))success failure:(void (^)(NSInteger statusCode))failure finish:(void (^)(void))finish;

#pragma mark - 通用GET请求(一般不用)
+ (void)getUrl:(NSString *)url parameters:(NSDictionary *)para success:(void (^)(id responseObject))success failure:(void (^)(void))failure finish:(void (^)(void))finish;

#pragma mark - 上传图片
+ (void)upLoadImage:(NSString *)url parameters:(NSDictionary *)para image:(UIImage *)image success:(void (^)(id responseObject))success failure:(void (^)(void))failure finish:(void (^)(void))finish;

#pragma mark - 上传多张图片
+ (void)upLoadImages:(NSString *)url parameters:(NSDictionary *)para image:(NSArray *)images success:(void (^)(id responseObject))success failure:(void (^)(void))failure finish:(void (^)(void))finish;

#pragma mark - 上传视频
+ (void)upLoadVideo:(NSString *)url parameters:(NSDictionary *)para dataArr:(NSArray *)dataArr fileName:(NSString *)fileName success:(void (^)(id responseObject))success failure:(void (^)(void))failure finish:(void (^)(void))finish uploadProgress:(void (^)(id completed))progress;

#pragma mark - 上传文件（图片、视频）
+ (void)upLoadFile:(NSString *)url parameters:(NSDictionary *)para image:(NSArray *)images data:(NSArray *)datas fileName:(NSString *)fileName success:(void (^)(id responseObject))success failure:(void (^)(void))failure finish:(void (^)(void))finish;

#pragma mark - 使用AFNetworking 将对象作为参数(放入body)传给后台
+ (void)startRequestWithUrl:(NSString *)url method:(NSString *) method params:(id)params success:(void (^)(id responseObject))success failure:(void (^)(NSInteger statusCode))failure finish:(void (^)(void))finish;

@end
