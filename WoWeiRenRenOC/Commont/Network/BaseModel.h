//
//  BaseModel.h
//  ScienceAndTechnologyCity
//
//  Created by 杨涛 on 2018/9/14.
//  Copyright © 2018年 znz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseModel : NSObject

//类型 1 关注类通知 2 动态类通知 3 圈子类通知 4 圈子审核信息
@property (nonatomic, assign) NSInteger t;

@end
