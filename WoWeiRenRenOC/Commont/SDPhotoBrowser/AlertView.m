//
//  AlertView.m
//  SchoolApp
//
//  Created by 杨涛 on 2019/5/14.
//  Copyright © 2019 znz. All rights reserved.
//

#import "AlertView.h"

@interface AlertView () <UITableViewDelegate, UITableViewDataSource>

@end

@implementation AlertView

- (void)createUI {
    self.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.delegate = self;
    self.dataSource = self;
    self.layer.cornerRadius = 3;
    [self setBackgroundColor:ClearColor];
    self.tableFooterView = nil;
    
    self.frame = CGRectMake(0, 0, self.width, self.height);
    
    self.layer.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0].CGColor;
    self.layer.cornerRadius = 3;
    self.layer.shadowColor = [UIColor colorWithRed:102/255.0 green:118/255.0 blue:123/255.0 alpha:0.08].CGColor;
    self.layer.shadowOffset = CGSizeMake(0,3);
    self.layer.shadowOpacity = 1;
    self.layer.shadowRadius = 7;
}

#pragma mark- UITableViewDelegate, UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return self.dataArr.count;
    } else {
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    [cell.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    [cell setBackgroundColor:WhiteColor];
    
    tableView.rowHeight = 55;
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.width, tableView.rowHeight)];
    if (indexPath.section == 0) {
        [titleLabel setText:[NSString stringWithFormat:@"%@",self.dataArr[indexPath.row]]];
        [titleLabel setTextColor:BlueColor];
    } else {
        [titleLabel setText:@"取消"];
        [titleLabel setTextColor:BlackColor2];
    }
    [titleLabel setTextAlignment:1];
    [titleLabel setFont:kPRFont(19)];
    [cell.contentView addSubview:titleLabel];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(20, tableView.rowHeight - 1, self.width - 20 * 2, 1)];
    [lineView setBackgroundColor:kColor(233)];
    [cell.contentView addSubview:lineView];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
        if (self.cellDidBlock) {
            self.cellDidBlock(indexPath.row + tagValue);
        }
    } else {
        if (self.cellDidBlock) {
            self.cellDidBlock(tagValue * 2);
        }
    }
}

#pragma mark - section头部间距
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == 1) {
        return 9.0f;
    }
    return 0.000000000000001f;
}

#pragma mark - section头部视图
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (section == 1) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake( 0, 0, KSCREEN_WIDTH, 9.0f)];
        view.backgroundColor = VCBgcolor;
        return view;
    }
    return nil;
}

@end
