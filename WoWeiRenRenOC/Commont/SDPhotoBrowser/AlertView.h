//
//  AlertView.h
//  SchoolApp
//
//  Created by 杨涛 on 2019/5/14.
//  Copyright © 2019 znz. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^CellDidBlock)(NSInteger tag);

@interface AlertView : UITableView

@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic,copy) CellDidBlock cellDidBlock;

- (void)createUI;

@end

NS_ASSUME_NONNULL_END
