//
//  JXQrCodeBottomView.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/18.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface JXQrCodeBottomView : UIView
@property (weak, nonatomic) IBOutlet UIButton *flashBtn;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

NS_ASSUME_NONNULL_END
