//
//  ShareView.h
//  SchoolApp
//
//  Created by 杨涛 on 2019/4/26.
//  Copyright © 2019 znz. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ShareView : UIView

- (void)createUI;

@property (nonatomic, copy) void(^buttonClick)(NSInteger tag);
@property (nonatomic, strong) UIButton *cancelBtn;
@property (assign, nonatomic) BOOL isShareNote;

+ (instancetype)showView;
+ (instancetype)showCodeView;
- (void)cancelView;

@end

NS_ASSUME_NONNULL_END
