//
//  LEUserManager.h
//  CarWash
//
//  Created by 汪涛 on 16/12/17.
//  Copyright © 2016年 汪涛. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LEUserManager : NSObject

#pragma mark - 用户Token 获取，存储，删除
+ (NSString *)token;
+ (BOOL)setToken:(NSString *)token;
+ (BOOL)deleteToken;


#pragma mark - 清空所有本地存储数据
+ (void)deleteAllObject;


@end
