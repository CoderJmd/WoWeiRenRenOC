//
//  LEUserManager.m
//  CarWash
//
//  Created by 汪涛 on 16/12/17.
//  Copyright © 2016年 汪涛. All rights reserved.
//

#import "LEUserManager.h"

#define kLastEnterTel            @"lastEnterTel"
#define kToken              @"access_token"



@implementation LEUserManager

#pragma mark - 用户token
+ (NSString *)token {
    return [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:kToken]];
}
+ (BOOL)setToken:(NSString *)token {
    if (token == nil) {
        NSLog(@"token为空");
        return NO;
    } else {
        [[NSUserDefaults standardUserDefaults] setObject:token forKey:kToken];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return YES;
    }
}
+ (BOOL)deleteToken {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    return YES;
}



@end
