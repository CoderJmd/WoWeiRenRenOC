//
//  NewShareView.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/7/25.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "NewShareView.h"
extern UserModel *userModel;

@implementation NewShareView

- (void)awakeFromNib {
    
    [super awakeFromNib];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cancelView)];
    [self addGestureRecognizer:tap];
    UITapGestureRecognizer *tapTwo = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap)];
    [self.bgView addGestureRecognizer:tapTwo];
    
    NSString *data = [self getShareURL];
    self.imgView.image = [SGQRCodeObtain generateQRCodeWithData:data size:210 logoImage:[UIImage imageNamed:@"logo2"] ratio:0.2];
    
    
    
}

- (void)setDataString:(NSString *)dataString {
    
    _dataString = dataString;
    self.imgView.image = [SGQRCodeObtain generateQRCodeWithData:dataString size:210 logoImage:[UIImage imageNamed:@"logo2"] ratio:0.2];
    
    
}

+ (instancetype)showView
{
    UIWindow *window = UIApplication.sharedApplication.keyWindow;

    NewShareView *view = [[NSBundle mainBundle] loadNibNamed:@"NewShareView" owner:self options:nil].lastObject;
    view.frame = window.bounds;
    view.backgroundColor =  [[UIColor blackColor] colorWithAlphaComponent:0.7];
    
    [window addSubview:view];

    
    
    return view;
}




- (void)cancelView {
    
    [UIView animateWithDuration:0.5 animations:^{
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)tap {
    
    
}
- (IBAction)saveImgClick:(id)sender {
    
    UIImageWriteToSavedPhotosAlbum(self.imgView.image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
    
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error  contextInfo:(void *)contextInfo{

 if (error) {

        [SVProgressHUD showErrorWithStatus:@"保存失败"];

    }else{

        [SVProgressHUD showSuccessWithStatus:@"保存成功"];
        [self cancelView];
    }

}


- (IBAction)cancelClick:(id)sender {
    
    [self cancelView];
}

- (NSString *)getShareURL {
    
    
    NSString *userCode= @"";
    if ([CommonTTool isStrNull:userModel.my_code]) {
        userCode = @"";
    }else {
        userCode = userModel.my_code;
    }
    
    return [NSString stringWithFormat:@"woweirenren-%@",userCode];
    
}

@end
