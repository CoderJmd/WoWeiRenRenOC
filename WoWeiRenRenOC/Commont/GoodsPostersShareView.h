//
//  GoodsPostersShareView.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/11/20.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MailGoodsDetailsModel.h"
#import "CustomLabel.h"

NS_ASSUME_NONNULL_BEGIN

@interface GoodsPostersShareView : UIView
@property (strong, nonatomic)NSString *dataString;
@property (weak, nonatomic) IBOutlet UIImageView *codeImageView;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UIImageView *deleteImageView;
@property (weak, nonatomic) IBOutlet UIView *bgView;
+ (instancetype)showView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconImageW;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@property (weak, nonatomic) IBOutlet UIView *priceBgView;


@property (weak, nonatomic) IBOutlet UIView *aBulkBgView;
            
@property (weak, nonatomic) IBOutlet UIView *startAbulkBgView;

@property (weak, nonatomic) IBOutlet UIButton *startAbulkBtn;
@property (strong, nonatomic)MailGoodsDetailsModel *model;



@property (weak, nonatomic) IBOutlet UILabel *oneVipPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *onePriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *onePriceInfoLabel;


@property (weak, nonatomic) IBOutlet UILabel *twoStockLabel;
@property (weak, nonatomic) IBOutlet UILabel *twoPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *twoTgPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *twoDayLabel;
@property (weak, nonatomic) IBOutlet UILabel *twoHoursLabel;
@property (weak, nonatomic) IBOutlet UILabel *twoMinutesLabel;
@property (weak, nonatomic) IBOutlet UILabel *twoSecondsLabel;
@property (strong, nonatomic)id timer;

@property (weak, nonatomic) IBOutlet UILabel *twoOneLabel;
@property (weak, nonatomic) IBOutlet UILabel *twoTwoLabel;
@property (weak, nonatomic) IBOutlet UILabel *twoThreeLabel;

@property (weak, nonatomic) IBOutlet UILabel *twoEndTimeLabel;




@property (weak, nonatomic) IBOutlet UILabel *threePriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *threeStatrNumberLabel;

@property (weak, nonatomic) IBOutlet UILabel *threeStartLabel;

@property (weak, nonatomic) IBOutlet CustomLabel *threeTimerLabel;

@property (weak, nonatomic) IBOutlet UILabel *threeTimeLookLabel;

@end

NS_ASSUME_NONNULL_END
