//
//  NewShareView.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/7/25.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NewShareView : UIView
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (strong, nonatomic)NSString *dataString;
+ (instancetype)showView;
- (void)cancelView;
@end

NS_ASSUME_NONNULL_END
