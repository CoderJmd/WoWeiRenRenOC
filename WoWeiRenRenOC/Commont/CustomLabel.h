//
//  CustomLabel.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/27.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomLabel : UILabel
- (void)dwMakeBottomRoundCornerWithRadius:(CGFloat)radius;
@end

NS_ASSUME_NONNULL_END
