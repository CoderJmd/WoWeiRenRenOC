//
//  CustomLabel.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/27.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "CustomLabel.h"

@implementation CustomLabel


- (void)drawRect:(CGRect)rect
{
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGPoint sPoints[3];//坐标点
//    sPoints[0] =CGPointMake(0, 15);//坐标2
//    sPoints[1] =CGPointMake(0, 9);//坐标1
//    sPoints[2] =CGPointMake(18, 15);//坐标3
//    CGContextAddLines(context, sPoints, 3);//添加线
//    CGContextClosePath(context);//封起来
//    CGContextDrawPath(context, kCGPathFillStroke); //根据坐标绘制路径

    CGContextRef context = UIGraphicsGetCurrentContext();
       
    //创建多个点 并且放入到数组中
    CGPoint point1 = CGPointMake(10, 0);
    CGPoint point2 = CGPointMake(self.frame.size.width, 0);
    CGPoint point3 = CGPointMake(self.frame.size.width, 18);
    CGPoint point4 = CGPointMake(10, 18);
    CGPoint point5 = CGPointMake(0, 9);
    CGPoint points[5] = {point1, point2, point3, point4, point5};

    //向上下文中添加线段
    CGContextAddLines(context, points, 5);

    CGContextSetLineWidth(context, 0);

    //设置填充颜色 在UIKit框架中，对某些功能进行了简单封装
    //直接调用UIKit中的接口也能够完成一部分的绘制操作
    [kRGBAColor(249, 213, 58, 1) setFill];


    CGContextDrawPath(context, kCGPathEOFillStroke);

}

- (void)dwMakeBottomRoundCornerWithRadius:(CGFloat)radius
{
//    CGSize size = self.frame.size;
//    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
//    [shapeLayer setFillColor:[[UIColor whiteColor] CGColor]];
//
//    CGMutablePathRef path = CGPathCreateMutable();
//    CGPathMoveToPoint(path, NULL, size.width - radius, size.height);
//    CGPathAddArc(path, NULL, size.width-radius, size.height-radius, radius, M_PI/2, 0.0, YES);
//    CGPathAddLineToPoint(path, NULL, size.width, 0.0);
//    CGPathAddLineToPoint(path, NULL, 0.0, 0.0);
//    CGPathAddLineToPoint(path, NULL, 0.0, size.height - radius);
//    CGPathAddArc(path, NULL, radius, size.height - radius, radius, M_PI, M_PI/2, YES);
//    CGPathCloseSubpath(path);
//    [shapeLayer setPath:path];
//    CFRelease(path);
//    self.layer.mask = shapeLayer;
    //第二种绘制线段的方法， 不需要使用路径
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //创建多个点 并且放入到数组中
    CGPoint point1 = CGPointMake(15, 0);
    CGPoint point2 = CGPointMake(self.frame.size.width, 0);
    CGPoint point3 = CGPointMake(self.frame.size.width, 18);
    CGPoint point4 = CGPointMake(15, 18);
    CGPoint point5 = CGPointMake(0, 9);
    CGPoint points[5] = {point1, point2, point3, point4, point5};
    
    //向上下文中添加线段
    CGContextAddLines(context, points, 5);
    
    CGContextSetLineWidth(context, 3);
    CGContextSetStrokeColorWithColor(context, [UIColor redColor].CGColor);
    //设置填充颜色 在UIKit框架中，对某些功能进行了简单封装
    //直接调用UIKit中的接口也能够完成一部分的绘制操作
    [[UIColor yellowColor] setFill];
    
    CGContextDrawPath(context, kCGPathEOFillStroke);
    
}

@end
