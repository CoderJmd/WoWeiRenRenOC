//
//  MyPostersShareView.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/11/20.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyPostersShareView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *codeImageView;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UIImageView *deleteImageView;
@property (weak, nonatomic) IBOutlet UIView *bgView;
+ (instancetype)showView;
@property (weak, nonatomic) IBOutlet UIView *cancelBgView;

@end

NS_ASSUME_NONNULL_END
