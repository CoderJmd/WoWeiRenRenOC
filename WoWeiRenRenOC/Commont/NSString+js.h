//
//  NSString+js.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/30.
//  Copyright © 2020 姜孟东. All rights reserved.
//


#import <UIKit/UIKit.h>

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (js)
/**
 宽度计算

 @param width 屏幕宽度
 @return 宽度
 */
+ (instancetype)getJSWithScreentWidth:(CGFloat) width;


/**
 高度计算

 @param webView
 @return 高度
 */
+ (instancetype)getJSWithScreentHeightWithWebView:(UIWebView*)webView;
@end

NS_ASSUME_NONNULL_END
