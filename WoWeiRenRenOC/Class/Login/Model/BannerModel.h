//
//  BannerModel.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/14.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BannerModel : BaseModel
@property (strong, nonatomic)NSString *jump_path;
@property (strong, nonatomic)NSString *depict;
@property (strong, nonatomic)NSString *banner_id;
@property (strong, nonatomic)NSString *img_path;
@property (strong, nonatomic)NSString *title;
@property (strong, nonatomic)NSString *jump_type;
@property (assign, nonatomic)int jump_module; //跳转模块 1商品 2商家 3团购 4拼团
@property (strong, nonatomic)NSString *jump_module_param;

@end

NS_ASSUME_NONNULL_END
