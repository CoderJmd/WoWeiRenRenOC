//
//  HomeDataModel.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/15.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeDataModel : BaseModel
@property (strong, nonatomic)NSString *user_count;
@property (strong, nonatomic)NSString *total_login_count;
@property (strong, nonatomic)NSString *xn_user_count;
@property (strong, nonatomic)NSString *business_count;
@property (strong, nonatomic)NSString *xn_business_count;
@property (strong, nonatomic)NSString *xn_total_login_count;
@property (strong, nonatomic)NSString *xn_testerday_login_count;
@property (strong, nonatomic)NSString *testerday_login_count;


@end

NS_ASSUME_NONNULL_END
