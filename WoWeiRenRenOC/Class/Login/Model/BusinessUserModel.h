//
//  BusinessUserModel.h
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/18.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BusinessUserModel : NSObject

@property (nonatomic, copy) NSString *access_token;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *business_license_path;
@property (nonatomic, copy) NSString *business_name;
@property (nonatomic, copy) NSString *contacts;
@property (nonatomic, copy) NSString *descriptionText;
@property (nonatomic, assign) NSInteger discount_id;
@property (nonatomic, copy) NSString *discount_name;
@property (nonatomic, copy) NSString *effective_time;
@property (nonatomic, copy) NSString *head_path;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *sh_remark;
@property (nonatomic, copy) NSString *busineee_tel;
@property (nonatomic, assign) NSInteger business_id;
@property (nonatomic, assign) NSInteger business_license_no;
@property (nonatomic, assign) NSInteger classification_id;
@property (nonatomic, assign) NSInteger create_time;
@property (nonatomic, assign) NSInteger hot_time;
@property (nonatomic, assign) NSInteger is_hot;
@property (nonatomic, assign) NSInteger sh_state;
@property (nonatomic, assign) NSInteger state;
@property (nonatomic, assign) NSInteger tj_user_id;
@property (nonatomic, assign) NSInteger update_time;

@end

NS_ASSUME_NONNULL_END
