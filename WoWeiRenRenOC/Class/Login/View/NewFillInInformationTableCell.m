//
//  NewFillInInformationTableCell.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/25.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "NewFillInInformationTableCell.h"
#import "WHAreaPickerView.h"
#import "ASBirthSelectSheet.h"
extern UserModel *userModel;
@interface NewFillInInformationTableCell()<WHAreaPickerViewDelegate>


@end
@implementation NewFillInInformationTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"《会员权益介绍》"attributes: @{NSFontAttributeName:kPMFont(16),NSForegroundColorAttributeName: [UIColor colorWithRed:206/255.0 green:160/255.0 blue:9/255.0 alpha:1.0],NSUnderlineStyleAttributeName:@(NSUnderlineStyleSingle)}];
    self.agreementLabel.attributedText = string;
    self.agreementLabel.textAlignment = NSTextAlignmentCenter;
    CAGradientLayer *gl = [CAGradientLayer layer];
    gl.frame = CGRectMake(0, 0, (kScreenW - 70)/2, 40);
    gl.startPoint = CGPointMake(0.22, 0.36);
    gl.endPoint = CGPointMake(0.85, 0.72);
    gl.colors = @[(__bridge id)[UIColor colorWithRed:221/255.0 green:169/255.0 blue:72/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:189/255.0 green:135/255.0 blue:35/255.0 alpha:1.0].CGColor];
    gl.locations = @[@(0), @(1.0f)];
    [self.twoBtn.layer insertSublayer:gl atIndex:0];    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(weixinTextFieldTextChanged) name:UITextFieldTextDidChangeNotification object:nil];
    
    [self.sexBtn addTarget:self action:@selector(selectSex) forControlEvents:UIControlEventTouchUpInside];
    [self.educationBtn addTarget:self action:@selector(selectEducation) forControlEvents:UIControlEventTouchUpInside];
    [self.constellationBtn addTarget:self action:@selector(selectConstellation) forControlEvents:UIControlEventTouchUpInside];

    [self.addressBtn addTarget:self action:@selector(seleCityClick) forControlEvents:UIControlEventTouchUpInside];
    [self.birthdayBtn addTarget:self action:@selector(seleBirthdayClick) forControlEvents:UIControlEventTouchUpInside];
    
    self.submitBtn.layer.cornerRadius = 5;
    self.submitBtn.clipsToBounds = YES;
    
    [self setUi];
    
}


- (void)setUi  {
    
    if (![CommonTTool isStrNull:userModel.name]) {
        self.nameTF.text = userModel.name;
    }
    if (![CommonTTool isStrNull:userModel.phone]) {
        self.phoneTF.text = userModel.phone;
    }
    if ([userModel.sex isEqualToString:@"1"]) {
        self.sexTF.text = @"男";
        self.sexInt = userModel.sex;
    }else if([userModel.sex isEqualToString:@"2"]){
        self.sexTF.text = @"女";
        self.sexInt = userModel.sex;
    }
    if (![CommonTTool isStrNull:userModel.idcard]) {
        self.cardOnTF.text = userModel.idcard;
    }
    if (![CommonTTool isStrNull:userModel.province] && ![CommonTTool isStrNull:userModel.city] && ![CommonTTool isStrNull:userModel.area]) {
        self.addressTF.text = [NSString stringWithFormat:@"%@%@%@",userModel.province,userModel.city,userModel.area];
    }
    if (![CommonTTool isStrNull:userModel.birthday]) {
        self.birthdayTF.text  = userModel.birthday;
    }
    if (![CommonTTool isStrNull:userModel.constellation]) {
        self.constellationTF.text = userModel.constellation;
    }
    if (![CommonTTool isStrNull:userModel.education]) {
        self.educationTF.text  = userModel.education;
    }
    
    if (![CommonTTool isStrNull:userModel.email]) {
        self.emailBtn.text  = userModel.email;
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)seleBirthdayClick {
    
    ASBirthSelectSheet *datesheet = [[ASBirthSelectSheet alloc] initWithFrame:self.bounds];
    //datesheet.selectDate = self.lblShowBirth.text;
    datesheet.GetSelectDate = ^(NSString *dateStr) {
        self.birthdayTF.text = dateStr;
    };
    [self addSubview:datesheet];
}

-(void)weixinTextFieldTextChanged {
    
    if ([self.cardOnTF isFirstResponder]) {
        
        if (self.cardOnTF.text.length >= 19) {
        NSMutableString *mutStr = [NSMutableString stringWithString:self.cardOnTF.text];
        NSString *str = [mutStr substringToIndex:18];
        self.cardOnTF.text = str;
        }
    }
    
    if ([self.phoneTF isFirstResponder]) {
        
        if (self.phoneTF.text.length >= 12) {
        NSMutableString *mutStr = [NSMutableString stringWithString:self.phoneTF.text];
        NSString *str = [mutStr substringToIndex:11];
        self.phoneTF.text = str;
        }
    }

}

- (void)selectSex {
    [CommonTTool dismisKeyBoard];
    NSArray *sexS = @[@"男",@"女",@"保密"];
    
    LCActionSheet *actionSheet = [LCActionSheet sheetWithTitle:@"" cancelButtonTitle:@"取消" clicked:^(LCActionSheet * _Nonnull actionSheet, NSInteger buttonIndex) {
        
        if (buttonIndex != 0) {
            // 系统提示框
            self.sexTF.text = sexS[buttonIndex-1];
            self.sexInt = [NSString stringWithFormat:@"%ld",(long)buttonIndex];
        }
        
    } otherButtonTitleArray:sexS];
    actionSheet.buttonHeight = 45;
    actionSheet.buttonColor = BlueColor;
    
    [actionSheet show];
    
    
}

- (void)selectEducation {
    
    
    
    [CommonTTool dismisKeyBoard];
    NSArray *educationS = @[@"初中及以下",@"高中",@"大专",@"本科",@"硕士",@"博士"];
    
    LCActionSheet *actionSheet = [LCActionSheet sheetWithTitle:@"" cancelButtonTitle:@"取消" clicked:^(LCActionSheet * _Nonnull actionSheet, NSInteger buttonIndex) {
        
        if (buttonIndex != 0) {
            // 系统提示框
            self.educationTF.text = educationS[buttonIndex-1];
        }
        
    } otherButtonTitleArray:educationS];
    actionSheet.buttonHeight = 45;
    actionSheet.buttonColor = BlueColor;
    
    [actionSheet show];
    
    
}

- (void)selectConstellation {
    [CommonTTool dismisKeyBoard];
    NSArray *ConstellationS = @[@"白羊座",@"金牛座",@"双子座",@"巨蟹座",@"狮子座",@"处女座",@"天秤座",@"天蝎座",@"射手座",@"摩羯座",@"水瓶座",@"双鱼座"];
    
    LCActionSheet *actionSheet = [LCActionSheet sheetWithTitle:@"" cancelButtonTitle:@"取消" clicked:^(LCActionSheet * _Nonnull actionSheet, NSInteger buttonIndex) {
        
        if (buttonIndex != 0) {
            // 系统提示框
            self.constellationTF.text = ConstellationS[buttonIndex-1];
        }
        
    } otherButtonTitleArray:ConstellationS];
    actionSheet.buttonHeight = 45;
    actionSheet.buttonColor = BlueColor;
    
    [actionSheet show];
    
}


#pragma mark - WHAreaPickerControllerDelegate
- (void)areaPickerViewDidFinish:(WHAreaPickerView *)picker
{
    self.addressTF.text = [picker.areaName componentsJoinedByString:@""];
    self.province = picker.areaName[0];
    self.city = picker.areaName[1];
    self.area = picker.areaName[2];
    
}

-  (void)seleCityClick {
    
    [CommonTTool dismisKeyBoard];
    
    NSArray *cityArray = [[NSUserDefaults standardUserDefaults] arrayForKey:kCityList];
    if (cityArray == nil) {
        SVPShowInternetWaiting;
        [NetRequestTool postUrl:@"sysCode/queryCityList" parameters:@{} success:^(id responseObject) {
             
            NSArray *array = responseObject[@"object"];
            WHAreaPickerView *area = [WHAreaPickerView areaPickerView];
            area.soucessArray = array;
            area.delegate = self;
            [[NSUserDefaults standardUserDefaults] setObject:array forKey:kCityList];
            
            
        } failure:^(NSInteger statusCode) {
            
        } finish:^{
            SVPDismiss
        }];
        
    }else {
        WHAreaPickerView *area = [WHAreaPickerView areaPickerView];
        area.soucessArray = cityArray;
        area.delegate = self;
    }
    

}
@end
