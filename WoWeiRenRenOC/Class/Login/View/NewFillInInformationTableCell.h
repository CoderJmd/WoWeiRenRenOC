//
//  NewFillInInformationTableCell.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/25.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NewFillInInformationTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *agreementLabel;

@property (weak, nonatomic) IBOutlet UIButton *oneBtn;
@property (weak, nonatomic) IBOutlet UIButton *twoBtn;

@property (weak, nonatomic) IBOutlet UITextField *nameTF;
@property (weak, nonatomic) IBOutlet UITextField *cardOnTF;
@property (weak, nonatomic) IBOutlet UITextField *phoneTF;
@property (weak, nonatomic) IBOutlet UITextField *sexTF;
@property (weak, nonatomic) IBOutlet UIButton *sexBtn;
@property (weak, nonatomic) IBOutlet UITextField *addressTF;
@property (weak, nonatomic) IBOutlet UIButton *addressBtn;
@property (weak, nonatomic) IBOutlet UITextField *birthdayTF;

@property (weak, nonatomic) IBOutlet UIButton *birthdayBtn;
@property (weak, nonatomic) IBOutlet UITextField *constellationTF;
@property (weak, nonatomic) IBOutlet UIButton *constellationBtn;

@property (weak, nonatomic) IBOutlet UITextField *educationTF;
@property (weak, nonatomic) IBOutlet UIButton *educationBtn;
@property (weak, nonatomic) IBOutlet UITextField *emailBtn;

@property (strong, nonatomic)NSString *sexInt;
@property (strong, nonatomic)NSString *province;
@property (strong, nonatomic)NSString *city;
@property (strong, nonatomic)NSString *area;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;

@end

NS_ASSUME_NONNULL_END
