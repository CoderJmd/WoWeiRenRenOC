//
//  FillInInformationVC.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/14.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FillInInformationVC : BaseViewController

@property (strong, nonatomic)NSString *use_code;
@property (strong, nonatomic)NSString *password;
@property (strong, nonatomic)NSString *username;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top;


@end

NS_ASSUME_NONNULL_END
