//
//  RegisterPaySuccessVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/16.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "RegisterPaySuccessVC.h"

@interface RegisterPaySuccessVC ()

@end

@implementation RegisterPaySuccessVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"注册成功";
}
- (IBAction)registerSuccessClick:(id)sender {
    
    
    [self back];
    
}

- (void)back {
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

@end
