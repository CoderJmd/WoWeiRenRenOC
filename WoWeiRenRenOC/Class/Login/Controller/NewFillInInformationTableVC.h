//
//  NewFillInInformationTableVC.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/25.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface NewFillInInformationTableVC : BaseTableViewController
@property (strong, nonatomic)NSString *use_code;
@property (strong, nonatomic)NSString *password;
@property (strong, nonatomic)NSString *username;
@property (assign, nonatomic)BOOL isBuyVip;
@end

NS_ASSUME_NONNULL_END
