//
//  ChangePasswordVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/7/31.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "ChangePasswordVC.h"
#import "LoginVC.h"

@interface ChangePasswordVC ()
@property (weak, nonatomic) IBOutlet UITextField *oldTF;
@property (weak, nonatomic) IBOutlet UITextField *PasswordTF;

@property (weak, nonatomic) IBOutlet UITextField *confimTF;
@property (weak, nonatomic) IBOutlet WRBaseButton *saveBtn;

@end

@implementation ChangePasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"修改密码";
    
}
- (IBAction)saveBtnClick:(id)sender {
    
    if ([CommonTTool isStrNull:self.oldTF.text]) {
        SVP(@"请输入旧密码");
        return;
    }
    
    if ([CommonTTool isStrNull:self.PasswordTF.text]) {
        SVP(@"请输入新密码");
        return;
    }
    
    if ([CommonTTool isStrNull:self.confimTF.text]) {
        SVP(@"请输入新密码");
        return;
    }
    
    if (![self.PasswordTF.text isEqualToString:self.confimTF.text]) {
        SVP(@"两次输入的新密码不同，请重试！");
        return;
    }
    
    NSString *url = @"user/updateUserPassword";
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"kUserIsMerchants"]) {
        url = @"business/updateBusinessPassword";
    }
    
    [NetRequestTool postUrl:url parameters:@{@"old_password":self.oldTF.text,@"new_password":self.PasswordTF.text} success:^(id responseObject) {
//        SVP(@"密码修改成功");
//        [self performSelector:@selector(back) withObject:self afterDelay:2.0];
        [self exitApp];
        
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        
    }];
    
    
}

#pragma mark - 退出app
- (void)exitApp {
//    [LEUserManager deleteAllObject]; // 清空所有本地存储数据
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kisLogin];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kUserInfo];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kUserMerchantsInfo];
    [LEUserManager deleteToken];
    UIViewController *currentVC = [CommonTTool getCurrentVC];
    

    
    UIAlertController *alterController = [UIAlertController alertControllerWithTitle:@"提示" message:@"密码修改成功，请重新登录" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *sureAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        LoginVC *vc = [[LoginVC alloc] init];
        [[CommonTTool getCurrentVC].navigationController pushViewController:vc animated:YES];
        [[NSNotificationCenter defaultCenter] postNotificationName:kUserInfoDetailsExitUpDate object:nil];
    }];
    [alterController addAction:sureAction];
    [currentVC presentViewController:alterController animated:YES completion:nil];
}


- (void)back {
    
    [self.navigationController popViewControllerAnimated:YES];
}




@end
