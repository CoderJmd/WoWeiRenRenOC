//
//  ServiceInfoVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/17.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "ServiceInfoVC.h"
#import "ServiceTextTableViewCell.h"

@interface ServiceInfoVC ()<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) UITableView *tableView;

@end

@implementation ServiceInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"服务条款";
    [self setUi];
}

- (void)setUi {
    
    [self.view addSubview:self.tableView];
    self.tableView.estimatedRowHeight = 100;
    if (@available(iOS 11.0, *)) {
        
        self.tableView.estimatedSectionFooterHeight = 0;
        self.tableView.estimatedSectionHeaderHeight= 0;
        self.tableView.contentInsetAdjustmentBehavior= UIScrollViewContentInsetAdjustmentNever;
    }
    
}


- (UITableView *)tableView {
    if (!_tableView) {
        CGRect frame = CGRectMake(0.0f, NavBarHeight, kScreenW, kScreenH - NavBarHeight);
        UITableView *tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
            // 设置tableView的headView
        tableView.tableHeaderView = [UIView new];
        tableView.tableFooterView = [UIView new];
        tableView.separatorStyle = NO;
        tableView.delegate = self;
        tableView.dataSource = self;
        // 设置tableView的sectionHeadHeight为segmentViewHeight
        tableView.showsVerticalScrollIndicator = false;
        [tableView registerNib:[UINib nibWithNibName:@"ServiceTextTableViewCell" bundle:nil] forCellReuseIdentifier:@"ServiceTextTableViewCell"];

        
        _tableView = tableView;
        _tableView.backgroundColor  = VCBgcolor;
        
    }
    
    return _tableView;
}


#pragma mark- UITableViewDelegate, UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ServiceTextTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ServiceTextTableViewCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.nameLabel.text = @"我为人人OFA简介\n\nOFA(ONE FOR ALL)平台是个人会员与实体商户的商务桥梁，OFA的宗旨是服务百姓、服务实体、回馈社会。\n加入OFA的会员能享受平台入驻实体商户给您的消费折扣，给您带来真正的生活实惠。\n加入OFA平台的会员，可以免费发布个人应聘信息，免费查看企业发布的招聘信息。\n加入OFA平台的实体可在发布有利于自身发展的信息，享受会员到实体消费的益处。\n加入OFA平台的实体可在发布招聘信息，免费享受到会员给你带来的人才的益处。\n\n    入驻我为人人OFA平台业务管理员的规定\n\n一、凡是OFA会员有向平台服务商申请成为其业务管理员的资格。\n二、加入OFA平台服务商业务管理员需是国家政策允许人员，国家明令禁止的人员不能成为业务管理员。\n三、业务管理员的人事关系或劳务关系属OFA平台的城市管理人，并与OFA平台城市管理人签订劳动合同或劳务协议。其工资或劳务费用有OFA平台城市管理人按规定按时发放。\n四、凡OFA平台城市管理人的业务员入驻OFA平台应在平台填写完整的信息资料，其服务会员及入驻平台的经济实体能随时联系的通信方式必填。\n五、业务管理员管理的会员及入驻平台的经济实体在平台上显示，其管理的会员和入驻的经济实体能看到你的联系方式，并提供24小时服务。\n六、OFA平台城市管理人若未按规定按时发放业务管理员的工资或劳务费，业务管理员可向OFA平台联系和投诉，平台会及时处理、维护其合法权利，并担保其工资或劳务费发放到位。\n七、入驻OFA平台业务管理员必须严格遵守国家法律、法规，遵守入驻OFA平台业务管理员的管理制度，并接受OFA平台业务的监督管理。\n八、OFA平台各城市管理人的资料会在OFA平台定期公布和更新。\n\n    加入我为人人OFA会员规定\n\n一、年满十八周岁的公民均可加入OFA会员，会员加入采用实名制。会员分为金卡、银卡、和普通会员三种。 \n二、会员加入原则：自愿原则；凡对OFA平台服务不满意的，普通会员自加入之日起60日内申请退出，全额退还会员加入的会员技术、咨询服务费。普通会员可升级为银卡、金卡会员，收取银卡、金卡会员的技术、咨询服务费一律不予退还。\n三、凡加入普通会员，应缴纳每人每年28元的技术、咨询服务费。银卡、金卡会员的技术、咨询服务费另定。\n四、会员加入的有效期：自会员提出申请，按规定缴纳年费，公司审核通过之日起至一年结束。会员加入时间和期限OFA系统会自动显示。\n五、会员有效期结束，不再续费的会员资格取消，再次加入需重新申请。\n六、普通会员加入OFA两个月后再申请退出的，会员所缴纳的技术、咨询服务费不予退还，系统自动维护有效期一年。\n七、会员加入OFA后，享受OFA平台的各类服务和入驻经济实体可提供的各类服务，平台不再收取任何费用。\n八、凡是OFA会员都有权申请成为公司合作单位（平台服务商）的业务管理员（兼职或专职）资格。\n九、鼓励会员推荐身边的人加入OFA会员，但此推荐是自愿的，公司不支付劳务费用。\n十、鼓励会员推荐经济实体入驻OFA平台，但此推荐经济实体入驻的工作，会员是出于自愿为OFA服务，公司不支付劳务费用。\n十一、会员推荐会员加入可使用会员本身的推荐码，或OFA通用的推荐码。\n十二、会员推荐新会员加入应对新会员说明会员规定，加入时应认真阅读加入会员的规定和协议。\n十三、会员推荐经济实体入驻平台，其资料和信息是真实的，提供的文字、照片等应遵守国家法律法规。\n十四、为防止个人信息泄露，公司会员填写联系电话时，不作强制要求录入。公司会严格按照国家规定对个人信息进行保护。\n十五、OFA会员可免费参加平台开展的各类团购活动，团购的产品一律底价销售，平台不再向会员收取任何费用。\n十六、凡2020年底前加入OFA的普通会员，其年会员技术、咨询服务费按第一次缴费金额十年内不涨。\n十七、会员应遵守国家法律法规和网络管理的法律、法规，并遵守会员规定。\n十八、普通会员只适用于OFA平台入驻店的服务，金卡、银卡会员适用于OFA入驻店和OFA直营实体店的服务（详见金卡、银卡会员规定）。\n十九、对在入驻平台经济实体凡涉及要会员预充值消费的，请会员慎重选择，平台不负责任何经济和法律责任。\n\n   经济实体入驻平台规定\n\n一、凡具有合法的营业执照、和经营场所的企业、个体工商户等经济实体均可入住OFA平台，涉及特殊行业如卫生行业应提供卫生许可证，涉烟草行业的应提供烟草专卖证等。\n二、入驻平台的经济实体应严格遵守国家的法律、法规，遵守OFA平台的管理制度。\n三、入驻平台的经济实体应如实填写有关资料，上传营业执照和经营场所等照片。经OFA平台审核通过即可入驻平台。\n四、入驻平台的经济实体，在平台开始运行起五年内OFA平台不收取咨询、技术服务费。\n五、入驻平台的经济实体每年到期前一月内如股东、经营场所等无变化可直接提交审核，有变化的需重新提交有关资料，供平台审核。\n六、经济实体可在OFA平台允许范围内发布信息或宣传材料，平台应进行审核，审核通过即可在平台发布。其发布的内容必须遵守国家的法律法规和网络管理的规定。\n七、入驻OFA平台经济实体提供消费类服务其折扣及优惠力度应高于同类服务商，应切实履行自己在平台上发布的承诺，若有会员投诉查实则列入平台黑名单。\n八、入驻OFA平台发布招聘信息，会员到实体应聘不允许收取任何费用，若有会员投诉查实，则列入平台黑名单。\n九、入驻OFA平台的经济实体需在平台广告栏发布广告，需联系平台，平台核实通过发布前，会向发布广告的经济实体收取相应的广告费用。\n十、实体入驻平台可直接和OFA平台联系，也可通过OFA城市代理人联系推荐入驻。 \n十一、入驻的经济实体OFA平台只作形式性审查，凡提供虚假信息入驻的经济实体其责任由提供虚假信息的单位或个人承担法律和经济责任；经济实体的经营责任与OFA平台无关，其法律和经济责任全部由入驻的经济实体自身负责。\n十二、OFA平台是会员与经济实体的桥梁，入驻的经济实体是为OFA平台会员提供服务的，应谋划经济实体与会员的共同收益和发展。";
    
    
    return cell;


    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return UITableViewAutomaticDimension;
}


#pragma mark - section头部间距
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {

    return 0.0000001;
}


#pragma mark - section头部视图
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {

    UIView *view= [[UIView alloc]initWithFrame:CGRectMake( 0, 0, KSCREEN_WIDTH, 0.00001)];
    view.backgroundColor = [UIColor whiteColor];
    return view;
}

#pragma mark - section底部间距
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == 0) {
        
        return 10;
    }
    return 0.0001;
}

#pragma mark - section底部视图
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake( 0, 0, KSCREEN_WIDTH, 0)];
    view.backgroundColor = VCBgcolor;
    return view;
}


@end
