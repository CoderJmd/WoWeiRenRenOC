//
//  VIPRightShowView.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/7/4.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface VIPRightShowView : BaseViewController
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end

NS_ASSUME_NONNULL_END
