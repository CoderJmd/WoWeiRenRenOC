//
//  NewFillInInformationTableVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/25.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "NewFillInInformationTableVC.h"
#import "NewFillInInformationTableCell.h"
#import "RegisterPayVC.h"
#import "VIPRightShowView.h"
#import "FuWuWebVC.h"
extern UserModel *userModel;
@interface NewFillInInformationTableVC ()
@property (nonatomic,strong) NewFillInInformationTableCell *cell;
@property (assign, nonatomic)int type;
@end

@implementation NewFillInInformationTableVC

- (void)viewDidLoad {
    [super viewDidLoad];

    
    self.title = @"填写会员信息";

    
    [self creatTableViewWithCellIdentity:@"NewFillInInformationTableCell"];

    self.view.backgroundColor = WhiteColor;
    self.tableView.backgroundColor = WhiteColor;
    self.tableView.rowHeight = kScreenH-NavBarHeight -NeedStatusViewHeight;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.frame = CGRectMake(0, NavBarHeight, kScreenW, kScreenH-NavBarHeight -NeedStatusViewHeight);

  
}





- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NewFillInInformationTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NewFillInInformationTableCell"];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    self.cell = cell;
    [cell.oneBtn addTarget:self action:@selector(oneBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [cell.twoBtn addTarget:self action:@selector(twoBtnClick) forControlEvents:UIControlEventTouchUpInside];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(jumpVipRightShow)];
    cell.agreementLabel.userInteractionEnabled = YES;
    [cell.agreementLabel addGestureRecognizer:tap];
    
    [cell.submitBtn addTarget:self action:@selector(submitBtnClick) forControlEvents:UIControlEventTouchUpInside];
    if (self.isBuyVip) {
        cell.submitBtn.hidden = YES;
        cell.oneBtn.hidden = NO;
        cell.twoBtn.hidden = NO;
    }else {
        cell.submitBtn.hidden = NO;
        cell.oneBtn.hidden = YES;
        cell.twoBtn.hidden = YES;
    }
    
    return cell;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}

- (void)submitBtnClick  {
    
    NSString *name = self.cell.nameTF.text;
    NSString *cardNo = self.cell.cardOnTF.text;
    NSString *sex = self.cell.sexInt;
    NSString *xueli = self.cell.educationTF.text;
    NSString *phone = self.cell.phoneTF.text;
    NSString *email = self.cell.emailBtn.text;
    if (cardNo == nil) {
        cardNo = @"";
    }
        
    if ([CommonTTool isStrNull:name]) {
        SVP(@"请输入姓名")
        return;
    }
    
    if([CommonTTool checkEmptyString:name]) {
        SVP(@"请输入姓名")
        return;
    }
    
    if ([CommonTTool isStrNull:sex]) {
        SVP(@"请选择您的性别")
        return;
    }
    
    if ([CommonTTool isStrNull:self.cell.province]) {
        SVP(@"请选择您省份")
        return;
    }
    
    if ([CommonTTool isStrNull:self.cell.city]) {
        SVP(@"请选择您的城市")
        return;
    }
    
    if ([CommonTTool isStrNull:self.cell.area]) {
        SVP(@"请选择您的地区")
        return;
    }
    
    
    
    NSDictionary *dict = @{@"name":name,
                           @"sex":sex,
                           @"idcard":cardNo,
                           @"education":xueli,
                           @"phone":phone,
                           @"email":email,
                           @"province":self.cell.province,
                           @"city":self.cell.city,
                           @"area":self.cell.area,
                           @"birthday":self.cell.birthdayTF.text,
                           @"constellation":self.cell.constellationTF.text,
                           
    };
    
    SVPShowInternetWaiting
    [NetRequestTool postUrl:@"user/updateUserInfo" parameters:dict success:^(id responseObject) {
        
        [self uploadUsetInfo];
        
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        
    }];
    
}

- (void)uploadUsetInfo {
    
    [NetRequestTool postUrl:@"user/getUserInfo" parameters:@{} success:^(id responseObject) {
            
        NSDictionary *dic = responseObject[@"object"];
        [[NSUserDefaults standardUserDefaults] setObject:[dic deleteNull] forKey:kUserInfo];
           
        userModel = [UserModel mj_objectWithKeyValues:dic];
        [[NSNotificationCenter defaultCenter] postNotificationName:kUserInfoDetailsUpDate object:nil];
        
        [self.navigationController popViewControllerAnimated:YES];

       } failure:^(NSInteger statusCode) {
           
       } finish:^{
           SVPDismiss
       }];
    
}


- (void)jumpVipRightShow {
    
//    VIPRightShowView *vc = [[VIPRightShowView alloc]initWithNibName:@"VIPRightShowView" bundle:nil];
//    [self.navigationController pushViewController:vc animated:YES];
    FuWuWebVC *vc = [[FuWuWebVC alloc]init];
    vc.title = @"会员权益介绍";
    vc.linkPath = [[NSBundle mainBundle] pathForResource:@"会员规则权益介绍.docx" ofType:nil];
    [self.navigationController pushViewController:vc animated:YES];
       
}


- (void)oneBtnClick {
    
    self.type = 0 ;
    [self nextBtnClick];
    
}

- (void)twoBtnClick {
    
    self.type = 1 ;
    [self nextBtnClick];
    
}

- (void)nextBtnClick {
    
    
    NSString *name = self.cell.nameTF.text;
    NSString *cardNo = self.cell.cardOnTF.text;
    NSString *sex = self.cell.sexInt;
    NSString *xueli = self.cell.educationTF.text;
    NSString *phone = self.cell.phoneTF.text;
    NSString *email = self.cell.emailBtn.text;
    if (cardNo == nil) {
        cardNo = @"";
    }
        
    if ([CommonTTool isStrNull:name]) {
        SVP(@"请输入姓名")
        return;
    }
    
    if([CommonTTool checkEmptyString:name]) {
        SVP(@"请输入姓名")
        return;
    }
    
//    if ([CommonTTool isStrNull:cardNo] || cardNo.length != 18) {
//        SVP(@"请输入18位身份证")
//        return;
//    }
//    if([CommonTTool checkEmptyString:cardNo]) {
//        SVP(@"请输入18位身份证")
//        return;
//    }
    
    if ([CommonTTool isStrNull:sex]) {
        SVP(@"请选择您的性别")
        return;
    }
    
    if ([CommonTTool isStrNull:self.cell.province]) {
        SVP(@"请选择您省份")
        return;
    }
    
    if ([CommonTTool isStrNull:self.cell.city]) {
        SVP(@"请选择您的城市")
        return;
    }
    
    if ([CommonTTool isStrNull:self.cell.area]) {
        SVP(@"请选择您的地区")
        return;
    }
    
    
    
    NSDictionary *dict = @{@"name":name,
                           @"sex":sex,
                           @"idcard":cardNo,
                           @"education":xueli,
                           @"phone":phone,
                           @"email":email,
                           @"use_code":self.use_code,
                           @"password":self.password,
                           @"username":self.username,
                           @"province":self.cell.province,
                           @"city":self.cell.city,
                           @"area":self.cell.area,
                           @"birthday":self.cell.birthdayTF.text,
                           @"constellation":self.cell.constellationTF.text,
                           
    };
    
    SVPShowInternetWaiting
    [NetRequestTool postUrl:@"user/registerSave" parameters:dict success:^(id responseObject) {
        RegisterPayVC *vc = [[RegisterPayVC alloc]init];
        vc.access_token = responseObject[@"object"][@"access_token"];
        if (self.type == 0) {
            vc.title = @"开通试用";
        }
        if (self.type == 1) {
            vc.title = @"开通正式会员";
        }
        vc.username = self.username;
        vc.password = self.password;
        vc.orderType = self.type;
        [self.navigationController pushViewController:vc animated:YES];
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        
    }];
    
    

}




//- (void)back {
//    
//
//    UIAlertController *alterController2 = [UIAlertController alertControllerWithTitle:nil message:@"是否确定放弃注册成为会员？" preferredStyle:UIAlertControllerStyleAlert];
//    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"狠心放弃" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//        
//        [self.navigationController popToRootViewControllerAnimated:YES];
//    }];
//
//    UIAlertAction *answerAction = [UIAlertAction actionWithTitle:@"再想想" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//      
//    }];
//    [alterController2 addAction:cancelAction];
//    [alterController2 addAction:answerAction];
//
//    [self presentViewController:alterController2 animated:YES completion:nil];
//    
//    
//    
//}


@end

