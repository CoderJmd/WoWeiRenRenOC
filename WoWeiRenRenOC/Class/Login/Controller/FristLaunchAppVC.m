//
//  FristLaunchAppVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/20.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "FristLaunchAppVC.h"
#import "ServiceInfoVC.h"
#import "LoginVC.h"

@interface FristLaunchAppVC ()<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UITextView *contentTF;

@end

@implementation FristLaunchAppVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithWhite:0.2f alpha: 0.5];
    self.bgView.layer.cornerRadius = 5;
    self.bgView.clipsToBounds = YES;
    
    self.contentTF.editable = NO;
    self.contentTF.delegate = self;
    //设置添加链接部分文字的颜色，即“《XXX隐私政策》”
    self.contentTF.linkTextAttributes = @{NSForegroundColorAttributeName:kRGBColor(105, 159, 258)};
    
    
    NSString *rangeStr = @"《服务协议》和《隐私政策》";
    NSString *protocolStr = [NSString stringWithFormat:@"你可阅读%@了解详细信息",rangeStr];
    NSRange privacyRange = [protocolStr rangeOfString:rangeStr];
    NSMutableAttributedString *privacyMutableAttrStr = [[NSMutableAttributedString alloc] initWithString:protocolStr attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0],NSForegroundColorAttributeName:kRGBColor(11, 11, 11)}];
    
    //给需要 点击事件的部分添加链接
    [privacyMutableAttrStr addAttribute:NSLinkAttributeName value:@"privacy://" range:privacyRange];
    self.contentTF.attributedText = privacyMutableAttrStr;
    
}

#pragma mark - UITextViewDelegate
-(BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange{
    if ([URL.scheme isEqualToString:@"privacy"]) {
        //这里调用方法跳到隐私政策页面
        ServiceInfoVC *vc = [[ServiceInfoVC alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
        return NO;
    }
    
    return YES;
}


- (IBAction)agreedClick:(id)sender {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"kIsAgreedToAgreement"];
    LoginVC *loginVC = [[LoginVC alloc] init];
    BaseNavigationController *nav = [[BaseNavigationController alloc] initWithRootViewController:loginVC];

    [self presentViewController:nav animated:YES completion:nil];
    
}
- (IBAction)cancelClick:(id)sender {
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    [UIView animateWithDuration:0.5f animations:^{
        window.alpha = 0;
//        window.frame = CGRectMake(0, window.bounds.size.height / 2, window.bounds.size.width, 0.5);
    } completion:^(BOOL finished) {
        exit(0);
    }];
    
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}



@end
