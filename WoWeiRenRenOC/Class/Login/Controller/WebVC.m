//
//  WebVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/20.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "WebVC.h"
#import <WebKit/WebKit.h>
@interface WebVC ()<WKUIDelegate>

@property (strong, nonatomic) WKWebView *baseWebView;


@end

@implementation WebVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = _webtitle;
    self.baseWebView = [[WKWebView alloc]initWithFrame:kFrame( 0, 0, KSCREEN_WIDTH, KSCREEN_HEIGHT)];
    [self.baseWebView setBackgroundColor:WhiteColor];
    [self.baseWebView setUIDelegate: self];
    [self.baseWebView.scrollView setShowsVerticalScrollIndicator: NO];
    [self.baseWebView.scrollView setShowsHorizontalScrollIndicator: NO];
    [self.view addSubview:self.baseWebView];
//    if ([CommonTTool isStrNull:self.linkPath]) {
//        [self.baseWebView loadHTMLString:[self reSizeImageWithHTML:self.htmlString] baseURL:nil];
//    }else {
//         [self.baseWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.linkPath]]];
//    }
    [self.baseWebView loadHTMLString:[self reSizeImageWithHTML:self.htmlString] baseURL:nil];

    
}

/* 修改返回的文本的格式 */
- (NSString *)reSizeImageWithHTML:(NSString *)html {
    return [NSString stringWithFormat:@"<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'><meta name='apple-mobile-web-app-capable' content='yes'><meta name='apple-mobile-web-app-status-bar-style' content='black'><meta name='format-detection' content='telephone=no'><style type='text/css'>img{width:%@} p{font-size:15px}</style>%@",@"100%",html];
}

- (void)dealloc {
    
    [self.baseWebView setNavigationDelegate:nil];
    [self.baseWebView setUIDelegate:nil];
}


@end
