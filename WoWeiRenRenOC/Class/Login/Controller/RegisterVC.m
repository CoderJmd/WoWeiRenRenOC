//
//  RegisterVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/14.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "RegisterVC.h"
#import "WRBaseButton.h"
#import "FillInInformationVC.h"
#import "ServiceInfoVC.h"
#import "NewFillInInformationVC.h"
#import "NewFillInInformationTableVC.h"
#import "FuWuWebVC.h"
#import "BaseTabBarController.h"
#import "SystemDictionaryModel.h"

extern UserModel *userModel;
@interface RegisterVC ()
@property (weak, nonatomic) IBOutlet UITextField *phoneTF;
@property (weak, nonatomic) IBOutlet UITextField *passwordTF;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswrodTF;
@property (weak, nonatomic) IBOutlet UIButton *lookPasswordBtn;
@property (weak, nonatomic) IBOutlet UIButton *twoLookPasswordBtn;

@property (weak, nonatomic) IBOutlet UITextField *codeTF;

@property (weak, nonatomic) IBOutlet UIButton *hookBtn;
@property (weak, nonatomic) IBOutlet UIButton *serviceBtn;
@property (weak, nonatomic) IBOutlet WRBaseButton *nextBtn;
@property (strong, nonatomic)SystemDictionaryModel *model;

@end

@implementation RegisterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"注册";
    [self setUi];
    
    [self queryIsBuyVip];
    
}

- (void)queryIsBuyVip {
    
    [NetRequestTool postUrl:@"sysCode/querySysCodeObject" parameters:@{@"type":@(15)} success:^(id responseObject) {
        self.model = [SystemDictionaryModel mj_objectWithKeyValues:responseObject[@"object"]];
        

    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        
    }];
    
}


- (void)setUi {
    
    
    if (NeediPhoneXiPhoneXR) {
           self.top.constant = NEW_StatusBarHeight;
    }
//    self.codeTF.text  = @"Ez6afn";
    
    [self.lookPasswordBtn addTarget:self action:@selector(lookPasswordBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.twoLookPasswordBtn addTarget:self action:@selector(twoLookPasswordBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.hookBtn addTarget:self action:@selector(hookBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.nextBtn addTarget:self action:@selector(nextBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
    NSString *code =  [CommonTTool getUserDefaults:SHARE_UserCode];
    if (![CommonTTool isStrNull:code]) {
        self.codeTF.text = code;
    }
}

- (void)nextBtnClick {
    

    NSString *phone = self.phoneTF.text;
    NSString *password = self.passwordTF.text;
    NSString *confirmPassword = self.confirmPasswrodTF.text;
    NSString *code = self.codeTF.text;
    
    if ([CommonTTool isStrNull:phone]) {
        SVP(@"账号不能为空")
        return;
    }
    
    if ([CommonTTool isStrNull:password]) {
        SVP(@"密码不能为空")
        return;
    }
    if (password.length < 6) {
        SVP(@"密码不能小于6位")
        return;
    }
    
    if (![password isEqualToString:confirmPassword]) {
        SVP(@"您输入的密码不一致，请重新输入")
        return;
    }
    
    if ([CommonTTool isStrNull:code]) {
        code = @"";
       }
//    if (code.length != 6) {
//       SVP(@"请输入6位推广码")
//       return;
//    }
    
    
    if (!self.hookBtn.isSelected) {
        SVP(@"请阅读并同意服务条款")
        return;
    }
    
    NSDictionary *para = @{@"username":phone,
                           @"password":password,
                           @"use_code":code
    };
    
    SVPShowInternetWaiting
//    [NetManager isRegisterWithParameters:para success:^(NSDictionary * _Nonnull dic) {
//
////        NewFillInInformationVC *vc = [[NewFillInInformationVC alloc]initWithNibName:@"FillInInformationVC" bundle:nil];
////        vc.username = dic[@"object"][@"username"];
////        vc.use_code = dic[@"object"][@"use_code"];
////        vc.access_token = dic[@"object"][@"access_token"];
////        [self.navigationController pushViewController:vc animated:YES];
//        // 记录是否是商户
//        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"kUserIsMerchants"];
//
//        BaseTabBarController *baseTabBar = [[BaseTabBarController alloc]init];
//        AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
//        baseTabBar.selectedIndex = 0;
//        delegate.window.rootViewController = baseTabBar;
//
//    } finish:^{
//
//    }];
//    [NetRequestTool postUrl:@"user/checkRegister" parameters:para success:^(id responseObject) {
//        NewFillInInformationTableVC *vc = [[NewFillInInformationTableVC alloc]init];
//
//        vc.username =para[@"username"];
//        vc.use_code = para[@"use_code"];
//        vc.password = para[@"password"];
//        [self.navigationController pushViewController:vc animated:YES];
//    } failure:^(NSInteger statusCode) {
//
//    } finish:^{
//
//    }];
    
//    [NetRequestTool LoginUrl:@"user/checkRegister" parameters:para success:^(NSInteger statusCode, id responseObject) {
//            FillInInformationVC *vc = [[FillInInformationVC alloc]initWithNibName:@"FillInInformationVC" bundle:nil];
//            vc.username =para[@"username"];
//            vc.use_code = para[@"use_code"];
//            vc.password = para[@"password"];
//            [self.navigationController pushViewController:vc animated:YES];
//
//    } failure:^(NSInteger statusCode) {
//
//    } finish:^{
//
//    }];
    

    if ([self.model.value isEqualToString:@"1"]) {
        [NetRequestTool postUrl:@"user/checkRegister" parameters:para success:^(id responseObject) {
            NewFillInInformationTableVC *vc = [[NewFillInInformationTableVC alloc]init];
            vc.isBuyVip = YES;
            vc.username =para[@"username"];
            vc.use_code = para[@"use_code"];
            vc.password = para[@"password"];
            [self.navigationController pushViewController:vc animated:YES];
        } failure:^(NSInteger statusCode) {

        } finish:^{

        }];
        
    }else {
        
        [NetRequestTool postUrl:@"user/registerSave" parameters:para success:^(id responseObject) {
            
            [self Login];
            
        } failure:^(NSInteger statusCode) {
            
        } finish:^{
            
        }];
    }
        
        
    
}

- (void)Login {
    
    
    NSString *phone = self.phoneTF.text;
    NSString *password = self.passwordTF.text;
    
    
    [NetManager isLoginWithParameters:@{@"username":phone,@"password":password} success:^{
        
        SVP(@"登录成功");
        dispatch_async(dispatch_get_main_queue(), ^{
            // 记录是否是商户
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"kUserIsMerchants"];
            
            BaseTabBarController *baseTabBar = [[BaseTabBarController alloc]init];
            AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
            baseTabBar.selectedIndex = 0;
            delegate.window.rootViewController = baseTabBar;
        });
        
        
    }];
}

- (void)uploadUsetInfo {
    [NetRequestTool postUrl:@"user/getUserInfo" parameters:@{} success:^(id responseObject) {
         
        NSDictionary *dic = responseObject[@"object"];
        [[NSUserDefaults standardUserDefaults] setObject:[dic deleteNull] forKey:kUserInfo];
        userModel = [UserModel mj_objectWithKeyValues:dic];
        [self setUi];
       
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        SVPDismiss
    }];
}

- (void)hookBtnClick:(UIButton *)btn {
    
    btn.selected = !btn.isSelected;
    if (btn.selected) {
        
        [btn setImage:[UIImage imageNamed:@"gouxuan_on"] forState:UIControlStateNormal];
        
    }else{
        
        [btn setImage:[UIImage imageNamed:@"gouxuan_off"] forState:UIControlStateNormal];
        
    }
    
    
}



- (void)twoLookPasswordBtnClick:(UIButton *)btn {
    
    btn.selected = !btn.isSelected;
    if (btn.selected) {
        
        [btn setImage:[UIImage imageNamed:@"zhengyan_on"] forState:UIControlStateNormal];
        self.confirmPasswrodTF.secureTextEntry = NO;
    }else{
        
        [btn setImage:[UIImage imageNamed:@"zhengyan_off"] forState:UIControlStateNormal];
        self.confirmPasswrodTF.secureTextEntry = YES;
    }
    
    
}


- (void)lookPasswordBtnClick:(UIButton *)btn {
    
    btn.selected = !btn.isSelected;
    if (btn.selected) {
        
        [btn setImage:[UIImage imageNamed:@"zhengyan_on"] forState:UIControlStateNormal];
        self.passwordTF.secureTextEntry = NO;
    }else{
        
        [btn setImage:[UIImage imageNamed:@"zhengyan_off"] forState:UIControlStateNormal];
        self.passwordTF.secureTextEntry = YES;
    }
    
    
}
- (IBAction)serviceBtnClick:(id)sender {
    
    FuWuWebVC *vc = [[FuWuWebVC alloc]init];
    vc.title = @"用户协议";
    vc.linkPath = [[NSBundle mainBundle] pathForResource:@"我为人人用户协议.docx" ofType:nil];
    [self.navigationController pushViewController:vc animated:YES];
    
//    ServiceInfoVC *vc = [[ServiceInfoVC alloc]init];
//    [self.navigationController pushViewController:vc animated:YES];
    
}


@end
