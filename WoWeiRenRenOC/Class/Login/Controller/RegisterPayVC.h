//
//  RegisterPayVC.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/16.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface RegisterPayVC : BaseViewController
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top;
@property (strong, nonatomic)NSString *access_token;
@property (assign, nonatomic)int type;
@property (assign, nonatomic)int orderType;//类型 0 vip试用会员 1 普通会员购买 2银卡会员购买
@property (weak, nonatomic) IBOutlet UILabel *payLabel;
@property (strong, nonatomic)NSString *password;
@property (strong, nonatomic)NSString *username;
@end

NS_ASSUME_NONNULL_END

