//
//  WebVC.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/20.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface WebVC : BaseViewController
@property (copy, nonatomic) NSString *webtitle;

@property (strong, nonatomic)NSString *htmlString;
@property (copy, nonatomic) NSString *linkPath;
@end

NS_ASSUME_NONNULL_END
