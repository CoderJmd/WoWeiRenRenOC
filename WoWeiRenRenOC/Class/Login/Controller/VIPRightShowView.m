//
//  VIPRightShowView.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/7/4.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "VIPRightShowView.h"

@interface VIPRightShowView ()

@end

@implementation VIPRightShowView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"会员权益";
    self.nameLabel.text = @"ＯFＡ（ONE　FOR　ALL）平台是个人会员与实体商户的商务桥梁，OFA的宗旨是服务百姓、服务实体、回馈社会。\n\n加入OFA的会员能享受平台入驻实体商户给您的消费折扣，给您带来真正的生活实惠。\n\n加入OFA平台的会员，可以免费发布个人应聘信息，免费查看企业发布的招聘信息。\n\n加入OFA平台的实体可在发布有利于自身发展的信息，享受会员到实体消费的益处。\n\n加入OFA平台的实体可在发布招聘信息，免费享受到会员给你带来的人才的益处。";
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
