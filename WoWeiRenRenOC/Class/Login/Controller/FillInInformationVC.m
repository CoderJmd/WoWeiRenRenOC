//
//  FillInInformationVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/14.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "FillInInformationVC.h"
#import "WRBaseButton.h"
#import "RegisterPayVC.h"

@interface FillInInformationVC ()
@property (weak, nonatomic) IBOutlet UITextField *nameTF;
@property (weak, nonatomic) IBOutlet UITextField *cardOnTF;
@property (weak, nonatomic) IBOutlet UIButton *sexTF;
@property (weak, nonatomic) IBOutlet UIButton *educationTF;
@property (weak, nonatomic) IBOutlet UITextField *phoneTF;
@property (weak, nonatomic) IBOutlet UITextField *emailTF;

@property (weak, nonatomic) IBOutlet WRBaseButton *nextBtn;

@property (weak, nonatomic) IBOutlet UITextField *xueliTF;
@property (strong, nonatomic)NSString *sexInt;

@property (weak, nonatomic) IBOutlet UITextField *xingbieTF;


@end

@implementation FillInInformationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"填写会员信息";
    [self setUi];
    
}


- (void)setUi {
    
//    self.nameTF.text = @"你好";
//    self.cardOnTF.text = @"123456789123456789";
    
    
    if (NeediPhoneXiPhoneXR) {
           self.top.constant = NEW_StatusBarHeight;
    }
    
    [self.sexTF addTarget:self action:@selector(selectSex) forControlEvents:UIControlEventTouchUpInside];
    
    [self.educationTF addTarget:self action:@selector(selectEducation) forControlEvents:UIControlEventTouchUpInside];
    
    self.nameTF.keyboardType =  UIKeyboardTypeDefault;
    self.cardOnTF.keyboardType = UIKeyboardTypeDefault;
    
    
    
}

- (void)selectSex {
    
    NSArray *sexS = @[@"男",@"女",@"保密"];
    
    LCActionSheet *actionSheet = [LCActionSheet sheetWithTitle:@"" cancelButtonTitle:@"取消" clicked:^(LCActionSheet * _Nonnull actionSheet, NSInteger buttonIndex) {
        
        if (buttonIndex != 0) {
            // 系统提示框
            self.xingbieTF.text = sexS[buttonIndex-1];
            self.sexInt = [NSString stringWithFormat:@"%ld",(long)buttonIndex];
        }
        
    } otherButtonTitleArray:sexS];
    actionSheet.buttonHeight = 45;
    actionSheet.buttonColor = BlueColor;
    
    [actionSheet show];
    
    
}


- (void)selectEducation {
    
    
    
    
    NSArray *educationS = @[@"初中及以下",@"高中",@"大专",@"本科",@"硕士",@"博士"];
    
    LCActionSheet *actionSheet = [LCActionSheet sheetWithTitle:@"" cancelButtonTitle:@"取消" clicked:^(LCActionSheet * _Nonnull actionSheet, NSInteger buttonIndex) {
        
        if (buttonIndex != 0) {
            // 系统提示框
            self.xueliTF.text = educationS[buttonIndex-1];
        }
        
    } otherButtonTitleArray:educationS];
    actionSheet.buttonHeight = 45;
    actionSheet.buttonColor = BlueColor;
    
    [actionSheet show];
    
    
}




- (IBAction)nextBtnClick:(id)sender {
    
    
    NSString *name = self.nameTF.text;
    NSString *cardNo = self.cardOnTF.text;
    NSString *sex = self.sexInt;
    NSString *xueli = self.xueliTF.text;
    NSString *phone = self.phoneTF.text;
    NSString *email = self.emailTF.text;
    
        
    if ([CommonTTool isStrNull:name]) {
        SVP(@"请输入姓名")
        return;
    }
    
    if([CommonTTool checkEmptyString:name]) {
        SVP(@"请输入姓名")
        return;
    }
    
    if ([CommonTTool isStrNull:cardNo] || cardNo.length < 17) {
        SVP(@"请输入18位身份证")
        return;
    }
    if([CommonTTool checkEmptyString:cardNo]) {
        SVP(@"请输入18位身份证")
        return;
    }
    
    if ([CommonTTool isStrNull:sex]) {
        SVP(@"请选择您的性别")
        return;
    }
    
    if ([CommonTTool isStrNull:xueli]) {
        SVP(@"请选择您的学历")
        return;
    }
    
    NSDictionary *dict = @{@"name":name,
                           @"sex":sex,
                           @"idcard":cardNo,
                           @"education":xueli,
                           @"phone":phone,
                           @"email":email,
                           @"use_code":self.use_code,
                           @"password":self.password,
                           @"username":self.username,
                           
    };
    
    SVPShowInternetWaiting
//    [NetManager isUpdateUserInfoWithParameters:dict success:^{
//
//        RegisterPayVC *vc = [[RegisterPayVC alloc]init];
//        vc.access_token = self.access_token;
//        [self.navigationController pushViewController:vc animated:YES];
//    }];
    
//    [NetRequestTool LoginUrl:@"user/registerSave" parameters:dict success:^(NSInteger statusCode, id responseObject) {
//            RegisterPayVC *vc = [[RegisterPayVC alloc]init];
//             vc.access_token = responseObject[@"object"][@"access_token"];
//            [self.navigationController pushViewController:vc animated:YES];
//
//    } failure:^(NSInteger statusCode) {
//
//    } finish:^{
//
//    }];
    
    [NetRequestTool postUrl:@"user/registerSave" parameters:dict success:^(id responseObject) {
        RegisterPayVC *vc = [[RegisterPayVC alloc]init];
         vc.access_token = responseObject[@"object"][@"access_token"];
        [self.navigationController pushViewController:vc animated:YES];
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        
    }];
    
    

}


- (void)back {
    

    UIAlertController *alterController2 = [UIAlertController alertControllerWithTitle:nil message:@"是否确定放弃注册成为会员？" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"狠心放弃" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self.navigationController popToRootViewControllerAnimated:YES];
    }];

    UIAlertAction *answerAction = [UIAlertAction actionWithTitle:@"再想想" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
      
    }];
    [alterController2 addAction:cancelAction];
    [alterController2 addAction:answerAction];

    [self presentViewController:alterController2 animated:YES completion:nil];
    
    
    
}



@end
