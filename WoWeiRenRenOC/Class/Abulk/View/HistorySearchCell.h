//
//  HistorySearchCell.h
//  WisdomCoalLogistics
//
//  Created by 杨涛 on 2018/4/24.
//  Copyright © 2018年 znz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistorySearchCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
