
//
//  HistorySearchCell.m
//  WisdomCoalLogistics
//
//  Created by 杨涛 on 2018/4/24.
//  Copyright © 2018年 znz. All rights reserved.
//

#import "HistorySearchCell.h"

@implementation HistorySearchCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString *id = @"HistorySearchCell";
    HistorySearchCell *cell = [tableView dequeueReusableCellWithIdentifier:id];
    if (cell == nil ) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"HistorySearchCell" owner:nil options:nil] lastObject];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

@end
