//
//  SearchHistoryWordsView.h
//  SchoolApp
//
//  Created by 姜伟 on 2019/7/28.
//  Copyright © 2019 znz. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^HistoryWordsBlock)(NSString *title);

@interface SearchHistoryWordsView : UIView

@property (weak, nonatomic) IBOutlet UIView *historyView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *historyHeight;
@property (strong, nonatomic) NSMutableArray *historyArray;

@property (weak, nonatomic) IBOutlet UIView *hotView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *hotHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *hotY;
@property (strong, nonatomic) NSMutableArray *hotArray;

@property (copy, nonatomic) HistoryWordsBlock historyWordsClick;

@end

NS_ASSUME_NONNULL_END
