//
//  SearchNavbarView.h
//  PensionUser
//
//  Created by 杨涛 on 2019/2/25.
//  Copyright © 2019 znz. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SearchNavbarView : UIView
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bgViewLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bgViewTrailing;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bgViewHeight;
@property (weak, nonatomic) IBOutlet UIView *bgView;

@property (weak, nonatomic) IBOutlet UIButton *searchBtn;
@property (weak, nonatomic) IBOutlet UITextField *tf;
@property (weak, nonatomic) IBOutlet UIButton *searchBtn2;

+ (instancetype)GetmyHeaderView;

@end

NS_ASSUME_NONNULL_END
