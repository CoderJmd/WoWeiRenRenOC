//
//  AbulkCell.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/13.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "AbulkCell.h"
#import "MailDetailsVC.h"

@implementation AbulkCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.bgView.layer.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0].CGColor;
    self.bgView.layer.cornerRadius = 6;
    self.bgView.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.1].CGColor;
    self.bgView.layer.shadowOffset = CGSizeMake(0,0);
    self.bgView.layer.shadowOpacity = 1;
    self.bgView.layer.shadowRadius = 4;
    self.timeOneLabel.layer.backgroundColor = [UIColor colorWithRed:17/255.0 green:16/255.0 blue:13/255.0 alpha:1.0].CGColor;
    self.timeOneLabel.textColor = [UIColor whiteColor];
    UIBezierPath *textOnePath = [UIBezierPath bezierPathWithRoundedRect:self.timeOneLabel.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerBottomLeft cornerRadii:CGSizeMake(2, 2)];

    CAShapeLayer *textOnelayer = [[CAShapeLayer alloc] init];
    textOnelayer.frame = self.self.timeOneLabel.bounds;
    textOnelayer.path = textOnePath.CGPath;
    self.timeOneLabel.layer.mask = textOnelayer;
    self.timeOneLabel.clipsToBounds = YES;
    
    self.timeTwoLabel.textColor = TextOrangeColor;
    self.timeTwoLabel.layer.borderColor = TextOrangeColor.CGColor;
    self.timeTwoLabel.layer.borderWidth = 1;
    UIBezierPath *textTwoPath = [UIBezierPath bezierPathWithRoundedRect:self.timeTwoLabel.bounds byRoundingCorners:UIRectCornerTopRight | UIRectCornerBottomRight cornerRadii:CGSizeMake(2, 2)];
    CAShapeLayer *textTwolayer = [[CAShapeLayer alloc] init];
    textTwolayer.frame = self.timeTwoLabel.bounds;
    textTwolayer.path = textTwoPath.CGPath;
    self.timeTwoLabel.layer.mask = textTwolayer;
    self.timeTwoLabel.clipsToBounds = YES;
    
    self.nextBtn.layer.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0].CGColor;
    self.nextBtn.layer.cornerRadius = 12;
    self.nextBtn.clipsToBounds = YES;
    self.nextBtn.layer.borderColor = TextOrangeColor.CGColor;
    self.nextBtn.layer.borderWidth = 1;
    
    [self.nextBtn addTarget:self action:@selector(nextBtnClick:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)nextBtnClick:(UIButton *)btn {
    
    MailDetailsVC *vc = [[MailDetailsVC alloc] init];
    vc.goodsModel = self.model;
    vc.hidesBottomBarWhenPushed = YES;
    [[CommonTTool getCurrentVC].navigationController pushViewController:vc animated:YES];
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)setModel:(MailGoodsModel *)model {
    
    _model = model;
    SD_SETIMAGE_PlaceHolder(self.imgView, model.img_path, @"default_image_square")
    self.nameLabel.text = model.sku_name;
    self.tGPriceLabel.text = [CommonTTool getPrice:model.buy_price];
    NSString *price = [NSString stringWithFormat:@"销售价¥%@",[CommonTTool getPrice:model.market_price]];
    NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:price];
    [attri addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlinePatternSolid | NSUnderlineStyleSingle) range:NSMakeRange(0, attri.length)];
    [attri addAttribute:NSStrikethroughColorAttributeName value: self.oldPriceLabel.textColor range:NSMakeRange(0, attri.length)];
    self.oldPriceLabel.attributedText = attri;

    self.stockLabel.text = [NSString stringWithFormat:@"仅剩%d件",model.stock];
    NSString *deadlineStr = self.model.end_time;
    // 倒计时的时间 测试数据
//    NSString *deadlineStr = @"2020-06-24 12:00:00";
    // 当前时间的时间戳
    NSString *nowStr = [CommonTTool getCurrentTimeyyyymmdd];
    // 计算时间差值
    NSInteger secondsCountDown = [CommonTTool getDateDifferenceWithNowDateStr:nowStr deadlineStr:deadlineStr];
    if (secondsCountDown <= 0) {
           self.timeTwoLabel.text = @" 团购已结束 ";
           return;
       }
    __weak __typeof(self) weakSelf = self;
    if (_timer == nil) {
        __block NSInteger timeout = secondsCountDown; // 倒计时时间
        
        if (timeout!=0) {
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
            dispatch_source_set_timer(_timer, dispatch_walltime(NULL, 0), 1.0*NSEC_PER_SEC,  0); //每秒执行
            dispatch_source_set_event_handler(_timer, ^{
                if(timeout <= 0){ //  当倒计时结束时做需要的操作: 关闭 活动到期不能提交
                    dispatch_source_cancel(self->_timer);
                    self->_timer = nil;
                    dispatch_async(dispatch_get_main_queue(), ^{
                       // weakSelf.timeLabel.text = @"当前活动已结束";
                        [[NSNotificationCenter defaultCenter] postNotificationName:kUserAbulkTableUpDate object:nil];
                    });
                } else { // 倒计时重新计算 时/分/秒
                    NSInteger days = (int)(timeout/(3600*24));
                    NSInteger hours = (int)((timeout-days*24*3600)/3600);
                    NSInteger minute = (int)(timeout-days*24*3600-hours*3600)/60;
                    //NSInteger second = timeout - days*24*3600 - hours*3600 - minute*60;
                    NSString *strTime = [NSString stringWithFormat:@" %ld小时%ld分钟 ", (long)hours, (long)minute];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (days == 0) {
                            weakSelf.timeTwoLabel.text = strTime;
                        } else {
                            weakSelf.timeTwoLabel.text = [NSString stringWithFormat:@" %ld天%ld小时%ld分钟 ", (long)days, (long)hours, (long)minute];
                        }
                        
                    });
                    timeout--; // 递减 倒计时-1(总时间以秒来计算)
                }
            });
            dispatch_resume(_timer);
        }
    }
    
}

@end
