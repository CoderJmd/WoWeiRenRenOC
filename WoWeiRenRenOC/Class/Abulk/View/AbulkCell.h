//
//  AbulkCell.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/13.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MailGoodsModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface AbulkCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *timeOneLabel;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;
@property (weak, nonatomic) IBOutlet UILabel *timeTwoLabel;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (strong, nonatomic)MailGoodsModel *model;

@property (weak, nonatomic) IBOutlet UILabel *tGPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *stockLabel;
@property (strong, nonatomic)id timer;
@property (weak, nonatomic) IBOutlet UILabel *oldPriceLabel;

@end

NS_ASSUME_NONNULL_END
