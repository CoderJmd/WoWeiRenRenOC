//
//  SearchHistoryWordsView.m
//  SchoolApp
//
//  Created by 姜伟 on 2019/7/28.
//  Copyright © 2019 znz. All rights reserved.
//

#import "SearchHistoryWordsView.h"

@implementation SearchHistoryWordsView

- (void)setHistoryArray:(NSArray *)historyArray
{
    _historyArray = historyArray;
    
    if (historyArray.count==0) {
        self.historyHeight.constant = 0.0;
        self.historyView.hidden = YES;
        self.hotY.constant = 0.0;
    }
    else {
        self.historyView.hidden = NO;
        
        // 删除原有HistoryWords
        for (UIView *view in self.historyView.subviews) {
            if (view.tag>999) {
                [view removeFromSuperview];
            }
        }
        // 添加新的HistoryWords
        CGFloat startX = 15.0;
        CGFloat startY = 45.0;
        CGFloat margin = 10.0;
        for (int i=0; i<historyArray.count; i++) {
            NSString *title = historyArray[i];
            UIButton *btn = [self cretButton:title];
            btn.tag = i + 1000;
            
            // 根据上一个按钮位置  来判断下一个按钮的位置
            UIView *lastView = [self.historyView.subviews lastObject];
            if (lastView.tag>999) {
                startX = CGRectGetMaxX(lastView.frame) + margin;
                if (startX+btn.width > KSCREEN_WIDTH-15.0) {
                    startY += (margin+btn.height);
                    startX = 15.0;
                }
                else {
                    startY = lastView.mj_y;
                }
            }
            btn.frame = CGRectMake(startX, startY, btn.width, btn.height);
            [self.historyView addSubview:btn];
            
            // 添加点击事件
            [btn addTarget:self action:@selector(wordsButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        }
        // 添加完按钮后 改变view高度
        UIView *lastView = [self.historyView.subviews lastObject];
        self.historyHeight.constant = CGRectGetMaxY(lastView.frame) + 20.0;
        self.hotY.constant = self.historyHeight.constant;
    }
}

- (void)setHotArray:(NSArray *)hotArray
{
    _hotArray = hotArray;
    
    if (hotArray.count==0) return;
    
    // 删除原有HotWords
    for (UIView *view in self.hotView.subviews) {
        if (view.tag>999) {
            [view removeFromSuperview];
        }
    }
    // 添加新的HistoryWords
    CGFloat startX = 15.0;
    CGFloat startY = 45.0;
    CGFloat margin = 10.0;
    for (int i=0; i<hotArray.count; i++) {
        NSString *title = hotArray[i];
        UIButton *btn = [self cretButton:title];
        btn.tag = i + 1000;
        
        // 根据上一个按钮位置  来判断下一个按钮的位置
        UIView *lastView = [self.hotView.subviews lastObject];
        if (lastView.tag>999) {
            startX = CGRectGetMaxX(lastView.frame) + margin;
            if (startX+btn.width > KSCREEN_WIDTH-15.0) {
                startY += (margin+btn.height);
                startX = 15.0;
            }
            else {
                startY = lastView.mj_y;
            }
        }
        btn.frame = CGRectMake(startX, startY, btn.width, btn.height);
        [self.hotView addSubview:btn];
        
        // 添加点击事件
        [btn addTarget:self action:@selector(wordsButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    // 添加完按钮后 改变view高度
    UIView *lastView = [self.hotView.subviews lastObject];
    self.hotHeight.constant = CGRectGetMaxY(lastView.frame) + 40.0;
}

// 点击标签
- (void)wordsButtonClick:(UIButton *)sender
{
    if (self.historyWordsClick) {
        self.historyWordsClick(sender.currentTitle);
    }
}

- (IBAction)deletHistoryClick {
    self.historyHeight.constant = 0.0;
    self.historyView.hidden = YES;
    self.hotY.constant = self.historyHeight.constant;
    [ZNZDefaults removeObjectForKey:@"History"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"History"];
    [self.historyArray removeAllObjects];
}


- (UIButton *)cretButton:(NSString *)title
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.clipsToBounds = YES;
    btn.layer.cornerRadius = 2.5;
//    btn.backgroundColor = [UIColor hexStringToColor:@"#F5F5F5"];
    btn.titleLabel.font = [UIFont systemFontOfSize:14.0];
    [btn setTitle:title forState:UIControlStateNormal];
//    [btn setTitleColor:[UIColor hexStringToColor:@"#808080"] forState:UIControlStateNormal];
    
    CGFloat btnW = [title boundingRectWithSize:CGSizeMake(KSCREEN_WIDTH-30.0, 30.0) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0]} context:nil].size.width + 24.0;
    btn.frame = CGRectMake(0.0, 0.0, btnW, 30.0);
    
    return btn;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
