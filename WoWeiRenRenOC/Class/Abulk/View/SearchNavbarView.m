//
//  SearchNavbarView.m
//  PensionUser
//
//  Created by 杨涛 on 2019/2/25.
//  Copyright © 2019 znz. All rights reserved.
//

#import "SearchNavbarView.h"

@implementation SearchNavbarView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

+ (instancetype)GetmyHeaderView {
    return [[[NSBundle mainBundle] loadNibNamed:@"SearchNavbarView" owner:nil options:nil] lastObject];
}

- (IBAction)backClick:(id)sender {
    [[CommonTTool getCurrentVC].navigationController popViewControllerAnimated:YES];
}

@end
