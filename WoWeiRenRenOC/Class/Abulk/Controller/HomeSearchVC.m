//
//  HomeSearchVC.m
//  PensionUser
//
//  Created by 杨涛 on 2019/2/25.
//  Copyright © 2019 znz. All rights reserved.
//

#import "HomeSearchVC.h"
#import "HistorySearchCell.h" // 历史记录cell
#import "SearchHistoryWordsView.h"
#import "SearchNavbarView.h"
#import "MailGoodsModel.h"
#import "BusinessModel.h"
#import "AbulkCell.h"
#import "MailCell.h"
#import "HomeCell.h"
#import "MailDetailsVC.h"
#import "BusinessDetailedVC.h"
#import "PagingViewController.h"
#import "SearchHomeDeleteView.h"


typedef void (^HistoryWordsBlock)(NSString *title);
static CGFloat const segmentViewHeight = 44.0;

@interface HomeSearchVC () <UITextFieldDelegate>
{
    SearchNavbarView *_searchNavbarView;
    NSString *_keyStr;
}
@property (nonatomic, assign) BOOL isHistory;
@property (strong, nonatomic) NSMutableArray *historyArray;
@property (strong, nonatomic) ZJScrollSegmentView *segmentView;
@property (strong, nonatomic) NSArray<NSString *> *titles;
@property (assign, nonatomic) NSInteger index;
@property (nonatomic, strong) NSMutableArray *jsonArray;
@property (nonatomic, copy) NSString *jsonStr;
// 历史搜索
@property (nonatomic, strong) SearchHistoryWordsView *historyWordsView;
@end

@implementation HomeSearchVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.view setBackgroundColor:VCBgcolor];
    
    _isHistory = YES;
    
    _keyStr = History;

    
    
    
    if ([ZNZDefaults objectForKey:_keyStr]) {
        self.historyArray = [NSMutableArray arrayWithArray:[ZNZDefaults objectForKey:_keyStr]];
    } else {
        self.historyArray = [NSMutableArray arrayWithCapacity:0];
    }
    
    [self createSearchView]; // 创建搜索框
    
    [self setupTableView]; // 创建tableview视图
    
    self.index = 0;
    
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES];
    
    [self.baseTableview reloadData];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.navigationController setNavigationBarHidden:NO];
}

- (SearchHistoryWordsView *)historyWordsView
{
    if (!_historyWordsView) {
        _historyWordsView = [[NSBundle mainBundle] loadNibNamed:@"SearchHistoryWordsView" owner:self options:nil].lastObject;
        _historyWordsView.frame = CGRectMake( 0, _searchNavbarView.bottom, KSCREEN_WIDTH, KSCREEN_HEIGHT - _searchNavbarView.bottom);
        [_historyWordsView layoutIfNeeded];
        
        // 设置点击事件
        __weak typeof(self) wkSelf = self;
        _historyWordsView.historyWordsClick = ^(NSString * _Nonnull title) {
            NSLog(@"historyWordsClick title = %@", title);
            _searchNavbarView.tf.text = title;
            [wkSelf searchClick];
        };
    }
    return _historyWordsView;
}

#pragma mark - 创建搜索框
- (void)createSearchView {
    _searchNavbarView = [SearchNavbarView GetmyHeaderView];
    [_searchNavbarView setFrame:kFrame( 0, 0, KSCREEN_WIDTH, NEW_NavHeight)];
    [_searchNavbarView.tf setDelegate:self];
    [_searchNavbarView.tf addTarget:self action:@selector(textFieldChanged:) forControlEvents:UIControlEventEditingChanged];
    [_searchNavbarView.searchBtn addTarget:self action:@selector(searchClick) forControlEvents:UIControlEventTouchUpInside];
    [_searchNavbarView.searchBtn2 addTarget:self action:@selector(searchClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_searchNavbarView];
}


#pragma mark - 创建tableview视图
- (void)setupTableView {
    self.tableViewFrame = CGRectMake( 0, _searchNavbarView.bottom, KSCREEN_WIDTH, KSCREEN_HEIGHT - _searchNavbarView.bottom);
    self.tableviewStyle = UITableViewStylePlain;
    self.baseTableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.baseTableview registerNib:[UINib nibWithNibName:@"MailCell" bundle:nil] forCellReuseIdentifier:@"MailCell"];
    [self.baseTableview registerNib:[UINib nibWithNibName:@"AbulkCell" bundle:nil] forCellReuseIdentifier:@"AbulkCell"];
    [self.baseTableview registerNib:[UINib nibWithNibName:@"HomeCell" bundle:nil] forCellReuseIdentifier:@"HomeCell"];

    UIView *tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, KSCREEN_WIDTH, 10)];
    [tableHeaderView setBackgroundColor:VCBgcolor];
    self.baseTableview.tableHeaderView = tableHeaderView;
    
    [self.baseTableview setBackgroundColor:VCBgcolor];
}

#pragma mark--------------------设置代理
#pragma mark - 返回标题文字
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = @"没有找到，重新搜索试试吧~";
    NSDictionary *attribute = @{NSFontAttributeName: kPLFont(14), NSForegroundColorAttributeName: BlackColor2};
    return [[NSAttributedString alloc] initWithString:text attributes:attribute];
}

#pragma mark - 是否允许滚动，默认NO
- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView {
    return YES;
}

#pragma mark -  返回图片
- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView{
    return [UIImage imageNamed:@"meisousuo"];
}

#pragma mark -- tableview datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_isHistory) {
        if (_historyArray.count) {
            return self.historyArray.count;
        } else {
            return 0;
        }
    }
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_isHistory) {
      
        HistorySearchCell *cell = [HistorySearchCell cellWithTableView:tableView];
        
        [cell setBackgroundColor:[UIColor whiteColor]];
        
        if (self.historyArray.count > 0) {
            cell.titleLabel.text = [self.historyArray objectAtIndex:indexPath.row];
            cell.deleteBtn.hidden = YES;
            [cell.deleteBtn setTag:indexPath.row];
            [cell.deleteBtn addTarget:self action:@selector(removeFromHistoryWithTag:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        tableView.rowHeight = 56;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
          
    } else {
        HistorySearchCell *cell = [HistorySearchCell cellWithTableView:tableView];
        tableView.rowHeight = 70;
        if (self.index == 0) {
            
            MailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MailCell"];
            cell.model = self.dataSource[indexPath.row];
            tableView.rowHeight = 135;
            return cell;
        }else if (self.index == 1) {
           
            
            AbulkCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AbulkCell"];
            cell.model = self.dataSource[indexPath.row];
            tableView.rowHeight = 150;
            return cell;
            
        }else {
            
            HomeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HomeCell"];
            cell.model = self.dataSource[indexPath.row];
            cell.discountBtn.hidden= YES;
            tableView.rowHeight = 135;
            return cell;
            
        }
            

        return cell;
    }
    
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    SearchHomeDeleteView *headerView = [[NSBundle mainBundle] loadNibNamed:@"SearchHomeDeleteView" owner:self options:nil].lastObject;
    [headerView setBackgroundColor:kColor(245)];
    [headerView.deleteBtn addTarget:self action:@selector(deleteHistoryData) forControlEvents:UIControlEventTouchUpInside];
    
    if (_isHistory) {
       
        
        
        return headerView;
    } else if (!_isHistory) {
       return self.segmentView;
    }
       
    return nil;
}

- (void)deleteHistoryData {
    
    UIAlertController *alterController = [UIAlertController alertControllerWithTitle:nil message:@"是否清除历史记录？" preferredStyle:UIAlertControllerStyleAlert];
    
    [alterController addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    [alterController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        _isHistory = YES;
        [self.historyArray removeAllObjects];
        [ZNZDefaults setObject:nil forKey:_keyStr];
        [self.baseTableview reloadData];
        
    }]];
    
    [self presentViewController:alterController animated:YES completion:nil];
    
}

#pragma mark- setter getter
- (ZJScrollSegmentView *)segmentView {
    if (_segmentView == nil) {
        ZJSegmentStyle *style = [[ZJSegmentStyle alloc] init];
        // 设置滚动条高度
        style.segmentHeight = 45;
        // 显示图片 (在显示图片的时候只有下划线的效果可以开启, 其他的'遮盖','渐变',效果会被内部关闭)
        style.showImage = NO;
        style.showLine = YES;
        style.scrollLineColor = kRGBAColor(221, 169, 72, 1);
        style.imagePosition = TitleImagePositionLeft;
        // 当标题(和图片)宽度总和小于ZJScrollPageView的宽度的时候, 标题会自适应宽度
        style.autoAdjustTitlesWidth = YES;
        style.showExtraButton = NO;
        // 设置附加按钮的背景图片
        style.extraBtnBackgroundImageName = @"";
        
        style.selectedTitleColor = kRGBAColor(221, 169, 72, 1);
        style.normalTitleColor = BlackColor2;
        style.titleFont = kPRFont(16);
        
        self.titles = @[@"商品",@"团购",@"商家"];
        
        // 注意: 一定要避免循环引用!!
        __weak typeof(self) weakSelf = self;
        ZJScrollSegmentView *segment = [[ZJScrollSegmentView alloc] initWithFrame:CGRectMake(0, NEW_NavHeight, self.view.bounds.size.width, segmentViewHeight - 1) segmentStyle:style delegate:self titles:self.titles titleDidClick:^(ZJTitleView *titleView, NSInteger index) {
            weakSelf.index = index;
            
            weakSelf.pageNum = 1;
            
            [weakSelf setMJRefresh];
            
            if (self.index == 0) {
               [self requestSku];
            }else if (self.index == 1) {
               [self requestSkuTG];
            }else {
               [self requestBussion];
            }

            
        }];
        segment.backgroundColor = [UIColor whiteColor];
        
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, segmentViewHeight - 1, KSCREEN_WIDTH, 1)];
        [lineView setBackgroundColor:kAColor(235, 0.6632)];
        [segment addSubview:lineView];
        
        _segmentView = segment;
    }
    return _segmentView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (_isHistory) {
        return 50.0f;
    } else if (!_isHistory) {
        return segmentViewHeight;
    }
    return 0.000001;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_isHistory) {
        _isHistory = NO;

        _searchNavbarView.tf.text = self.historyArray[indexPath.row];
        self.pageNum = 1;
        
        [self setMJRefresh];
        if (self.index == 0) {
           [self requestSku];
        }else if (self.index == 1) {
           [self requestSkuTG];
        }else {
           [self requestBussion];
        }
        
    } else {
        
        if (self.index == 2) {
             
            BusinessModel *model = self.dataSource[indexPath.row];
            PagingViewController *vc= [[PagingViewController alloc]init];
            vc.business_id = [NSString stringWithFormat:@"%ld",(long)model.business_id];
            [self.navigationController pushViewController:vc animated:YES];
            
//            if (model.zxtg_state == 4) {
//               PagingViewController *vc= [[PagingViewController alloc]init];
//               vc.business_id = [NSString stringWithFormat:@"%ld",(long)model.business_id];
//               [self.navigationController pushViewController:vc animated:YES];
//
//            }else {
//
//               BusinessDetailedVC *vc = [[BusinessDetailedVC alloc] init];
//               vc.model = self.dataSource[indexPath.row];
//
//               [self.navigationController pushViewController:vc animated:YES];
//
//            }
            
        }else {
           
            MailDetailsVC *vc = [[MailDetailsVC alloc] init];
            vc.goodsModel = self.dataSource[indexPath.row];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
            
            
        }
        
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self searchClick]; // 搜索事件
    return YES;
}
// 文字改变时调用
- (void)textFieldChanged:(UITextField*)textField {
    if ([CommonTTool isBlankString:textField.text]) {
        self.historyWordsView.hidden = NO;
    }
}

#pragma mark - 搜索事件
- (void)searchClick {
    [CommonTTool dismisKeyBoard];
    self.historyWordsView.hidden = YES;

    if (_searchNavbarView.tf.text.length == 0) {
        _isHistory = YES;
    } else {
        _isHistory = NO;
        if (![self.historyArray containsObject:_searchNavbarView.tf.text]) {
            [self.historyArray addObject:_searchNavbarView.tf.text];

            if (self.historyArray.count == 6) {
                [self.historyArray removeObjectAtIndex:0];
            }
            [ZNZDefaults setObject:self.historyArray forKey:_keyStr];
            self.historyWordsView.historyArray = self.historyArray;
        }
    }
    
    if (self.index == 0) {
        [self requestSku];
    }else if (self.index == 1) {
        [self requestSkuTG];
    }else {
        [self requestBussion];
    }
}

#pragma mark - 删除某一个历史记录
- (void)removeFromHistoryWithTag:(UIButton*)sender {
    if (self.historyArray.count > 1) {
        [self.historyArray removeObjectAtIndex:sender.tag];
        [ZNZDefaults setObject:self.historyArray forKey:_keyStr];
        
    } else {  /** 全部删除 */
        [self.historyArray removeObjectAtIndex:sender.tag];
            [ZNZDefaults setObject:nil forKey:_keyStr];
        }
        [self.baseTableview reloadData];
}

#pragma mark - 设置刷新
- (void)setMJRefresh {
    self.baseTableview.mj_header = [ MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(requrstNewData)];
    
    self.baseTableview.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(requestMoreData)];
}

- (void)requrstNewData {
    self.pageNum = 1;
    
    if (self.index == 0) {
        [self requestSku];
    }else if (self.index == 1) {
        [self requestSkuTG];
    }else {
        [self requestBussion];
    }
    

    
}

- (void)requestMoreData {
    self.pageNum ++;
    
    if (self.index == 0) {
        [self requestSku];
    }else if (self.index == 1) {
        [self requestSkuTG];
    }else {
        [self requestBussion];
    }
}

#pragma mark - 请求商品
- (void)requestSku {
    
    [self.baseTableview.mj_header endRefreshing];
    [self.baseTableview.mj_footer endRefreshing];
    NSMutableDictionary *para = [NSMutableDictionary dictionary];
    [para setObject:@(self.pageNum) forKey:@"page"];
    [para setObject:_searchNavbarView.tf.text forKey:@"sku_name"];
    [para setObject:@(10) forKey:@"limit"];
    
    

    [NetRequestTool postUrl:@"sku/queryGoodsList" parameters:para success:^(id responseObject) {
        
        if (self.pageNum == 1) {
            
            [self.dataSource removeAllObjects];
            [self.dataSource addObjectsFromArray:[MailGoodsModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]]];
        } else {
            
            [self.dataSource addObjectsFromArray:[MailGoodsModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]]];
            if (![CommonTTool isStrNull:responseObject[@"page"]]) {
                NSArray *dicArr = responseObject[@"object"];
                if ([responseObject[@"page"][@"total_page"] integerValue] == self.pageNum || [responseObject[@"page"][@"total_page"] integerValue] == 1 || [responseObject[@"page"][@"total_page"] integerValue] == 0 || dicArr.count < 10) {
                    [self.baseTableview.mj_footer endRefreshingWithNoMoreData];
                }
            } else {
                [self.baseTableview.mj_footer endRefreshingWithNoMoreData];
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.baseTableview reloadData];
        });
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        
    }];

    
  
}

#pragma mark - 请求团购
- (void)requestSkuTG{
    
    [self.baseTableview.mj_header endRefreshing];
    [self.baseTableview.mj_footer endRefreshing];
    NSMutableDictionary *para = [NSMutableDictionary dictionary];
    [para setObject:@(self.pageNum) forKey:@"page"];
    [para setObject:_searchNavbarView.tf.text forKey:@"sku_name"];
    [para setObject:@(10) forKey:@"limit"];
    
    

    [NetRequestTool postUrl:@"sku/queryTgGoodsList" parameters:para success:^(id responseObject) {
        
        if (self.pageNum == 1) {
            
            [self.dataSource removeAllObjects];
            [self.dataSource addObjectsFromArray:[MailGoodsModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]]];
        } else {
            
            [self.dataSource addObjectsFromArray:[MailGoodsModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]]];
            if (![CommonTTool isStrNull:responseObject[@"page"]]) {
                NSArray *dicArr = responseObject[@"object"];
                if ([responseObject[@"page"][@"total_page"] integerValue] == self.pageNum || [responseObject[@"page"][@"total_page"] integerValue] == 1 || [responseObject[@"page"][@"total_page"] integerValue] == 0 || dicArr.count < 10) {
                    [self.baseTableview.mj_footer endRefreshingWithNoMoreData];
                }
            } else {
                [self.baseTableview.mj_footer endRefreshingWithNoMoreData];
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.baseTableview reloadData];
        });
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        
    }];
    
    

    
}
#pragma mark - 请求商家
- (void)requestBussion {

    
    [self.baseTableview.mj_header endRefreshing];
    [self.baseTableview.mj_footer endRefreshing];
    NSMutableDictionary *para = [NSMutableDictionary dictionary];
    [para setObject:@(self.pageNum) forKey:@"page"];
    [para setObject:_searchNavbarView.tf.text forKey:@"business_name"];
    [para setObject:@(10) forKey:@"limit"];
    [para setObject:@(2) forKey:@"is_hot"];
    [para setObject:@(4) forKey:@"zxtg_state"];

    [NetRequestTool postUrl:@"business/queryBusinessList" parameters:para success:^(id responseObject) {
       
       if (self.pageNum == 1) {
           
           [self.dataSource removeAllObjects];
           [self.dataSource addObjectsFromArray:[BusinessModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]]];
       } else {
           
           [self.dataSource addObjectsFromArray:[BusinessModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]]];
           if (![CommonTTool isStrNull:responseObject[@"page"]]) {
               NSArray *dicArr = responseObject[@"object"];
               if ([responseObject[@"page"][@"total_page"] integerValue] == self.pageNum || [responseObject[@"page"][@"total_page"] integerValue] == 1 || [responseObject[@"page"][@"total_page"] integerValue] == 0 || dicArr.count < 10) {
                   [self.baseTableview.mj_footer endRefreshingWithNoMoreData];
               }
           } else {
               [self.baseTableview.mj_footer endRefreshingWithNoMoreData];
           }
       }
       
       dispatch_async(dispatch_get_main_queue(), ^{
           [self.baseTableview reloadData];
       });
    } failure:^(NSInteger statusCode) {
       
    } finish:^{
       
    }];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
