//
//  AbulkDetailsVC.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/17.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseTableViewController.h"
#import "UserShoppingOrderModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface AbulkDetailsVC : BaseTableViewController
@property (strong, nonatomic)UserShoppingOrderModel *model;

@end

NS_ASSUME_NONNULL_END
