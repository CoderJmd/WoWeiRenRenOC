//
//  ZNZBaseViewController.h
//  CrossborderLogistics
//
//  Created by Apple on 2018/7/30.
//  Copyright © 2018年 谁敢横刀立马，唯我彭大将军. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZNZBaseViewController : UIViewController

@property (nonatomic, assign) BOOL isLoginVC;
@property (nonatomic, strong)  UIButton *leftButton;
@property (nonatomic, strong)  UIButton *rightBtn;


//------创建导航栏按钮，参数类型可以是：UIImage或者NSString

//创建导航栏左边一个按钮，右边两个按钮
- (void)createNavButton:(id)leftButtonParam rightButton1:(id)rightButtonParam1 rightButton2:(id)rightButtonParam2;

//创建导航栏右边一个按钮
- (void)createNavRightButton:(id)rightButton;

//创建导航栏右边两个按钮
- (void)createNavButton: (id)rightButton1 rightButton:(id)rightButton2;

/** 返回按钮点击事件 */
@property (nonatomic, copy) void(^buttonClick)(NSInteger tag);

//创建导航栏左边按钮
- (void)createNavLeftButton:(id)leftButton;

//创建左边导航栏，显示图片及文字
- (void)createNavLeftButton:(UIImage *)image title:(NSString *)title font:(UIFont *)font titleColor:(UIColor *)titleColor highlightedColor:(UIColor *)highlightedColor;

//创建右边导航栏，显示图片及文字
- (void)createNavRightButton:(UIImage *)image title:(NSString *)title font:(UIFont *)font titleColor:(UIColor *)titleColor highlightedColor:(UIColor *)highlightedColor;

//创建左边导航栏，显示图片及文字
//- (void)createNavLeftButton:(UIImage *)image title:(NSString *)title font:(UIFont *)font titleColor:(UIColor *)titleColor highlightedColor:(UIColor *)highlightedColor;


@property (nonatomic, strong) UIImageView  *baseImageView;

@end
