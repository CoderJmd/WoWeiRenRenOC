//
//  ZNZBaseTableViewController.h
//  CrossborderLogistics
//
//  Created by Apple on 2018/7/30.
//  Copyright © 2018年 谁敢横刀立马，唯我彭大将军. All rights reserved.
//
#import <UIKit/UIKit.h>

#import "ZNZBaseViewController.h"

@interface ZNZBaseTableViewController : ZNZBaseViewController

@property (nonatomic, assign) CGRect tableViewFrame;

@property (nonatomic, assign) NSInteger tableviewStyle;

@property (nonatomic, strong) UITableView *baseTableview;

@property (nonatomic, strong) SDCycleScrollView *cycleScrollView;

@property (nonatomic, strong) NSMutableArray *dataSource;

@property (assign, nonatomic) NSInteger pageNum;

@property (assign, nonatomic) NSInteger totalPage;


- (void)setMJRefresh;

@end
