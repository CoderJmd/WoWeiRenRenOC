//
//  ZNZBaseViewController.m
//  CrossborderLogistics
//
//  Created by Apple on 2018/7/30.
//  Copyright © 2018年 谁敢横刀立马，唯我彭大将军. All rights reserved.
//

#import "ZNZBaseViewController.h"

@interface ZNZBaseViewController () <UINavigationControllerDelegate,UIGestureRecognizerDelegate>
{
    UIPercentDrivenInteractiveTransition *_interactiveTransition;
}
@end

@implementation ZNZBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setBackgroundColor:VCBgcolor];
    
    [self setMyNaviBar];
    
    id target = self.navigationController.interactivePopGestureRecognizer.delegate;

    // handleNavigationTransition:为系统私有API,即系统自带侧滑手势的回调方法，我们在自己的手势上直接用它的回调方法
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:target action:@selector(handleNavigationTransition:)];
    panGesture.delegate = self; // 设置手势代理，拦截手势触发
    [self.view addGestureRecognizer:panGesture];

    // 一定要禁止系统自带的滑动手势
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
}


// 什么时候调用，每次触发手势之前都会询问下代理方法，是否触发
// 作用：拦截手势触发
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    // 当当前控制器是根控制器时，不可以侧滑返回，所以不能使其触发手势
    
    if (self.navigationController.childViewControllers.count == 1) {
        return NO;
    }
    
    CGPoint touchPoint = [gestureRecognizer locationInView:[[UIApplication sharedApplication] keyWindow]];
    
    if (touchPoint.x > 40) {
        return NO;
    }
    
    return YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [CommonTTool dismisKeyBoard];
    
    
    SVPDismiss
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [CommonTTool dismisKeyBoard];
}

- (void)setMyNaviBar {
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName: kPMFont(18),NSForegroundColorAttributeName:BlackColor}];
    
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    
   
}

- (void)back {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//创建导航栏右边2个按钮
- (void)createNavButton: (id)rightButton1 rightButton:(id)rightButton2 {
    
    UIView *barView;
    UIButton *btn2;
    btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    barView.backgroundColor = [UIColor blueColor];
    if ([rightButton1 isKindOfClass:[UIImage class]] && [rightButton2 isKindOfClass:[UIImage class]] ) {
        barView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 80, 40)];
    } else {
        barView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KSCREEN_WIDTH/2-20, 40)];
    }
    [btn2.titleLabel setFont:kPRFont(16)];
    [btn2 setTitleColor:BlueColor forState:UIControlStateNormal];
    [btn2 addTarget:self action:@selector(clickOne) forControlEvents:UIControlEventTouchUpInside];
    [barView addSubview:btn2];
    [btn2 sizeToFit];
    
    if (btn2.bounds.size.width < 40) {
        CGFloat width = 40 / btn2.bounds.size.height * btn2.bounds.size.width;
        btn2.bounds = CGRectMake(0, 0, width, 40);
    }
    if ([rightButton2 isKindOfClass:[UIImage class]]) {
        [btn2 setImage:rightButton2 forState:UIControlStateNormal];
        btn2.frame = CGRectMake(barView.width - 40, 0, 40, 40);
        
    }
    if ([rightButton2 isKindOfClass:[NSString class]]) {
        [btn2 setTitle:rightButton2 forState:UIControlStateNormal];
        
        btn2.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 15);
        
        CGSize size = CGSizeMake(MAXFLOAT, 40.0f);
        CGSize buttonSize = [btn2.titleLabel.text boundingRectWithSize:size
                                                               options:NSStringDrawingTruncatesLastVisibleLine  | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                                            attributes:@{ NSFontAttributeName:btn2.titleLabel.font}
                                                               context:nil].size;
        
        btn2.bounds = CGRectMake(0, 0, buttonSize.width+15, 40);
        btn2.mj_x = barView.width - buttonSize.width - 15;
        btn2.mj_y = 0;
    }
    
    UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [btn1 setTitleColor:BlueColor forState:UIControlStateNormal];
    [btn1 addTarget:self action:@selector(clickTwo) forControlEvents:UIControlEventTouchUpInside];
    [barView addSubview:btn1];
    [btn1 sizeToFit];
    
    if ([rightButton1 isKindOfClass:[UIImage class]]) {
        btn1.frame = CGRectMake(barView.width - btn2.width-40, 0, 40, 40);
        
        [btn1 setImage:rightButton1 forState:UIControlStateNormal];
    }
    if ([rightButton1 isKindOfClass:[NSString class]]){
        
        [btn1 setTitle:rightButton1 forState:UIControlStateNormal];
        btn1.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 15);
        
        CGSize size = CGSizeMake(MAXFLOAT, 40.0f);
        CGSize buttonSize = [btn1.titleLabel.text boundingRectWithSize:size
                                                               options:NSStringDrawingTruncatesLastVisibleLine  | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                                            attributes:@{ NSFontAttributeName:btn1.titleLabel.font}
                                                               context:nil].size;
        
        btn1.bounds = CGRectMake(0, 0,  buttonSize.width + 15, 40);
        btn1.mj_x = barView.width - btn2.size.width-buttonSize.width-15;
        ;
        btn1.mj_y = 0;
        
    }
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:barView];
}




// 创建导航栏上三个按钮
- (void)createNavButton:(id)leftButtonParam rightButton1:(id)rightButtonParam1 rightButton2:(id)rightButtonParam2 {
    if (![CommonTTool isStrNull:leftButtonParam]) {
        [self createNavLeftButton:leftButtonParam];
    }
    
    if ((![CommonTTool isStrNull:rightButtonParam1] && [CommonTTool isStrNull:rightButtonParam2])) {
        [self createNavRightButton:rightButtonParam1];
    }
    
    if ([CommonTTool isStrNull:rightButtonParam1] && ![CommonTTool isStrNull:rightButtonParam2]) {
        [self createNavRightButton:rightButtonParam2];
    }
    
    if (![CommonTTool isStrNull:rightButtonParam1] && ![CommonTTool isStrNull:rightButtonParam2]) {
        [self createNavButton:rightButtonParam1 rightButton:rightButtonParam2];
    }
}

- (void)lefButtonclick {
    if (self.buttonClick) {
        self.buttonClick(102);
    }
}

- (void)clickOne {
    if (self.buttonClick) {
        self.buttonClick(101);
    }
}

- (void)clickTwo {
    if (self.buttonClick) {
        self.buttonClick(102);
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
