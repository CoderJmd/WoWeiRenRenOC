//
//  HomeSearchVC.h
//  PensionUser
//
//  Created by 杨涛 on 2019/2/25.
//  Copyright © 2019 znz. All rights reserved.
//

#import "ZNZBaseTableViewController.h"
#import "ZJScrollPageView.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeSearchVC : ZNZBaseTableViewController <ZJScrollPageViewChildVcDelegate>


@end

NS_ASSUME_NONNULL_END
