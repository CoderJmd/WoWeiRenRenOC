//
//  AbulkVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/13.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "AbulkVC.h"
#import "BusinessModel.h"
#import "HomeIconModel.h"
#import "WebVC.h"
#import "RegisterPayVC.h"
#import "MailCell.h"
#import "BusinessDetailedVC.h"
#import "MailClassHeadView.h"
#import "MailDetailsVC.h"
#import "AbulkHeadSearchView.h"
#import "AbulkCell.h"
#import "MailGoodsModel.h"

extern UserModel *userModel;
@interface AbulkVC ()

@property (nonatomic,strong) AbulkHeadSearchView *headView;
@property (strong, nonatomic)NSString *serachName;

@end

@implementation AbulkVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.defaultStartPage = 1;
    [self creatTableViewWithCellIdentity:@"AbulkCell"];
    self.tableView.backgroundColor = kRGBColor(246, 246, 246);
    self.tableView.rowHeight = 135;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.frame = CGRectMake(0, StatusViewHeight + 50, kScreenW, kScreenH-StatusViewHeight-TabBarHeight-50);
    
    
    [self setRefresh];
    
    SVPShowInternetWaiting
    [self requestData];
    
    AbulkHeadSearchView  *headView = [[NSBundle mainBundle] loadNibNamed:@"AbulkHeadSearchView" owner:self options:nil].lastObject;
    self.headView = headView;
    headView.frame = CGRectMake(0, StatusViewHeight, kScreenW, 50);
    headView.searchName = ^(NSString * name) {
        self.page = 1;
        [self requestData];
    };
    
    [self.view addSubview:headView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateRequestData) name:kUserAbulkTableUpDate object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateRequestData)  name:kUserAbulkDetailsUpDate object:nil];
}


- (void)updateRequestData {
    
    self.page = 1;
    [self requestData];
    
}

- (void)requestData
{
    
    self.serachName = self.headView.searchTF.text;
    if (self.serachName == nil) {
      self.serachName = @"";
    }
    SVPShowInternetWaiting
    // 请求列表
     WeakSelf(ws)
    [NetRequestTool postUrl:@"sku/queryTgGoodsList" parameters:@{@"page":@(self.page), @"limit":@"10",@"sku_name":self.serachName} success:^(id responseObject) {
         NSMutableArray *arr = [MailGoodsModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]];
         if (self.page==self.defaultStartPage) {
             ws.dataArray = arr;
         }
         else {
             [ws.dataArray addObjectsFromArray:arr];
         }
         [ws.tableView reloadData];
         
         // 控制页数
         NSDictionary *page = [responseObject objectForKey:@"page"];
         if ([page isKindOfClass:NSDictionary.class]) {
             NSInteger totalP = [page[@"total_page"] integerValue];
             if (totalP > self.page) {
                 self.page++;
                 [self endRefresh];
             }
             else {
                 [self endRefreshNoMoreData];
             }
         }
         else {
             [self endRefreshNoMoreData];
         }
     } failure:^(NSInteger statusCode) {
         [ws endRefreshNoMoreData];
     } finish:^{
         SVPDismiss
     }];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AbulkCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AbulkCell"];
    cell.model = self.dataArray[indexPath.row];
    tableView.rowHeight = 150;
//    cell.nextBtn.tag = indexPath.row;
//    [cell.nextBtn addTarget:self action:@selector(nextBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

#pragma mark - 马上抢
- (void)nextBtnClick:(UIButton *)btn {
    
    MailDetailsVC *vc = [[MailDetailsVC alloc] init];
    vc.goodsModel = self.dataArray[btn.tag];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    

    MailDetailsVC *vc = [[MailDetailsVC alloc] init];
    vc.goodsModel = self.dataArray[indexPath.row];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [CommonTTool isVipShow:YES];
    
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}
@end
