//
//  ZNZBaseTableViewController.m
//  CrossborderLogistics
//
//  Created by Apple on 2018/7/30.
//  Copyright © 2018年 谁敢横刀立马，唯我彭大将军. All rights reserved.
//

#import "ZNZBaseTableViewController.h"

@interface ZNZBaseTableViewController ()< UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetDelegate, DZNEmptyDataSetSource>

@end

@implementation ZNZBaseTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.pageNum = 1;
    self.totalPage = 0;
    [self.view addSubview:self.baseTableview];
}
#pragma mark - 懒加载
- (NSMutableArray *)dataSource {
    if (!_dataSource) {
        _dataSource = [NSMutableArray arrayWithCapacity:0];
    }
    return _dataSource;
}

- (UITableView *)baseTableview {
    if (!_baseTableview) {
        _baseTableview = [[UITableView alloc]initWithFrame:_tableViewFrame style:_tableviewStyle];
        _baseTableview.delegate = self;
        _baseTableview.dataSource = self;
        _baseTableview.showsVerticalScrollIndicator = NO;
        _baseTableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return  _baseTableview;
}

- (void)setTableviewStyle:(NSInteger)tableviewStyle {
    _tableviewStyle = tableviewStyle;
    [_baseTableview removeFromSuperview];
    _baseTableview = [[UITableView alloc]initWithFrame:_tableViewFrame style:_tableviewStyle];
    _baseTableview.backgroundColor = [UIColor clearColor];
    _baseTableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    _baseTableview.delegate = self;
    _baseTableview.dataSource = self;
    _baseTableview.showsVerticalScrollIndicator = NO;
    if (iPhoneX) {
        CGRect frame = _baseTableview.frame;
        _baseTableview.frame = kFrame(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height - 20);
    }
    [self.view addSubview:_baseTableview];
    
    self.baseTableview.emptyDataSetSource = self;
    self.baseTableview.emptyDataSetDelegate = self;
    if (@available(iOS 11.0, *)) {
        _baseTableview.estimatedRowHeight = 0;
        _baseTableview.estimatedSectionFooterHeight = 0;
        _baseTableview.estimatedSectionHeaderHeight= 0;
        _baseTableview.contentInsetAdjustmentBehavior= UIScrollViewContentInsetAdjustmentNever;
    }
    
}

#pragma mark--------------------设置代理
#pragma mark - 返回标题文字
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = @"没有相关信息";
    NSDictionary *attribute = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:14.0f], NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    return [[NSAttributedString alloc] initWithString:text attributes:attribute];
}

#pragma mark - 是否允许滚动，默认NO
- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView {
    return YES;
}

#pragma mark -  返回图片
- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView{
    return [UIImage imageNamed:@"meiyouxinxi"];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    if (section == 0) return (160.10f/667.10f) * KSCREEN_HEIGHT;
//    return 0.0000001;
//}
//
//
//#pragma mark - section头部视图
//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//    if (section == 0) {
//        return self.cycleScrollView;
//    }
//    UIView *view= [[UIView alloc]initWithFrame:CGRectMake( 0, 0, KSCREEN_WIDTH, 0.00001)];
//    view.backgroundColor = [UIColor whiteColor];
//    return view;
//}

#pragma mark - section底部间距
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0001;
}

#pragma mark - section底部视图
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake( 0, 0, KSCREEN_WIDTH, 0)];
    view.backgroundColor = VCBgcolor;
    return view;
}

#pragma mark - 设置刷新
- (void)setMJRefresh {
    self.baseTableview.mj_header = [ MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(requrstNewData)];
    
    self.baseTableview.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(requestMoreData)];
}

- (void)requrstNewData {
    self.pageNum = 1;
    [self requestData];
}


- (void)requestMoreData {
    self.pageNum ++;
    
    [self requestData];
}


#pragma mark--------------------数据请求
-(void)requestData {
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
