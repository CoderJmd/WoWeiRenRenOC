//
//  AbulkDetailsVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/17.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "AbulkDetailsVC.h"
#import "ShoppingCartHeadView.h"
#import "AbulkCell.h"
#import "ShoppingCartGroupHeadView.h"
#import "ShoppingCartCell.h"
#import "ShoppingCartBottomView.h"
#import "UserOrderChildGroupView.h"
#import "UserOrderChildCell.h"
#import "UserOrderDetailsVC.h"
#import "AbulkDetailsGoodsCell.h"
#import "AbulkDetailsGoodsFooterView.h"
#import "AbulkDetailsInfoCell.h"
#import "AbulkDetailsRulesCell.h"

extern UserModel *userModel;
@interface AbulkDetailsVC ()

@end

@implementation AbulkDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.defaultStartPage = 1;
    self.title = @"参团详情";
    [self creatTableViewStyle:UITableViewStyleGrouped cellIdentity:@"AbulkDetailsGoodsCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"AbulkDetailsInfoCell" bundle:nil] forCellReuseIdentifier:@"AbulkDetailsInfoCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"AbulkDetailsRulesCell" bundle:nil] forCellReuseIdentifier:@"AbulkDetailsRulesCell"];
    
    self.tableView.backgroundColor = kRGBColor(246, 246, 246);
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.frame = CGRectMake(0, NavBarHeight, kScreenW, kScreenH-NavBarHeight -NeedStatusViewHeight);

    
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        AbulkDetailsGoodsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AbulkDetailsGoodsCell"];
        //cell.model = self.dataArray[indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        tableView.rowHeight = 128;
        return cell;
    }else if (indexPath.section == 1)  {
        
        AbulkDetailsInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AbulkDetailsInfoCell"];
        //cell.model = self.dataArray[indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        tableView.rowHeight = 195;
        return cell;
    }else {
        
        AbulkDetailsRulesCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AbulkDetailsRulesCell"];
        //cell.model = self.dataArray[indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        tableView.rowHeight = 50;
        return cell;
        
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        UserOrderChildGroupView *view = [[NSBundle mainBundle] loadNibNamed:@"UserOrderChildGroupView" owner:self options:nil].lastObject;
           
        return view;
    }else {
        
        UIView *view = [[UIView alloc]init];
        view.backgroundColor = [UIColor clearColor];

        return view;;
        
    }
    
   
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        
        return 50;
    }
    
    return 10;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (section == 0) {
        AbulkDetailsGoodsFooterView *footer = [[NSBundle mainBundle] loadNibNamed:@"AbulkDetailsGoodsFooterView" owner:self options:nil].lastObject;
        
        return footer;
    }else {
        
        UIView *view = [[UIView alloc]init];
        view.backgroundColor = [UIColor clearColor];

        return view;;
    }
    
   
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    if (section == 0) {
        
        return 50;
    }
    
    return 0.01f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section  {
    
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 3;
}
    


@end

