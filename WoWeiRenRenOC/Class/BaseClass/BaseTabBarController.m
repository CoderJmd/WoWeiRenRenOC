//
//  BaseTabBarController.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/13.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseTabBarController.h"
#import "BaseNavigationController.h"
#import "HomeVC.h"
#import "UserMineVC.h"
#import "BusinessMineVC.h"
#import "MailVC.h"
#import "AbulkVC.h"
#import "ShoppingCartVC.h"
#import "NewHomeVC.h"
#import "BenDiHomeVC.h"
#import "BenDiTableVC.h"


extern UserModel *userModel;
@interface BaseTabBarController ()

@end

@implementation BaseTabBarController

-(void)initialize {
    if (@available(iOS 9.0, *)) {
        [UITabBar appearance].translucent = NO;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    /** 去掉顶部的分割线 */
    [self.tabBar setClipsToBounds:YES];
    self.tabBar.barStyle = UIBarStyleDefault;

    self.delegate = self;
    self.tabBar.tintColor = kRGBAColor(221, 169, 72, 1);
    [self setUpChildVC];
    
    self.view.layer.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0].CGColor;
    self.view.layer.shadowColor = [UIColor colorWithRed:45/255.0 green:45/255.0 blue:45/255.0 alpha:0.06].CGColor;
    self.view.layer.shadowOffset = CGSizeMake(0,-1);
    self.view.layer.shadowOpacity = 1;
    self.view.layer.shadowRadius = 0;
    
}

#pragma mark - 初始化tabBar
-(void)setUpChildVC {
    
   
    
 
   
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"kUserIsMerchants"] && [[NSUserDefaults standardUserDefaults] boolForKey:kisLogin]) {
        
        BenDiHomeVC *homeVC = [[BenDiHomeVC alloc]init];
        [self setUpChildVCWithVC:homeVC orignalImage:@"shouye_off" selectedImage:@"shouye_on" title:@"首页"];
        
        BusinessMineVC *mineVC = [[BusinessMineVC alloc]init];
        [self setUpChildVCWithVC:mineVC orignalImage:@"wode_off" selectedImage:@"wode_on" title:@"我的"];
    }
    else {
        NewHomeVC *homeVC = [[NewHomeVC alloc]init];
        [self setUpChildVCWithVC:homeVC orignalImage:@"shouye_off" selectedImage:@"shouye_on" title:@"首页"];
        
//        MailVC *mailVC = [[MailVC alloc]init];
//         [self setUpChildVCWithVC:mailVC orignalImage:@"barzhixiaoshangcheng_off" selectedImage:@"barzhixiaoshangcheng_on" title:@"直销商城"];
        
        BenDiTableVC *mailVC = [[BenDiTableVC alloc]init];
         [self setUpChildVCWithVC:mailVC orignalImage:@"barzhixiaoshangcheng_off" selectedImage:@"barzhixiaoshangcheng_on" title:@"本地生活"];
        

         AbulkVC *abulkVC = [[AbulkVC alloc]init];
         [self setUpChildVCWithVC:abulkVC orignalImage:@"bartuangou_off" selectedImage:@"bartuangou_on" title:@"众多团购"];

         ShoppingCartVC *shoppingCartVC = [[ShoppingCartVC alloc]init];
         [self setUpChildVCWithVC:shoppingCartVC orignalImage:@"bargouwuche_off" selectedImage:@"bargouwuche_on" title:@"购物车"];
        
        
        NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:kUserInfo];
        userModel = [UserModel mj_objectWithKeyValues:userInfo];
        UserMineVC *mineVC = [[UserMineVC alloc]init];
        [self setUpChildVCWithVC:mineVC orignalImage:@"wode_off" selectedImage:@"wode_on" title:@"我的"];
    }
}

- (void)setUpChildVCWithVC:(UIViewController*)childVC orignalImage:(NSString*)originalImageStr selectedImage:(NSString*)selectedImageStr title:(NSString*)title {
    
    UIImage *orignalImage = [UIImage imageNamed:originalImageStr];
    orignalImage = [orignalImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    UIImage *selectedImage = [UIImage imageNamed:selectedImageStr];
    selectedImage = [selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    /** 创建tabBarItem */
    childVC.tabBarItem.image = orignalImage;
    childVC.tabBarItem.selectedImage = selectedImage;
    childVC.tabBarItem.title = title;
    
//    UIFont *teleF = [UIFont ];
    [childVC.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName :  [UIColor grayColor]} forState:UIControlStateNormal];
    
    [childVC.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName : kRGBAColor(221, 169, 72, 1)} forState:UIControlStateSelected];
    
    BaseNavigationController *baseNav = [[BaseNavigationController alloc]initWithRootViewController:childVC];
    [self addChildViewController:baseNav];
}

@end
