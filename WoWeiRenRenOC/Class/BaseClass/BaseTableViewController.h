//
//  BaseTableViewController.h
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/14.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BaseTableViewController : BaseViewController <UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource>

@property (strong, nonatomic) NSMutableArray *dataArray;
@property (assign, nonatomic) __block NSInteger page;

@property (assign, nonatomic) NSInteger defaultStartPage;

// tableView & collectionView
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) UICollectionView *collectionView;

@property (copy, nonatomic) NSString *emptyString;

- (void)setRefresh;
- (void)setRefreshNoEmpty;

- (void)endRefresh;
- (void)endRefreshNoMoreData;

- (void)setEmptyView;
- (void)needRefresh;

// 快速创建tableView
- (void)creatTableViewStyle:(UITableViewStyle)style cellIdentity:(NSString *)identity;
- (void)creatTableViewWithCellIdentity:(NSString *)identity;

// 快速创建collectionView
- (void)creatCollectionViewLayout:(UICollectionViewFlowLayout *)layout cellIdentity:(NSString *)identity;
- (void)creatCollectionViewWithCellIdentity:(NSString *)identity;

@end

NS_ASSUME_NONNULL_END
