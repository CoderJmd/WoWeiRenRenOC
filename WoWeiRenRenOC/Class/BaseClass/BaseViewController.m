//
//  BaseViewController.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/13.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseViewController.h"
#import "LoginVC.h"

@interface BaseViewController ()<UINavigationControllerDelegate,UIGestureRecognizerDelegate>
{
    UIPercentDrivenInteractiveTransition *_interactiveTransition;
}

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [self.view setBackgroundColor:VCBgcolor];
    
    [self setMyNaviBar];
    
    id target = self.navigationController.interactivePopGestureRecognizer.delegate;

    // handleNavigationTransition:为系统私有API,即系统自带侧滑手势的回调方法，我们在自己的手势上直接用它的回调方法
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:target action:@selector(handleNavigationTransition:)];
    panGesture.delegate = self; // 设置手势代理，拦截手势触发
    [self.view addGestureRecognizer:panGesture];

    // 一定要禁止系统自带的滑动手势
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
}


// 什么时候调用，每次触发手势之前都会询问下代理方法，是否触发
// 作用：拦截手势触发
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    // 当当前控制器是根控制器时，不可以侧滑返回，所以不能使其触发手势
    
    if (self.navigationController.childViewControllers.count == 1) {
        return NO;
    }
    
    CGPoint touchPoint = [gestureRecognizer locationInView:[[UIApplication sharedApplication] keyWindow]];
    
    if (touchPoint.x > 40) {
        return NO;
    }
    
    return YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [CommonTTool dismisKeyBoard];
    
    
    
    
    SVPDismiss
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [CommonTTool dismisKeyBoard];
}

- (void)setMyNaviBar {
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName: kPMFont(18),NSForegroundColorAttributeName:BlackColor}];
    
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    
    if (self.navigationController.childViewControllers.count > 1) {
        self.leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.leftButton.frame = CGRectMake(0, 0, 49, 40);
        [self.leftButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        [self.leftButton setImage:[UIImage imageNamed:@"fanhui"] forState:UIControlStateNormal];
        [self.leftButton setTitleColor:BlueColor forState:UIControlStateNormal];

        [self.leftButton.titleLabel setFont:kPRFont(17)];
        // 调整间距
        self.leftButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
//        [self.leftButton layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleLeft imageTitleSpace:4.0f];

        UIBarButtonItem * item = [[UIBarButtonItem alloc]initWithCustomView:self.leftButton];
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSpacer.width = 15;
        self.navigationItem.leftBarButtonItems = @[negativeSpacer, item];
    }
}

- (void)back {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)requestData
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}






@end
