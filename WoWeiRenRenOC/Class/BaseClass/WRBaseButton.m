//
//  WRBaseButton.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/13.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "WRBaseButton.h"

@implementation WRBaseButton

- (void)awakeFromNib {
    
    [super awakeFromNib];
   self.layer.cornerRadius = 6.0;
    self.layer.masksToBounds = YES;
    CAGradientLayer *gl = [CAGradientLayer layer];
    gl.frame = CGRectMake(0, 0, kScreenW - 30, 40);
    gl.startPoint = CGPointMake(0.22, 0.36);
    gl.endPoint = CGPointMake(0.85, 0.72);
    gl.colors = @[(__bridge id)[UIColor colorWithRed:221/255.0 green:169/255.0 blue:72/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:189/255.0 green:135/255.0 blue:35/255.0 alpha:1.0].CGColor];
    gl.locations = @[@(0), @(1.0f)];

    [self.layer insertSublayer:gl atIndex:0];
    self.clipsToBounds = YES;
}

- (void)setUi {
    self.layer.cornerRadius = 6.0;
    
    self.layer.masksToBounds = YES;
    CAGradientLayer *gl = [CAGradientLayer layer];
    gl.frame = CGRectMake(0, 0, kScreenW - 30, 40);
    gl.startPoint = CGPointMake(0.22, 0.36);
    gl.endPoint = CGPointMake(0.85, 0.72);
    gl.colors = @[(__bridge id)[UIColor colorWithRed:221/255.0 green:169/255.0 blue:72/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:189/255.0 green:135/255.0 blue:35/255.0 alpha:1.0].CGColor];
    gl.locations = @[@(0), @(1.0f)];

    [self.layer insertSublayer:gl atIndex:0];
}

@end
