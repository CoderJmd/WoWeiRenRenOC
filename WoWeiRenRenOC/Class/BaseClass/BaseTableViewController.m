//
//  BaseTableViewController.m
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/14.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseTableViewController.h"
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>

@interface BaseTableViewController () <DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>

@end

@implementation BaseTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.emptyString = @"暂无数据";
}

// 快速创建TableView
- (void)creatTableViewStyle:(UITableViewStyle)style cellIdentity:(NSString *)identity
{
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, NavBarHeight, kScreenW, kScreenH-NavBarHeight) style:style];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    self.tableView.tableFooterView = [UITableView new];
    [self.view addSubview:self.tableView];
    
    if (![CommonTTool isBlankString:identity]) {
        [self.tableView registerNib:[UINib nibWithNibName:identity bundle:nil] forCellReuseIdentifier:identity];
    }
}

- (void)creatTableViewWithCellIdentity:(NSString *)identity
{
    [self creatTableViewStyle:UITableViewStylePlain cellIdentity:identity];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [[UITableViewCell alloc] init];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

// 快速创建collectionView
- (void)creatCollectionViewWithCellIdentity:(NSString *)identity
{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    layout.minimumInteritemSpacing = 0.0;
    layout.minimumLineSpacing = 0.0;
    
    [self creatCollectionViewLayout:layout cellIdentity:identity];
}

- (void)creatCollectionViewLayout:(UICollectionViewFlowLayout *)layout cellIdentity:(NSString *)identity
{
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, NavBarHeight, kScreenW, kScreenH-NavBarHeight) collectionViewLayout:layout];
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    [self.view addSubview:self.collectionView];
    
    [self.collectionView registerNib:[UINib nibWithNibName:identity bundle:nil] forCellWithReuseIdentifier:identity];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [[UICollectionViewCell alloc] init];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
}

// 设置刷新
- (void)setRefreshNoEmpty
{
    self.page = self.defaultStartPage;
    
    // 下拉刷新
    __weak typeof(self) weakSelf = self;
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.page = weakSelf.defaultStartPage;
        [weakSelf requestData];
    }];

    // 上拉加载
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(requestData)];
    [footer setTitle:@"" forState:MJRefreshStateNoMoreData];
//    footer.automaticallyHidden = YES;

    if (self.tableView) {
        self.tableView.mj_header = header;
        self.tableView.mj_footer = footer;
    }
    if (self.collectionView) {
        self.collectionView.mj_header = header;
        self.collectionView.mj_footer = footer;
    }
}

- (void)setRefresh
{
    [self setRefreshNoEmpty];
    // 无数据页面
    [self setEmptyView];
}

- (void)endRefresh
{
    if (self.tableView) {
        [self.tableView.mj_footer resetNoMoreData];
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    }
    if (self.collectionView) {
        [self.collectionView.mj_footer resetNoMoreData];
        [self.collectionView.mj_header endRefreshing];
        [self.collectionView.mj_footer endRefreshing];
    }
}

- (void)endRefreshNoMoreData
{
    if (self.tableView) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }
    if (self.collectionView) {
        [self.collectionView.mj_header endRefreshing];
        [self.collectionView.mj_footer endRefreshingWithNoMoreData];
    }
}

- (void)needRefresh
{
    if (self.tableView) {
        [self.tableView.mj_header beginRefreshing];
    }
    if (self.collectionView) {
        [self.collectionView.mj_header beginRefreshing];
    }
}

// 设置空数据页面
- (void)setEmptyView
{
    if (self.tableView) {
        self.tableView.emptyDataSetSource = self;
        self.tableView.emptyDataSetDelegate = self;
    }
    if (self.collectionView) {
        self.collectionView.emptyDataSetSource = self;
        self.collectionView.emptyDataSetDelegate = self;
    }
}
- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView
{
    UIImage *img = [UIImage imageNamed:@"wuneirong"];
    return img;
}

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = self.emptyString;
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:18.0f],
                                 NSForegroundColorAttributeName: FloatColor(0x999999)};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView
{
    CGFloat h = 0.0;
    if (self.tableView) {
        h = self.tableView.tableHeaderView.frame.size.height;
    }
    return h*0.5;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView
{
    return YES;
}

- (BOOL)emptyDataSetShouldBeForcedToDisplay:(UIScrollView *)scrollView
{
    return NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
