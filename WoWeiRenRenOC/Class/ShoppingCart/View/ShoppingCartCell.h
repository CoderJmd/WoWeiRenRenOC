//
//  ShoppingCartCell.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/13.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShoppingCartModel.h"


NS_ASSUME_NONNULL_BEGIN

typedef void(^RefreshPric)(void);
@interface ShoppingCartCell : UITableViewCell
@property (strong, nonatomic)ShoppingCartGoodsMode *model;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIButton *selectorBtn;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIImageView *reduceImg;
@property (weak, nonatomic) IBOutlet UITextField *countTF;
@property (weak, nonatomic) IBOutlet UIImageView *addImg;
@property (weak, nonatomic) IBOutlet UIButton *addBtn;
@property (weak, nonatomic) IBOutlet UIButton *reduceBtn;
@property (copy, nonatomic) RefreshPric refreshPric;
@property (strong, nonatomic)NSString *business_id;

@property (weak, nonatomic) IBOutlet UILabel *goodsLabel;

@property (weak, nonatomic) IBOutlet UIImageView *goodsTypeImgView;
@property (weak, nonatomic) IBOutlet UILabel *shixiaoLabel;

@property (weak, nonatomic) IBOutlet UIView *shixiaoBgView;
@property (weak, nonatomic) IBOutlet UIView *shixiaoBgTwoView;

@end

NS_ASSUME_NONNULL_END
