//
//  ShoppingCartHeadView.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/13.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "ShoppingCartHeadView.h"

@implementation ShoppingCartHeadView

- (void)awakeFromNib {
    
    [super awakeFromNib];
    CAGradientLayer *gl = [CAGradientLayer layer];
    gl.frame = CGRectMake(0,0,54,24);
    gl.startPoint = CGPointMake(0.22, 0.36);
    gl.endPoint = CGPointMake(0.85, 0.72);
    gl.colors = @[(__bridge id)[UIColor colorWithRed:221/255.0 green:169/255.0 blue:72/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:189/255.0 green:135/255.0 blue:35/255.0 alpha:1.0].CGColor];
    gl.locations = @[@(0), @(1.0f)];
    self.ManageBtn.layer.cornerRadius = 12;
    [self.ManageBtn.layer insertSublayer:gl atIndex:0];
    self.ManageBtn.clipsToBounds = YES;
    
    
}

@end
