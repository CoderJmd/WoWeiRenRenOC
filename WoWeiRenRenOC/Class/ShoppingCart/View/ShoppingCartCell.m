//
//  ShoppingCartCell.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/13.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "ShoppingCartCell.h"
@interface ShoppingCartCell()


@end;
@implementation ShoppingCartCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.addBtn addTarget:self action:@selector(addCount) forControlEvents:UIControlEventTouchUpInside];
    [self.reduceBtn addTarget:self action:@selector(subtractionCount) forControlEvents:UIControlEventTouchUpInside];
    [self.selectorBtn addTarget:self action:@selector(selectorBtnClick) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - 添加
- (void)addCount {
    
   
    int count = self.model.num;
    count +=1;
    NSDictionary *para = @{@"type":@(self.model.sku_type),
                           @"sku_id":self.model.sku_id,
                           @"num":@(count),
                           @"business_id":self.business_id,
                           @"push_res":@"2",
    };
    
    SVPShowInternetWaiting;
    [NetRequestTool postUrl:@"shoppingCar/plusOrReduceShoppingCar" parameters:para success:^(id responseObject) {
        self.model.num++;
        self.reduceImg.image = [UIImage imageNamed:@"jian1"];
        self.countTF.text = [NSString stringWithFormat:@"%.0ld",(long)self.model.num];
        if (self.refreshPric) {
            self.refreshPric();
        }
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        SVPDismiss
    }];
    
    
   
    
    
}

#pragma mark - 减少
- (void)subtractionCount {
    
    
    if (self.model.num == 1) {
        SVP(@"购买数量最少为1");
        return;
    }
    int count = self.model.num;
    count -=1;
    NSDictionary *para = @{@"type":@(self.model.sku_type),
                           @"sku_id":self.model.sku_id,
                           @"num":@(count),
                           @"business_id":self.business_id,
                           @"push_res":@"2",
    };
    
    SVPShowInternetWaiting;
    [NetRequestTool postUrl:@"shoppingCar/plusOrReduceShoppingCar" parameters:para success:^(id responseObject) {
        self.model.num--;
        self.countTF.text = [NSString stringWithFormat:@"%.0ld",(long)self.model.num];
        if (self.model.num == 1) {
            self.reduceImg.image = [UIImage imageNamed:@"jian"];
        }
        if (self.refreshPric) {
            self.refreshPric();
        }
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        SVPDismiss
    }];
    

}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(ShoppingCartGoodsMode *)model {
    
    _model = model;
    self.nameLabel.textColor = [UIColor blackColor];
    self.nameLabel.text = model.sku_name;
    self.contentLabel.text = [CommonTTool getGoodsSpecifications:model.gg_info];
    
    self.priceLabel.text = [CommonTTool getPrice:model.vip_price];
    if (model.is_yk == 2) {
        self.priceLabel.text = [CommonTTool getPrice:model.yk_price];
    }
    if (model.sku_type == 2 || model.sku_type == 9) {
        self.priceLabel.text = [CommonTTool getPrice:model.buy_price];
    }
    
    self.countTF.text = [NSString stringWithFormat:@"%.0ld",(long)model.num];
    if (model.num>1) {
        self.reduceImg.image = [UIImage imageNamed:@"jian1"];
    }else {
        self.reduceImg.image = [UIImage imageNamed:@"jian"];
    }
    SD_SETIMAGE_PlaceHolder(self.imgView, model.img_path, @"default_image_square");
    if (self.model.isSelector) {
        self.selectorBtn.selected = YES;
    }else {
        self.selectorBtn.selected = NO;
    }
    self.goodsTypeImgView.hidden = YES;
    self.goodsLabel.hidden = YES;
    if (model.is_yk == 2) {
        self.goodsTypeImgView.hidden = NO;
        self.goodsLabel.hidden = NO;
        self.goodsTypeImgView.image = [UIImage imageNamed:@"yinkazhuangshu"];
        self.goodsLabel.text = @"银卡专属";
        
    }
    if (model.sku_type == 2 || model.sku_type == 9) {
        self.goodsTypeImgView.hidden = NO;
        self.goodsLabel.hidden = NO;
        self.goodsTypeImgView.image = [UIImage imageNamed:@"pintuanshangping"];
        self.goodsLabel.text = @"团购商品";
    }
    self.shixiaoLabel.hidden = YES;
    self.shixiaoBgView.hidden = YES;

    if (model.state != 1) {
        [self setStateUi];
    }
    if (model.sku_type == 2 || model.sku_type == 9) {
        NSString *deadlineStr = model.end_time;
        // 当前时间的时间戳
        NSString *nowStr = [CommonTTool getCurrentTimeyyyymmdd];
        // 计算时间差值
        NSInteger secondsCountDown = [CommonTTool getDateDifferenceWithNowDateStr:nowStr deadlineStr:deadlineStr];
        if (secondsCountDown <= 0 ) {
            [self setStateUi];
        }
        if (model.tg_order_num > 100) {
            [self setStateUi];
        }

    }

}

- (void)setStateUi {
    
//    self.shixiaoLabel.hidden = NO;
//    self.shixiaoLabel.layer.cornerRadius = 8;
//    self.shixiaoLabel.clipsToBounds = YES;
//    self.nameLabel.textColor = [UIColor grayColor];
    self.shixiaoBgView.hidden = NO;
    self.shixiaoBgView.backgroundColor =  [UIColor colorWithWhite:0.2f alpha: 0.5];
    self.shixiaoBgTwoView.backgroundColor =  [UIColor colorWithWhite:0.1f alpha: 0.8];
    self.shixiaoBgTwoView.layer.cornerRadius = 30;
    self.shixiaoBgTwoView.clipsToBounds = YES;
}


- (void)selectorBtnClick {
    
    self.selectorBtn.selected = !self.selectorBtn.selected;
    self.model.isSelector = self.selectorBtn.selected;
    if (self.refreshPric) {
        self.refreshPric();
    }
}



@end
