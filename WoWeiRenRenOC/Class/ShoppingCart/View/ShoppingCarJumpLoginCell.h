//
//  ShoppingCarJumpLoginCell.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/7/2.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ShoppingCarJumpLoginCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *jumpLoginBtn;

@end

NS_ASSUME_NONNULL_END
