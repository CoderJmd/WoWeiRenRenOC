//
//  ShoppingCartModel.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/20.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseModel.h"


NS_ASSUME_NONNULL_BEGIN

@interface ShoppingCartModel : BaseModel

@property (nonatomic,strong) NSString *business_name;
@property (nonatomic,assign) NSInteger sh_state;
@property (nonatomic,assign) NSInteger zxtg_state;

@property (nonatomic,strong) NSArray *shopping_car_list;
@property (nonatomic,assign) NSInteger state;
@property (nonatomic,strong) NSString *business_id;
@property (assign, nonatomic)BOOL isSelector;
@property (strong, nonatomic)NSString *remarks;
@end

@interface ShoppingCartGoodsMode : BaseModel
@property (nonatomic,strong) NSString *att_title;
@property (nonatomic,strong) NSString *pt_ct_rs;
@property (nonatomic,strong) NSString *pt_start_time;
@property (nonatomic,strong) NSString *shopping_car_id;
@property (nonatomic,strong) NSString *pt_end_time;
@property (nonatomic,strong) NSString *sku_name;
@property (nonatomic,assign) int stock;
@property (nonatomic,assign) float pt_price;
@property (nonatomic,strong) NSString *end_time;
@property (nonatomic,strong) NSString *sku_id;
@property (nonatomic,strong) NSString *start_time;
@property (nonatomic,strong) NSString *img_path;
@property (nonatomic,assign) float freight_price;
@property (nonatomic,assign) int tg_order_num;
@property (nonatomic,assign) float market_price;
@property (nonatomic,assign) float yk_price;
@property (nonatomic,assign) float buy_price;
@property (nonatomic,assign) float vip_price;
@property (nonatomic,strong) NSString *gg_info;
@property (nonatomic,strong) NSString *sku_code;
@property (strong, nonatomic)NSString *business_id;

@property (nonatomic,assign) NSInteger sku_type;
@property (nonatomic,assign) NSInteger is_pt;
@property (nonatomic,assign) NSInteger is_free_shipping;
@property (nonatomic,assign) NSInteger state;
@property (nonatomic,assign) NSInteger is_yk;
@property (nonatomic,assign) NSInteger num;
@property (assign, nonatomic)BOOL isSelector;
@property (assign, nonatomic)int is_ht; //是否海淘 1不是 2是

@end



NS_ASSUME_NONNULL_END
