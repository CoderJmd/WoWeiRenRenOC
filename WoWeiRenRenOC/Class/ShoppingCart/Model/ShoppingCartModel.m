//
//  ShoppingCartModel.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/20.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "ShoppingCartModel.h"


@implementation ShoppingCartModel

+ (NSDictionary *)objectClassInArray
{
    return @{@"shopping_car_list": [ShoppingCartGoodsMode class]};
}

@end

@implementation ShoppingCartGoodsMode


@end

