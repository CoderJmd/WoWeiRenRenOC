//
//  AddressSelectVC.h
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/19.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface AddressSelectVC : BaseTableViewController

@property (nonatomic, copy) void(^selectFinish)(NSString *address);

@end

NS_ASSUME_NONNULL_END
