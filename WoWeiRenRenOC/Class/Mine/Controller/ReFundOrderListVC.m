//
//  ReFundOrderListVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/9/1.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "ReFundOrderListVC.h"
#import "ShoppingCartHeadView.h"
#import "AbulkCell.h"
#import "ShoppingCartGroupHeadView.h"
#import "ShoppingCartCell.h"
#import "ShoppingCartBottomView.h"
#import "UserOrderChildGroupView.h"
#import "UserOrderChildCell.h"
#import "UserOrderDetailsVC.h"
#import "UserShoppingOrderModel.h"
#import "UserOrderChildFooterView.h"
#import "PagingViewController.h"
#import "ReFundOrderListFooterView.h"



extern UserModel *userModel;
@interface ReFundOrderListVC ()

@end

@implementation ReFundOrderListVC



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.defaultStartPage = 1;
    
    self.title = @"退款/售后";
    [self creatTableViewStyle:UITableViewStyleGrouped cellIdentity:@"UserOrderChildCell"];
    self.tableView.backgroundColor = kRGBColor(246, 246, 246);
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.frame = CGRectMake(0, NavBarHeight, kScreenW, kScreenH-TabBarHeight+StatusViewHeight-NavBarHeight);
    [self setRefresh];
    [self requestData];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateRequestData) name:kUserOrderTableUpDate object:nil];
    
}



- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];

    
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
}

#pragma mark - 刷新
- (void)updateRequestData {
    
    self.page = 1;
    [self requestData];
    
}

- (void)requestData
{
    SVPShowInternetWaiting
    NSMutableDictionary *para = [NSMutableDictionary dictionary];
    [para setObject:@(self.page) forKey:@"page"];
    [para setObject:@(10) forKey:@"limit"];
    
    [para setObject:@"2" forKey:@"is_td"];
    
    WeakSelf(ws)
    [NetRequestTool postUrl:@"skuOrder/querySkuOrderList" parameters:para success:^(id responseObject) {
        NSMutableArray *arr = [UserShoppingOrderModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]];
        if (self.page==self.defaultStartPage) {
            ws.dataArray = arr;
        }
        else {
            [ws.dataArray addObjectsFromArray:arr];
        }
        [ws.tableView reloadData];
        
        // 控制页数
        NSDictionary *page = [responseObject objectForKey:@"page"];
        if ([page isKindOfClass:NSDictionary.class]) {
            NSInteger totalP = [page[@"total_page"] integerValue];
            if (totalP > self.page) {
                self.page++;
                [self endRefresh];
            }
            else {
                [self endRefreshNoMoreData];
            }
        }
        else {
            [self endRefreshNoMoreData];
        }
    } failure:^(NSInteger statusCode) {
        [ws endRefreshNoMoreData];
    } finish:^{
        SVPDismiss
        [self endRefresh];
    }];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserOrderChildCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserOrderChildCell"];
    UserShoppingOrderModel *model = self.dataArray[indexPath.section];
    cell.model = model.sku_order_detail_list[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (cell.model.is_td == 5 || cell.model.is_td == 2 || cell.model.is_td == 6) {

        
        cell.reFundLabel.hidden = NO;
        cell.reFundCoutnLabel.hidden = NO;
        tableView.rowHeight = 150;
    }else {
        
        cell.reFundLabel.hidden = YES;
        cell.reFundCoutnLabel.hidden = YES;
        tableView.rowHeight = 110;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserShoppingOrderModel *model = self.dataArray[indexPath.section];
    UserOrderDetailsVC *vc = [[UserOrderDetailsVC alloc]init];
    vc.order_code = model.order_code;
    [self.navigationController pushViewController:vc animated:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UserOrderChildGroupView *view = [[NSBundle mainBundle] loadNibNamed:@"UserOrderChildGroupView" owner:self options:nil].lastObject;
    UserShoppingOrderModel *model = self.dataArray[section];
    
    view.model = model;
    view.jumpBtn.tag = section;
    [view.jumpBtn addTarget:self action:@selector(headVieTapClick:) forControlEvents:UIControlEventTouchUpInside];
    view.tag = section;
    
    return view;
}

- (void)headVieTapClick:(UIButton *)view {
    ShoppingCartModel *model = self.dataArray[view.tag];
    PagingViewController *vc= [[PagingViewController alloc]init];
    vc.business_id = model.business_id;
    [self.navigationController pushViewController:vc animated:YES];
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 60.0f;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    ReFundOrderListFooterView *view = [[NSBundle mainBundle] loadNibNamed:@"ReFundOrderListFooterView" owner:self options:nil].lastObject;
    
    UserShoppingOrderModel *model = self.dataArray[section];
    view.model = model;
    
    
    return view;;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 75;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section  {
    UserShoppingOrderModel *model =  self.dataArray[section];
    
    return model.sku_order_detail_list.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return self.dataArray.count;
}
    


@end
