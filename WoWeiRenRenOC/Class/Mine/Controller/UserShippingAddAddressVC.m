//
//  UserShippingAddAddressVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/12.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "UserShippingAddAddressVC.h"
#import "UserShippingAddAddressCell.h"

@interface UserShippingAddAddressVC ()
@property (nonatomic,strong) UserShippingAddAddressCell *cell;
@property (strong, nonatomic)UIButton *addBtn;
@end

@implementation UserShippingAddAddressVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.defaultStartPage = 1;
    
    self.title = @"新增地址";
    if (self.model != nil) {
        self.title = @"编辑地址";
    }
    [self creatTableViewWithCellIdentity:@"UserShippingAddAddressCell"];

    self.view.backgroundColor = WhiteColor;
    self.tableView.backgroundColor = WhiteColor;
    self.tableView.rowHeight = kScreenH-NavBarHeight -NeedStatusViewHeight-50;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.frame = CGRectMake(0, NavBarHeight, kScreenW, kScreenH-NavBarHeight -NeedStatusViewHeight-50);
    
    
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(15, KSCREEN_HEIGHT - 50 - NeedStatusViewHeight, KSCREEN_WIDTH - 30, 40)];
    button.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 16];
    [button setTitle:@"保存" forState:UIControlStateNormal];
    [button setTitleColor:kRGBColor(255, 246, 203) forState:UIControlStateNormal];
    CAGradientLayer *gl = [CAGradientLayer layer];
    gl.frame = CGRectMake(0,0,KSCREEN_WIDTH - 30,40);
    gl.startPoint = CGPointMake(0.22, 0.36);
    gl.endPoint = CGPointMake(0.85, 0.72);
    gl.colors = @[(__bridge id)[UIColor colorWithRed:221/255.0 green:169/255.0 blue:72/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:189/255.0 green:135/255.0 blue:35/255.0 alpha:1.0].CGColor];
    gl.locations = @[@(0), @(1.0f)];
    button.layer.cornerRadius = 4;
    button.clipsToBounds = YES;
    [button.layer insertSublayer:gl atIndex:0];
    [self.view addSubview:button];
    [button  addTarget:self action:@selector(saveAddressClick) forControlEvents:UIControlEventTouchUpInside];
    self.addBtn = button;
}

- (void)saveAddressClick {
    
    if (self.model != nil) {
        [self changeAddress];
        return;
    }
    
    NSString *name = self.cell.nameTF.text;
    NSString *phone = self.cell.phoneTF.text;
    NSString *city = self.cell.selectorCityTF.text;
    NSString *address = self.cell.detailsAddressTF.text;
    NSString *def = @"1";
    if ([CommonTTool isStrNull:name]) {
        SVP(@"请输入您的姓名")
        return;
    }
    if ([CommonTTool isStrNull:phone]) {
        SVP(@"请输入您的手机号")
        return;
    }
    if ([CommonTTool isStrNull:city]) {
        SVP(@"请选择您的省市区")
        return;
    }
    if ([CommonTTool isStrNull:address]) {
        SVP(@"请输入您的详细地址")
        return;
    }
    if (!self.cell.defaultBtn.selected) {
        def = @"2";
    }
    self.addBtn.userInteractionEnabled = NO;
    NSDictionary *para = @{@"name":name,
                           @"phone":phone,
                           @"province":self.cell.province,
                           @"city":self.cell.city,
                           @"area":self.cell.area,
                           @"address":address,
                           @"def":def,
    };
    SVPShowInternetWaiting;
    [NetRequestTool postUrl:@"address/saveAddress" parameters:para success:^(id responseObject) {
        
        SVP(@"保存地址成功");
        [[NSNotificationCenter defaultCenter] postNotificationName:kUserAddShopingAddressUpDate object:nil];
        [self performSelector:@selector(saveSuucess) withObject:self afterDelay:2];
        
    } failure:^(NSInteger statusCode) {
        SVPDismiss;
    } finish:^{
       self.addBtn.userInteractionEnabled = YES;
    }];
    
}


- (void)changeAddress {
    
    NSString *name = self.cell.nameTF.text;
    NSString *phone = self.cell.phoneTF.text;
    NSString *city = self.cell.selectorCityTF.text;
    NSString *address = self.cell.detailsAddressTF.text;
    NSString *def = @"1";
    if ([CommonTTool isStrNull:name]) {
       SVP(@"请输入您的姓名")
       return;
    }
    if ([CommonTTool isStrNull:phone]) {
       SVP(@"请输入您的手机号")
       return;
    }
    if ([CommonTTool isStrNull:city]) {
       SVP(@"请选择您的省市区")
       return;
    }
    if ([CommonTTool isStrNull:address]) {
       SVP(@"请输入您的详细地址")
       return;
    }
    if (!self.cell.defaultBtn.selected) {
       def = @"2";
    }
    

    NSDictionary *para = @{@"name":name,
                          @"phone":phone,
                          @"province":self.cell.province,
                          @"city":self.cell.city,
                          @"area":self.cell.area,
                          @"address":address,
                          @"def":def,
                          @"address_id":self.model.address_id
    };
    SVPShowInternetWaiting;
    [NetRequestTool postUrl:@"address/updateAddress" parameters:para success:^(id responseObject) {
       
       SVP(@"修改地址成功");
       [self performSelector:@selector(saveSuucess) withObject:self afterDelay:1.5];
       
    } failure:^(NSInteger statusCode) {
       SVPDismiss;
    } finish:^{
      
    }];
    
}


- (void)saveSuucess {
    [[NSNotificationCenter defaultCenter] postNotificationName:kUserAddShopingAddressUpDate object:nil];
    [self.navigationController popViewControllerAnimated:YES];
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UserShippingAddAddressCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserShippingAddAddressCell"];
    
    if (self.model != nil) {
        cell.model = self.model;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    self.cell = cell;
    return cell;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}


@end
