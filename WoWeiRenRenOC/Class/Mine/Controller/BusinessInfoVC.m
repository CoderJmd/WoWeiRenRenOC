//
//  BusinessInfoVC.m
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/18.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BusinessInfoVC.h"
#import "BusinessInfoCell.h"
#import "BusinessInfoImageCell.h"
#import "UserMerchantsPromoteDetailsVC.h"

@interface BusinessInfoVC ()

@end

@implementation BusinessInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"我的信息";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(businessInfoUpDate) name:@"BusinessInfoUpDate" object:nil];
    
    NSDictionary *dict = [[NSUserDefaults standardUserDefaults] objectForKey:kUserMerchantsInfo];
    self.dataArray = @[@{@"title":@"商家名称", @"content":dict[@"business_name"]}, @{@"title":@"商家电话", @"content":dict[@"busineee_tel"]}, @{@"title":@"商家类型", @"content":dict[@"description"]}, @{@"title":@"商家折扣", @"content":dict[@"discount_name"]}, @{@"title":@"详细地址", @"content":dict[@"address"]}, @{@"title":@"联系人", @"content":dict[@"contacts"]}, @{@"title":@"手机号", @"content":dict[@"phone"]}, @{@"title":@"营业执照号", @"content":dict[@"business_license_no"]}, @{@"title":@"营业执照", @"content":dict[@"business_license_path"]}, @{@"title":@"门头照", @"content":dict[@"head_path"]}].mutableCopy;
    
    [self creatTableViewWithCellIdentity:@"BusinessInfoCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"BusinessInfoImageCell" bundle:nil] forCellReuseIdentifier:@"BusinessInfoImageCell"];
    
//    WRBaseButton *button = [[WRBaseButton alloc]init];
//    button.frame = CGRectMake(15, kScreenH - 55, kScreenW - 30, 40);
//    [button setTitle:@"年度审核" forState:UIControlStateNormal];
//    button.titleLabel.font = [UIFont systemFontOfSize:14];
//    [button setUi];
//    [self.view addSubview:button];
//    [button setBackgroundColor:kRGBColor(221, 169, 72)];
//    [button addTarget:self action:@selector(updateBusinessInfo) forControlEvents:UIControlEventTouchUpInside];
    
}


- (void)businessInfoUpDate {
    
    NSDictionary *dict = [[NSUserDefaults standardUserDefaults] objectForKey:kUserMerchantsInfo];
    NSString *business_id = dict[@"business_id"];
    [NetRequestTool postUrl:@"business/getBusinessInfo" parameters:@{@"business_id":business_id} success:^(id responseObject) {
       
       NSDictionary *dict = [responseObject objectForKey:@"object"];
       // 去除空值后 直接存字典
       [[NSUserDefaults standardUserDefaults] setObject:[dict deleteNull] forKey:kUserMerchantsInfo];
        self.dataArray = @[@{@"title":@"商家名称", @"content":dict[@"business_name"]}, @{@"title":@"商家电话", @"content":dict[@"busineee_tel"]}, @{@"title":@"商家类型", @"content":dict[@"description"]}, @{@"title":@"商家折扣", @"content":dict[@"discount_name"]}, @{@"title":@"详细地址", @"content":dict[@"address"]}, @{@"title":@"联系人", @"content":dict[@"contacts"]}, @{@"title":@"手机号", @"content":dict[@"phone"]}, @{@"title":@"营业执照号", @"content":dict[@"business_license_no"]}, @{@"title":@"营业执照", @"content":dict[@"business_license_path"]}, @{@"title":@"门头照", @"content":dict[@"head_path"]}].mutableCopy;
        [self.tableView reloadData];
        
    } failure:^(NSInteger statusCode) {
    //        [ws endRefreshNoMoreData];
    } finish:^{
        SVPDismiss
    }];
    
    
    
    
}
    


- (void)updateBusinessInfo {
    
    
    NSDate *datenow = [NSDate date];//现在时间,你可以输出来看下是什么格式

    NSInteger currentTime = [datenow timeIntervalSince1970];
    NSDictionary *dict = [[NSUserDefaults standardUserDefaults] objectForKey:kUserMerchantsInfo];
    NSString *oldTImeStr = dict[@"effective_time"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];// 创建一个时间格式化对象
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"]; //设定时间的格式
    NSDate *tempDate = [dateFormatter dateFromString:oldTImeStr];//将字符串转换为时间对象
    NSInteger oldTime = [tempDate timeIntervalSince1970];
    
    NSInteger time = oldTime - currentTime;
    
    if (time > 60*60*24*30) {
        
        SVP(@"请在商家有效期到期30天进行年度审核");
       
    }else {
        
        UserMerchantsPromoteDetailsVC *vc = [[UserMerchantsPromoteDetailsVC alloc]init];
        vc.changeModel = [BusinessModel mj_objectWithKeyValues:dict];
        [self.navigationController pushViewController:vc animated:YES];
        
    }
    
    

    
}
    

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = self.dataArray[indexPath.row];
    if (indexPath.row<=7) {
        BusinessInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BusinessInfoCell"];
        cell.titleLab.text = dict[@"title"];
        cell.contentLab.text = [NSString stringWithFormat:@"%@", dict[@"content"]];
        return cell;
    }
    else {
        BusinessInfoImageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BusinessInfoImageCell"];
        cell.titleLab.text = dict[@"title"];

        NSString *imgURL = dict[@"content"];
        UIImage *cachedImage = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:imgURL];
        if (cachedImage ) {
            cell.imgView.image = cachedImage;
        }
        else {
            // 利用 SDWebImage 框架提供的功能下载图片
            [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:imgURL] options:SDWebImageDownloaderUseNSURLCache progress:nil completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
                [[SDImageCache sharedImageCache] storeImage:image forKey:imgURL toDisk:YES completion:nil];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.tableView reloadData];
                });
            }];
        }
      
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==8 || indexPath.row==9) {
        NSDictionary *dict = self.dataArray[indexPath.row];
        UIImage *image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey: dict[@"content"]];
        if (image) {
            CGFloat imageH = (image.size.height * (kScreenW-30)) / image.size.width;
            return imageH + 65;
        }
        else {
            return 500.0;
        }
    }
    else {
        return 50.0;
    }
}

- (void)dealloc
{
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
