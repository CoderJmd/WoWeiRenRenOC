//
//  UserOrderMainVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/13.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "UserOrderMainVC.h"
#import "CMPageTitleView.h"
#import "UserOrderChildVC.h"

@interface UserOrderMainVC ()<CMPageTitleViewDelegate>
/**视图控制器数组*/
@property (nonatomic,copy) NSArray *childControllers;
/**default notes*/
@property (nonatomic,strong) CMPageTitleView *pageTitleView;

@end

@implementation UserOrderMainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"我的订单";
    
    
    
    
    
    CMPageTitleConfig *config = [CMPageTitleConfig defaultConfig];  
    config.cm_titleHeight = 54;
    config.cm_childControllers = self.childControllers;
    config.cm_gradientStyle = CMTitleColorGradientStyle_None;
    config.cm_switchMode = CMPageTitleSwitchMode_Underline;
    config.cm_font = [UIFont fontWithName:@"PingFangSC-Medium" size: 12];
    config.cm_selectedColor = [UIColor blackColor];
    config.cm_underlineWidth = 24;
    config.cm_underlineHeight = 3;
    [self.childControllers setValue:@(config.cm_seperateLineHeight+config.cm_titleHeight) forKey:@"topInset"];
    self.pageTitleView.cm_config = config;
    self.pageTitleView.delegate = self;
    [self.view addSubview:self.pageTitleView];
}

- (CMPageTitleView *)pageTitleView {
    if (!_pageTitleView) {
        _pageTitleView = [[CMPageTitleView alloc] init];
        
        _pageTitleView.frame = CGRectMake(0, NavBarHeight, kScreenW, kScreenH-NavBarHeight-NeedStatusViewHeight);
        
    }
    
    return _pageTitleView;
    
}


- (NSArray *)childControllers {
    
    if (!_childControllers) {
        
        UserOrderChildVC *vc0 = [[UserOrderChildVC alloc]init];
        UserOrderChildVC *vc1 = [[UserOrderChildVC alloc]init];
        UserOrderChildVC *vc2 = [[UserOrderChildVC alloc]init];
        UserOrderChildVC *vc3 = [[UserOrderChildVC alloc]init];
        UserOrderChildVC *vc4 = [[UserOrderChildVC alloc]init];
        
        vc0.order_state = @"";
        vc1.order_state = @"1";
        vc2.order_state = @"3";
        vc3.order_state = @"4";
        vc4.order_state = @"5";
        
//        vc0.order_type = @"1";
//        vc1.order_type = @"1";
//        vc2.order_type = @"1";
//        vc3.order_type = @"1";
//        vc4.order_type = @"1";
        
        vc0.title = @"全部";
        vc1.title = @"待付款";
        vc2.title = @"待发货";
        vc3.title = @"待收货";
        vc4.title = @"已完成";
        
        _childControllers = @[vc0,vc1,vc2,vc3,vc4];
    }
    
    return _childControllers;
}




@end
