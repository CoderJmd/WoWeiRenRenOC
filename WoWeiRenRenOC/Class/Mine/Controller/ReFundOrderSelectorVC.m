//
//  ReFundOrderSelectorVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/9/1.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "ReFundOrderSelectorVC.h"
#import "ReFundOrderShowCell.h"
#import "UserOrderChildGroupView.h"

@interface ReFundOrderSelectorVC ()

@end

@implementation ReFundOrderSelectorVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"申请退货";
    [self creatTableViewStyle:UITableViewStyleGrouped cellIdentity:@"ReFundOrderShowCell"];

    self.tableView.backgroundColor = kRGBColor(246, 246, 246);
    self.tableView.rowHeight = 120;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    self.tableView.frame = CGRectMake(0, NavBarHeight, kScreenW, kScreenH-TabBarHeight+StatusViewHeight-NavBarHeight);
    
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(15, KSCREEN_HEIGHT - 50 - NeedStatusViewHeight, KSCREEN_WIDTH - 30, 40)];
       button.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 16];
       [button setTitle:@"申请退货" forState:UIControlStateNormal];
       [button setTitleColor:kRGBColor(255, 246, 203) forState:UIControlStateNormal];
       CAGradientLayer *gl = [CAGradientLayer layer];
       gl.frame = CGRectMake(0,0,KSCREEN_WIDTH - 30,40);
       gl.startPoint = CGPointMake(0.22, 0.36);
       gl.endPoint = CGPointMake(0.85, 0.72);
       gl.colors = @[(__bridge id)[UIColor colorWithRed:221/255.0 green:169/255.0 blue:72/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:189/255.0 green:135/255.0 blue:35/255.0 alpha:1.0].CGColor];
       gl.locations = @[@(0), @(1.0f)];
       button.layer.cornerRadius = 4;
       button.clipsToBounds = YES;
       [button.layer insertSublayer:gl atIndex:0];
       [self.view addSubview:button];
       [button  addTarget:self action:@selector(reFundButtonClick) forControlEvents:UIControlEventTouchUpInside];
}


- (void)reFundButtonClick {
    
    
    UIAlertController *alterController = [UIAlertController alertControllerWithTitle:nil message:@"是否确认申请退款？" preferredStyle:UIAlertControllerStyleAlert];
     [alterController addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
         
         
     }]];
     
     [alterController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
         NSMutableArray *goods_order_detail_list = [NSMutableArray array];

         for (MailGoodsDetailsModel *superModel in self.model.sku_order_detail_list) {
             if (superModel.isSelector) {
                 NSDictionary *dict = @{@"sku_id":superModel.sku_id,
                                        @"t_num":@(superModel.num)
                                        };
                 [goods_order_detail_list addObject:dict];
             }
            
         }
         if (goods_order_detail_list.count == 0) {
             SVP(@"请先选择要退货的商品");
             return ;
         }
         NSData *dataFriends = [NSJSONSerialization dataWithJSONObject:goods_order_detail_list options:NSJSONWritingPrettyPrinted error:nil];

         NSString *jsonString = [[NSString alloc] initWithData:dataFriends encoding:NSUTF8StringEncoding];
         SVPShowInternetWaiting;
         [NetRequestTool postUrl:@"skuOrder/refundOrder" parameters:@{@"order_code":self.order_code,@"order_detail_list":jsonString} success:^(id responseObject) {
             [[NSNotificationCenter defaultCenter] postNotificationName:kUserOrderDetilsUpDate object:nil];
             [[NSNotificationCenter defaultCenter] postNotificationName:kUserOrderTableUpDate object:nil];
             [self.navigationController popViewControllerAnimated:YES];
         } failure:^(NSInteger statusCode) {
            
         } finish:^{
            
         }];
         
     }]];
     
     [[CommonTTool getCurrentVC] presentViewController:alterController animated:YES completion:nil];
     
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    

    
    return self.model.sku_order_detail_list.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    
    return 1;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   ReFundOrderShowCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ReFundOrderShowCell"];

    cell.model = self.model.sku_order_detail_list[indexPath.row];

    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UserOrderChildGroupView *view = [[NSBundle mainBundle] loadNibNamed:@"UserOrderChildGroupView" owner:self options:nil].lastObject;
    view.nameLabel.text = self.model.business_name;

    
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 60;
   
}

@end
