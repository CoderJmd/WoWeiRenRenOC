//
//  QueryGoodsLogisticsVC.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/9/11.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QueryGoodsLogisticsVC : BaseTableViewController
@property (strong, nonatomic)NSString *order_code;
@end

NS_ASSUME_NONNULL_END
