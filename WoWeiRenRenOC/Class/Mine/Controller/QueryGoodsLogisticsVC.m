//
//  QueryGoodsLogisticsVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/9/11.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "QueryGoodsLogisticsVC.h"
#import "ShoppingCartHeadView.h"
#import "AbulkCell.h"
#import "ShoppingCartGroupHeadView.h"
#import "ShoppingCartCell.h"
#import "ShoppingCartBottomView.h"
#import "UserOrderChildGroupView.h"
#import "UserOrderChildCell.h"
#import "UserOrderDetailsVC.h"
#import "UserShoppingOrderModel.h"
#import "UserOrderChildFooterView.h"
#import "PagingViewController.h"
#import "ReFundOrderListFooterView.h"
#import "LogisticsModel.h"
#import "QueryGoodsLogisticsShowCell.h"
#import "QueryGoodsLogisticsHeadView.h"
#import "QueryGoodsLogisticsFooterView.h"

extern UserModel *userModel;
@interface QueryGoodsLogisticsVC ()

@end

@implementation QueryGoodsLogisticsVC



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.defaultStartPage = 1;
    
    self.title = @"查看物流";
    [self creatTableViewStyle:UITableViewStyleGrouped cellIdentity:@"QueryGoodsLogisticsShowCell"];
    self.tableView.backgroundColor = kRGBColor(246, 246, 246);
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.frame = CGRectMake(0, NavBarHeight, kScreenW, kScreenH-TabBarHeight+StatusViewHeight-NavBarHeight);
//    [self setRefresh];
    [self requestData];


    
}



- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];

    
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
}

#pragma mark - 刷新
- (void)updateRequestData {
    
    self.page = 1;
    [self requestData];
    
}

- (void)requestData
{
    SVPShowInternetWaiting
    NSMutableDictionary *para = [NSMutableDictionary dictionary];

    
    [para setObject:self.order_code forKey:@"order_code"];
    
    WeakSelf(ws)
    [NetRequestTool postUrl:@"skuOrder/querySkuOrderExpressList" parameters:para success:^(id responseObject) {
        NSMutableArray *arr = [LogisticsModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]];
        ws.dataArray = arr;
        [ws.tableView reloadData];
        
        
    } failure:^(NSInteger statusCode) {
    } finish:^{
        SVPDismiss

    }];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QueryGoodsLogisticsShowCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QueryGoodsLogisticsShowCell"];
    LogisticsModel *model = self.dataArray[indexPath.section];
    cell.model = model.express_order_detail_list[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    tableView.rowHeight = UITableViewAutomaticDimension;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 65;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    QueryGoodsLogisticsHeadView *head = [[NSBundle mainBundle]loadNibNamed:@"QueryGoodsLogisticsHeadView" owner:self options:nil].lastObject;

    head.model = self.dataArray[section];
    
    return head;
    
    
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    QueryGoodsLogisticsFooterView *footer = [[NSBundle mainBundle]loadNibNamed:@"QueryGoodsLogisticsFooterView" owner:self options:nil].lastObject;
    LogisticsModel *model = self.dataArray[section];

    footer.coutLabel.text = [NSString stringWithFormat:@"共%lu件商品",(unsigned long)model.express_order_detail_list.count];
    
    return footer;
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 40.0f;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section  {
    LogisticsModel *model =  self.dataArray[section];
    
    return model.express_order_detail_list.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return self.dataArray.count;
}
    


@end
