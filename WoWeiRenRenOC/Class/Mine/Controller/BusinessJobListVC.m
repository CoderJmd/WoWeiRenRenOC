//
//  BusinessJobListVC.m
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/18.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BusinessJobListVC.h"
#import "BusinessJobCell.h"
#import "AddBusinessJobVC.h"
#import "BusinessJobDetailC.h"

@interface BusinessJobListVC ()

@end

@implementation BusinessJobListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"人员招聘";
    self.defaultStartPage = 1;
    
    UIBarButtonItem *addBtn = [[UIBarButtonItem alloc] initWithTitle:@"新增" style:UIBarButtonItemStyleDone target:self action:@selector(rightBarButtonClick)];
    self.navigationItem.rightBarButtonItem = addBtn;
    
    [self creatTableViewWithCellIdentity:@"BusinessJobCell"];
    self.tableView.backgroundColor = kRGBColor(246, 246, 246);
    self.tableView.rowHeight = 95;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.frame = CGRectMake(0, 0, kScreenW, kScreenH);
    
    [self setRefresh];
    SVPShowInternetWaiting
    [self requestData];
}

- (void)rightBarButtonClick
{
    AddBusinessJobVC *vc = [[AddBusinessJobVC alloc] init];
    vc.parentsVC = self;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)requestData
{
    // 请求列表
    WeakSelf(ws)
    [NetRequestTool postUrl:@"business/queryRecruitList" parameters:@{@"page":@(self.page), @"limit":@"10", @"is_my":@(1)} success:^(id responseObject) {
        NSMutableArray *arr = [BusinessJobModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]];
        if (self.page==self.defaultStartPage) {
            ws.dataArray = arr;
        }
        else {
            [ws.dataArray addObjectsFromArray:arr];
        }
        [ws.tableView reloadData];
        
        // 控制页数
        NSDictionary *page = [responseObject objectForKey:@"page"];
        if ([page isKindOfClass:NSDictionary.class]) {
            NSInteger totalP = [page[@"total_page"] integerValue];
            if (totalP > self.page) {
                self.page++;
                [self endRefresh];
            }
            else {
                [self endRefreshNoMoreData];
            }
        }
        else {
            [self endRefreshNoMoreData];
        }
    } failure:^(NSInteger statusCode) {
        [ws endRefreshNoMoreData];
    } finish:^{
        SVPDismiss
    }];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BusinessJobCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BusinessJobCell"];
    cell.model = self.dataArray[indexPath.row];
    cell.editeBtn.tag = indexPath.row;
    [cell.editeBtn addTarget:self action:@selector(cellEditeButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BusinessJobDetailC *vc = [[BusinessJobDetailC alloc] init];
    vc.model = self.dataArray[indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)cellEditeButtonClick:(UIButton *)sender
{
    AddBusinessJobVC *vc = [[AddBusinessJobVC alloc] init];
    vc.parentsVC = self;
    vc.model = self.dataArray[sender.tag];
    [self.navigationController pushViewController:vc animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
