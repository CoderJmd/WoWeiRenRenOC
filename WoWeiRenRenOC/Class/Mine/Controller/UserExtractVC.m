//
//  UserExtractVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/13.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "UserExtractVC.h"
#import "UserExtractHeadView.h"
#import "UserExtractCell.h"
#import "ChangeZhiFuBaoVC.h"
extern UserModel *userModel;
@interface UserExtractVC ()

@property (nonatomic,strong) UserExtractHeadView *headView;
@end

@implementation UserExtractVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.defaultStartPage = 1;
    self.title = @"提现";
    [self creatTableViewWithCellIdentity:@"UserExtractCell"];
    self.tableView.backgroundColor = WhiteColor;
    self.tableView.estimatedRowHeight = 50;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.frame = CGRectMake(0, NavBarHeight, kScreenW, kScreenH-NavBarHeight -NeedStatusViewHeight);
    self.tableView.tableHeaderView = self.headView;
    self.dataArray = [NSMutableArray arrayWithArray:@[@"1、单笔提现最小金额为50元；",@"2、用户可季度提现"]];
    
}

- (UserExtractHeadView *)headView {
    
    if (!_headView) {
        _headView = [[[NSBundle mainBundle] loadNibNamed:@"UserExtractHeadView" owner:nil options:nil] lastObject];
        _headView.frame = CGRectMake(0, 0, kScreenW, 320);
        UITapGestureRecognizer *changeTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changeTapClick)];
        [_headView.changeBtn addGestureRecognizer:changeTap];
        _headView.priceLabel.text = [CommonTTool getPrice:userModel.balance];
        [_headView.inputPriceTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
        [_headView.submitBtn addTarget:self action:@selector(saveCashOut) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _headView;
}

- (void)textFieldDidChange:(UITextField *)tf {
    
    if ([tf isEqual:self.headView.inputPriceTF]) {
        
        float price = [self.headView.inputPriceTF.text floatValue];
        if (price >= 50 && price <= userModel.balance) {

            self.headView.submitBtn.backgroundColor =  kRGBAColor(221, 169, 72, 1);
            [self.headView.submitBtn setTitleColor:WhiteColor forState:UIControlStateNormal];

            self.headView.submitBtn.userInteractionEnabled = YES;

        }else {
            
            [self.headView.submitBtn setTitleColor:kRGBAColor(167, 167, 167, 1) forState:UIControlStateNormal];
            self.headView.submitBtn.backgroundColor =  kRGBAColor(242, 242, 242, 1);
            self.headView.submitBtn.userInteractionEnabled = NO;
        }

    }
    
}


- (void)changeTapClick {
    
    ChangeZhiFuBaoVC *vc = [[ChangeZhiFuBaoVC alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserExtractCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserExtractCell"];
    cell.nameLabel.text = self.dataArray[indexPath.row];
    tableView.rowHeight = UITableViewAutomaticDimension;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
}

#pragma mark - 提现提交
- (void)saveCashOut {
    
    SVPShowInternetWaiting;
    [NetRequestTool postUrl:@"user/updateUserCashOut" parameters:@{@"money":self.headView.inputPriceTF.text} success:^(id responseObject) {
        SVP(@"提交申请成功");
        [self uploadUsetInfo];
        
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        
    }];
    
}

- (void)uploadUsetInfo {
    
    [NetRequestTool postUrl:@"user/getUserInfo" parameters:@{} success:^(id responseObject) {
            
           NSDictionary *dic = responseObject[@"object"];
           [[NSUserDefaults standardUserDefaults] setObject:[dic deleteNull] forKey:kUserInfo];
           
            userModel = [UserModel mj_objectWithKeyValues:dic];
            self.headView.priceLabel.text = [CommonTTool getPrice:userModel.balance];

       } failure:^(NSInteger statusCode) {
           
       } finish:^{
           SVPDismiss
       }];
    
}

@end
