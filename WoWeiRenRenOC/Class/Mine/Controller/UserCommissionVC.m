//
//  UserCommissionVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/13.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "UserCommissionVC.h"
#import "UserVIIPPromoteModel.h"
#import "UserCommissionCell.h"
#import "UserCommissionHeadView.h"
#import "UserExtractVC.h"
#import "WithdrawalModel.h"
#import "UserCommissionModel.h"


extern UserModel *userModel;
@interface UserCommissionVC ()

@property (nonatomic,strong) UserCommissionHeadView *headView;

@property (nonatomic,strong) NSString *totalCount;
@end

@implementation UserCommissionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"我的佣金";
    
    self.defaultStartPage = 1;
    [self creatTableViewWithCellIdentity:@"UserCommissionCell"];

    self.tableView.backgroundColor = kRGBColor(246, 246, 246);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.frame = CGRectMake(0, 0, kScreenW, kScreenH);
    
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(15, KSCREEN_HEIGHT - 50 - NeedStatusViewHeight, KSCREEN_WIDTH - 30, 40)];
    button.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 16];
    [button setTitle:@"提现" forState:UIControlStateNormal];
    [button setTitleColor:kRGBColor(255, 246, 203) forState:UIControlStateNormal];
    CAGradientLayer *gl = [CAGradientLayer layer];
    gl.frame = CGRectMake(0,0,KSCREEN_WIDTH - 30,40);
    gl.startPoint = CGPointMake(0.22, 0.36);
    gl.endPoint = CGPointMake(0.85, 0.72);
    gl.colors = @[(__bridge id)[UIColor colorWithRed:221/255.0 green:169/255.0 blue:72/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:189/255.0 green:135/255.0 blue:35/255.0 alpha:1.0].CGColor];
    gl.locations = @[@(0), @(1.0f)];
    button.layer.cornerRadius = 4;
    button.clipsToBounds = YES;
    [button.layer insertSublayer:gl atIndex:0];
    [self.view addSubview:button];
    [button  addTarget:self action:@selector(extractClick) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self setRefresh];
    SVPShowInternetWaiting
    [self requestData];
    
    
    
    
}

- (void)extractClick {
    
    UserExtractVC *vc = [[UserExtractVC alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
    
}



- (void)uploadUsetInfo {
    
    [NetRequestTool postUrl:@"user/getUserInfo" parameters:@{} success:^(id responseObject) {
            
           NSDictionary *dic = responseObject[@"object"];
           [[NSUserDefaults standardUserDefaults] setObject:[dic deleteNull] forKey:kUserInfo];
           
            userModel = [UserModel mj_objectWithKeyValues:dic];
            self.headView.priceLabel.text = [NSString stringWithFormat:@"¥%@",[CommonTTool getPrice:userModel.balance]];

       } failure:^(NSInteger statusCode) {
           
       } finish:^{
           SVPDismiss
       }];
    
}


- (void)requestData
{
    // 请求列表
    WeakSelf(ws)
    [NetRequestTool postUrl:@"user/queryBalanceInfoList" parameters:@{@"page":@(self.page), @"limit":@"10",} success:^(id responseObject) {
       
        if (self.page == 1) {
            ws.dataArray = [UserCommissionModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]];
            
        }else {
            NSArray *newArray = [UserCommissionModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]];
            [ws.dataArray addObjectsFromArray:newArray];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [ws.tableView  reloadData];
        });
        
        NSDictionary *page = [responseObject objectForKey:@"page"];
        if (![CommonTTool isStrNull:page]) {
            self.totalCount = page[@"total_count"];
            
            if ([page isKindOfClass:NSDictionary.class]) {
              NSInteger totalP = [page[@"total_page"] integerValue];
              if (totalP > self.page) {
                  self.page++;
                  [self endRefresh];
              }
              else {
                  [self endRefreshNoMoreData];
              }
           }
           else {
              [self endRefreshNoMoreData];
           }
        }else {
            [self endRefreshNoMoreData];
            self.totalCount = @"0";
            
        }
       
        
    } failure:^(NSInteger statusCode) {
//        [ws endRefreshNoMoreData];
    } finish:^{
        SVPDismiss
        [self endRefresh];
    }];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserCommissionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserCommissionCell"];
    cell.commissionModel = self.dataArray[indexPath.row];
    tableView.rowHeight = 74;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - section头部间距
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {

    return 155;
}


#pragma mark - section头部视图
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {

    UIView *view= [[UIView alloc]initWithFrame:CGRectMake( 0, 0, KSCREEN_WIDTH, 155)];
    view.backgroundColor = VCBgcolor;
    [view addSubview:self.headView];
    self.headView.priceLabel.text = [NSString stringWithFormat:@"¥%@",[CommonTTool getPrice:userModel.balance]];
    return view;
}

#pragma mark - section底部间距
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 0.0001;
}

#pragma mark - section底部视图
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake( 0, 0, KSCREEN_WIDTH, 0)];
    view.backgroundColor = VCBgcolor;
    return view;
}


- (UserCommissionHeadView *)headView {
    
    if (!_headView) {
        _headView = [UserCommissionHeadView getView];
        _headView.frame = CGRectMake(0, 0, kScreenW, 155);
    }
    
    return _headView;
    
}


- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self uploadUsetInfo];

    self.page = 1;
    [self requestData];

    
}

@end
