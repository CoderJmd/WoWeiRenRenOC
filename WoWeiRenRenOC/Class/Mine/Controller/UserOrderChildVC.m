//
//  UserOrderChildVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/13.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "UserOrderChildVC.h"
#import "ShoppingCartHeadView.h"
#import "AbulkCell.h"
#import "ShoppingCartGroupHeadView.h"
#import "ShoppingCartCell.h"
#import "ShoppingCartBottomView.h"
#import "UserOrderChildGroupView.h"
#import "UserOrderChildCell.h"
#import "UserOrderDetailsVC.h"
#import "UserShoppingOrderModel.h"
#import "UserOrderChildFooterView.h"
#import "PagingViewController.h"



extern UserModel *userModel;
@interface UserOrderChildVC ()

@end

@implementation UserOrderChildVC



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.defaultStartPage = 1;
    
    [self creatTableViewStyle:UITableViewStyleGrouped cellIdentity:@"UserOrderChildCell"];
    self.tableView.backgroundColor = kRGBColor(246, 246, 246);
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.frame = CGRectMake(0, 0, kScreenW, kScreenH-TabBarHeight+StatusViewHeight-StatusViewHeight-50);
    [self setRefresh];
    [self requestData];

    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateRequestData) name:kUserOrderTableUpDate object:nil];
    
}



- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];

    
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
}

#pragma mark - 刷新
- (void)updateRequestData {
    
    self.page = 1;
    [self requestData];
    
}

- (void)requestData
{
    SVPShowInternetWaiting
    NSMutableDictionary *para = [NSMutableDictionary dictionary];
    [para setObject:@(self.page) forKey:@"page"];
    [para setObject:@(10) forKey:@"limit"];
    
    if (![CommonTTool isStrNull:self.order_state]) {
        
        [para setObject:self.order_state forKey:@"order_state"];
    }
    
    if (![CommonTTool isStrNull:self.order_type]) {
        
        [para setObject:self.order_type forKey:@"order_type"];
    }
    if (![CommonTTool isStrNull:self.tg_group_state]) {
        
        [para setObject:self.tg_group_state forKey:@"tg_group_state"];
    }
    
    WeakSelf(ws)
    [NetRequestTool postUrl:@"skuOrder/querySkuOrderList" parameters:para success:^(id responseObject) {
        NSMutableArray *arr = [UserShoppingOrderModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]];
        if (self.page==self.defaultStartPage) {
            ws.dataArray = arr;
        }
        else {
            [ws.dataArray addObjectsFromArray:arr];
        }
        [ws.tableView reloadData];
        
        // 控制页数
        NSDictionary *page = [responseObject objectForKey:@"page"];
        if ([page isKindOfClass:NSDictionary.class]) {
            NSInteger totalP = [page[@"total_page"] integerValue];
            if (totalP > self.page) {
                self.page++;
                [self endRefresh];
            }
            else {
                [self endRefreshNoMoreData];
            }
        }
        else {
            [self endRefreshNoMoreData];
        }
    } failure:^(NSInteger statusCode) {
        [ws endRefreshNoMoreData];
    } finish:^{
        SVPDismiss
        [self endRefresh];
    }];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserOrderChildCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserOrderChildCell"];
    UserShoppingOrderModel *model = self.dataArray[indexPath.section];
    cell.model = model.sku_order_detail_list[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (cell.model.is_td == 5 || cell.model.is_td == 2 || cell.model.is_td == 6) {

        
        cell.reFundLabel.hidden = NO;
        cell.reFundCoutnLabel.hidden = NO;
        tableView.rowHeight = 150;
    }else {
        
        cell.reFundLabel.hidden = YES;
        cell.reFundCoutnLabel.hidden = YES;
        tableView.rowHeight = 110;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserShoppingOrderModel *model = self.dataArray[indexPath.section];
    UserOrderDetailsVC *vc = [[UserOrderDetailsVC alloc]init];
    vc.order_code = model.order_code;
    [self.navigationController pushViewController:vc animated:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UserOrderChildGroupView *view = [[NSBundle mainBundle] loadNibNamed:@"UserOrderChildGroupView" owner:self options:nil].lastObject;
    UserShoppingOrderModel *model = self.dataArray[section];
    
    view.model = model;
    view.jumpBtn.tag = section;
    [view.jumpBtn addTarget:self action:@selector(headVieTapClick:) forControlEvents:UIControlEventTouchUpInside];
    view.tag = section;
    
    return view;
}

- (void)headVieTapClick:(UIButton *)view {
    ShoppingCartModel *model = self.dataArray[view.tag];
    PagingViewController *vc= [[PagingViewController alloc]init];
    vc.business_id = model.business_id;
    [self.navigationController pushViewController:vc animated:YES];
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 60.0f;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UserOrderChildFooterView *view = [[NSBundle mainBundle] loadNibNamed:@"UserOrderChildFooterView" owner:self options:nil].lastObject;
    
    UserShoppingOrderModel *model = self.dataArray[section];
    view.model = model;
    
    
    return view;;
}




- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    UserShoppingOrderModel *model = self.dataArray[section];
    if (model.is_td != 1) {
        
        if (model.order_state == 4) {
            
            return 87;
        }
        return 87;
    }
    
    if (model.order_state == 1 || model.order_state == 0 || model.order_state  == 5) {
        
        return  87;
    }
    
    if (model.order_state == 4) {
        
        return  87;
    }
    
    //如果是拼团 订单
    if (model.order_state == 3 && model.order_type == 4) {
        
        return 87;
        
    }
    
    
    return 41;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section  {
    UserShoppingOrderModel *model =  self.dataArray[section];
    
    return model.sku_order_detail_list.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return self.dataArray.count;
}
    


@end
