//
//  ContactServiceVC.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/17.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseTableViewController.h"
#import "SystemDictionaryModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ContactServiceVC : BaseTableViewController
@property (strong, nonatomic)SystemDictionaryModel *model;
@end

NS_ASSUME_NONNULL_END
