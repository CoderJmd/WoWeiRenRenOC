//
//  UserMerchantsPromoteDetailsVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/19.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "UserMerchantsPromoteDetailsVC.h"
#import "UserMerchantsPromoteDetailsCell.h"
#import "AddressSelectVC.h"


@interface UserMerchantsPromoteDetailsVC ()
@property (nonatomic,strong) UserMerchantsPromoteDetailsCell *cell;
@end

@implementation UserMerchantsPromoteDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"商家入驻";
    
    
    [self creatTableViewWithCellIdentity:@"HomeCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"UserMerchantsPromoteDetailsCell" bundle:nil] forCellReuseIdentifier:@"UserMerchantsPromoteDetailsCell"];
    self.tableView.backgroundColor = kRGBColor(246, 246, 246);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.frame = CGRectMake(0, 0, kScreenW, kScreenH);
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(businessInfoUpDate) name:@"BusinessInfoUpDate" object:nil];
    
    if (self.model.sh_state == 2) {
        WRBaseButton *button = [[WRBaseButton alloc]init];
       button.frame = CGRectMake(15, kScreenH - 55, kScreenW - 30, 40);
       [button setTitle:@"年度审核" forState:UIControlStateNormal];
       button.titleLabel.font = [UIFont systemFontOfSize:14];
       [button setUi];
       [self.view addSubview:button];
       [button setBackgroundColor:kRGBColor(221, 169, 72)];
       [button addTarget:self action:@selector(updateBusinessInfo) forControlEvents:UIControlEventTouchUpInside];
    }
    
    if (self.model.sh_state == 3 && self.type == 0) {
        WRBaseButton *button = [[WRBaseButton alloc]init];
       button.frame = CGRectMake(15, kScreenH - 55, kScreenW - 30, 40);
       [button setTitle:@"重新编辑" forState:UIControlStateNormal];
       button.titleLabel.font = [UIFont systemFontOfSize:14];
       [button setUi];
       [self.view addSubview:button];
       [button setBackgroundColor:kRGBColor(221, 169, 72)];
       [button addTarget:self action:@selector(chengeBusinessInfo) forControlEvents:UIControlEventTouchUpInside];
    }
    
    
       
    
}

- (void)chengeBusinessInfo {
    
    UserMerchantsPromoteDetailsVC *vc = [[UserMerchantsPromoteDetailsVC alloc]init];
    vc.model= self.model;
    vc.type = 1;
    [self.navigationController pushViewController:vc animated:YES];
    
}


- (void)updateBusinessInfo {
    
    NSDate *datenow = [NSDate date];//现在时间,你可以输出来看下是什么格式

    NSInteger currentTime = [datenow timeIntervalSince1970];
    NSString *oldTImeStr = self.model.effective_time;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];// 创建一个时间格式化对象
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"]; //设定时间的格式
    NSDate *tempDate = [dateFormatter dateFromString:oldTImeStr];//将字符串转换为时间对象
    NSInteger oldTime = [tempDate timeIntervalSince1970];
    
    NSInteger time = oldTime - currentTime;
    
    if (time > 60*60*24*30) {
        
        SVP(@"请在商家有效期到期30天进行年度审核");
       
    }else {
        
        UserMerchantsPromoteDetailsVC *vc = [[UserMerchantsPromoteDetailsVC alloc]init];
        vc.changeModel = self.model;
        [self.navigationController pushViewController:vc animated:YES];
        
    }
    
}

- (void)businessInfoUpDate {
    
    NSDictionary *dict = [[NSUserDefaults standardUserDefaults] objectForKey:kUserMerchantsInfo];
    NSString *business_id = dict[@"business_id"];
    [NetRequestTool postUrl:@"business/getBusinessInfo" parameters:@{@"business_id":business_id} success:^(id responseObject) {
       
       NSDictionary *dict = [responseObject objectForKey:@"object"];
       // 去除空值后 直接存字典
       [[NSUserDefaults standardUserDefaults] setObject:[dict deleteNull] forKey:kUserMerchantsInfo];
        self.model = [BusinessModel mj_objectWithKeyValues:dict];
        [self.tableView reloadData];
        
    } failure:^(NSInteger statusCode) {
    //        [ws endRefreshNoMoreData];
    } finish:^{
        SVPDismiss
    }];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserMerchantsPromoteDetailsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserMerchantsPromoteDetailsCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    self.cell = cell;
    if (self.model == nil) {
        
        [cell.submitBtn addTarget:self action:@selector(submitBtnClick) forControlEvents:UIControlEventTouchUpInside];
        tableView.rowHeight = 1200;
        
    }else {
        if (self.model.sh_state == 3 && self.type == 1) {
            cell.changeModel = self.model;
            [cell.submitBtn addTarget:self action:@selector(changeBtnClick) forControlEvents:UIControlEventTouchUpInside];
            tableView.rowHeight = 1100;
        }else {
            tableView.rowHeight = 1100;
            cell.model = self.model;
        }
        
    }
    
    return cell;
}

- (void)changeBtnClick {
    
    NSString *business_name = self.cell.nameLabel.text;
    NSString *busineee_tel = self.cell.merchantsPhoneLabel.text;
    NSString *contacts = self.cell.contactsLabel.text;
    NSString *classification_id = self.cell.typeModel.classify_id;
    NSString *discount_id = self.cell.zhekouModel.Id;
    NSString *address = self.cell.merchantsAddressLabel.text;
    NSString *phone = self.cell.phoneLabel.text;
    NSString *business_license_no = self.cell.business_license_noLabel.text;
    NSString *business_license_path = self.cell.business_license_path;
    NSString *head_path = self.cell.head_path;
    NSString *description = self.cell.descriptionLabel.text;
    NSString *houseNumber = self.cell.houseNumberTF.text;

    if ([CommonTTool isStrNull:business_name]) {
        SVP(@"请输入商家名称")
        return;
    }

    if ([CommonTTool isStrNull:busineee_tel]) {
        SVP(@"请输入商家电话")
        return;
    }
    if ([CommonTTool isStrNull:classification_id]) {
        SVP(@"请选择商家分类")
        return;
    }
    if ([CommonTTool isStrNull:discount_id]) {
        SVP(@"请选择折扣力度")
        return;
    }
    if ([CommonTTool isStrNull:address]) {
        SVP(@"请选择商家地址")
        return;
    }
    if ([CommonTTool isStrNull:contacts]) {
        SVP(@"请输入联系人")
        return;
    }
    if ([CommonTTool isStrNull:phone]) {
       SVP(@"请输入手机号")
       return;
    }
    if ([CommonTTool isStrNull:business_license_no]) {
      SVP(@"请输入营业执照号")
      return;
    }
    if ([CommonTTool isStrNull:business_license_path]) {
      SVP(@"请选择营业执照图片")
      return;
    }
    if ([CommonTTool isStrNull:head_path]) {
      SVP(@"请选择门头照图片")
      return;
    }
    
    
    
    NSString *business_id = [NSString stringWithFormat:@"%ld",(long)self.model.business_id];


    NSDictionary *para = @{@"business_id":business_id,
                           @"business_name":business_name,
                           @"busineee_tel":busineee_tel,
                           @"classification_id":classification_id,
                           @"discount_id":discount_id,
                           @"contacts":contacts,
                           @"business_license_no":business_license_no,
                           @"business_license_path":business_license_path,
                           @"head_path":head_path,
                           @"description":description,
                           @"address":address,
                           @"house_number":houseNumber
                           
    };
    
    UIAlertController *alterController = [UIAlertController alertControllerWithTitle:nil message:@"请确认资料完整正确，是否确定提交？" preferredStyle:UIAlertControllerStyleAlert];
          
    [alterController addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
      
    }]];

    [alterController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        SVPShowInternetWaiting
         [NetRequestTool postUrl:@"business/updateBusinessInfo" parameters:para success:^(id responseObject) {
            
             SVP(@"提交成功");
             [[NSNotificationCenter defaultCenter] postNotificationName:@"BusinessCheckIn" object:nil];
             //self.navigationController.childViewControllers[2]
             [self.navigationController popToViewController:self.navigationController.childViewControllers[1] animated:YES];
             
         } failure:^(NSInteger statusCode) {
         //        [ws endRefreshNoMoreData];
         } finish:^{
             SVPDismiss
         }];
      
    }]];

    [self presentViewController:alterController animated:YES completion:nil];

   
    
}


- (void)submitBtnClick {
    
    NSString *business_name = self.cell.nameLabel.text;
    NSString *busineee_tel = self.cell.merchantsPhoneLabel.text;
    NSString *contacts = self.cell.contactsLabel.text;
    NSString *classification_id = self.cell.typeModel.classify_id;
    NSString *discount_id = self.cell.zhekouModel.Id;
    NSString *address = self.cell.merchantsAddressLabel.text;
    NSString *phone = self.cell.phoneLabel.text;
    NSString *password = self.cell.passwordLabel.text;
    NSString *confirmPassword = self.cell.confirmPasswordLabel.text;
    NSString *business_license_no = self.cell.business_license_noLabel.text;
    NSString *business_license_path = self.cell.business_license_path;
    NSString *head_path = self.cell.head_path;
    NSString *description = self.cell.descriptionLabel.text;
    NSString *houseNumber = self.cell.houseNumberTF.text;
    if ([houseNumber isEqualToString:@"暂无数据"]) {
        houseNumber = @"";
    }
    
    if ([CommonTTool isStrNull:business_name]) {
        SVP(@"请输入商家名称")
        return;
    }
    
    if ([CommonTTool isStrNull:busineee_tel]) {
        SVP(@"请输入商家电话")
        return;
    }
    if ([CommonTTool isStrNull:classification_id]) {
        SVP(@"请选择商家分类")
        return;
    }
    if ([CommonTTool isStrNull:discount_id]) {
        SVP(@"请选择折扣力度")
        return;
    }
    if ([CommonTTool isStrNull:address]) {
        SVP(@"请选择商家地址")
        return;
    }
    if ([CommonTTool isStrNull:contacts]) {
        SVP(@"请输入联系人")
        return;
    }
    if ([CommonTTool isStrNull:phone]) {
       SVP(@"请输入手机号")
       return;
    }
    if ([CommonTTool isStrNull:password]) {
       SVP(@"请输入密码")
       return;
    }
    if (![password isEqualToString:confirmPassword]) {
       SVP(@"两次密码不一致，请重新输入")
       return;
    }
    if ([CommonTTool isStrNull:business_license_no]) {
      SVP(@"请输入营业执照号")
      return;
    }
    if ([CommonTTool isStrNull:business_license_path]) {
      SVP(@"请选择营业执照图片")
      return;
    }
    if ([CommonTTool isStrNull:head_path]) {
      SVP(@"请选择门头照图片")
      return;
    }
    
    
    
    NSDictionary *para = @{@"business_name":business_name,
                           @"busineee_tel":busineee_tel,
                           @"classification_id":classification_id,
                           @"discount_id":discount_id,
                           @"contacts":contacts,
                           @"phone":phone,
                           @"password":password,
                           @"business_license_no":business_license_no,
                           @"business_license_path":business_license_path,
                           @"head_path":head_path,
                           @"description":description,
                           @"address":address,
                           @"house_number":houseNumber
                           
    };
    
    SVPShowInternetWaiting
    [NetRequestTool postUrl:@"business/checkIn" parameters:para success:^(id responseObject) {
       
        SVP(@"提交成功");
        [[NSNotificationCenter defaultCenter] postNotificationName:@"BusinessCheckIn" object:nil];
        [self.navigationController popViewControllerAnimated:YES];
        
    } failure:^(NSInteger statusCode) {
//        [ws endRefreshNoMoreData];
    } finish:^{
        SVPDismiss
    }];
    
    
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - section头部间距
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {

    return 0.001;
}


#pragma mark - section头部视图
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {

    UIView *view= [[UIView alloc]initWithFrame:CGRectMake( 0, 0, KSCREEN_WIDTH, 0)];
    view.backgroundColor = VCBgcolor;
    return view;
}

#pragma mark - section底部间距
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 0.0001;
}

#pragma mark - section底部视图
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake( 0, 0, KSCREEN_WIDTH, 0)];
    view.backgroundColor = VCBgcolor;
    return view;
}





@end
