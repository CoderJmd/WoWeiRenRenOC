//
//  UserShippingAddressVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/12.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "UserShippingAddressVC.h"
#import "UserShippingAddressCell.h"
#import "UserShippingAddAddressVC.h"

@interface UserShippingAddressVC ()

@end

@implementation UserShippingAddressVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.defaultStartPage = 1;
    self.title = @"我的收货地址";
    [self creatTableViewWithCellIdentity:@"UserShippingAddressCell"];

    self.view.backgroundColor = kRGBColor(246, 246, 246);
    self.tableView.backgroundColor = kRGBColor(246, 246, 246);
    self.tableView.rowHeight = 102;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.frame = CGRectMake(0, NavBarHeight, kScreenW, kScreenH-NavBarHeight -NeedStatusViewHeight-50);
    
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(15, KSCREEN_HEIGHT - 50 - NeedStatusViewHeight, KSCREEN_WIDTH - 30, 40)];
    button.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 16];
    [button setTitle:@"添加新地址" forState:UIControlStateNormal];
    [button setTitleColor:kRGBColor(255, 246, 203) forState:UIControlStateNormal];
    CAGradientLayer *gl = [CAGradientLayer layer];
    gl.frame = CGRectMake(0,0,KSCREEN_WIDTH - 30,40);
    gl.startPoint = CGPointMake(0.22, 0.36);
    gl.endPoint = CGPointMake(0.85, 0.72);
    gl.colors = @[(__bridge id)[UIColor colorWithRed:221/255.0 green:169/255.0 blue:72/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:189/255.0 green:135/255.0 blue:35/255.0 alpha:1.0].CGColor];
    gl.locations = @[@(0), @(1.0f)];
    button.layer.cornerRadius = 4;
    button.clipsToBounds = YES;
    [button.layer insertSublayer:gl atIndex:0];
    [self.view addSubview:button];
    [button  addTarget:self action:@selector(addAddressClick) forControlEvents:UIControlEventTouchUpInside];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateAddress) name:kUserAddShopingAddressUpDate object:nil];
    
    [self setRefresh];
    SVPShowInternetWaiting
    [self requestData];
    
    
}

- (void)updateAddress {
    
    self.page = 1;
    [self requestData];
    
}

- (void)requestData
{
    SVPShowInternetWaiting;
    // 请求列表
    WeakSelf(ws)
    [NetRequestTool postUrl:@"address/queryAddressList" parameters:@{@"page":@(self.page), @"limit":@"10",@"is_tj":@"1"} success:^(id responseObject) {
       
        if (self.page == 1) {
            ws.dataArray = [UserShipingAddressModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]];
            
        }else {
            NSArray *newArray = [UserShipingAddressModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]];
            [ws.dataArray addObjectsFromArray:newArray];
        }
        // 控制页数
        NSDictionary *page = [responseObject objectForKey:@"page"];
        if ([page isKindOfClass:NSDictionary.class]) {
            NSInteger totalP = [page[@"total_page"] integerValue];
            if (totalP > self.page) {
                self.page++;
                [self endRefresh];
            }
            else {
                [self endRefreshNoMoreData];
            }
        }
        else {
            [self endRefreshNoMoreData];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [ws.tableView  reloadData];
        });
        
    } failure:^(NSInteger statusCode) {
//        [ws endRefreshNoMoreData];
    } finish:^{
        SVPDismiss
    }];
}


- (void)addAddressClick {
    
    UserShippingAddAddressVC *vc = [[UserShippingAddAddressVC alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UserShippingAddressCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserShippingAddressCell"];
    cell.model = self.dataArray[indexPath.section];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.selectorAddressModel) {
        self.selectorAddressModel(self.dataArray[indexPath.section]);
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArray.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    UIView *view = [[UIView alloc]init];
    
    view.backgroundColor = [UIColor clearColor];
    return view;
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 10;
}



@end
