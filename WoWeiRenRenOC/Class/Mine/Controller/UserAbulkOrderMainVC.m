//
//  UserAbulkOrderMainVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/15.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "UserAbulkOrderMainVC.h"
#import "CMPageTitleView.h"
#import "UserOrderChildVC.h"

@interface UserAbulkOrderMainVC ()<CMPageTitleViewDelegate>
/**视图控制器数组*/
@property (nonatomic,copy) NSArray *childControllers;
/**default notes*/
@property (nonatomic,strong) CMPageTitleView *pageTitleView;

@end

@implementation UserAbulkOrderMainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"我的拼团";
    [self.view addSubview:self.pageTitleView];
    
    
    
    self.pageTitleView.delegate = self;
    CMPageTitleConfig *config = [CMPageTitleConfig defaultConfig];
    config.cm_titleHeight = 54;
    config.cm_childControllers = self.childControllers;
    config.cm_gradientStyle = CMTitleColorGradientStyle_None;
    config.cm_switchMode = CMPageTitleSwitchMode_Underline;
    config.cm_font = [UIFont fontWithName:@"PingFangSC-Medium" size: 12];
    config.cm_selectedColor = [UIColor blackColor];
    config.cm_underlineWidth = 24;
    config.cm_underlineHeight = 3;
    self.pageTitleView.cm_config = config;
}

- (CMPageTitleView *)pageTitleView {
    if (!_pageTitleView) {
        CMPageTitleView *pageTitleView = [[CMPageTitleView alloc] init];
        
        _pageTitleView = pageTitleView;
        
        _pageTitleView.frame = CGRectMake(0, NavBarHeight, kScreenW, kScreenH-NavBarHeight-NeedStatusViewHeight);
        
    }
    
    return _pageTitleView;
    
}


- (NSArray *)childControllers {
    
    if (!_childControllers) {
        
        UserOrderChildVC *vc0 = [[UserOrderChildVC alloc]init];
        UserOrderChildVC *vc1 = [[UserOrderChildVC alloc]init];
        UserOrderChildVC *vc2 = [[UserOrderChildVC alloc]init];
        UserOrderChildVC *vc3 = [[UserOrderChildVC alloc]init];
        vc0.order_type = @"4";
        vc1.order_type = @"4";
        vc2.order_type = @"4";
        vc3.order_type = @"4";
        
        vc0.tg_group_state = @"";
        vc1.tg_group_state = @"1";
        vc2.tg_group_state = @"2";
        vc3.tg_group_state   = @"3";
        
        
        vc0.title = @"全部";
        vc1.title = @"拼团中";
        vc2.title = @"拼团成功";
        vc3.title = @"拼团失败";
        
        
        
        _childControllers = @[vc0,vc1,vc2,vc3];
    }
    
    return _childControllers;
}




@end
