//
//  ChangeZhiFuBaoVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/18.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "ChangeZhiFuBaoVC.h"
#import "JQScanViewController.h"
#import "ChangeZhiFuBaoCell.h"
extern UserModel *userModel;
@interface ChangeZhiFuBaoVC ()
@property (nonatomic,strong) ChangeZhiFuBaoCell *cell;
@end

@implementation ChangeZhiFuBaoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.defaultStartPage = 1;
    self.title = @"资料提交";
    [self creatTableViewWithCellIdentity:@"ChangeZhiFuBaoCell"];

    self.view.backgroundColor = WhiteColor;
    self.tableView.backgroundColor = WhiteColor;
    self.tableView.rowHeight = 240;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.frame = CGRectMake(0, NavBarHeight, kScreenW, kScreenH-NavBarHeight -NeedStatusViewHeight-50);
    
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ChangeZhiFuBaoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChangeZhiFuBaoCell"];
    //cell.model = self.dataArray[indexPath.row];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    self.cell = cell;
    if (![CommonTTool isStrNull:userModel.alipay_account]) {
        cell.accountTF.text = userModel.alipay_account;

    }
    if (![CommonTTool isStrNull:userModel.alipay_name]) {
        cell.nameTF.text = userModel.alipay_name;

    }
    [cell.changeBtn addTarget:self action:@selector(submitClick) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (void)submitClick {
    
   
    
    if ([CommonTTool isStrNull:self.cell.accountTF.text]) {
        SVP(@"请输入支付宝账号!");
        return;
    }
    
    if ([CommonTTool isStrNull:self.cell.nameTF.text]) {
        SVP(@"请输入支付宝姓名!");
        return;
    }
    [CommonTTool dismisKeyBoard];
    SVPShowInternetWaiting;
    [NetRequestTool postUrl:@"user/updateUserAlipay" parameters:@{@"alipay_account":self.cell.accountTF.text,@"alipay_name":self.cell.nameTF.text} success:^(id responseObject) {
//        SVP(@"修改成功");
        [self uploadUsetInfo];
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        
    }];
    
    
}

- (void)uploadUsetInfo {
    
    [NetRequestTool postUrl:@"user/getUserInfo" parameters:@{} success:^(id responseObject) {
         SVP(@"修改成功");
        NSDictionary *dic = responseObject[@"object"];
        [[NSUserDefaults standardUserDefaults] setObject:[dic deleteNull] forKey:kUserInfo];
        userModel = [UserModel mj_objectWithKeyValues:dic];
        [self performSelector:@selector(back) withObject:nil afterDelay:2];
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        SVPDismiss
    }];
}


- (void)back {
    
    [self.navigationController popViewControllerAnimated:YES];
}



@end
