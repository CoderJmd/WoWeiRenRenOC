//
//  UserShippingAddressVC.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/12.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseTableViewController.h"
#import "UserShipingAddressModel.h"
typedef void (^SelectorAddressModel)(UserShipingAddressModel *model);
NS_ASSUME_NONNULL_BEGIN

@interface UserShippingAddressVC : BaseTableViewController
@property (copy, nonatomic)SelectorAddressModel selectorAddressModel;
@end

NS_ASSUME_NONNULL_END
