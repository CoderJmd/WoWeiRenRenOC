//
//  ReFundOrderSelectorVC.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/9/1.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseTableViewController.h"
#import "UserShoppingOrderDetailsModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ReFundOrderSelectorVC : BaseTableViewController
@property (strong, nonatomic)UserShoppingOrderDetailsModel *model;
@property (strong, nonatomic)NSString *order_code;

@end

NS_ASSUME_NONNULL_END
