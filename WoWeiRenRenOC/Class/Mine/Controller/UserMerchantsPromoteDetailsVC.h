//
//  UserMerchantsPromoteDetailsVC.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/19.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseTableViewController.h"
#import "BusinessModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface UserMerchantsPromoteDetailsVC : BaseTableViewController
@property (strong, nonatomic)BusinessModel *model;
@property (strong, nonatomic)BusinessModel *changeModel;
@property (assign , nonatomic)int type; //1是修改
    
@end

NS_ASSUME_NONNULL_END
