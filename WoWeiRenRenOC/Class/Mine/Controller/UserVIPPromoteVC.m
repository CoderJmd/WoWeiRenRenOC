//
//  UserVIPPromoteVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/18.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "UserVIPPromoteVC.h"
#import "UserVIIPPromoteModel.h"
#import "UserVIPPromoteVCShowTableViewCell.h"
#import "UserVIPPromoteHeadView.h"
extern UserModel *userModel;
@interface UserVIPPromoteVC ()

@property (nonatomic,strong) UserVIPPromoteHeadView *headView;

@property (nonatomic,strong) NSString *totalCount;
@end

@implementation UserVIPPromoteVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"会员推广";
    
    self.defaultStartPage = 1;
    [self creatTableViewWithCellIdentity:@"HomeCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"UserVIPPromoteVCShowTableViewCell" bundle:nil] forCellReuseIdentifier:@"UserVIPPromoteVCShowTableViewCell"];
    self.tableView.backgroundColor = kRGBColor(246, 246, 246);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.frame = CGRectMake(0, 0, kScreenW, kScreenH);
    
    [self setRefresh];
    SVPShowInternetWaiting
    [self requestData];
    
    
}


- (void)setHeadUi {
    
    self.headView.userCodeLable.text = userModel.my_code;
    self.headView.countLabel.text = [NSString stringWithFormat:@"%@",userModel.frist_number];

}


- (void)requestData
{
    // 请求列表
    WeakSelf(ws)
    [NetRequestTool postUrl:@"user/queryTjUserList" parameters:@{@"page":@(self.page), @"limit":@"10",} success:^(id responseObject) {
       
        if (self.page == 1) {
            ws.dataArray = [UserVIIPPromoteModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]];
            
        }else {
            NSArray *newArray = [UserVIIPPromoteModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]];
            [ws.dataArray addObjectsFromArray:newArray];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [ws.tableView  reloadData];
        });
        
        NSDictionary *page = [responseObject objectForKey:@"page"];
        if (![CommonTTool isStrNull:page]) {
            self.totalCount = page[@"total_count"];
            [self setHeadUi];
            if ([page isKindOfClass:NSDictionary.class]) {
              NSInteger totalP = [page[@"total_page"] integerValue];
              if (totalP > self.page) {
                  self.page++;
                  [self endRefresh];
              }
              else {
                  [self endRefreshNoMoreData];
              }
           }
           else {
              [self endRefreshNoMoreData];
           }
        }else {
            [self endRefreshNoMoreData];
            self.totalCount = @"0";
            [self setHeadUi];
        }
       
        
    } failure:^(NSInteger statusCode) {
//        [ws endRefreshNoMoreData];
    } finish:^{
        SVPDismiss
        [self endRefresh];
    }];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserVIPPromoteVCShowTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserVIPPromoteVCShowTableViewCell"];
    cell.model = self.dataArray[indexPath.row];
    tableView.rowHeight = 74;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - section头部间距
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {

    return 192;
}


#pragma mark - section头部视图
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {

    UIView *view= [[UIView alloc]initWithFrame:CGRectMake( 0, 0, KSCREEN_WIDTH, 192)];
    view.backgroundColor = VCBgcolor;
    [view addSubview:self.headView];
    return view;
}

#pragma mark - section底部间距
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 0.0001;
}

#pragma mark - section底部视图
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake( 0, 0, KSCREEN_WIDTH, 0)];
    view.backgroundColor = VCBgcolor;
    return view;
}


- (UserVIPPromoteHeadView *)headView {
    
    if (!_headView) {
        _headView = [UserVIPPromoteHeadView getView];
        _headView.frame = CGRectMake(0, 0, kScreenW, 192);
    }
    
    return _headView;
    
}

@end
