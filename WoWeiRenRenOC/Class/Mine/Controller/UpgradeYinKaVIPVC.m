//
//  UpgradeYinKaVIPVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/7/1.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "UpgradeYinKaVIPVC.h"
#import "UpgradeYinKaVIPShowCell.h"
#import "RegisterPayVC.h"


@interface UpgradeYinKaVIPVC ()
@property (nonatomic,strong) UIButton *buyBtn;
@end

@implementation UpgradeYinKaVIPVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"银卡会员介绍";
    self.defaultStartPage = 1;
    [self creatTableViewWithCellIdentity:@"UpgradeYinKaVIPShowCell"];

    self.tableView.backgroundColor = WhiteColor;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.frame = CGRectMake(0, NavBarHeight, kScreenW, kScreenH - NavBarHeight);
    
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(15, KSCREEN_HEIGHT - 50 - NeedStatusViewHeight, KSCREEN_WIDTH - 30, 40)];
    button.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 16];
    [button setTitle:@"确认支付100元升级" forState:UIControlStateNormal];
    [button setTitleColor:kRGBColor(255, 246, 203) forState:UIControlStateNormal];
    CAGradientLayer *gl = [CAGradientLayer layer];
    gl.frame = CGRectMake(0,0,KSCREEN_WIDTH - 30,40);
    gl.startPoint = CGPointMake(0.22, 0.36);
    gl.endPoint = CGPointMake(0.85, 0.72);
    gl.colors = @[(__bridge id)[UIColor colorWithRed:221/255.0 green:169/255.0 blue:72/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:189/255.0 green:135/255.0 blue:35/255.0 alpha:1.0].CGColor];
    gl.locations = @[@(0), @(1.0f)];
    button.layer.cornerRadius = 4;
    button.clipsToBounds = YES;
    [button.layer insertSublayer:gl atIndex:0];
    [self.view addSubview:button];
    [button  addTarget:self action:@selector(upgradeYinKaClick) forControlEvents:UIControlEventTouchUpInside];
    self.buyBtn = button;
    [self requestDataTwo];
}

#pragma mark - 升级银卡
- (void)upgradeYinKaClick {
        
    RegisterPayVC *vc = [[RegisterPayVC alloc]initWithNibName:@"RegisterPayVC" bundle:nil];
    vc.type = 2;
    vc.orderType = 2;
    vc.title  = @"升级银卡会员";
    [self.navigationController pushViewController:vc animated:YES];
    
    
}

- (void)requestDataTwo {
        
    
    [NetRequestTool postUrl:@"sysCode/querySysCodeObject" parameters:@{@"type":@(10)} success:^(id responseObject) {
        NSString *str = [NSString stringWithFormat:@"确认支付%@元升级",responseObject[@"object"][@"value"]];
        [self.buyBtn setTitle:str forState:UIControlStateNormal];

    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        
    }];
    
    
}


#pragma mark- UITableViewDelegate, UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UpgradeYinKaVIPShowCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UpgradeYinKaVIPShowCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.nameLabel.text = @"享银卡会员专区货品专供\n\n享银卡会员专区银卡会员价\n\n享银卡会员产品需求订购";
    
    
    return cell;


    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return UITableViewAutomaticDimension;
}


#pragma mark - section头部间距
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {

    return 0.0000001;
}


#pragma mark - section头部视图
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {

    UIView *view= [[UIView alloc]initWithFrame:CGRectMake( 0, 0, KSCREEN_WIDTH, 0.00001)];
    view.backgroundColor = [UIColor whiteColor];
    return view;
}

#pragma mark - section底部间距
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {

    return 0.0001;
}

#pragma mark - section底部视图
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake( 0, 0, KSCREEN_WIDTH, 0)];
    view.backgroundColor = VCBgcolor;
    return view;
}


@end
