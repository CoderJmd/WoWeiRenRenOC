//
//  AboutWeVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/18.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "AboutWeVC.h"
#import "ServiceInfoVC.h"
#import "FuWuWebVC.h"

@interface AboutWeVC ()

@end

@implementation AboutWeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"关于我们";
    [self setUi];
}


- (void)setUi {
    
    if (NeediPhoneXiPhoneXR) {
        self.top.constant = NEW_StatusBarHeight;
    }
    
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *app_Version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    self.visionLabel.text = [NSString stringWithFormat:@"我为人人 %@v",app_Version];
    
    self.contentTF.text = @"    我为人人-我的会员店是由我为众多(江苏)科技有限公司创立于2019年。是一家主打会员制的线上电商平台。\n\n   我的会员店旨在为大家精选优质商品，保证正品。以高品质，低价格，注重用户体验为核心，将线下店，优质供应商和消费者紧密联系，形成优质的会员电商体系。";
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapClick)];
    [self.serviceLabel addGestureRecognizer:tap];
    
}


- (void)tapClick  {
    
    FuWuWebVC *vc = [[FuWuWebVC alloc]init];
    vc.title = @"服务条款";
    vc.linkPath = [[NSBundle mainBundle] pathForResource:@"服务条款-更新.docx" ofType:nil];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
