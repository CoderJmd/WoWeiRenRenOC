//
//  UserMyInfoViewController.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/18.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "UserMyInfoViewController.h"
#import "UserMyInforHeadView.h"
#import "UserMyInforShowTableViewCell.h"
#import "WRBaseButton.h"
#import "UserMyInforBottomView.h"
#import "RegisterPayVC.h"
#import "UpgradeYinKaVIPVC.h"

extern UserModel *userModel;
@interface UserMyInfoViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UserMyInforHeadView *headView;
@property (strong, nonatomic) UITableView *tableView;

@property (nonatomic,strong) NSArray *titleArrays;
@end


@implementation UserMyInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self setUi];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setUi) name:kUserInfoDetailsUpDate object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHidden:YES];
    
    
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setHidden:NO];

    
}

- (void)setUi {
    self.view.backgroundColor = VCBgcolor;
    [self.view addSubview:self.headView];
    [self.view addSubview:self.tableView];
    self.titleArrays = @[@"姓   名",@"身份证",@"手机号",@"性   别",@"地   区",@"生   日",@"星   座",@"学   历",@"邮   箱"];
    if (@available(iOS 11.0, *)) {
        self.tableView.estimatedRowHeight = 0;
        self.tableView.estimatedSectionFooterHeight = 0;
        self.tableView.estimatedSectionHeaderHeight= 0;
        self.tableView.contentInsetAdjustmentBehavior= UIScrollViewContentInsetAdjustmentNever;
    }
    
    [self setHeadData];
    [self.tableView reloadData];
}


- (void)setHeadData {
    
    NSString *time = [self ConvertStrToTime:userModel.vip_end_time];
    self.headView.endVipLabel.text = [NSString stringWithFormat:@"我为人人会员VIP将于%@到期",time];
    if (userModel.vip_type == 0) {
        
    }else if (userModel.vip_type == 1) {
        self.headView.vipTypeLabel.text = @"正式会员VIP";
        self.headView.vipTypeImgView.image = [UIImage imageNamed:@"zhengshihuiyuan"];
        self.headView.upgradeBtn.hidden = NO;
    }else if (userModel.vip_type == 2) {
        self.headView.vipTypeLabel.text = @"银卡会员";
        self.headView.vipTypeImgView.image = [UIImage imageNamed:@"yinkahuiyuan"];
        self.headView.upgradeBtn.hidden = YES;

    }else {
        self.headView.vipTypeLabel.text = @"试用会员VIP";
        self.headView.vipTypeImgView.image = [UIImage imageNamed:@"shiyomghuiyuan"];
        self.headView.upgradeBtn.hidden = YES;
    }
    
}

//时间戳变为格式时间
- (NSString *)ConvertStrToTime:(NSString *)timeStr
{

    long long time=[timeStr longLongValue] / 1000;

    NSDate *date = [[NSDate alloc]initWithTimeIntervalSince1970:time];

    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];

    [formatter setDateFormat:@"yyyy-MM-dd"];

    NSString*timeString=[formatter stringFromDate:date];

    return timeString;

}


- (UITableView *)tableView {
    if (!_tableView) {
        CGRect frame = CGRectMake(0.0f, 160+15 + NeedStatusViewHeight, kScreenW, kScreenH - 175 - NeedStatusViewHeight);
        UITableView *tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
            // 设置tableView的headView
        tableView.tableHeaderView = [UIView new];
        tableView.tableFooterView = [UIView new];
        tableView.separatorStyle = NO;
        tableView.delegate = self;
        tableView.dataSource = self;
        // 设置tableView的sectionHeadHeight为segmentViewHeight
        tableView.showsVerticalScrollIndicator = false;
        [tableView registerNib:[UINib nibWithNibName:@"UserMyInforShowTableViewCell" bundle:nil] forCellReuseIdentifier:@"UserMyInforShowTableViewCell"];
        
        _tableView = tableView;
        _tableView.backgroundColor  = VCBgcolor;
        
    }
    
    return _tableView;
}

- (UserMyInforHeadView *)headView {
    
    if (!_headView) {
        _headView = [UserMyInforHeadView getView];
        _headView.frame = CGRectMake(0, 0, kScreenW, 160 + NeedStatusViewHeight);
        [_headView.fanhuiBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        [_headView.upgradeBtn addTarget:self action:@selector(UpgradeYinKaClick) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _headView;
}

- (void)UpgradeYinKaClick {
    
    UpgradeYinKaVIPVC *vc =[[UpgradeYinKaVIPVC alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
    
}

#pragma mark- UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.titleArrays.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UserMyInforShowTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserMyInforShowTableViewCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.nameLabel.text = self.titleArrays[indexPath.row];
    cell.backgroundColor = VCBgcolor;
    tableView.rowHeight = 60;
    
    switch (indexPath.row) {
        case 0:
            {
                if ([CommonTTool isStrNull:userModel.name]) {
                    cell.contentLabel.text = @"未填写";

                }else {
                    cell.contentLabel.text = userModel.name;
                }

            }
            break;
        case 1:
            {
                cell.contentLabel.text  = userModel.idcard;
                if ([CommonTTool isStrNull:userModel.idcard]) {
                    cell.contentLabel.text  = @"未填写";
                }else {
                    cell.contentLabel.text  = userModel.idcard;
                }
            }
            break;
        case 2:
            {
                cell.contentLabel.text  = userModel.phone;;
                if ([CommonTTool isStrNull:userModel.phone]) {
                    cell.contentLabel.text  = @"未填写";
                }else {
                    cell.contentLabel.text  = userModel.phone;
                }
            }
            break;
        case 3:
            {
                if ([userModel.sex isEqualToString:@"1"]) {
                    cell.contentLabel.text = @"男";
                }else if([userModel.sex isEqualToString:@"2"]){
                    cell.contentLabel.text = @"女";
                }else {
                    cell.contentLabel.text = @"保密";
                }
                
            }
            break;
        case 4:
           {
               if ([CommonTTool isStrNull:userModel.province] && [CommonTTool isStrNull:userModel.city] && [CommonTTool isStrNull:userModel.area]) {
                  cell.contentLabel.text  = @"未填写";
               }else {
                  cell.contentLabel.text = [NSString stringWithFormat:@"%@%@%@",userModel.province,userModel.city,userModel.area];
                   
               }
           }
            break;
        case 5:
            {
                if ([CommonTTool isStrNull:userModel.birthday]) {
                   cell.contentLabel.text  = @"未填写";
                }else {
                   cell.contentLabel.text  = userModel.birthday;
                }
            }
             break;
        case 6:
           {
               
                if ([CommonTTool isStrNull:userModel.constellation]) {
                   cell.contentLabel.text  = @"未填写";
                }else {
                   cell.contentLabel.text  = userModel.constellation;
                }
           }
            break;
        case 7:
            {
                if ([CommonTTool isStrNull:userModel.education]) {
                     cell.contentLabel.text  = @"未填写";
                }else {
                     cell.contentLabel.text  = userModel.education;
                }
            }
            break;
        case 8:
           {
               if ([CommonTTool isStrNull:userModel.email]) {
                    cell.contentLabel.text  = @"未填写";
               }else {
                    cell.contentLabel.text  = userModel.phone;
               }
              
           }
            break;
            
        default:
            break;
    }
    
    return cell;
    
    
}


#pragma mark - section头部间距
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {

    return 0.0000001;
}


#pragma mark - section头部视图
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {

    UIView *view= [[UIView alloc]initWithFrame:CGRectMake( 0, 0, KSCREEN_WIDTH, 0.00001)];
    view.backgroundColor = [UIColor whiteColor];
    return view;
}

#pragma mark - section底部间距
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == 0) {
        
        return 60;
    }
    return 0.0001;
}

#pragma mark - section底部视图
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake( 0, 0, KSCREEN_WIDTH, 60)];
    view.backgroundColor = VCBgcolor;
    UserMyInforBottomView *userMyInforBottomView = [UserMyInforBottomView getView];
    if(userModel.vip_type == 1) {
        
        [userMyInforBottomView.xufeiBtn setTitle:@"续费正式会员" forState:UIControlStateNormal];
    }else if (userModel.vip_type == 2) {
        [userMyInforBottomView.xufeiBtn setTitle:@"续费银卡会员" forState:UIControlStateNormal];
    }else {
        [userMyInforBottomView.xufeiBtn setTitle:@"开通正式会员" forState:UIControlStateNormal];
    }
    
    [view addSubview:userMyInforBottomView];
    userMyInforBottomView.frame = view.bounds;
    [userMyInforBottomView.xufeiBtn addTarget:self action:@selector(xufeiClick) forControlEvents:UIControlEventTouchUpInside];
    
    CAGradientLayer *gl = [CAGradientLayer layer];
    gl.frame = CGRectMake(0, 0, kScreenW - 30, 40);
    gl.startPoint = CGPointMake(0.22, 0.36);
    gl.endPoint = CGPointMake(0.85, 0.72);
    gl.colors = @[(__bridge id)[UIColor colorWithRed:221/255.0 green:169/255.0 blue:72/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:189/255.0 green:135/255.0 blue:35/255.0 alpha:1.0].CGColor];
    gl.locations = @[@(0), @(1.0f)];

    [userMyInforBottomView.xufeiBtn.layer insertSublayer:gl atIndex:0];
    
    return view;
}


- (void)xufeiClick {
    
    RegisterPayVC *vc = [[RegisterPayVC alloc]initWithNibName:@"RegisterPayVC" bundle:nil];
    vc.type = 1;
    
    if(userModel.vip_type == 1) {
        vc.orderType = userModel.vip_type;
        vc.title  = @"续费正式会员";
    }else if (userModel.vip_type == 2) {
        vc.orderType = userModel.vip_type;
        vc.title  = @"续费银卡会员";
    }else {
        vc.orderType = 1;
        vc.title  = @"开通正式会员";
    }
    
    [self.navigationController pushViewController:vc animated:YES];
    
}

@end
