//
//  BusinessMineVC.m
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/17.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BusinessMineVC.h"
#import "UserMineHeadShowCell.h"
#import "BusinessMineHeadView.h"
#import "BusinessInfoVC.h"
#import "BusinessActivityListVC.h"
#import "AboutWeVC.h"
#import "UserSetingVC.h"
#import "BusinessJobListVC.h"
#import "CustomActivity.h"

@interface BusinessMineVC ()

@property (nonatomic, strong) BusinessMineHeadView *header;
@property (nonatomic, strong) UIView *footer;
@end

@implementation BusinessMineVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.dataArray = @[@{@"name":@"我的信息",@"icon":@"wodexinxi"},
    @{@"name":@"折扣商品",@"icon":@"zhekoushangping"},
    @{@"name":@"人员招聘",@"icon":@"renyuanzhaoping"},
    @{@"name":@"关于我们",@"icon":@"guanyuwomen"},
    @{@"name":@"分享APP",@"icon":@"fenxiang"},
    @{@"name":@"设置",@"icon":@"shezhi"}].mutableCopy;
    
    [self creatTableViewWithCellIdentity:@"UserMineHeadShowCell"];
    self.tableView.backgroundColor = kRGBColor(246, 246, 246);
    self.tableView.rowHeight = 50;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.frame = CGRectMake(0, -StatusViewHeight, kScreenW, kScreenH+StatusViewHeight);
    self.tableView.tableHeaderView = self.header;
    self.tableView.tableFooterView = self.footer;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserMineHeadShowCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserMineHeadShowCell"];
    cell.nameLabel.text = self.dataArray[indexPath.row][@"name"];
    cell.iconImageView.image = [UIImage imageNamed:self.dataArray[indexPath.row][@"icon"]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
        {
            BusinessInfoVC *vc = [[BusinessInfoVC alloc] init];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
        case 1:
        {
            BusinessActivityListVC *vc = [[BusinessActivityListVC alloc] init];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
        case 2:
        {
            BusinessJobListVC *vc = [[BusinessJobListVC alloc] init];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
        case 3:
        {
            AboutWeVC *vc = [[AboutWeVC alloc] init];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
        case 4:
        {
            [self share];
        }
        case 5: {
            UserSetingVC *vc = [[UserSetingVC alloc] init];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
            
        }
            break;
        default:
            break;
    }
}

- (BusinessMineHeadView *)header
{
    if (!_header) {
        _header = [[NSBundle mainBundle] loadNibNamed:@"BusinessMineHeadView" owner:self options:nil].lastObject;
        _header.frame = CGRectMake(0, 0, kScreenW, 266);
    }
    return _header;
}

- (UIView *)footer
{
    if (!_footer) {
        _footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, 60)];
        _footer.backgroundColor = kRGBColor(246, 246, 246);
        
        UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kScreenW, 40)];
        lab.textColor = kRGBColor(189, 135, 35);
        lab.font = kFont(14.0);
        lab.textAlignment = NSTextAlignmentCenter;
        NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:kUserMerchantsInfo];
        lab.text = [NSString stringWithFormat:@"入驻有效期至%@", userInfo[@"effective_time"]];
        [_footer addSubview:lab];
    }
    return _footer;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHidden:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setHidden:NO];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)share {
    
    // 1、设置分享的内容，并将内容添加到数组中
    NSString *shareText = @"点击链接即可下载我为人人APP:";
    NSURL *shareUrl = [NSURL URLWithString:@"http://api.ofajs.cn/ofaapi/index.html"];
    NSArray *activityItemsArray = @[shareText,shareUrl];
    
    // 自定义的CustomActivity，继承自UIActivity
    CustomActivity *customActivity = [[CustomActivity alloc]initWithTitle:shareText ActivityImage:[UIImage imageNamed:@""] URL:shareUrl ActivityType:@"Custom"];
    NSArray *activityArray = @[customActivity];
    
    // 2、初始化控制器，添加分享内容至控制器
    UIActivityViewController *activityVC = [[UIActivityViewController alloc]initWithActivityItems:activityItemsArray applicationActivities:activityArray];
    activityVC.modalInPopover = YES;
    // 3、设置回调
    if ([UIDevice currentDevice].systemVersion.floatValue >= 8.0) {
        // ios8.0 之后用此方法回调
        UIActivityViewControllerCompletionWithItemsHandler itemsBlock = ^(UIActivityType __nullable activityType, BOOL completed, NSArray * __nullable returnedItems, NSError * __nullable activityError){
            NSLog(@"activityType == %@",activityType);
            if (completed == YES) {
                NSLog(@"completed");
            }else{
                NSLog(@"cancel");
            }
        };
        activityVC.completionWithItemsHandler = itemsBlock;
    }else{
        // ios8.0 之前用此方法回调
        UIActivityViewControllerCompletionHandler handlerBlock = ^(UIActivityType __nullable activityType, BOOL completed){
            NSLog(@"activityType == %@",activityType);
            if (completed == YES) {
                NSLog(@"completed");
            }else{
                NSLog(@"cancel");
            }
        };
        activityVC.completionHandler = handlerBlock;
    }
    // 4、调用控制器
    [[CommonTTool getCurrentVC] presentViewController:activityVC animated:YES completion:nil];
    
}


@end
