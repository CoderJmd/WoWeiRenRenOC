//
//  ContactServiceVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/17.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "ContactServiceVC.h"
#import "ContactServiceHeadView.h"
#import "ContactServiceShowCell.h"
@interface ContactServiceVC ()
@property (nonatomic,strong) ContactServiceHeadView *headView;
@property (strong, nonatomic)UIView *footerView;
@end

@implementation ContactServiceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.defaultStartPage = 1;
    self.title = @"联系客服";
    [self creatTableViewWithCellIdentity:@"ContactServiceShowCell"];

    self.view.backgroundColor = WhiteColor;
    self.tableView.backgroundColor = WhiteColor;
    self.tableView.rowHeight = 50;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.frame = CGRectMake(0, NavBarHeight, kScreenW, kScreenH-NavBarHeight -NeedStatusViewHeight);
    self.tableView.tableHeaderView = self.headView;
    
    self.footerView = [[UIView alloc]init];
     self.footerView.frame = CGRectMake(0, 0, kScreenW, 70);
    
    self.tableView.tableFooterView =  self.footerView;
    
    UIButton *button = [[UIButton alloc]init];
    button.frame = CGRectMake(15, 30, kScreenW - 30, 40);
    button.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size:14];
    [button setTitle:@"拨打客服电话" forState:UIControlStateNormal];
    [button setTitleColor:kRGBColor(255, 246, 203) forState: UIControlStateNormal];
    [self.footerView addSubview:button];
    CAGradientLayer *gl = [CAGradientLayer layer];
    gl.frame = button.bounds;
    gl.startPoint = CGPointMake(0.22, 0.36);
    gl.endPoint = CGPointMake(0.85, 0.72);
    gl.colors = @[(__bridge id)[UIColor colorWithRed:221/255.0 green:169/255.0 blue:72/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:189/255.0 green:135/255.0 blue:35/255.0 alpha:1.0].CGColor];
    gl.locations = @[@(0), @(1.0f)];
    button.layer.cornerRadius = 4;
    button.clipsToBounds = YES;
    [button.layer insertSublayer:gl atIndex:0];
    [button addTarget:self action:@selector(phoneTapClick) forControlEvents:UIControlEventTouchUpInside];
    [self requestData];
}

- (void)requestData {
    
    int type = 2;
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"kUserIsMerchants"]) {
        type = 4;
    }
    
    [NetRequestTool postUrl:@"sysCode/querySysCodeObject" parameters:@{@"type":@(type)} success:^(id responseObject) {
        self.model = [SystemDictionaryModel mj_objectWithKeyValues:responseObject[@"object"]];
        [self.tableView reloadData];
        SD_SETIMAGE_PlaceHolder(self.headView.codeImgView, self.model.depict, @"default_image_square")

    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        
    }];
    
    
}



- (ContactServiceHeadView *)headView {
    
    
    if (!_headView) {
        _headView = [[NSBundle mainBundle] loadNibNamed:@"ContactServiceHeadView" owner:self options:nil].lastObject;
        _headView.frame = CGRectMake(0, 0, kScreenW, 350);
        
    }
    
    return _headView;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContactServiceShowCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ContactServiceShowCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.row == 0) {
        
        cell.nameLabel.text = @"客服微信号";
        cell.contentLabel.text = self.model.content;
        
    }else {
        cell.nameLabel.text = @"客服电话";
        cell.contentLabel.text = self.model.value;
        
    }
    return cell;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 2;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];

        pasteboard.string = self.model.content;
        
        SVP(@"微信号已复制至剪贴板");
    }
    
}

- (void)phoneTapClick {
    
    NSString *url = [NSString stringWithFormat:@"tel://%@",self.model.value];
    [[UIApplication   sharedApplication] openURL:[NSURL URLWithString:url]];
    
}



@end
