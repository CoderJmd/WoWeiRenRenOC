//
//  AddBusinessJobVC.m
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/18.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "AddBusinessJobVC.h"

@interface AddBusinessJobVC ()

@property (weak, nonatomic) IBOutlet UITextField *jobTitle;
@property (weak, nonatomic) IBOutlet UITextField *salaryLab;
@property (weak, nonatomic) IBOutlet IQTextView *introduceText;

@property (weak, nonatomic) IBOutlet UIButton *saveBtn;

@end

@implementation AddBusinessJobVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.saveBtn.layer.cornerRadius = 4;
    
    if (self.model) {
        self.title = @"编辑招聘";
        self.jobTitle.text = self.model.recruit_name;
        self.salaryLab.text = self.model.wages;
        self.introduceText.text = self.model.descriptionText;
        
        UIBarButtonItem *deleteBtn = [[UIBarButtonItem alloc] initWithTitle:@"删除" style:UIBarButtonItemStyleDone target:self action:@selector(rightBarButtonClick)];
        self.navigationItem.rightBarButtonItem = deleteBtn;
    }
    else {
        self.title = @"新增招聘";
    }
}

- (void)rightBarButtonClick
{
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"提示" message:@"是否删除该招聘" preferredStyle:UIAlertControllerStyleAlert];
    WeakSelf(ws)
    [alertController addAction: [UIAlertAction actionWithTitle: @"确定" style: UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [ws deleteJob];
    }]];
    [alertController addAction: [UIAlertAction actionWithTitle: @"取消" style: UIAlertActionStyleCancel handler:nil]];
    [self presentViewController: alertController animated:YES completion:nil];
}

- (void)deleteJob
{
    SVPShowInternetWaiting
    [NetRequestTool postUrl:@"business/deleteRecruitById" parameters:@{@"recruit_id":@(self.model.recruit_id)} success:^(id responseObject) {
        SVP(@"删除成功");
        [self.parentsVC needRefresh];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.navigationController popViewControllerAnimated:YES];
        });
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        SVPDismiss
    }];
}

- (IBAction)saveJobClick {
    NSString *jobStr = self.jobTitle.text;
    if ([CommonTTool isBlankString:jobStr]) {
        SVP(@"请输入职位名称");
        return;
    }
    NSString *salaryStr = self.salaryLab.text;
    if ([CommonTTool isBlankString:salaryStr]) {
        SVP(@"请输入工资");
        return;
    }
    NSString *contentStr = self.introduceText.text;
    if ([CommonTTool isBlankString:contentStr]) {
        SVP(@"请输入职位描述");
        return;
    }
    
    SVPShowInternetWaiting
    NSString *hitStr = @"添加成功";
    NSDictionary *para = @{@"recruit_name":jobStr, @"wages":salaryStr, @"description":contentStr};
    NSString *urlStr = @"business/saveRecruit";
    if (self.model) {
        hitStr = @"修改成功";
        para = @{@"recruit_id":@(self.model.recruit_id), @"recruit_name":jobStr, @"wages":salaryStr, @"description":contentStr};
        urlStr = @"business/updateRecruit";
    }
    [NetRequestTool postUrl:urlStr parameters:para success:^(id responseObject) {
        SVP(hitStr)
        [self.parentsVC needRefresh];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.navigationController popViewControllerAnimated:YES];
        });
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        SVPDismiss
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
