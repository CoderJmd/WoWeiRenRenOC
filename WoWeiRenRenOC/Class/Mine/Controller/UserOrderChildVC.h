//
//  UserOrderChildVC.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/13.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserOrderChildVC : BaseTableViewController

@property (strong, nonatomic)NSString *order_state;
@property (strong, nonatomic)NSString *order_type;
@property (strong, nonatomic)NSString *tg_group_state;
@property (nonatomic,assign) CGFloat topInset;

@end

NS_ASSUME_NONNULL_END
