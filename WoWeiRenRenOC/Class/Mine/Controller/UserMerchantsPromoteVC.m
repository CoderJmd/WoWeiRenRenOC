//
//  UserMerchantsPromoteVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/19.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "UserMerchantsPromoteVC.h"
#import "UserMerchantsPromoteHeadView.h"
#import "UserMerchantsPromoteShowCell.h"
#import "UserMerchantsPromoteDetailsVC.h"
#import "UserMerchantsPromoteDetailsVC.h"
#import "UserMerchantsPromoteModel.h"



@interface UserMerchantsPromoteVC ()

@property (nonatomic,strong) UserMerchantsPromoteHeadView *headView;

@property (nonatomic,strong) NSString *totalCount;
@end

@implementation UserMerchantsPromoteVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"商家推广";
    
    self.defaultStartPage = 1;
    [self creatTableViewWithCellIdentity:@"HomeCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"UserMerchantsPromoteShowCell" bundle:nil] forCellReuseIdentifier:@"UserMerchantsPromoteShowCell"];
    self.tableView.backgroundColor = kRGBColor(246, 246, 246);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.frame = CGRectMake(0, 0, kScreenW, kScreenH);
    
    [self setRefresh];
    SVPShowInternetWaiting
    [self requestData];
    
    WRBaseButton *button = [[WRBaseButton alloc]init];
    button.frame = CGRectMake(15, kScreenH - 55, kScreenW - 30, 40);
    [button setTitle:@"推荐商家入驻" forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:14];
    [button setUi];
    [self.view addSubview:button];
    [button setBackgroundColor:kRGBColor(221, 169, 72)];
    [button addTarget:self action:@selector(addMerchantsClcik) forControlEvents:UIControlEventTouchUpInside];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(businessCheckIn) name:@"BusinessCheckIn" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(businessCheckIn) name:@"BusinessInfoUpDate" object:nil];
    self.tableView.backgroundColor = [UIColor whiteColor];
    
}

- (void)businessCheckIn {
    
    self.page = 1;
    [self requestData];
    
}

- (void)addMerchantsClcik {
    
    UserMerchantsPromoteDetailsVC *vc = [[UserMerchantsPromoteDetailsVC alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
    
}


- (void)setHeadUi {
    

    self.headView.coutLabel.text = [NSString stringWithFormat:@"%@",self.totalCount];

    
}


- (void)requestData
{
    // 请求列表
    WeakSelf(ws)
    [NetRequestTool postUrl:@"business/queryBusinessList" parameters:@{@"page":@(self.page), @"limit":@"10",@"is_tj":@"1"} success:^(id responseObject) {
       
        if (self.page == 1) {
            ws.dataArray = [UserMerchantsPromoteModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]];
            
        }else {
            NSArray *newArray = [UserMerchantsPromoteModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]];
            [ws.dataArray addObjectsFromArray:newArray];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [ws.tableView  reloadData];
        });
        
        if (self.dataArray.count == 0) {
            self.totalCount = @"0";
            [self endRefresh];
            [self endRefreshNoMoreData];
            return ;
        }
        [self setHeadUi];
        
        
        // 控制页数
        NSDictionary *page = [responseObject objectForKey:@"page"];
        self.totalCount = page[@"total_count"];
        [self setHeadUi];
        if ([page isKindOfClass:NSDictionary.class]) {
            NSInteger totalP = [page[@"total_page"] integerValue];
            if (totalP > self.page) {
                self.page++;
                [self endRefresh];
            }
            else {
                [self endRefreshNoMoreData];
            }
        }
        else {
            [self endRefreshNoMoreData];
        }
    } failure:^(NSInteger statusCode) {
//        [ws endRefreshNoMoreData];
    } finish:^{
        SVPDismiss
    }];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserMerchantsPromoteShowCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserMerchantsPromoteShowCell"];
    cell.model = self.dataArray[indexPath.row];
    tableView.rowHeight = 124;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    BusinessModel *model = self.dataArray[indexPath.row];
    if (model.sh_state == 1) {
        SVP(@"正在审核中，请耐心等待");
        return;
    }
    
    
    UserMerchantsPromoteDetailsVC *vc = [[UserMerchantsPromoteDetailsVC alloc]init];
    
    vc.model= self.dataArray[indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
    
    
}

#pragma mark - section头部间距
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {

    return 165;
}


#pragma mark - section头部视图
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {

    UIView *view= [[UIView alloc]initWithFrame:CGRectMake( 0, 0, KSCREEN_WIDTH, 165)];
    view.backgroundColor = VCBgcolor;
    [view addSubview:self.headView];
    return view;
}

#pragma mark - section底部间距
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 0.0001;
}

#pragma mark - section底部视图
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake( 0, 0, KSCREEN_WIDTH, 0)];
    view.backgroundColor = VCBgcolor;
    return view;
}


- (UserMerchantsPromoteHeadView *)headView {
    
    if (!_headView) {
        _headView = [UserMerchantsPromoteHeadView getView];
        _headView.frame = CGRectMake(0, 0, kScreenW, 165);
    }
    
    return _headView;
    
}

@end
