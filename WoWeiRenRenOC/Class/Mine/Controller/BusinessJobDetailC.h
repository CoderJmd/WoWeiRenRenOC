//
//  BusinessJobDetailC.h
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/19.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseTableViewController.h"
#import "BusinessJobModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BusinessJobDetailC : BaseTableViewController

@property (nonatomic, strong) BusinessJobModel *model;

@end

NS_ASSUME_NONNULL_END
