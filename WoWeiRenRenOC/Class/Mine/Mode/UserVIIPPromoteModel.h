//
//  UserVIIPPromoteModel.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/18.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserVIIPPromoteModel : BaseModel
@property (strong, nonatomic)NSString *create_time;
@property (strong, nonatomic)NSString *idcard;
@property (strong, nonatomic)NSString *name;
@property (strong, nonatomic)NSString *username;

@end

NS_ASSUME_NONNULL_END
