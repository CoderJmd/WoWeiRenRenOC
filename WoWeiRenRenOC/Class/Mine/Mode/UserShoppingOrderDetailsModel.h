//
//  UserShoppingOrderDetailsModel.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/28.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseModel.h"
#import "UserShoppingOrderModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserShoppingOrderDetailsModel : BaseModel
@property (assign, nonatomic)int pay_state;
@property (assign, nonatomic)int num;
@property (assign, nonatomic)int is_pt_type;
@property (assign, nonatomic)int tg_group_state;
@property (assign, nonatomic)int pay_type;
@property (assign, nonatomic)int order_type;
@property (assign, nonatomic)float pay_price;
@property (assign, nonatomic)int zxtg_state;
@property (assign, nonatomic)int sh_state;
@property (assign, nonatomic)float freight_price;
@property (assign, nonatomic)int parent_order_code;
@property (assign, nonatomic)float sku_price;
@property (assign, nonatomic)int order_state;
@property (assign, nonatomic)int business_state;
@property (assign, nonatomic)int is_td;//拼团失败产生退单状态 1未申请 2申请退单 3审核不通过 4 审核通过 5第三方退单中 6退单成功 7退单失败

@property (strong, nonatomic)NSString *express_no;
@property (strong, nonatomic)NSString *head_path;
@property (strong, nonatomic)NSString *business_name;
@property (strong, nonatomic)NSString *create_time;
@property (strong, nonatomic)NSString *sku_order_id;
@property (strong, nonatomic)NSString *tg_group_id;
@property (strong, nonatomic)NSString *express_org;
@property (strong, nonatomic)NSString *order_code;
@property (strong, nonatomic)NSString *user_id;
@property (strong, nonatomic)NSString *business_id;
@property (strong, nonatomic)NSString *sq_refund_time;
@property (strong, nonatomic)NSString *refund_time;
@property (strong, nonatomic)NSString *remark;
@property (strong, nonatomic)NSArray *sku_order_detail_list;


@property (strong, nonatomic)NSString *address_id;
@property (strong, nonatomic)NSString *add_name;
@property (strong, nonatomic)NSString *add_province;
@property (strong, nonatomic)NSString *add_city;
@property (strong, nonatomic)NSString *add_area;
@property (strong, nonatomic)NSString *add_phone;
@property (strong, nonatomic)NSString *add_address;
@property (strong, nonatomic)NSString *pay_time;
@property (strong, nonatomic)NSString *fh_time;
@property (strong, nonatomic)NSString *sh_time;
@property (strong, nonatomic)NSString *last_pay_time;




@end

NS_ASSUME_NONNULL_END
