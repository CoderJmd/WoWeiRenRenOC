//
//  LogisticsModel.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/9/11.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface LogisticsModel : BaseModel
@property (strong, nonatomic)NSString *order_code;

@property (strong, nonatomic)NSString *pay_time;
@property (strong, nonatomic)NSArray *express_order_detail_list;
@end

@interface express_order_detail : BaseModel
@property (strong, nonatomic)NSString *img_path;
@property (strong, nonatomic)NSString *sku_id;
@property (strong, nonatomic)NSString *sku_name;
@property (strong, nonatomic)NSString *sku_code;
@property (strong, nonatomic)NSString *express_org;
@property (strong, nonatomic)NSString *express_no;
@end
NS_ASSUME_NONNULL_END

