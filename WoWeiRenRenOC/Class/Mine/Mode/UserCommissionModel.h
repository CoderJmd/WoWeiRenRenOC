//
//  UserCommissionModel.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/8/24.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserCommissionModel : BaseModel
@property (strong, nonatomic)NSString *create_time;
@property (strong, nonatomic)NSString *balance;
@property (assign, nonatomic)int state;
@property (assign, nonatomic)int type;

@property (strong, nonatomic)NSString *title;
@property (strong, nonatomic)NSString *balance_info_id;
@property (strong, nonatomic)NSString *fk_id;
@property (strong, nonatomic)NSString *user_id;


@end

NS_ASSUME_NONNULL_END
