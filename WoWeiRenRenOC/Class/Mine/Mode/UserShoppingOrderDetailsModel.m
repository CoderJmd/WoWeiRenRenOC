//
//  UserShoppingOrderDetailsModel.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/28.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "UserShoppingOrderDetailsModel.h"
#import "MailGoodsDetailsModel.h"

@implementation UserShoppingOrderDetailsModel
+ (NSDictionary *)objectClassInArray
{
    return @{@"sku_order_detail_list":[MailGoodsDetailsModel class]};
}
@end
