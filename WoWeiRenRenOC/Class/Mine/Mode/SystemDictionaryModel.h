//
//  SystemDictionaryModel.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/7/1.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SystemDictionaryModel : BaseModel
@property (strong, nonatomic)NSString *name;
@property (strong, nonatomic)NSString *value;
@property (strong, nonatomic)NSString *content;
@property (strong, nonatomic)NSString *depict;



@end

NS_ASSUME_NONNULL_END
