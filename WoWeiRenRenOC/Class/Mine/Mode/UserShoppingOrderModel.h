//
//  UserShoppingOrderModel.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/26.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserShoppingOrderModel : BaseModel

@property (assign, nonatomic)int pay_state;
@property (assign, nonatomic)int num;
@property (assign, nonatomic)int is_pt_type;
@property (assign, nonatomic)int tg_group_state;
@property (assign, nonatomic)int pay_type;
@property (assign, nonatomic)int order_type;
@property (assign, nonatomic)float pay_price;
@property (assign, nonatomic)int zxtg_state;
@property (assign, nonatomic)int sh_state;
@property (assign, nonatomic)float freight_price;
@property (assign, nonatomic)int parent_order_code;
@property (assign, nonatomic)float sku_price;
@property (assign, nonatomic)int order_state;
@property (assign, nonatomic)int business_state;
@property (assign, nonatomic)int is_td;
@property (assign, nonatomic)float t_total_pay_price;
@property (strong, nonatomic)NSString *express_no;
@property (strong, nonatomic)NSString *head_path;
@property (strong, nonatomic)NSString *business_name;
@property (strong, nonatomic)NSString *create_time;
@property (strong, nonatomic)NSString *sku_order_id;
@property (strong, nonatomic)NSString *tg_group_id;
@property (strong, nonatomic)NSString *express_org;
@property (strong, nonatomic)NSString *order_code;
@property (strong, nonatomic)NSString *user_id;
@property (strong, nonatomic)NSString *business_id;
@property (strong, nonatomic)NSString *sq_refund_time;
@property (strong, nonatomic)NSString *refund_time;

@property (strong, nonatomic)NSArray *sku_order_detail_list;
@property (strong, nonatomic)NSString *last_pay_time;

@end





NS_ASSUME_NONNULL_END
