//
//  DiscountModel.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/19.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface DiscountModel : BaseModel
@property (strong, nonatomic)NSString *depict;
@property (strong, nonatomic)NSString *name;
@property (strong, nonatomic)NSString *value;
@property (strong, nonatomic)NSString *content;
@property (strong, nonatomic)NSString *Id;

@property (strong, nonatomic)NSString *classify_name;
@property (strong, nonatomic)NSString *classify_id;
@end

NS_ASSUME_NONNULL_END
