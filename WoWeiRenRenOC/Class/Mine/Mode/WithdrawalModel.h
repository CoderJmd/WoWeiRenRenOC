//
//  WithdrawalModel.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/27.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface WithdrawalModel : BaseModel
@property (strong, nonatomic)NSString *create_time;
@property (strong, nonatomic)NSString *money;
@property (strong, nonatomic)NSString *state;
@property (strong, nonatomic)NSString *title;
@property (strong, nonatomic)NSString *cash_out_id;


@end

NS_ASSUME_NONNULL_END
