//
//  UserShipingAddressModel.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/19.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserShipingAddressModel : BaseModel
@property (strong, nonatomic)NSString *address_id;
@property (strong, nonatomic)NSString *area;
@property (strong, nonatomic)NSString *province;
@property (strong, nonatomic)NSString *address;
@property (strong, nonatomic)NSString *city;
@property (strong, nonatomic)NSString *phone;
@property (strong, nonatomic)NSString *name;
@property (strong, nonatomic)NSString *def;
@end

NS_ASSUME_NONNULL_END
