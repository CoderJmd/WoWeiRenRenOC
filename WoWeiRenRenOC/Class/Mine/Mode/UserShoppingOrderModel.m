//
//  UserShoppingOrderModel.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/26.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "UserShoppingOrderModel.h"
#import "MailGoodsDetailsModel.h"

@implementation UserShoppingOrderModel

+ (NSDictionary *)objectClassInArray
{
    return @{@"sku_order_detail_list":[MailGoodsDetailsModel class]};
}

@end

