//
//  SearcherTextField.m
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/19.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "SearcherTextField.h"

@implementation SearcherTextField

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (CGRect)leftViewRectForBounds:(CGRect)bounds
{
    CGRect iconRect = [super leftViewRectForBounds:bounds];
    iconRect.origin.x += 10; //像右边偏10
    return iconRect;
}

//UITextField 文字与输入框的距离
- (CGRect)textRectForBounds:(CGRect)bounds{
    return CGRectInset(bounds, 35, 0);
}

//控制文本的位置
- (CGRect)editingRectForBounds:(CGRect)bounds{
    return CGRectInset(bounds, 35, 0);
}

@end
