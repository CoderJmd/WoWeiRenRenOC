//
//  UserOrderChildFooterView.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/26.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "UserOrderChildFooterView.h"
#import "OrderPayVC.h"
#import "AbulkDetailsVC.h"
#import "PTOrderDetailsVC.h"
#import "UserShoppingOrderDetailsLogisticsView.h"
#import "QueryGoodsLogisticsVC.h"

@implementation UserOrderChildFooterView

- (void)awakeFromNib {
    
    [super awakeFromNib];
    self.oneBtn.layer.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0].CGColor;
    self.oneBtn.layer.cornerRadius = 14.5;
    self.oneBtn.clipsToBounds = YES;
    self.oneBtn.layer.borderWidth = 1;
    self.oneBtn.layer.borderColor = kRGBAColor(206, 160, 9, 1).CGColor;
    
    self.twoBtn.layer.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0].CGColor;
    self.twoBtn.layer.cornerRadius = 14.5;
    self.twoBtn.clipsToBounds = YES;
    self.twoBtn.layer.borderWidth = 1;
    self.twoBtn.layer.borderColor = kRGBAColor(206, 160, 9, 1).CGColor;
    
    self.deleteBtn.layer.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0].CGColor;
    self.deleteBtn.layer.cornerRadius = 14.5;
    self.deleteBtn.clipsToBounds = YES;
    self.deleteBtn.layer.borderWidth = 1;
    self.deleteBtn.layer.borderColor = kRGBAColor(206, 160, 9, 1).CGColor;
    
}


- (void)setModel:(UserShoppingOrderModel *)model {
    
    _model = model;
    self.numLabel.text = [NSString stringWithFormat:@"共%lu件",(unsigned long)model.sku_order_detail_list.count];
    self.priceLabel.text = [CommonTTool getPrice:model.pay_price];
    self.timeLabel.hidden = YES;
    self.oneLabel.hidden = YES;
    self.twoBtn.hidden = YES;
    self.deleteBtn.hidden = YES;
    self.deleteBtnLeft.constant =  100;

    [self.twoBtn addTarget:self action:@selector(showLogisticsView) forControlEvents:UIControlEventTouchUpInside];
   
    if (model.order_state == 0) {
        self.oneBtn.hidden  = YES;
        self.deleteBtn.hidden  = NO;
       self.deleteBtn.hidden = NO;
       [self.deleteBtn setTitle:@"删除订单" forState:UIControlStateNormal];
       [self.deleteBtn addTarget:self action:@selector(deleteBtnClick) forControlEvents:UIControlEventTouchUpInside];
   }
    
    if (model.order_state == 1) {
        [self.oneBtn setTitle:@"立即付款" forState:UIControlStateNormal];
        self.oneLabel.hidden = NO;
        self.timeLabel.hidden = NO;
        self.twoBtn.hidden = YES;
        [self.oneBtn addTarget:self action:@selector(paymentBtnClick) forControlEvents:UIControlEventTouchUpInside];
        self.deleteBtn.hidden = NO;
        [self.deleteBtn setTitle:@"取消订单" forState:UIControlStateNormal];
        [self.deleteBtn addTarget:self action:@selector(cancelBtnClick) forControlEvents:UIControlEventTouchUpInside];
        self.deleteBtnLeft.constant =  100;
        [self setEndTime];
        
    }
    
    if (model.order_state == 4) {
        [self.oneBtn setTitle:@"确认收货" forState:UIControlStateNormal];
        [self.oneBtn addTarget:self action:@selector(goodsBtnClick) forControlEvents:UIControlEventTouchUpInside];
        self.oneLabel.hidden = YES;
        self.timeLabel.hidden = YES;
        self.twoBtn.hidden = NO;
        self.deleteBtnLeft.constant =  15;


    }
    
    if (model.order_state == 3 && model.order_type == 4) {
        
        [self.oneBtn setTitle:@"查看进度" forState:UIControlStateNormal];
        self.oneLabel.hidden = NO;
        self.twoBtn.hidden = YES;
        [self.oneBtn addTarget:self action:@selector(queryPtDetailsClick) forControlEvents:UIControlEventTouchUpInside];
        self.deleteBtnLeft.constant =  15;

    }
    
    if (model.order_state == 5) {
        
        self.deleteBtn.hidden = NO;
        [self.deleteBtn setTitle:@"删除订单" forState:UIControlStateNormal];
        [self.deleteBtn addTarget:self action:@selector(deleteBtnClick) forControlEvents:UIControlEventTouchUpInside];
        self.oneBtn.hidden  = YES;
        
    }
    
    
    
    
    
}

#pragma mark - 取消订单
- (void)cancelBtnClick {
    
    // 系统提示框
    UIAlertController *alterController = [UIAlertController alertControllerWithTitle:nil message:@"是否取消该订单？" preferredStyle:UIAlertControllerStyleAlert];
    [alterController addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
        
    }]];
    
    [alterController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        SVPShowInternetWaiting;
        [NetRequestTool postUrl:@"skuOrder/cancelOrder" parameters:@{@"order_code":self.model.order_code} success:^(id responseObject) {
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kUserOrderTableUpDate object:nil];
           
        } failure:^(NSInteger statusCode) {
           
        } finish:^{
           
        }];
        
    }]];
    
    [[CommonTTool getCurrentVC] presentViewController:alterController animated:YES completion:nil];
    
}


#pragma mark - 删除订单
- (void)deleteBtnClick {
    
    // 系统提示框
    UIAlertController *alterController = [UIAlertController alertControllerWithTitle:nil message:@"是否删除该订单？" preferredStyle:UIAlertControllerStyleAlert];
    [alterController addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
        
    }]];
    
    [alterController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        SVPShowInternetWaiting;
        [NetRequestTool postUrl:@"skuOrder/deleteOrder" parameters:@{@"order_code":self.model.order_code} success:^(id responseObject) {
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kUserOrderTableUpDate object:nil];
           
        } failure:^(NSInteger statusCode) {
           
        } finish:^{
           
        }];
        
    }]];
    
    [[CommonTTool getCurrentVC] presentViewController:alterController animated:YES completion:nil];
    
}

#pragma mark - 确认收货
- (void)goodsBtnClick {
    
    // 系统提示框
    UIAlertController *alterController = [UIAlertController alertControllerWithTitle:nil message:@"是否确认收货？" preferredStyle:UIAlertControllerStyleAlert];
    [alterController addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
        
    }]];
    
    [alterController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        SVPShowInternetWaiting;
        [NetRequestTool postUrl:@"skuOrder/confirmShOrder" parameters:@{@"order_code":self.model.order_code} success:^(id responseObject) {
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kUserOrderTableUpDate object:nil];
           
        } failure:^(NSInteger statusCode) {
           
        } finish:^{
           
        }];
        
    }]];
    
    [[CommonTTool getCurrentVC] presentViewController:alterController animated:YES completion:nil];
    
}


#pragma mark - 显示物流编号
- (void)showLogisticsView {
    
//    UserShoppingOrderDetailsLogisticsView*view = [UserShoppingOrderDetailsLogisticsView showView];
//    view.nameLabel.text = self.model.express_org;
//    view.codeLabel.text = self.model.express_no;
    
    QueryGoodsLogisticsVC *vc = [[QueryGoodsLogisticsVC alloc]init];
    vc.order_code = self.model.order_code;
    [[CommonTTool getCurrentVC].navigationController pushViewController:vc animated:YES];
    
}


#pragma mark - 设置倒计时
- (void)setEndTime {
    
    NSString *deadlineStr = self.model.last_pay_time;
    // 倒计时的时间 测试数据
    //    NSString *deadlineStr = @"2020-06-24 12:00:00";
    // 当前时间的时间戳
    NSString *nowStr = [CommonTTool getCurrentTimeyyyymmdd];
    // 计算时间差值
    NSInteger secondsCountDown = [CommonTTool getDateDifferenceWithNowDateStr:nowStr deadlineStr:deadlineStr];
    
    if (secondsCountDown <= 0) {
        return;
    }
    __weak __typeof(self) weakSelf = self;
    if (_timer == nil) {
        __block NSInteger timeout = secondsCountDown; // 倒计时时间
        
        if (timeout!=0) {
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
            dispatch_source_set_timer(_timer, dispatch_walltime(NULL, 0), 1.0*NSEC_PER_SEC,  0); //每秒执行
            dispatch_source_set_event_handler(_timer, ^{
                if(timeout <= 0){ //  当倒计时结束时做需要的操作: 关闭 活动到期不能提交
                    dispatch_source_cancel(self->_timer);
                    self->_timer = nil;
                    dispatch_async(dispatch_get_main_queue(), ^{
                       // weakSelf.timeLabel.text = @"当前活动已结束";
                        [[NSNotificationCenter defaultCenter] postNotificationName:kUserOrderTableUpDate object:nil];
                    });
                } else { // 倒计时重新计算 时/分/秒
                    NSInteger days = (int)(timeout/(3600*24));
                    NSInteger hours = (int)((timeout-days*24*3600)/3600);
                    NSInteger minute = (int)(timeout-days*24*3600-hours*3600)/60;
                    NSInteger second = timeout - days*24*3600 - hours*3600 - minute*60;
                    NSString *strTime = [NSString stringWithFormat:@"%02ld:%02ld:%02ld ", hours, minute,second];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSString *ptCountStr = [NSString stringWithFormat:@"付款倒计时：%@",strTime];
                        NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:ptCountStr];
                        [attri addAttribute:NSForegroundColorAttributeName value: [UIColor redColor] range:NSMakeRange(6, strTime.length)];
                        self.timeLabel.attributedText =attri;
                        
                        
                    });
                    timeout--; // 递减 倒计时-1(总时间以秒来计算)
                }
            });
            dispatch_resume(_timer);
        }
    }
    
}


#pragma mark - 查询拼团进度
- (void)queryPtDetailsClick {
    
    PTOrderDetailsVC *vc = [[PTOrderDetailsVC alloc]init];
    vc.order_code = self.model.order_code;

    [[CommonTTool getCurrentVC].navigationController pushViewController:vc animated:YES];
    
}

#pragma mark - 立即支付
- (void)paymentBtnClick {
    
    
    
    MailGoodsDetailsModel *model = self.model.sku_order_detail_list[0];
    
    SVPShowInternetWaiting;
    [NetRequestTool postUrl:@"skuOrder/getPayOrderCode" parameters:@{@"order_code":self.model.order_code} success:^(id responseObject) {
        NSString *order = responseObject[@"object"];
        OrderPayVC *vc = [[OrderPayVC alloc]init];
        vc.type = self.model.order_type;
        vc.parent_order_code = order;
        vc.is_ht = model.is_ht;
        [[CommonTTool getCurrentVC].navigationController pushViewController:vc animated:YES];
        
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        
    }];
    
   
    
}
@end
