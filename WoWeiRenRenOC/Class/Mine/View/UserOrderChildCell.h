//
//  UserOrderChildCell.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/13.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserShoppingOrderModel.h"
#import "MailGoodsDetailsModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserOrderChildCell : UITableViewCell
@property (strong, nonatomic)MailGoodsDetailsModel *model;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *numLabel;
@property (weak, nonatomic) IBOutlet UILabel *reFundLabel;
@property (weak, nonatomic) IBOutlet UILabel *reFundCoutnLabel;


@end

NS_ASSUME_NONNULL_END
