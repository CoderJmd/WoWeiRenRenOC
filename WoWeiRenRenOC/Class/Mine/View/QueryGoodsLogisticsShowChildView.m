//
//  QueryGoodsLogisticsShowChildView.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/12/28.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "QueryGoodsLogisticsShowChildView.h"
#import "QueryGoodsLogisticsShowChildView.h"

@implementation QueryGoodsLogisticsShowChildView

+(instancetype)getView {
    
    return [[[NSBundle mainBundle] loadNibNamed:@"QueryGoodsLogisticsShowChildView" owner:nil options:nil] lastObject];
    
}
- (IBAction)addBtn:(id)sender {
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];

    pasteboard.string = self.nameLabel.text;

    SVP(@"物流单号已复制");
    
}

-(void)layoutSubviews
{
    [super layoutSubviews];

    CGRect frame = self.frame;
    CGFloat w = kScreenW - 195;

   [self setFrame:CGRectMake(frame.origin.x, frame.origin.y, w, 20)];
}

@end
