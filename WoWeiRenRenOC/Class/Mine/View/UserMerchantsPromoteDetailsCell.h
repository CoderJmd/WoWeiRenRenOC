//
//  UserMerchantsPromoteDetailsCell.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/19.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DiscountModel.h"
#import "BusinessModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserMerchantsPromoteDetailsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *nameLabel;
@property (weak, nonatomic) IBOutlet UITextField *merchantsPhoneLabel;
@property (weak, nonatomic) IBOutlet UITextField *merchantsTypeLabel;
@property (weak, nonatomic) IBOutlet UIButton *merchantsTypeBtn;
@property (weak, nonatomic) IBOutlet UITextField *merchantsZheKouLabel;
@property (weak, nonatomic) IBOutlet UIButton *merchantsZheKouBtn;
@property (weak, nonatomic) IBOutlet UITextField *merchantsAddressLabel;

@property (weak, nonatomic) IBOutlet UIButton *merchantsAddressBtn;
@property (weak, nonatomic) IBOutlet UITextField *contactsLabel;

@property (weak, nonatomic) IBOutlet UITextField *phoneLabel;
@property (weak, nonatomic) IBOutlet UITextField *passwordLabel;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordLabel;
@property (weak, nonatomic) IBOutlet UITextField *business_license_noLabel;
@property (weak, nonatomic) IBOutlet UITextField *descriptionLabel;

@property (weak, nonatomic) IBOutlet UIButton *business_license_pathBtn;
@property (weak, nonatomic) IBOutlet UIButton *head_pathBtn;
@property (weak, nonatomic) IBOutlet WRBaseButton *submitBtn;
@property (strong, nonatomic)DiscountModel *zhekouModel;
@property (strong, nonatomic)DiscountModel *typeModel;

@property (strong, nonatomic)NSString *business_license_path;
@property (strong, nonatomic)NSString *head_path;
@property (strong, nonatomic)BusinessModel *model;
@property (weak, nonatomic) IBOutlet UILabel *passwordTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *passwordTextLabelOnt;
@property (weak, nonatomic) IBOutlet UIView *lineView;

@property (weak, nonatomic) IBOutlet UIView *lineTwoView;
@property (strong, nonatomic)BusinessModel *changeModel;

@property (weak, nonatomic) IBOutlet UITextField *houseNumberTF;

@end

NS_ASSUME_NONNULL_END
