//
//  UserMerchantsPromoteHeadView.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/19.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserMerchantsPromoteHeadView : UIView
+(instancetype)getView;
@property (weak, nonatomic) IBOutlet UILabel *coutLabel;

@end

NS_ASSUME_NONNULL_END
