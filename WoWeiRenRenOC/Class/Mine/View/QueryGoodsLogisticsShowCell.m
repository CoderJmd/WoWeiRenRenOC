//
//  QueryGoodsLogisticsShowCell.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/9/11.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "QueryGoodsLogisticsShowCell.h"
#import "QueryGoodsLogisticsShowChildView.h"

@implementation QueryGoodsLogisticsShowCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(express_order_detail *)model {
    
    _model = model;
    SD_SETIMAGE_PlaceHolder(self.goodsImageView, model.img_path, @"default_image_square");
    self.nameLabel.text = model.sku_name;
    self.complanyLabel.text = model.express_org;
    
    NSArray *strArray =  [model.express_no componentsSeparatedByString:@","];
    CGFloat height = strArray.count * 20;
    if (height < 50) {
        height = 50;
    }
    self.bgViewHeight.constant = height;
    
    for (int  i = 0; i <  strArray.count; i++) {
        
        QueryGoodsLogisticsShowChildView *view = [QueryGoodsLogisticsShowChildView getView];
        CGFloat w = kScreenW - 195;
        [self.bgViiew  addSubview:view];

        view.frame  = CGRectMake(0, i * 20, w, 20);
        view.nameLabel.text  = strArray[i];
        
    }
    
    
}

@end
