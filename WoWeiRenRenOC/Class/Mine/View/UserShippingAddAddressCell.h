//
//  UserShippingAddAddressCell.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/12.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserShipingAddressModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface UserShippingAddAddressCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *nameTF;
@property (weak, nonatomic) IBOutlet UITextField *detailsAddressTF;
@property (weak, nonatomic) IBOutlet UITextField *phoneTF;
@property (weak, nonatomic) IBOutlet UIButton *defaultBtn;
@property (weak, nonatomic) IBOutlet UITextField *selectorCityTF;
@property (weak, nonatomic) IBOutlet UIView *selectorCityBtn;
@property (strong, nonatomic)NSString *province;
@property (strong, nonatomic)NSString *city;
@property (strong, nonatomic)NSString *area;
@property (strong, nonatomic)UserShipingAddressModel *model;

@end

NS_ASSUME_NONNULL_END
