//
//  UserMyInforHeadView.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/18.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "UserMyInforHeadView.h"
#import "NewFillInInformationTableVC.h"

@implementation UserMyInforHeadView

+(instancetype)getView {
    
    return [[[NSBundle mainBundle] loadNibNamed:@"UserMyInforHeadView" owner:nil options:nil] lastObject];
    
}


- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    
}
- (IBAction)jumpInfoClick:(id)sender {
    
    NewFillInInformationTableVC  *vc = [[NewFillInInformationTableVC alloc]init];
    [[CommonTTool getCurrentVC].navigationController pushViewController:vc animated:YES];
}

@end
