//
//  UserExtractHeadView.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/13.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "UserExtractHeadView.h"

@implementation UserExtractHeadView

- (void)awakeFromNib {
    
    [super awakeFromNib];

//    self.bgView.layer.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0].CGColor;
//    self.bgView.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.1].CGColor;
//    self.bgView.layer.shadowOffset = CGSizeMake(0,-2);
//    self.bgView.layer.shadowOpacity = 1;
//    self.bgView.layer.shadowRadius = 12;
    self.bgView.layer.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0].CGColor;
    self.bgView.layer.cornerRadius = 12;
    self.bgView.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.08].CGColor;
    self.bgView.layer.shadowOffset = CGSizeMake(0,-2);
    self.bgView.layer.shadowOpacity = 1;
    self.bgView.layer.shadowRadius = 8;
    
    self.priceBgView.layer.cornerRadius = 2;
    self.priceBgView.layer.borderColor = [UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:1.0].CGColor;
    self.priceBgView.layer.borderWidth = 1;
    self.priceBgView.clipsToBounds = YES;
    
    self.submitBtn.layer.cornerRadius = 5;
    self.submitBtn.layer.backgroundColor = [UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:1.0].CGColor;
    self.submitBtn.clipsToBounds = YES;

    
}

@end
