//
//  UserOrderChildFooterView.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/26.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserShoppingOrderModel.h"
#import "MailGoodsDetailsModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface UserOrderChildFooterView : UIView
@property (weak, nonatomic) IBOutlet UILabel *numLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIButton *oneBtn;
@property (weak, nonatomic) IBOutlet UIButton *twoBtn;
@property (weak, nonatomic) IBOutlet UILabel *oneLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic)UserShoppingOrderModel *model;
@property (strong, nonatomic)id timer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *deleteBtnLeft;

@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;

@end

NS_ASSUME_NONNULL_END
