//
//  QueryGoodsLogisticsHeadView.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/9/11.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "QueryGoodsLogisticsHeadView.h"

@implementation QueryGoodsLogisticsHeadView

- (void)setModel:(LogisticsModel *)model {
    
    _model = model;
    self.ordeCodeLabel.text = [NSString stringWithFormat:@"%@ [复制]",model.order_code];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(copyOrderCode)];
    self.ordeCodeLabel.userInteractionEnabled = YES;
    [self.ordeCodeLabel addGestureRecognizer:tap];
    self.timeLabel.text = model.pay_time;
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(copyExpress_no)];
    self.logisticsLabel.userInteractionEnabled = YES;
    [self.logisticsLabel addGestureRecognizer:tap1];
    
}

- (void)copyExpress_no {
    
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];

    
    
    
}


- (void)copyOrderCode {
    
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];

    pasteboard.string = self.model.order_code;

    SVP(@"订单编号已复制");
    
    
}

@end
