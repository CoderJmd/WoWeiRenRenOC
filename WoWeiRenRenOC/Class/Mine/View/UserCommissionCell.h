//
//  UserCommissionCell.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/13.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WithdrawalModel.h"
#import "UserCommissionModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserCommissionCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic)WithdrawalModel *model;
@property (strong, nonatomic)UserCommissionModel *commissionModel;
@end

NS_ASSUME_NONNULL_END
