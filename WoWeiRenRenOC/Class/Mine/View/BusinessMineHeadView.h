//
//  BusinessMineHeadView.h
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/17.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BusinessMineHeadView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *topImage;
@property (weak, nonatomic) IBOutlet UIView *coverView;

@end

NS_ASSUME_NONNULL_END
