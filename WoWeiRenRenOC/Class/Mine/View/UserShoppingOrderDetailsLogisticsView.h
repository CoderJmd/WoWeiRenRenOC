//
//  UserShoppingOrderDetailsLogisticsView.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/28.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserShoppingOrderDetailsLogisticsView : UIView
+ (instancetype)showView;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UILabel *codeLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIView *bgView;

@end

NS_ASSUME_NONNULL_END
