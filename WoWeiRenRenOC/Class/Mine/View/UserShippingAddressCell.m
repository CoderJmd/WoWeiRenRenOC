//
//  UserShippingAddressCell.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/12.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "UserShippingAddressCell.h"
#import "UserShippingAddAddressVC.h"

@implementation UserShippingAddressCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(UserShipingAddressModel *)model {
    
    _model = model;
    self.nameLabel.text = model.name;
    self.phoneLabel.text = model.phone;
    
    self.addressLabel.text = [NSString stringWithFormat:@"%@%@%@%@",model.province,model.city,model.area,model.address];
    if ([model.def isEqualToString:@"1"]) {
        self.defBtn.selected = YES;
    }else {
        self.defBtn.selected = NO;;

    }
    
}

- (IBAction)defBtnClick:(id)sender {
    
    if ([self.model.def isEqualToString:@"1"]) {
        
        return;
    }
    NSDictionary *para = @{@"address_id":self.model.address_id,
                          @"def":@"1",
                       
        };
    SVPShowInternetWaiting;
    [NetRequestTool postUrl:@"address/updateAddress" parameters:para success:^(id responseObject) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kUserAddShopingAddressUpDate object:nil];
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        SVPDismiss;
    }];
    
}

- (IBAction)deleteBtnClick:(id)sender {
    
    UIAlertController *alterController = [UIAlertController alertControllerWithTitle:nil message:@"是否删除该地址？" preferredStyle:UIAlertControllerStyleAlert];
    
    [alterController addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
       
    }]];
    
    [alterController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        SVPShowInternetWaiting;
        NSDictionary *para = @{@"address_id":self.model.address_id,
        };
        [NetRequestTool postUrl:@"address/deleteAddress" parameters:para success:^(id responseObject) {
          [[NSNotificationCenter defaultCenter] postNotificationName:kUserAddShopingAddressUpDate object:nil];
        } failure:^(NSInteger statusCode) {
          
        } finish:^{
          SVPDismiss;
        }];
        
    }]];
    
    [[CommonTTool getCurrentVC] presentViewController:alterController animated:YES completion:nil];
    
    
}




- (IBAction)editBtnClick:(id)sender {
    
    UserShippingAddAddressVC *vc = [[UserShippingAddAddressVC alloc]init];
    vc.model = self.model;
    [[CommonTTool getCurrentVC].navigationController pushViewController:vc animated:YES];
    
}


@end
