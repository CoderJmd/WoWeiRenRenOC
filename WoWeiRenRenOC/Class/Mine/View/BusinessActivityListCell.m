//
//  BusinessActivityListCell.m
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/18.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BusinessActivityListCell.h"

@interface BusinessActivityListCell ()
@property (weak, nonatomic) IBOutlet UIView *bgView;

@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UIButton *stateBtn;

@end

@implementation BusinessActivityListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.bgView.layer.backgroundColor = [UIColor whiteColor].CGColor;
    self.bgView.layer.cornerRadius = 4;
    self.bgView.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.2].CGColor;
    self.bgView.layer.shadowOffset = CGSizeMake(0, 0);
    self.bgView.layer.shadowOpacity = 1;
    self.bgView.layer.shadowRadius = 4;
}

- (void)setModel:(ActivityModel *)model
{
    _model = model;
    
    SD_SETIMAGE_PlaceHolder(self.imgView, model.img_path, @"default_image_square")
    
    self.titleLab.text = model.activity_name;
    
    if (model.sh_state==1) {
        // 审核中
        [self.stateBtn setTitle:@"审核中" forState:UIControlStateNormal];
        [self.stateBtn setBackgroundImage:[UIImage imageNamed:@"shenhezhong"] forState:UIControlStateNormal];
        self.editeBtn.hidden = YES;
    }
    else if (model.sh_state==2) {
        // 已通过
        [self.stateBtn setTitle:@"已通过" forState:UIControlStateNormal];
        [self.stateBtn setBackgroundImage:[UIImage imageNamed:@"tiruzhu"] forState:UIControlStateNormal];
        self.editeBtn.hidden = NO;
    }
    else if (model.sh_state==3) {
        // 未通过
        [self.stateBtn setTitle:@"未通过" forState:UIControlStateNormal];
        [self.stateBtn setBackgroundImage:[UIImage imageNamed:@"weitongguo"] forState:UIControlStateNormal];
        self.editeBtn.hidden = NO;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
