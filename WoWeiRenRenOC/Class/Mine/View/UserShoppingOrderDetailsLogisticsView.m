//
//  UserShoppingOrderDetailsLogisticsView.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/28.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "UserShoppingOrderDetailsLogisticsView.h"

@implementation UserShoppingOrderDetailsLogisticsView

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [self.cancelBtn addTarget:self action:@selector(copyLogistics) forControlEvents:UIControlEventTouchUpInside];
    
    UITapGestureRecognizer *tap =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cancelView)];
    
    [self addGestureRecognizer:tap];
    
    
    UITapGestureRecognizer *tapNull =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap)];
    
    [self.bgView addGestureRecognizer:tapNull];
    
    self.bgView.layer.cornerRadius = 5;
    self.bgView.clipsToBounds = YES;
    
}

- (void)tap {
    
}


/**
 快速创建
 */
+ (instancetype)showView
{
    UIWindow *window = UIApplication.sharedApplication.keyWindow;

    UserShoppingOrderDetailsLogisticsView *view = [[NSBundle mainBundle] loadNibNamed:@"UserShoppingOrderDetailsLogisticsView" owner:self options:nil].lastObject;
    view.frame = window.bounds;
    view.backgroundColor =  [[UIColor blackColor] colorWithAlphaComponent:0.7];

    [window addSubview:view];
    
    return view;
}

- (void)cancelView {
    
    [UIView animateWithDuration:0.5 animations:^{
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}


- (void)copyLogistics {
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];

    pasteboard.string = self.codeLabel.text;

    SVP(@"单号已复制至剪贴板");
    
    [self cancelView];
    
    
}


@end
