//
//  QueryGoodsLogisticsShowChildView.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/12/28.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QueryGoodsLogisticsShowChildView : UIView
+(instancetype)getView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end

NS_ASSUME_NONNULL_END
