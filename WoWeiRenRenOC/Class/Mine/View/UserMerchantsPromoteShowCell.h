//
//  UserMerchantsPromoteShowCell.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/19.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserMerchantsPromoteModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserMerchantsPromoteShowCell : UITableViewCell
@property (unsafe_unretained, nonatomic) IBOutlet UIView *bgView;

@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *imgView;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *titleLab;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *addressBtn;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *phoneBtn;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *discountBtn;

@property (nonatomic, strong) UserMerchantsPromoteModel *model;

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottom;

@end

NS_ASSUME_NONNULL_END
