//
//  UserOrderChildCell.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/13.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "UserOrderChildCell.h"

@implementation UserOrderChildCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(MailGoodsDetailsModel *)model {
    
    _model = model;
    self.nameLabel.text = model.sku_name;
    self.contentLabel.text = [CommonTTool getGoodsSpecifications:model.gg_info];
    self.priceLabel.text = [CommonTTool getPrice:model.unit_price];
    
    self.numLabel.text = [NSString stringWithFormat:@"x%.0ld",(long)model.num];
    SD_SETIMAGE_PlaceHolder(self.imgView, model.img_path, @"default_image_square");
    self.reFundCoutnLabel.text = [NSString stringWithFormat:@"退款数量:x%@",model.t_num];

    switch (model.is_td) {
        case 2:
            {
      
                self.reFundLabel.text = @"退单审核中";
            }
            break;
        case 3:
            {

              self.reFundLabel.text = @"审核不通过";
            }
            break;
        case 4:
            {

               self.reFundLabel.text = @"退款中";
            }
            break;
        case 5:
            {

                self.reFundLabel.text = @"退款中";
            }
            break;
        case 6:
            {
                self.reFundLabel.text = @"退款成功";
            }
            break;
        case 7:
            {
                self.reFundLabel.text = @"退单失败";
            }
            break;
            
        default:
            break;
    }

}

@end
