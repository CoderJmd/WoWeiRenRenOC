//
//  UserMerchantsPromoteDetailsCell.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/19.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "UserMerchantsPromoteDetailsCell.h"
#import "AddressSelectVC.h"
#import "UserMerchantsPromoteDetailsVC.h"



@interface UserMerchantsPromoteDetailsCell()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate,UITextViewDelegate,UIGestureRecognizerDelegate>{
    UIImagePickerController *_imagePicker; // 图片选择控制器
    BOOL _IsCamera; // 是否调用相机
    UIImage *_image;

}

@property (nonatomic,strong) NSArray *zhekouArray;
@property (nonatomic,strong) NSMutableArray *zhekouTitleArray;

@property (nonatomic,strong) NSArray *typeArray;
@property (nonatomic,strong) NSMutableArray *typeTitleArray;
@property (assign, nonatomic)BOOL isHeadPath;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *passwordHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *paswordOneHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *phoneHeight;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabrl;

@property (weak, nonatomic) IBOutlet UIView *lineThree;

@end

@implementation UserMerchantsPromoteDetailsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self getZheKou];
    [self getType];
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)setModel:(BusinessModel *)model {
    
    _model = model;
    
    
    self.nameLabel.text = model.business_name;
    self.merchantsPhoneLabel.text = model.busineee_tel;
    self.merchantsAddressLabel.text = model.address;
    self.contactsLabel.text = model.contacts;
    self.phoneLabel.text = model.phone;
    self.business_license_noLabel.text = model.business_license_no;
    self.descriptionLabel.text = model.descriptionText;
    self.houseNumberTF.text = model.house_number;
    if ([model.house_number isEqualToString:@"暂无数据"]) {
        self.houseNumberTF.text = @"";
    }
//    if (model.sh_state == 1 || model.sh_state == 2) {
//        self.nameLabel.enabled = NO;
//         self.merchantsPhoneLabel.enabled = NO;
//         self.merchantsAddressLabel.enabled = NO;
//         self.contactsLabel.enabled = NO;
//         self.phoneLabel.enabled = NO;
//         self.houseNumberTF.enabled = NO;
//         self.descriptionLabel.enabled = NO;
//
//         self.head_pathBtn.enabled = NO;
//         self.merchantsTypeBtn.enabled = NO;
//         self.merchantsAddressBtn.enabled = NO;
//         self.business_license_pathBtn.enabled = NO;
//         self.merchantsZheKouBtn.enabled = NO;
//    }
    
    self.nameLabel.enabled = NO;
    self.merchantsPhoneLabel.enabled = NO;
    self.merchantsAddressLabel.enabled = NO;
    self.contactsLabel.enabled = NO;
    self.phoneLabel.enabled = NO;
    self.houseNumberTF.enabled = NO;
    self.descriptionLabel.enabled = NO;
    
    self.head_pathBtn.enabled = NO;
    self.merchantsTypeBtn.enabled = NO;
    self.merchantsAddressBtn.enabled = NO;
    self.business_license_pathBtn.enabled = NO;
    self.merchantsZheKouBtn.enabled = NO;
    
    
    self.head_path = model.head_path;
    self.business_license_path = model.business_license_path;
    [self.head_pathBtn sd_setImageWithURL:[NSURL URLWithString:model.head_path] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"mentouzhao"]];
    [self.business_license_pathBtn sd_setImageWithURL:[NSURL URLWithString:model.business_license_path] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"yingyezhizhao"]];

    
    self.passwordHeight.constant = 0;
    self.paswordOneHeight.constant = 0;
    self.passwordTextLabelOnt.hidden = YES;
    self.passwordTextLabel.hidden = YES;
    self.lineView.hidden = YES;
    self.lineTwoView.hidden = YES;
    self.submitBtn.enabled = NO;
    self.submitBtn.hidden = YES;
   
//    if (self.model.sh_state == 3) {
//        self.submitBtn.enabled = YES;
//        self.submitBtn.hidden = NO;
//    }else {
//        self.submitBtn.enabled = NO;
//        self.submitBtn.hidden = YES;
//
//    }
    
    self.submitBtn.enabled = NO;
    self.submitBtn.hidden = YES;
    
    
}


- (void)yearSubmitClick {
    
    NSDate *datenow = [NSDate date];//现在时间,你可以输出来看下是什么格式

    NSInteger currentTime = [datenow timeIntervalSince1970];
    NSString *oldTImeStr = self.model.effective_time;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];// 创建一个时间格式化对象
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"]; //设定时间的格式
    NSDate *tempDate = [dateFormatter dateFromString:oldTImeStr];//将字符串转换为时间对象
    NSInteger oldTime = [tempDate timeIntervalSince1970];
    
    NSInteger time = oldTime - currentTime;
    
    if (time > 60*60*24*30) {
        
        SVP(@"请在商家有效期到期30天进行年度审核");
       
    }else {
        
        UserMerchantsPromoteDetailsVC *vc = [[UserMerchantsPromoteDetailsVC alloc]init];
        vc.changeModel = self.model;
        [[CommonTTool getCurrentVC].navigationController pushViewController:vc animated:YES];
        
    }
    
    
}

- (void)setChangeModel:(BusinessModel *)changeModel {
    
    _changeModel = changeModel;
    
    self.nameLabel.text = changeModel.business_name;
    self.merchantsPhoneLabel.text = changeModel.busineee_tel;
    self.merchantsAddressLabel.text = changeModel.address;
    self.contactsLabel.text = changeModel.contacts;
    self.phoneLabel.text = changeModel.phone;
    self.business_license_noLabel.text = changeModel.business_license_no;
    self.descriptionLabel.text = changeModel.descriptionText;
    self.houseNumberTF.text = changeModel.house_number;
    if ([changeModel.house_number isEqualToString:@"暂无数据"]) {
        self.houseNumberTF.text = @"";
    }
    [self.head_pathBtn sd_setImageWithURL:[NSURL URLWithString:changeModel.head_path] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"mentouzhao"]];
    self.head_path = changeModel.head_path;
    
    [self.business_license_pathBtn sd_setImageWithURL:[NSURL URLWithString:changeModel.business_license_path] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"yingyezhizhao"]];
    self.business_license_path = changeModel.business_license_path;

    self.passwordHeight.constant = 0;
    self.paswordOneHeight.constant = 0;
    self.phoneHeight.constant = 0;
    self.phoneLabel.hidden = YES;
    self.passwordTextLabelOnt.hidden = YES;
    self.passwordTextLabel.hidden = YES;
    self.phoneLabrl.hidden = YES;
    self.lineThree.hidden = YES;
    self.lineView.hidden = YES;
    self.lineTwoView.hidden = YES;
}


- (void)getZheKou {
    
    WeakSelf(ws)
    [NetRequestTool postUrl:@"sysCode/querySysCodeList" parameters:@{@"type":@"1"} success:^(id responseObject) {
       
        ws.zhekouArray = [DiscountModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]];
        ws.zhekouTitleArray = [NSMutableArray array];
        for (DiscountModel *model in ws.zhekouArray) {
            [ws.zhekouTitleArray addObject:model.name];
            if (self.model.discount_id == [model.Id integerValue]) {
                self.merchantsZheKouLabel.text = model.name;
                self.zhekouModel = model;
            }
            if (self.changeModel.discount_id == [model.Id integerValue]) {
                self.merchantsZheKouLabel.text = model.name;
                self.zhekouModel = model;
            }
        }
        
        
        
    } failure:^(NSInteger statusCode) {
//        [ws endRefreshNoMoreData];
    } finish:^{
        SVPDismiss
    }];
    
    
}

- (void)getType {
    
    WeakSelf(ws)
    [NetRequestTool postUrl:@"business/getClassifyList" parameters:@{} success:^(id responseObject) {
       
        ws.typeArray = [DiscountModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]];
        ws.typeTitleArray = [NSMutableArray array];
        for (DiscountModel *model in ws.typeArray) {
            [ws.typeTitleArray addObject:model.classify_name];
            if (self.model.classification_id == [model.classify_id integerValue]) {
                self.merchantsTypeLabel.text = model.classify_name;
                self.typeModel = model;
            }
           if (self.changeModel.classification_id == [model.classify_id integerValue]) {
                self.merchantsTypeLabel.text = model.classify_name;
                self.typeModel = model;
            }
        }
        
    } failure:^(NSInteger statusCode) {
//        [ws endRefreshNoMoreData];
    } finish:^{
        SVPDismiss
    }];
    
    
}



- (IBAction)typeClick:(id)sender {
    
    LCActionSheet *actionSheet = [LCActionSheet sheetWithTitle:@"" cancelButtonTitle:@"取消" clicked:^(LCActionSheet * _Nonnull actionSheet, NSInteger buttonIndex) {
        
        if (buttonIndex != 0) {
            // 系统提示框
            self.typeModel = self.typeArray[buttonIndex-1];
            self.merchantsTypeLabel.text = self.typeModel.classify_name;
        }
        
    } otherButtonTitleArray:self.typeTitleArray];
    actionSheet.buttonHeight = 45;
    actionSheet.buttonColor = BlueColor;
    
    [actionSheet show];
    
    
}

- (IBAction)zhekouClick:(id)sender {
    
    LCActionSheet *actionSheet = [LCActionSheet sheetWithTitle:@"" cancelButtonTitle:@"取消" clicked:^(LCActionSheet * _Nonnull actionSheet, NSInteger buttonIndex) {
        
        if (buttonIndex != 0) {
            // 系统提示框
            self.zhekouModel = self.zhekouArray[buttonIndex-1];
            self.merchantsZheKouLabel.text = self.zhekouModel.name;
        }
        
    } otherButtonTitleArray:self.zhekouTitleArray];
    actionSheet.buttonHeight = 45;
    actionSheet.buttonColor = BlueColor;
    
    [actionSheet show];
    
}

- (IBAction)headClick:(id)sender {
    
    self.isHeadPath = YES;
    [self create_ActionSheet];
    
}

- (IBAction)addressClick:(id)sender {
    AddressSelectVC *vc = [[AddressSelectVC alloc]init];
    vc.selectFinish = ^(NSString * _Nonnull address) {
        //self.merchantsAddressLabel.text = address;
        self.houseNumberTF.text = address;
    };
    [[CommonTTool getCurrentVC].navigationController pushViewController:vc animated:YES];
}
- (IBAction)submitClick:(id)sender {
}
- (IBAction)licenseClcik:(id)sender {
    
    
    self.isHeadPath = NO;
    [self create_ActionSheet];
}


#pragma mark - 点击换头像事件
- (void)create_ActionSheet {
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"选择" message: nil preferredStyle:UIAlertControllerStyleActionSheet];
    [alertController addAction: [UIAlertAction actionWithTitle: @"拍照" style: UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self use_camera];
    }]];
    [alertController addAction: [UIAlertAction actionWithTitle:@"我的相册" style: UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self pickPhoto];
    }]];
    [alertController addAction: [UIAlertAction actionWithTitle: @"取消" style: UIAlertActionStyleCancel handler:nil]];
    [[CommonTTool getCurrentVC] presentViewController: alertController animated: YES completion: nil];
}

#pragma mark - 调用相机
- (void)use_camera {
    _IsCamera = YES;
    UIImagePickerController *imageController = [[UIImagePickerController alloc]init];
    imageController.delegate = self;
    imageController.allowsEditing = NO;
    imageController.sourceType = UIImagePickerControllerSourceTypeCamera;
    [[CommonTTool getCurrentVC] presentViewController:imageController animated:YES completion:nil];
}

#pragma mark - 点击从本地相册调用图片
- (void)pickPhoto {
    _IsCamera = NO;
    UIImagePickerController *imagePickerTemp = [[UIImagePickerController alloc]init];
    _imagePicker = imagePickerTemp;
    _imagePicker.delegate = self;
    _imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    _imagePicker.allowsEditing = YES;
    [[CommonTTool getCurrentVC] presentViewController:_imagePicker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:^{
        
    }];
    //    info.fileType
        UIImage *headerImg;
    if (_IsCamera) {
       
        headerImg = [info objectForKey:UIImagePickerControllerOriginalImage];
       headerImg = [info objectForKey:UIImagePickerControllerOriginalImage];
       if(headerImg.imageOrientation!=UIImageOrientationUp){
           // Adjust picture Angle
           UIGraphicsBeginImageContext(CGSizeMake(headerImg.size.width * 0.4, headerImg.size.height * 0.4));
           [headerImg drawInRect:CGRectMake(0, 0,headerImg.size.width * 0.4, headerImg.size.height * 0.4)];
           headerImg = UIGraphicsGetImageFromCurrentImageContext();
           UIGraphicsEndImageContext();
       }
       headerImg = [CommonTTool imageCompression:headerImg];
        
    }else {
       
            headerImg = [info objectForKey:UIImagePickerControllerEditedImage];
        
    }
    
    [self uploadHeadImage:headerImg]; // 图片上传接口
    //     _image = [info objectForKey: UIImagePickerControllerEditedImage];
    
}

- (void)uploadHeadImage:(UIImage *)image {
    __weak typeof(self) weakSelf = self;
    NSArray *imageArray = [NSArray arrayWithObjects:image, nil];
    [NetManager isUpdateImgArr:imageArray andData:nil andFileName:nil success:^(NSDictionary *dic) {

        if (weakSelf.isHeadPath) {
            NSString *imagePath = dic[@"object"];
            [weakSelf.head_pathBtn sd_setImageWithURL:[NSURL URLWithString:imagePath] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"mentouzhao"]];
            weakSelf.head_path = imagePath;
        }else {
            NSString *imagePath = dic[@"object"];
            [weakSelf.business_license_pathBtn sd_setImageWithURL:[NSURL URLWithString:imagePath] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"yingyezhizhao"]];
            weakSelf.business_license_path = imagePath;
        }
        
        
    } uploadProgress:^(NSProgress *_Nonnull str) {
        
    }];
}



@end
