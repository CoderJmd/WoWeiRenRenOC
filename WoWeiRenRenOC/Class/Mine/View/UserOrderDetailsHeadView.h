//
//  UserOrderDetailsHeadView.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/13.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserShoppingOrderDetailsModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface UserOrderDetailsHeadView : UIView
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (strong, nonatomic)UserShoppingOrderDetailsModel *model;
@property (weak, nonatomic) IBOutlet UILabel *paymentLabel;

@property (weak, nonatomic) IBOutlet UIImageView *paymentImgView;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *goodsLabel;
@property (weak, nonatomic) IBOutlet UIImageView *goodsImgView;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UIView *addressBgView;

@property (weak, nonatomic) IBOutlet UIImageView *addressLineView;


@property (strong, nonatomic)id timer;
@end

NS_ASSUME_NONNULL_END
