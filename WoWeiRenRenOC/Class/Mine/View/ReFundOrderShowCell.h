//
//  ReFundOrderShowCell.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/9/1.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MailGoodsDetailsModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ReFundOrderShowCell : UITableViewCell
@property (strong, nonatomic)MailGoodsDetailsModel *model;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIButton *selectorBtn;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIImageView *reduceImg;
@property (weak, nonatomic) IBOutlet UITextField *countTF;
@property (weak, nonatomic) IBOutlet UIImageView *addImg;
@property (weak, nonatomic) IBOutlet UIButton *addBtn;
@property (weak, nonatomic) IBOutlet UIButton *reduceBtn;

@property (strong, nonatomic)NSString *business_id;

@property (weak, nonatomic) IBOutlet UILabel *goodsLabel;

@property (weak, nonatomic) IBOutlet UIImageView *goodsTypeImgView;
@end

NS_ASSUME_NONNULL_END
