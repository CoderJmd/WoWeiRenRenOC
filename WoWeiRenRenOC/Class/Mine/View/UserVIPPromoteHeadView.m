//
//  UserVIPPromoteHeadView.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/18.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "UserVIPPromoteHeadView.h"

@implementation UserVIPPromoteHeadView

+(instancetype)getView {
    
    return [[[NSBundle mainBundle] loadNibNamed:@"UserVIPPromoteHeadView" owner:nil options:nil] lastObject];
    
}

@end
