//
//  UserOrderChildGroupView.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/13.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserShoppingOrderModel.h"
#import "ShoppingCartModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserOrderChildGroupView : UIView
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic)UserShoppingOrderModel *model;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIButton *jumpBtn;
@end

NS_ASSUME_NONNULL_END
