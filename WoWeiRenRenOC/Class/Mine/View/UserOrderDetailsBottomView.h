//
//  UserOrderDetailsBottomView.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/15.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserShoppingOrderDetailsModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserOrderDetailsBottomView : UIView
@property (weak, nonatomic) IBOutlet UIButton *LogisticsBtn;
@property (weak, nonatomic) IBOutlet UIButton *goodsBtn;
@property (weak, nonatomic) IBOutlet UIButton *paymentBtn;
@property (weak, nonatomic) IBOutlet UIButton *reFundBtn;

@property (strong, nonatomic)UserShoppingOrderDetailsModel *model;
@end

NS_ASSUME_NONNULL_END
