//
//  UserMineHeadView.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/17.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserMineHeadView : UIView

+(instancetype)getView;
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *tuiguangLabel;
@property (weak, nonatomic) IBOutlet UIButton *shenqingBtn;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet UIImageView *yinkaImgView;

@end

NS_ASSUME_NONNULL_END
