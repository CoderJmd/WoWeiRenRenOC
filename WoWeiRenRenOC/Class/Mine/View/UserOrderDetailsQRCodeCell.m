//
//  UserOrderDetailsQRCodeCell.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/7/4.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "UserOrderDetailsQRCodeCell.h"

@implementation UserOrderDetailsQRCodeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(UserShoppingOrderDetailsModel *)model {
    
    _model = model;
    
    NSString *data = [NSString  stringWithFormat:@"%@,%@",model.order_code,model.business_id];
    self.imgView.image = [SGQRCodeObtain generateQRCodeWithData:data size:200];

    
    
}

@end
