//
//  UserCommissionHeadView.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/13.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "UserCommissionHeadView.h"

@implementation UserCommissionHeadView

+(instancetype)getView {
    
    return [[[NSBundle mainBundle] loadNibNamed:@"UserCommissionHeadView" owner:nil options:nil] lastObject];
    
}
@end
