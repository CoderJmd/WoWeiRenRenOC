//
//  BusinessActivityListCell.h
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/18.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActivityModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BusinessActivityListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *editeBtn;

@property (nonatomic, strong) ActivityModel *model;

@end

NS_ASSUME_NONNULL_END
