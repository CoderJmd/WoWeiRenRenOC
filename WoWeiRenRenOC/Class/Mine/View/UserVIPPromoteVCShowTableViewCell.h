//
//  UserVIPPromoteVCShowTableViewCell.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/18.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserVIIPPromoteModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserVIPPromoteVCShowTableViewCell : UITableViewCell
@property (strong, nonatomic)UserVIIPPromoteModel *model;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *codeLabel;

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;


@end

NS_ASSUME_NONNULL_END
