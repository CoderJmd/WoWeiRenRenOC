//
//  ApplySalesmanView.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/20.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "ApplySalesmanView.h"
static ApplySalesmanView *baseView;
@interface ApplySalesmanView ()
@property(strong,nonatomic)UIWindow *window;
@end

@implementation ApplySalesmanView

+(instancetype)getView {
    
    return [[[NSBundle mainBundle] loadNibNamed:@"ApplySalesmanView" owner:nil options:nil] lastObject];
    
}
- (void)awakeFromNib {
    
    [super awakeFromNib];
//    _window = [[UIApplication sharedApplication] keyWindow];
//    _window.backgroundColor = [UIColor whiteColor];
//    [_window addSubview:self];
    self.bgView.layer.cornerRadius = 5;
    self.bgView.clipsToBounds = YES;
    
    
    
}

- (IBAction)cancelClick:(id)sender {
    self.hidden = YES;
}

- (IBAction)confrimClick:(id)sender {
    
    NSString *name = self.nameTF.text;
    NSString *phone = self.phoneTF.text;
    if ([CommonTTool isStrNull:name]) {
        SVP(@"请输入联系地址")
        return;
    }
    if ([CommonTTool isStrNull:phone]) {
        SVP(@"请输入您的手机号")
        return;
    }
    
    NSDictionary *para = @{@"address":name,
                           @"phone":phone
    };
    
    // 请求列表
    SVPShowInternetWaiting
    [NetRequestTool postUrl:@"user/updateUserYwy" parameters:para success:^(id responseObject) {
       
        NSLog(@"%@",responseObject);
        [[NSNotificationCenter defaultCenter] postNotification:@"upDateShenQing"];
    } failure:^(NSInteger statusCode) {
    } finish:^{
        SVPDismiss
    }];
    
    
}


+ (void)showView {
    
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{

        baseView = [ApplySalesmanView getView];
        
        baseView.frame = CGRectMake(0, 0, KSCREEN_WIDTH , KSCREEN_HEIGHT);
        
        baseView.backgroundColor = [UIColor colorWithWhite:0.2f alpha: 0.5];
    });
    
    baseView.hidden = NO;
   
}


@end
