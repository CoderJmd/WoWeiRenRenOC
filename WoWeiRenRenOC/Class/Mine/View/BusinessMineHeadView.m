//
//  BusinessMineHeadView.m
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/17.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BusinessMineHeadView.h"

@implementation BusinessMineHeadView

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    CGRect rect = CGRectMake(0, 0, kScreenW, 32);
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:rect byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(10000,10000)];
    CAShapeLayer *layer = [[CAShapeLayer alloc] init];
    layer.frame = rect;
    layer.path = path.CGPath;
    self.coverView.layer.mask = layer;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
