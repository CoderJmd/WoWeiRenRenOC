//
//  UserMyInforBottomView.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/18.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "UserMyInforBottomView.h"

@implementation UserMyInforBottomView

+(instancetype)getView {
    
    return [[[NSBundle mainBundle] loadNibNamed:@"UserMyInforBottomView" owner:nil options:nil] lastObject];
    
}

@end
