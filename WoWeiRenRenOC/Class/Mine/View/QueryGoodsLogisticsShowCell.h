//
//  QueryGoodsLogisticsShowCell.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/9/11.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LogisticsModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface QueryGoodsLogisticsShowCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *goodsImageView;

@property (strong, nonatomic)express_order_detail *model;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *complanyLabel;
@property (weak, nonatomic) IBOutlet UIView *bgViiew;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bgViewHeight;



@end

NS_ASSUME_NONNULL_END
