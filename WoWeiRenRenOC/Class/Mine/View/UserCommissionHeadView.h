//
//  UserCommissionHeadView.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/13.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserCommissionHeadView : UIView
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
+(instancetype)getView;
@end

NS_ASSUME_NONNULL_END
