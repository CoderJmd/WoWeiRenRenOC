//
//  UserShippingAddAddressCell.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/12.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "UserShippingAddAddressCell.h"
#import "WHAreaPickerView.h"
@interface UserShippingAddAddressCell ()<WHAreaPickerViewDelegate>

@end
@implementation UserShippingAddAddressCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.defaultBtn addTarget:self action:@selector(selectorDefaultBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    UITapGestureRecognizer *seleCityTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(seleCityClick)];
    [self.selectorCityBtn addGestureRecognizer:seleCityTap];
    [self.phoneTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)textFieldDidChange:(UITextField *)textField
{
    if (textField == self.phoneTF) {
        if (textField.text.length > 11) {
            textField.text = [textField.text substringToIndex:11];
        }
    }
}

-  (void)seleCityClick {
    
    [CommonTTool dismisKeyBoard];
    NSArray *cityArray = [[NSUserDefaults standardUserDefaults] arrayForKey:kCityList];
    if (cityArray == nil) {
        [NetRequestTool postUrl:@"sysCode/queryCityList" parameters:@{} success:^(id responseObject) {
             
            NSArray *array = responseObject[@"object"];
            WHAreaPickerView *area = [WHAreaPickerView areaPickerView];
            area.soucessArray = array;
            area.delegate = self;
            [[NSUserDefaults standardUserDefaults] setObject:array forKey:kCityList];
            
            
        } failure:^(NSInteger statusCode) {
            
        } finish:^{
            SVPDismiss
        }];
        
    }else {
        WHAreaPickerView *area = [WHAreaPickerView areaPickerView];
        area.soucessArray = cityArray;
        area.delegate = self;
    }
    

}

#pragma mark - WHAreaPickerControllerDelegate
- (void)areaPickerViewDidFinish:(WHAreaPickerView *)picker
{
    self.selectorCityTF.text = [picker.areaName componentsJoinedByString:@""];
    self.province = picker.areaName[0];
    self.city = picker.areaName[1];
    self.area = picker.areaName[2];
    
}

- (void)selectorDefaultBtnClick:(UIButton *)btn {
    
    btn.selected = !btn.selected;
    
    
}

- (void)setModel:(UserShipingAddressModel *)model {
    
    _model = model;
    self.nameTF.text = model.name;
    self.phoneTF.text = model.phone;
    self.selectorCityTF.text = [NSString stringWithFormat:@"%@%@%@",model.province,model.city,model.area];
    self.detailsAddressTF.text = model.address;
    if ([model.def isEqualToString:@"1"]) {
        self.defaultBtn.selected = YES;
    }
    self.province = model.province;
    self.city = model.city;
    self.area = model.area;

    
    
}

@end
