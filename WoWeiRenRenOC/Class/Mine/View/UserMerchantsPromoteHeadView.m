//
//  UserMerchantsPromoteHeadView.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/19.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "UserMerchantsPromoteHeadView.h"

@implementation UserMerchantsPromoteHeadView

+(instancetype)getView {
    
    return [[[NSBundle mainBundle] loadNibNamed:@"UserMerchantsPromoteHeadView" owner:nil options:nil] lastObject];
    
}

@end
