//
//  ReFundOrderListFooterView.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/9/1.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserShoppingOrderModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ReFundOrderListFooterView : UIView
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (strong, nonatomic)UserShoppingOrderModel *model;
@end

NS_ASSUME_NONNULL_END
