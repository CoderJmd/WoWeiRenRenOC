//
//  BusinessJobDetailFooter.h
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/19.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BusinessJobDetailFooter : UIView

@property (weak, nonatomic) IBOutlet UILabel *jobTitle;
@property (weak, nonatomic) IBOutlet UILabel *salaryLab;
@property (weak, nonatomic) IBOutlet UILabel *jobDes;

@property (assign, nonatomic) CGFloat footerH;

@end

NS_ASSUME_NONNULL_END
