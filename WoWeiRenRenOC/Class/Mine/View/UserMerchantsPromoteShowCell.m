//
//  UserMerchantsPromoteShowCell.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/19.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "UserMerchantsPromoteShowCell.h"

@implementation UserMerchantsPromoteShowCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
//    self.bgView.layer.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0].CGColor;
    self.bgView.layer.cornerRadius = 6;
    self.bgView.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.1].CGColor;
    self.bgView.layer.shadowOffset = CGSizeMake(0,0);
    self.bgView.layer.shadowOpacity = 1;
    self.bgView.layer.shadowRadius = 4;
}

- (void)setModel:(UserMerchantsPromoteModel *)model
{
    _model = model;
    
    SD_SETIMAGE_PlaceHolder(self.imgView, model.head_path, @"xiangqing")
    
    self.titleLab.text = model.business_name;
    
    [self.addressBtn setTitle:[NSString stringWithFormat:@"  %@", model.address] forState:UIControlStateNormal];
    
    [self.phoneBtn setTitle:[NSString stringWithFormat:@"  %@", model.busineee_tel] forState:UIControlStateNormal];
    
   
    
    switch (model.sh_state) {
        case 1:
            {
                [self.discountBtn setTitle:@"审核中" forState:UIControlStateNormal];
                [self.discountBtn setBackgroundImage:[UIImage imageNamed:@"shenhezhong"] forState:UIControlStateNormal];
                self.bottom.constant = 10;
                self.timeLabel.hidden = YES;
            }
            break;
        case 2:
           {
               [self.discountBtn setTitle:@"已入驻" forState:UIControlStateNormal];
               [self.discountBtn setBackgroundImage:[UIImage imageNamed:@"tiruzhu"] forState:UIControlStateNormal];
               self.bottom.constant = 30;
               self.timeLabel.hidden = NO;
               
//               NSString *time = [self getTimeFromTimestamp:model.effective_time];
               self.timeLabel.text = [NSString stringWithFormat:@"入驻有效期至%@",model.effective_time];
           }
           break;
        case 3:
           {
               [self.discountBtn setTitle:@"未通过" forState:UIControlStateNormal];
               [self.discountBtn setBackgroundImage:[UIImage imageNamed:@"weitongguo"] forState:UIControlStateNormal];
               self.bottom.constant = 10;
               self.timeLabel.hidden = YES;
           }
           break;
        default:
            break;
    }
}

#pragma mark ---- 将时间戳转换成时间

- (NSString *)getTimeFromTimestamp:(NSString *)time{

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];// 创建一个时间格式化对象
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"]; //设定时间的格式
    NSDate *myDate = [dateFormatter dateFromString:time];//将字符串转换为时间对象


    [dateFormatter setDateFormat:@"YYYY.MM.dd"];

    //将时间转换为字符串

    NSString *timeStr=[dateFormatter stringFromDate:myDate];

    return timeStr;

}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
