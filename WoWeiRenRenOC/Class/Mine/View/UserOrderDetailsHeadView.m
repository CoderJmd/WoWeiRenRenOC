//
//  UserOrderDetailsHeadView.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/13.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "UserOrderDetailsHeadView.h"

@implementation UserOrderDetailsHeadView

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    CAGradientLayer *gl = [CAGradientLayer layer];
    gl.frame = CGRectMake(0,0,kScreenW,85);
    gl.startPoint = CGPointMake(0.22, 0.36);
    gl.endPoint = CGPointMake(0.85, 0.72);
    gl.colors = @[(__bridge id)[UIColor colorWithRed:221/255.0 green:169/255.0 blue:72/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:189/255.0 green:135/255.0 blue:35/255.0 alpha:1.0].CGColor];
    gl.locations = @[@(0), @(1.0f)];
    [self.bgView.layer insertSublayer:gl atIndex:0];
    
    
}


- (void)setModel:(UserShoppingOrderDetailsModel *)model {
    
    _model = model;
    self.nameLabel.text = model.add_name;
    self.phoneLabel.text = model.add_phone;
    self.addressLabel.text = [NSString stringWithFormat:@"%@%@%@%@",model.add_province,model.add_city,model.add_area,model.add_address];
    
    self.paymentLabel.hidden = YES;
    self.paymentImgView.hidden = YES;
    self.goodsLabel.hidden = YES;
    self.goodsImgView.hidden = YES;
    self.timeLabel.hidden = YES;
    
    if (model.is_td != 1) {
        
        switch (model.is_td) {
            case 2:
            {
                self.goodsLabel.hidden = NO;
                self.goodsImgView.hidden = NO;
                self.goodsImgView.image = [UIImage imageNamed:@"yiwancheng"];
                self.goodsLabel.text = @"退单中";
            }
                break;
            case 5:
            {
                self.goodsLabel.hidden = NO;
                self.goodsImgView.hidden = NO;
                self.goodsImgView.image = [UIImage imageNamed:@"yiwancheng"];
                self.goodsLabel.text = @"退单中";
            }
                break;
            case 6:
            {
                self.goodsLabel.hidden = NO;
                self.goodsImgView.hidden = NO;
                self.goodsImgView.image = [UIImage imageNamed:@"yiwancheng"];
                self.goodsLabel.text = @"退单成功";
                
            }
                break;
            case 7:
            {
               self.goodsLabel.hidden = NO;
               self.goodsImgView.hidden = NO;
               self.goodsImgView.image = [UIImage imageNamed:@"yiwancheng"];
               self.goodsLabel.text = @"退单失败";
               
            }
               break;
            default:
                break;
        }
        return;
        
    }
    
    switch (model.order_state) {
        case 0:
        {
            self.goodsLabel.hidden = NO;
            self.goodsImgView.hidden = NO;
            self.goodsImgView.image = [UIImage imageNamed:@"yiwancheng"];
            self.goodsLabel.text = @"已取消";
        }
            break;
        case 1:
        {
            self.paymentLabel.hidden = NO;
            self.paymentImgView.hidden = NO;
            self.paymentLabel.text = @"待付款";
            self.timeLabel.hidden = NO;
            [self setEndTime];
        }
            break;
        case 2:
        {
            self.paymentLabel.hidden = NO;
            self.paymentImgView.hidden = NO;
            self.paymentLabel.text = @"支付中";
            self.timeLabel.hidden = NO;
            [self setEndTime];
        }
            break;
        case 3:
        {
            self.goodsLabel.hidden = NO;
            self.goodsImgView.hidden = NO;
            self.goodsImgView.image = [UIImage imageNamed:@"daifahuo"];
            self.goodsLabel.text = @"待发货";
            if (model.order_type == 3 || model.order_type == 9) {
                self.goodsLabel.text = @"待核销";
            }
            
            if (model.order_type == 4) {
                if (model.tg_group_state == 1) {
                    self.goodsLabel.text = @"拼团中";
                }
            }
            
            
        }
            break;
        case 4:
        {
            self.goodsLabel.hidden = NO;
            self.goodsImgView.hidden = NO;
            self.goodsImgView.image = [UIImage imageNamed:@"daishouhuo"];
            self.goodsLabel.text = @"待收货";
            if (model.order_type == 3 || model.order_type == 9) {
               self.goodsLabel.text = @"已核销";
            }
        }
            break;
        case 5:
        {
            self.goodsLabel.hidden = NO;
            self.goodsImgView.hidden = NO;
            self.goodsImgView.image = [UIImage imageNamed:@"yiwancheng"];
            self.goodsLabel.text = @"已完成";
            if (model.order_type == 3 || model.order_type == 9) {
               self.goodsLabel.text = @"已核销";
            }
        }
        break;
        default:
            break;
    }

    
    
}







#pragma mark - 设置倒计时
- (void)setEndTime {
    
    NSString *deadlineStr = self.model.last_pay_time;
    // 倒计时的时间 测试数据
    //    NSString *deadlineStr = @"2020-06-24 12:00:00";
    // 当前时间的时间戳
    NSString *nowStr = [CommonTTool getCurrentTimeyyyymmdd];
    // 计算时间差值
    NSInteger secondsCountDown = [CommonTTool getDateDifferenceWithNowDateStr:nowStr deadlineStr:deadlineStr];
    if (secondsCountDown <= 0) {
        return;
    }
    __weak __typeof(self) weakSelf = self;
    if (_timer == nil) {
        __block NSInteger timeout = secondsCountDown; // 倒计时时间
        
        if (timeout!=0) {
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
            dispatch_source_set_timer(_timer, dispatch_walltime(NULL, 0), 1.0*NSEC_PER_SEC,  0); //每秒执行
            dispatch_source_set_event_handler(_timer, ^{
                if(timeout <= 0){ //  当倒计时结束时做需要的操作: 关闭 活动到期不能提交
                    dispatch_source_cancel(self->_timer);
                    self->_timer = nil;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[NSNotificationCenter defaultCenter] postNotificationName:kUserOrderDetilsUpDate object:nil];
                    });
                } else { // 倒计时重新计算 时/分/秒
                    NSInteger days = 0;
                    NSInteger hours = (int)((timeout-days*24*3600)/3600);
                    NSInteger minute = (int)(timeout-days*24*3600-hours*3600)/60;
                    NSInteger second = timeout - days*24*3600 - hours*3600 - minute*60;
                    NSString *strTime = [NSString stringWithFormat:@"%02ld:%02ld:%02ld ", hours, minute,second];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSString *ptCountStr = [NSString stringWithFormat:@"付款倒计时：%@",strTime];
                        self.timeLabel.text = ptCountStr;
                        
                        
                    });
                    timeout--; // 递减 倒计时-1(总时间以秒来计算)
                }
            });
            dispatch_resume(_timer);
        }
    }
    
}



@end
