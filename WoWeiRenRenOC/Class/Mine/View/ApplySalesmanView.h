//
//  ApplySalesmanView.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/20.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ApplySalesmanView : UIView
@property (weak, nonatomic) IBOutlet UITextField *phoneTF;
@property (weak, nonatomic) IBOutlet UITextField *nameTF;
@property (weak, nonatomic) IBOutlet UIView *bgView;
+ (void)showView;
+(instancetype)getView;
@end

NS_ASSUME_NONNULL_END
