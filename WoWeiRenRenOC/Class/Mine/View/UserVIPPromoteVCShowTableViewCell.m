//
//  UserVIPPromoteVCShowTableViewCell.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/18.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "UserVIPPromoteVCShowTableViewCell.h"

@implementation UserVIPPromoteVCShowTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(UserVIIPPromoteModel *)model {
    
    _model = model;
    self.nameLabel.text = model.username;
    self.timeLabel.text = model.create_time;
    self.codeLabel.text = model.idcard;
    
    
}

@end
