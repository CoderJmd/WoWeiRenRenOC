//
//  AddressSelectHeader.m
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/19.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "AddressSelectHeader.h"

@implementation AddressSelectHeader

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ssk_dn"]];
    self.searchText.leftView = img;
    self.searchText.leftViewMode = UITextFieldViewModeAlways;
    self.searchText.delegate = self;
}

- (IBAction)cityButtonClick:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(headerCityButtonClick:)]) {
        [self.delegate headerCityButtonClick:sender];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([self.delegate respondsToSelector:@selector(headerSearchButtonClick:)]) {
        [self.delegate headerSearchButtonClick:textField.text];
    }
    return YES;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
