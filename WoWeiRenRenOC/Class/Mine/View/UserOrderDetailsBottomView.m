//
//  UserOrderDetailsBottomView.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/15.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "UserOrderDetailsBottomView.h"
#import "MailGoodsDetailsModel.h"

@implementation UserOrderDetailsBottomView

- (void)awakeFromNib {
    
    [super awakeFromNib];
    CAGradientLayer *gl = [CAGradientLayer layer];
    gl.frame = CGRectMake(0,0,kScreenW - 30,40);
    gl.startPoint = CGPointMake(0.22, 0.36);
    gl.endPoint = CGPointMake(0.85, 0.72);
    gl.colors = @[(__bridge id)[UIColor colorWithRed:221/255.0 green:169/255.0 blue:72/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:189/255.0 green:135/255.0 blue:35/255.0 alpha:1.0].CGColor];
    gl.locations = @[@(0), @(1.0f)];
    CAGradientLayer *gl2 = [CAGradientLayer layer];
    gl2.frame = CGRectMake(0,0,kScreenW - 30,40);
    gl2.startPoint = CGPointMake(0.22, 0.36);
    gl2.endPoint = CGPointMake(0.85, 0.72);
    gl2.colors = @[(__bridge id)[UIColor colorWithRed:221/255.0 green:169/255.0 blue:72/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:189/255.0 green:135/255.0 blue:35/255.0 alpha:1.0].CGColor];
    gl2.locations = @[@(0), @(1.0f)];
    
    [self.paymentBtn.layer insertSublayer:gl atIndex:0];
    [self.reFundBtn.layer insertSublayer:gl2 atIndex:0];

    
}


- (void)setModel:(UserShoppingOrderDetailsModel *)model {
    
    _model = model;
    self.goodsBtn.hidden = YES;
    self.paymentBtn.hidden = YES;
    self.LogisticsBtn.hidden = YES;
    self.reFundBtn.hidden = YES;
    self.frame = CGRectMake(0, KSCREEN_HEIGHT -110 -NeedStatusViewHeight, kScreenW, 90);
    if (model.order_state == 1) {
        self.paymentBtn.hidden = NO;
        self.hidden = NO;

    }
    
    if (model.order_state == 3 && model.is_td == 1) {
        MailGoodsDetailsModel *item = model.sku_order_detail_list[0];
        if (item.sku_type != 99) {
            self.reFundBtn.hidden = NO;
            self.hidden = NO;
        }
        
    }

    
    
    if (model.order_state == 4 ) {
        if (model.is_td == 1) {
            self.goodsBtn.hidden = NO;
            self.LogisticsBtn.hidden = NO;
            MailGoodsDetailsModel *item = model.sku_order_detail_list[0];
            if (item.sku_type != 99) {
               self.reFundBtn.hidden = NO;
            }
        }else {
            self.frame = CGRectMake(0, KSCREEN_HEIGHT -60 -NeedStatusViewHeight, kScreenW, 40);
            self.goodsBtn.hidden = NO;
            self.LogisticsBtn.hidden = NO;
        }
        self.hidden = NO;

    }
    
    
}

@end
