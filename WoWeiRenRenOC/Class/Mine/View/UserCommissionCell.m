//
//  UserCommissionCell.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/13.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "UserCommissionCell.h"

@implementation UserCommissionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(WithdrawalModel *)model {
    
    _model = model;
    self.nameLabel.text = model.title;
    self.timeLabel.text = model.create_time;
    self.priceLabel.text = [NSString stringWithFormat:@"-%@",model.money];
    
    
}

- (void)setCommissionModel:(UserCommissionModel *)commissionModel {
    
    _commissionModel = commissionModel;
    self.nameLabel.text = commissionModel.title;
    self.timeLabel.text = commissionModel.create_time;
    if (commissionModel.type == 1) {
        self.priceLabel.text = [NSString stringWithFormat:@"+%@",commissionModel.balance];
    }else {
        self.priceLabel.text = [NSString stringWithFormat:@"-%@",commissionModel.balance];
    }
    
    
}

@end
