//
//  UserMyInforHeadView.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/18.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserMyInforHeadView : UIView
+(instancetype)getView;
@property (weak, nonatomic) IBOutlet UILabel *vipTypeLabel;

@property (weak, nonatomic) IBOutlet UILabel *endVipLabel;
@property (weak, nonatomic) IBOutlet UIButton *fanhuiBtn;
@property (weak, nonatomic) IBOutlet UIButton *upgradeBtn;
@property (weak, nonatomic) IBOutlet UIImageView *vipTypeImgView;

@end

NS_ASSUME_NONNULL_END
