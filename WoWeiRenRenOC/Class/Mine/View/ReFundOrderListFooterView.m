//
//  ReFundOrderListFooterView.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/9/1.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "ReFundOrderListFooterView.h"

@implementation ReFundOrderListFooterView


- (void)setModel:(UserShoppingOrderModel *)model {
    
    _model = model;
    if (model.is_td == 6) {
        self.statusLabel.text = @"退单成功";
        self.contentLabel.hidden = NO;
        self.contentLabel.text = [NSString stringWithFormat:@"退单金额:¥%@",[CommonTTool getPrice:model.t_total_pay_price]];
    }else if (model.is_td == 7) {
        self.statusLabel.text = @"退单失败";
    }else {
        self.statusLabel.text = @"退单中";
    }
    
}

@end
