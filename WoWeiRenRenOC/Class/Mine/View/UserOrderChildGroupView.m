//
//  UserOrderChildGroupView.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/13.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "UserOrderChildGroupView.h"

@implementation UserOrderChildGroupView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setModel:(UserShoppingOrderModel *)model {
 
    _model = model;
    self.nameLabel.text = model.business_name;
    if (model.is_td != 1) {
        

        switch (model.is_td) {
            case 2:
            {
                
                self.statusLabel.text = @"退单中";
            }
                break;
            case 5:
            {
                
                self.statusLabel.text = @"退单中";
            }
                break;
            case 6:
            {
                
                self.statusLabel.text = @"退单成功";
                
            }
                break;
            case 7:
            {
               
               self.statusLabel.text = @"退单失败";
               
            }
               break;
            default:
                break;
        }
        if (model.order_state == 4) {
            self.statusLabel.text = @"待收货";
            return;
        }
        
        return;
        
    }
    switch (model.order_state) {
        case 0:
        {
            self.statusLabel.text = @"已取消";
        }
            break;
        case 1:
        {
            self.statusLabel.text = @"待付款";
        }
            break;
        case 2:
        {
            self.statusLabel.text = @"支付中";
        }
            break;
        case 3:
        {

            self.statusLabel.text = @"待发货";
            if (model.order_type == 3 || model.order_type == 9) {
                self.statusLabel.text = @"待核销";
            }
            
            if (model.order_type == 4) {
                if (model.tg_group_state == 1) {
                    self.statusLabel.text = @"拼团中";
                }
            }
            
            
        }
            break;
        case 4:
        {
           
            self.statusLabel.text = @"待收货";
            if (model.order_type == 3 ||model.order_type == 9) {
               self.statusLabel.text = @"已核销";
            }
        }
            break;
        case 5:
        {
           
            self.statusLabel.text = @"已完成";
            if (model.order_type == 3|| model.order_type == 9) {
               self.statusLabel.text = @"已核销";
            }
        }
        break;
        default:
            break;
    }
    
    
    
    
    
}


    
    

@end
