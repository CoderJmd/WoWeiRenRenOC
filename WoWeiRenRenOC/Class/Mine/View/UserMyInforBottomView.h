//
//  UserMyInforBottomView.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/18.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserMyInforBottomView : UIView
+(instancetype)getView;
@property (weak, nonatomic) IBOutlet WRBaseButton *xufeiBtn;

@end

NS_ASSUME_NONNULL_END
