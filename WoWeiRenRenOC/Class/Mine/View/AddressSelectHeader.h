//
//  AddressSelectHeader.h
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/19.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearcherTextField.h"

NS_ASSUME_NONNULL_BEGIN

@protocol AddressSelectHeaderDelegate <NSObject>

@optional
- (void)headerCityButtonClick:(UIButton *)cityBtn;
- (void)headerSearchButtonClick:(NSString *)searchText;

@end

@interface AddressSelectHeader : UIView <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIButton *cityButton;

@property (weak, nonatomic) IBOutlet SearcherTextField *searchText;

@property (weak, nonatomic) id<AddressSelectHeaderDelegate> delegate;
@property (weak, nonatomic) IBOutlet WRBaseButton *searchBtn;

@end

NS_ASSUME_NONNULL_END
