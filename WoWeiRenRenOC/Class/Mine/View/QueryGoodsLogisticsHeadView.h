//
//  QueryGoodsLogisticsHeadView.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/9/11.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LogisticsModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface QueryGoodsLogisticsHeadView : UIView
@property (weak, nonatomic) IBOutlet UILabel *ordeCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyLabel;
@property (weak, nonatomic) IBOutlet UILabel *logisticsLabel;
@property (strong, nonatomic)LogisticsModel *model;
@end

NS_ASSUME_NONNULL_END
