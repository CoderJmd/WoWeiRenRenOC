//
//  UserOrderDetailsQRCodeCell.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/7/4.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserShoppingOrderDetailsModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserOrderDetailsQRCodeCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (strong, nonatomic)UserShoppingOrderDetailsModel *model;
@end

NS_ASSUME_NONNULL_END
