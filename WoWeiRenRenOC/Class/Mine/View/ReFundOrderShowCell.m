//
//  ReFundOrderShowCell.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/9/1.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "ReFundOrderShowCell.h"

@implementation ReFundOrderShowCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.addBtn addTarget:self action:@selector(addCount) forControlEvents:UIControlEventTouchUpInside];
    [self.reduceBtn addTarget:self action:@selector(subtractionCount) forControlEvents:UIControlEventTouchUpInside];
    [self.selectorBtn addTarget:self action:@selector(selectorBtnClick) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - 添加
- (void)addCount {
    
   
    int count = self.model.num;
    count +=1;
    self.model.num++;
    self.reduceImg.image = [UIImage imageNamed:@"jian1"];
    self.countTF.text = [NSString stringWithFormat:@"%.0ld",(long)self.model.num];
    
   
    
    
}

#pragma mark - 减少
- (void)subtractionCount {
    
    
    if (self.model.num == 1) {
        SVP(@"购买数量最少为1");
        return;
    }

    self.model.num--;
    self.countTF.text = [NSString stringWithFormat:@"%.0ld",(long)self.model.num];
    if (self.model.num == 1) {
        self.reduceImg.image = [UIImage imageNamed:@"jian"];
    }
    

}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(MailGoodsDetailsModel *)model {
    
    _model = model;
    self.nameLabel.text = model.sku_name;
    self.contentLabel.text = [CommonTTool getGoodsSpecifications:model.gg_info];
    self.priceLabel.text = [CommonTTool getPrice:model.unit_price];
    
    self.countTF.text = [NSString stringWithFormat:@"%.0ld",(long)model.num];
    SD_SETIMAGE_PlaceHolder(self.imgView, model.img_path, @"default_image_square");
    
    
}




- (void)selectorBtnClick {
    
    self.selectorBtn.selected = !self.selectorBtn.selected;
    self.model.isSelector = self.selectorBtn.selected;
//    if (self.refreshPric) {
//        self.refreshPric();
//    }
}




@end
