//
//  MailDetailsAddAbulkCell.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/17.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MailGoodsDetailsModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface MailDetailsAddAbulkCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *addBtn;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *ptCountLabel;


@property (strong, nonatomic)pt_group_listModel *model;
@property (strong, nonatomic)id timer;
@end

NS_ASSUME_NONNULL_END
