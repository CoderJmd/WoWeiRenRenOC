//
//  MailClassHeadView.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/12.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^SearchName)(NSString *);
@interface MailClassHeadView : UIView
@property (weak, nonatomic) IBOutlet UIView *searcheBgView;
@property (weak, nonatomic) IBOutlet UIView *buttonBgView;
@property (weak, nonatomic) IBOutlet UIImageView *searchImgView;
@property (weak, nonatomic) IBOutlet UILabel *searchLabel;
@property (weak, nonatomic) IBOutlet UITextField *searchTF;
@property (copy, nonatomic)SearchName searchName;

@end

NS_ASSUME_NONNULL_END
