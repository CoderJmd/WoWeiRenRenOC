//
//  PropertyCell.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/9/18.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "PropertyCell.h"

@implementation PropertyCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.layer.borderWidth = 1;
    self.layer.cornerRadius = 12.5;
    self.clipsToBounds = YES;
}

- (void)setTintStyleColor:(UIColor *)color {
    self.layer.borderColor = color.CGColor;
    self.propertyL.textColor = color;
}

- (void)setCellStatus:(int)type {
    
    
    
    if (type == 1) {
        //可用
        self.backgroundColor = kRGBColor(233, 233,233);
        self.layer.borderColor = kRGBColor(233, 233,233).CGColor;
        self.propertyL.textColor = kRGBColor(17, 16, 13);
    }else if (type == 2) {
        //不可用
        self.backgroundColor = kRGBColor(250, 250,250);
        self.layer.borderColor = kRGBColor(250, 250,250).CGColor;
        self.propertyL.textColor = kRGBColor(219, 219, 219);
        
    }else {
        //选中
        self.backgroundColor = kRGBAColor(206, 160, 9, 0.2);
        self.layer.borderColor = kRGBAColor(189, 135, 35, 1).CGColor;
        self.propertyL.textColor = kRGBAColor(189, 135, 35, 1);
    }
    
    
    
}

@end
