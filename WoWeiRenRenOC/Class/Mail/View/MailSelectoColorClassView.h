//
//  MailSelectoColorClassView.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/12.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MailGoodsDetailsModel.h"
#import "ORSKUDataFilter.h"

NS_ASSUME_NONNULL_BEGIN
typedef void(^RefreshMailGoodsModel)();
@interface MailSelectoColorClassView : UIView
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceInfoLabel;

@property (weak, nonatomic) IBOutlet UIButton *nextBtn;
@property (weak, nonatomic) IBOutlet UIImageView *addImgView;
@property (weak, nonatomic) IBOutlet UITextField *countTF;
@property (weak, nonatomic) IBOutlet UIImageView *subtractionBtn;
@property (strong, nonatomic)NSString *count;
@property (strong, nonatomic)MailGoodsDetailsModel *model;
@property (strong, nonatomic)NSArray *goodsAttrubuteArray;
@property (weak, nonatomic) IBOutlet UIView *oneBgView;
@property (nonatomic,strong) NSMutableArray *buttonArrayS;

@property (weak, nonatomic) IBOutlet UIButton *addShoppingCarBtn;
@property (weak, nonatomic) IBOutlet UIButton *buyBtn;
@property (weak, nonatomic) IBOutlet UIView *twoBgView;
@property (copy, nonatomic)RefreshMailGoodsModel refreshMailGoodsModel;
@property (assign, nonatomic)BOOL isStartPt;//是否开团
@property (assign, nonatomic)BOOL isAddPt;//是否参团
@property (assign, nonatomic)BOOL isShoppingCar;//是否购物车
@property (assign, nonatomic)BOOL isBuyClik;//是否立即购买
@property (assign, nonatomic)int num;

@property (weak, nonatomic) IBOutlet UILabel *oneCount;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *oneBgViewHeight;
@property (weak, nonatomic) IBOutlet UIScrollView *bgScrollview;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bgScrollviewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bgViewHeight;
@property (weak, nonatomic) IBOutlet UIButton *guanbiBtn;
@property (strong, nonatomic)NSMutableArray *availableSkuAttArray;
@property (weak, nonatomic) IBOutlet UIView *guanbiView;
@property (nonatomic, strong) ORSKUDataFilter *filter;
@end

NS_ASSUME_NONNULL_END
