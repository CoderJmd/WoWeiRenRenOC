//
//  MailDetailsAlreadyAbulkCell.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/17.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MailGoodsDetailsModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface MailDetailsAlreadyAbulkCell : UITableViewCell
@property (strong, nonatomic)MailGoodsDetailsModel *model;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end

NS_ASSUME_NONNULL_END
