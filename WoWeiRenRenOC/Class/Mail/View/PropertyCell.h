//
//  PropertyCell.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/9/18.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PropertyCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *propertyL;
- (void)setTintStyleColor:(UIColor *)color;
- (void)setCellStatus:(int)type;
@end

NS_ASSUME_NONNULL_END
