//
//  MailDetailsAlreadyAbulkCell.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/17.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "MailDetailsAlreadyAbulkCell.h"

@implementation MailDetailsAlreadyAbulkCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(MailGoodsDetailsModel *)model {
    
    _model = model;
    self.nameLabel.text = [NSString stringWithFormat:@"%@人参加",model.pt_user_count];
    int w = 15;
    for (int i = 0; i<model.pt_user_list.count; i++) {
        NSDictionary *dict = model.pt_user_list[i];
        UIImageView *imgView = [[UIImageView alloc]init];
        imgView.frame = CGRectMake(w, 15, 30, 30);
        imgView.layer.cornerRadius = 15;
        imgView.clipsToBounds = YES;
        NSString *url = dict[@"head_img_path"];
        if ([CommonTTool isStrNull:url]) {
            url = @"";
        }
        SD_SETIMAGE_PlaceHolder(imgView, url, @"defHead");
        w += 25;
        if (w > kScreenW - 120) {
            return;
        }
        [self.contentView addSubview:imgView];

    }
    
    
}

@end
