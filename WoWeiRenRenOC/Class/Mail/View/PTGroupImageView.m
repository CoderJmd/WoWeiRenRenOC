//
//  PTGroupImageView.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/7/2.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "PTGroupImageView.h"

@implementation PTGroupImageView

+ (instancetype)getView {
    
    return [[NSBundle mainBundle]loadNibNamed:@"PTGroupImageView" owner:self options:nil].lastObject;
}

- (void)awakeFromNib {
    
    [super awakeFromNib];
    self.imgView.layer.cornerRadius = 25;
    self.imgView.clipsToBounds = YES;
    
}

@end
