//
//  MailSelectorColorCell.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/12.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "MailSelectorColorCell.h"

@implementation MailSelectorColorCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setSelectGoodAttributeArray:(NSMutableArray *)selectGoodAttributeArray {
    
    _selectGoodAttributeArray = selectGoodAttributeArray;
    NSString *str = @"已选：";
    for (NSString *seletStr in selectGoodAttributeArray) {
       str = [NSString stringWithFormat:@"%@ %@",str,seletStr];
    }
    self.nameLabel.text = str;
}

@end
