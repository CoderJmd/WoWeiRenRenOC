//
//  SelectorShenFenView.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/7/31.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "SelectorShenFenView.h"

@implementation SelectorShenFenView

- (void)awakeFromNib {
    
    [super awakeFromNib];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cancelView)];
    [self addGestureRecognizer:tap];
    UITapGestureRecognizer *tapTwo = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap)];
    [self.bgView addGestureRecognizer:tapTwo];
    self.bgView.layer.cornerRadius = 5;
    self.bgView.clipsToBounds = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(weixinTextFieldTextChanged) name:UITextFieldTextDidChangeNotification object:nil];
}


-(void)weixinTextFieldTextChanged {
    
    if ([self.codeTF isFirstResponder]) {
        
        if (self.codeTF.text.length >= 19) {
        NSMutableString *mutStr = [NSMutableString stringWithString:self.codeTF.text];
        NSString *str = [mutStr substringToIndex:18];
        self.codeTF.text = str;
        }
    }

}



+ (instancetype)showView
{
    UIWindow *window = UIApplication.sharedApplication.keyWindow;

    SelectorShenFenView *view = [[NSBundle mainBundle] loadNibNamed:@"SelectorShenFenView" owner:self options:nil].lastObject;
    view.frame = window.bounds;
    view.backgroundColor =  [[UIColor blackColor] colorWithAlphaComponent:0.7];
    
    [window addSubview:view];

    
    
    return view;
}




- (void)cancelView {
    
    [UIView animateWithDuration:0.5 animations:^{
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)tap {
    
    
}
- (IBAction)cancelBtnClick:(id)sender {
    [self cancelView];
}
- (IBAction)saveBtnClcik:(id)sender {
    
    if ([CommonTTool isStrNull:self.nameTF.text]) {
        SVP(@"请输入付款人真实姓名");
        return;
    }
    if ([CommonTTool isStrNull:self.codeTF.text]) {
        SVP(@"请输入付款人身份证号");
        return;
    }
    
    NSString *url = @"user/saveUserIdcard";
    NSMutableDictionary *para = [NSMutableDictionary dictionary];
    [para setObject:self.nameTF.text forKey:@"idcard_name"];
    [para setObject:self.codeTF.text forKey:@"idcard_no"];

    if (![CommonTTool isStrNull:self.htModel.user_idcard_id]) {
        url = @"user/updateUserIdcard";
        [para setObject:self.htModel.user_idcard_id forKey:@"user_idcard_id"];
    }
    
    [NetRequestTool postUrl:url parameters:para success:^(id responseObject) {
        if (![CommonTTool isStrNull:self.htModel.user_idcard_id]) {
            SVP(@"修改成功");
        }else {
            SVP(@"添加成功");
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:kHTShenFenJobUpDate object:nil];
        [self cancelView];
        
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        
    }];
    
    
    
}

- (void)setHtModel:(HTShenFenModel *)htModel {
    
    _htModel = htModel;
    
    self.nameTF.text = htModel.idcard_name;
    self.codeTF.text = htModel.idcard_no;
    
}

@end
