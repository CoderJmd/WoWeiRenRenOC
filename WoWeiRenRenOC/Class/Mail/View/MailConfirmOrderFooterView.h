//
//  MailConfirmOrderFooterView.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/13.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShoppingCartModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface MailConfirmOrderFooterView : UIView
@property (weak, nonatomic) IBOutlet UILabel *numLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UITextField *remkTF;
@property (weak, nonatomic) IBOutlet UILabel *courierLabel;
@property (strong, nonatomic)ShoppingCartModel *model;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *yundeiHeight;

@end

NS_ASSUME_NONNULL_END
