//
//  MailConfirmOrderGoodsCell.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/12.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "MailConfirmOrderGoodsCell.h"

@implementation MailConfirmOrderGoodsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(ShoppingCartGoodsMode *)model {
    
    _model = model;
    self.nameLabel.text = model.sku_name;
    
    self.contentLabel.text = [CommonTTool getGoodsSpecifications:model.gg_info];
    
    self.priceLabel.text = [CommonTTool getPrice:model.vip_price];
    if (model.is_yk == 2) {
        self.priceLabel.text = [CommonTTool getPrice:model.yk_price];
    }
    if (model.sku_type == 2 || model.sku_type == 9) {
        self.priceLabel.text = [CommonTTool getPrice:model.buy_price];
    }
    if (self.order_type == 4) {
        self.priceLabel.text = [CommonTTool getPrice:model.pt_price];
    }

    self.numLabel.text = [NSString stringWithFormat:@"x%.0ld",(long)model.num];
    SD_SETIMAGE_PlaceHolder(self.imgView, model.img_path, @"default_image_square");
    
}


@end
