//
//  PTOrderDetailsGoodsCell.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/7/6.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserShoppingOrderModel.h"
#import "MailGoodsDetailsModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PTOrderDetailsGoodsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberLabel;
@property (weak, nonatomic) IBOutlet UILabel *oldPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *ptStartLabel;

@property (weak, nonatomic) IBOutlet UIImageView *startImgView;

@property (weak, nonatomic) IBOutlet UILabel *successLabel;
@property (strong, nonatomic)MailGoodsDetailsModel *model;
@property (strong, nonatomic)pt_group_listModel *ptModel;


@end

NS_ASSUME_NONNULL_END
