//
//  MailHeadView.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/12.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeIconModel.h"
#import "BannerModel.h"

@protocol MailHeadViewDelegata <NSObject>

@optional
- (void)MailHeadViewBannerClickAtIndex:(NSInteger)index model:(BannerModel *_Nullable)model;
@end

NS_ASSUME_NONNULL_BEGIN
typedef void(^SearchName)(NSString *);
@interface MailHeadView : UIView
@property (weak, nonatomic) IBOutlet UIView *searcheBgView;
@property (weak, nonatomic) IBOutlet UIImageView *searchImgView;
@property (weak, nonatomic) IBOutlet UILabel *searchLabel;
@property (weak, nonatomic) IBOutlet UITextField *searchTF;
@property (nonatomic, strong) NSArray *iconArray;
@property (nonatomic, strong) NSArray *bannerArray;

@property (nonatomic, strong) NSDictionary *numDict;

@property (nonatomic, weak) id<MailHeadViewDelegata> delegate;
@property (copy, nonatomic)SearchName searchName;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchTop;

@end

NS_ASSUME_NONNULL_END

