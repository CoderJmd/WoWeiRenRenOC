//
//  MailConfirmOrderGoodsCell.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/12.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShoppingCartModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface MailConfirmOrderGoodsCell : UITableViewCell
@property (strong, nonatomic)ShoppingCartGoodsMode *model;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@property (assign, nonatomic)int order_type; //1混合订单 3虚拟订单 4拼团订单

@property (weak, nonatomic) IBOutlet UILabel *numLabel;

@end

NS_ASSUME_NONNULL_END
