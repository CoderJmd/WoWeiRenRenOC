//
//  AbulkDetailsInfoCell.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/17.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "AbulkDetailsInfoCell.h"
#import "PTGroupImageView.h"

@implementation AbulkDetailsInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    // gradient
    CAGradientLayer *gl = [CAGradientLayer layer];
    gl.frame = CGRectMake(0, 0, kScreenW - 60, 40);
    gl.startPoint = CGPointMake(0.22, 0.36);
    gl.endPoint = CGPointMake(0.85, 0.72);
    gl.colors = @[(__bridge id)[UIColor colorWithRed:221/255.0 green:169/255.0 blue:72/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:189/255.0 green:135/255.0 blue:35/255.0 alpha:1.0].CGColor];
    gl.locations = @[@(0), @(1.0f)];
    self.addBtn.layer.cornerRadius = 6;
    self.addBtn.clipsToBounds = YES;
    [self.addBtn.layer insertSublayer:gl atIndex:0];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(pt_group_listModel *)model {
    
    _model = model;
    int count = [model.tg_group_ct_rs intValue] - [model.tg_group_pt_rs intValue];
    NSString *countStr = [NSString stringWithFormat:@"%d",count];
    NSString *ptCountStr = [NSString stringWithFormat:@"还差%d人,即可拼团成功,",count];
    NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:ptCountStr];
    [attri addAttribute:NSForegroundColorAttributeName value: [UIColor redColor] range:NSMakeRange(2, countStr.length + 1)];
    self.countLabel.attributedText =attri;
    
    if (model.tg_group_state != 1) {
        self.btnHeight.constant = 0;
    }

    
    
    NSString *deadlineStr = model.tg_group_end_time;
    // 倒计时的时间 测试数据
    //    NSString *deadlineStr = @"2020-06-24 12:00:00";
    // 当前时间的时间戳
    NSString *nowStr = [CommonTTool getCurrentTimeyyyymmdd];
    // 计算时间差值
    NSInteger secondsCountDown = [CommonTTool getDateDifferenceWithNowDateStr:nowStr deadlineStr:deadlineStr];
    if (secondsCountDown <= 0) {
        self.timeLabel.text = @"该拼团已结束";
        return;
    }

    __weak __typeof(self) weakSelf = self;
    if (_timer == nil) {
        __block NSInteger timeout = secondsCountDown; // 倒计时时间
        
        if (timeout!=0) {
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
            dispatch_source_set_timer(_timer, dispatch_walltime(NULL, 0), 1.0*NSEC_PER_SEC,  0); //每秒执行
            dispatch_source_set_event_handler(_timer, ^{
                if(timeout <= 0){ //  当倒计时结束时做需要的操作: 关闭 活动到期不能提交
                    dispatch_source_cancel(self->_timer);
                    self->_timer = nil;
                    dispatch_async(dispatch_get_main_queue(), ^{
                       // weakSelf.timeLabel.text = @"当前活动已结束";
                    });
                } else { // 倒计时重新计算 时/分/秒
                    NSInteger days = 0;
                    NSInteger hours = (int)((timeout-days*24*3600)/3600);
                    NSInteger minute = (int)(timeout-days*24*3600-hours*3600)/60;
                    NSInteger second = timeout - days*24*3600 - hours*3600 - minute*60;
                    NSString *strTime = [NSString stringWithFormat:@"%02ld:%02ld:%02ld 后结束", hours, minute,(long)second];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        weakSelf.timeLabel.text = strTime;
                        
                    });
                    timeout--; // 递减 倒计时-1(总时间以秒来计算)
                }
            });
            dispatch_resume(_timer);
        }
    }

    
}


- (void)setPtList:(NSMutableArray *)ptList {
    
    
    _ptList = ptList;
    
    CGFloat x = kScreenW / (ptList.count + 1) - 85;
    if (ptList.count >  1) {
        x = kScreenW / (ptList.count + 1) - 45;
    }

    for (int i = 0 ; i < ptList.count + 1; i++) {
        
        PTGroupImageView *groupView = [PTGroupImageView getView];
        groupView.frame = CGRectMake(x, 0, 75, 75);
        x += 85;
        [self.ptBgView addSubview:groupView];
        if (i != ptList.count) {
            UserModel *model = ptList[i];
            SD_SETIMAGE_PlaceHolder(groupView.imgView, model.head_img_path, @"defHead");
        }
        if (i != 0) {
            groupView.huangGuanImgView.hidden = YES;
        }

        
    }
    
    
    
    
    
}





@end
