//
//  MailConfirmOrderFooterView.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/13.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "MailConfirmOrderFooterView.h"

@implementation MailConfirmOrderFooterView

- (void)awakeFromNib {
    
    [super awakeFromNib];
    [self.remkTF addTarget:self action:@selector(textFieldDidChange) forControlEvents:UIControlEventEditingChanged];
    
}

- (void)textFieldDidChange {
    self.model.remarks = self.remkTF.text;
}

@end
