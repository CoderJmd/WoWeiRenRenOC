//
//  MailClassHeadView.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/12.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "MailClassHeadView.h"
@interface MailClassHeadView ()<UITextFieldDelegate>

@end

@implementation MailClassHeadView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

    self.searcheBgView.layer.cornerRadius = 15;
    self.searcheBgView.clipsToBounds = YES;
    self.searchTF.delegate = self;
    self.searchTF.returnKeyType = UIReturnKeySearch;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
//    self.searchImgView.hidden = YES;
    self.searchLabel.hidden = YES;
    NSLog(@"2");// 准备开始输入  文本字段将成为第一响应者
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    NSLog(@"3");//文本彻底结束编辑时调用
    if (textField.text.length == 0) {
//        self.searchImgView.hidden = NO;
        self.searchLabel.hidden = NO;
    }
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    if (![CommonTTool isStrNull:textField.text]) {
        
        if (self.searchName) {
            self.searchName(textField.text);
        }
        
    }else {
        SVP(@"请输入需要搜索的内容");
    }
    
    return YES;
}


@end
