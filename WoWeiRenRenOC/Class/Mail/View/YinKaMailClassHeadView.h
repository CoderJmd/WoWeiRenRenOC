//
//  YinKaMailClassHeadView.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/12/14.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^SearchName)(NSString *);

@interface YinKaMailClassHeadView : UIView
@property (weak, nonatomic) IBOutlet UIView *searcheBgView;
@property (weak, nonatomic) IBOutlet UIView *buttonBgView;
@property (weak, nonatomic) IBOutlet UIImageView *searchImgView;
@property (weak, nonatomic) IBOutlet UILabel *searchLabel;
@property (weak, nonatomic) IBOutlet UITextField *searchTF;
@property (copy, nonatomic)SearchName searchName;
@property (weak, nonatomic) IBOutlet UIView *twoButtonBgView;

@end

NS_ASSUME_NONNULL_END
