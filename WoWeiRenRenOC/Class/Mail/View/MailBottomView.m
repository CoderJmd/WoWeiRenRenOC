//
//  MailBottomView.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/12.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "MailBottomView.h"

@implementation MailBottomView

- (void)awakeFromNib {
    
    [super awakeFromNib];
    self.layer.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0].CGColor;
    self.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.1].CGColor;
    self.layer.shadowOffset = CGSizeMake(0,-2);
    self.layer.shadowOpacity = 1;
    self.layer.shadowRadius = 8;
    
    
}

@end
