//
//  PTOrderDetailsGoodsCell.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/7/6.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "PTOrderDetailsGoodsCell.h"

@implementation PTOrderDetailsGoodsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(MailGoodsDetailsModel *)model {
    
    _model = model;
    self.nameLabel.text = model.sku_name;
    self.contentLabel.text = [CommonTTool getGoodsSpecifications:model.gg_info];
    self.priceLabel.text = [CommonTTool getPrice:model.unit_price];
    


    SD_SETIMAGE_PlaceHolder(self.imgView, model.img_path, @"default_image_square");
    self.ptStartLabel.layer.borderWidth = 0.5;
    self.ptStartLabel.layer.borderColor = kRGBAColor(206, 160, 9, 1).CGColor;
    self.ptStartLabel.textColor = kRGBAColor(206, 160, 9, 1);
    
    self.numberLabel.textColor = WhiteColor;
    self.numberLabel.backgroundColor = kRGBAColor(206, 160, 9, 1);

    [self requestData];
    
    
}

- (void)setPtModel:(pt_group_listModel *)ptModel {
    
    _ptModel = ptModel;
    if (ptModel.tg_group_state == 2) {
        self.startImgView.hidden = NO;
        self.startImgView.image = [UIImage imageNamed:@"pintuanchenggong"];

    }else if (ptModel.tg_group_state == 3) {
        self.startImgView.hidden = NO;
        self.startImgView.image = [UIImage imageNamed:@"pintuanshibai"];

    }else {
        self.startImgView.hidden = YES;
    }

    
}


- (void)requestData
{
    NSString *url = @"sku/queryGoodsInfo";
    NSMutableDictionary *para = [NSMutableDictionary dictionary];
    [para setObject:self.model.sku_id forKey:@"sku_id"];
    [para setObject:@"2" forKey:@"is_pt"];
    
    
    [NetRequestTool postUrl:url parameters:para success:^(id responseObject) {
        MailGoodsDetailsModel *model = [MailGoodsDetailsModel mj_objectWithKeyValues:responseObject[@"object"]];
        self.successLabel.text = [NSString stringWithFormat:@"已拼%d件",model.sold];
        
        NSString *price = [NSString stringWithFormat:@"%@",[CommonTTool getPrice:model.market_price]];
           NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:price];
        [attri addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlinePatternSolid | NSUnderlineStyleSingle) range:NSMakeRange(0, attri.length)];
        self.oldPriceLabel.attributedText = attri;
        
        self.numberLabel.text = [NSString  stringWithFormat:@" %@ ",model.pt_ct_rs];

        
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        
    }];
}

@end
