//
//  AbulkDetailsInfoCell.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/17.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserShoppingOrderDetailsModel.h"
#import "MailGoodsDetailsModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface AbulkDetailsInfoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *addBtn;
@property (strong, nonatomic)pt_group_listModel *model;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic)id timer;
@property (weak, nonatomic) IBOutlet UIView *ptBgView;
@property (strong, nonatomic)NSMutableArray *ptList;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnHeight;

@end

NS_ASSUME_NONNULL_END
