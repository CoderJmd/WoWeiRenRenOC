//
//  MailCell.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/12.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "MailCell.h"

@implementation MailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.bgView.layer.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0].CGColor;
    self.bgView.layer.cornerRadius = 6;
    self.bgView.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.1].CGColor;
    self.bgView.layer.shadowOffset = CGSizeMake(0,0);
    self.bgView.layer.shadowOpacity = 1;
    self.bgView.layer.shadowRadius = 4;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(MailGoodsModel *)model {
    
    _model = model;
    SD_SETIMAGE_PlaceHolder(self.imgView, model.img_path, @"default_image_square")
    self.titleLabel.text = model.sku_name;
    self.vipPriceLabel.text = [CommonTTool getPrice:model.vip_price];
    NSString *price = [NSString stringWithFormat:@"销售价¥%@",[CommonTTool getPrice:model.market_price]];
    NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:price];
    [attri addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlinePatternSolid | NSUnderlineStyleSingle) range:NSMakeRange(0, attri.length)];
    [attri addAttribute:NSStrikethroughColorAttributeName value: self.priceLabel.textColor range:NSMakeRange(0, attri.length)];
    self.priceLabel.attributedText = attri;
    self.contentLabel.text = model.att_title;
    
    self.goodsTypeImgView.hidden = YES;
    self.goodsTypeLabel.hidden = YES;
    self.usYinKaLabel.text = @"会员补贴价";
    if (model.is_yk == 2) {
        self.goodsTypeImgView.hidden = NO;
        self.goodsTypeLabel.hidden = NO;
        self.goodsTypeImgView.image = [UIImage imageNamed:@"yinkazhuangshu"];
        self.goodsTypeLabel.text = @"银卡专属";
        self.usYinKaLabel.text = @"银卡会员价";
        self.vipPriceLabel.text = [CommonTTool getPrice:model.yk_price];

    }
    if (model.is_pt == 2) {
        self.goodsTypeImgView.hidden = NO;
        self.goodsTypeLabel.hidden = NO;
        self.goodsTypeImgView.image = [UIImage imageNamed:@"pintuanshangping"];
        self.goodsTypeLabel.text = @"拼团商品";
    }
    
}

@end
