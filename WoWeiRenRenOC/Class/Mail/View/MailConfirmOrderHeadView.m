//
//  MailConfirmOrderHeadView.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/12.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "MailConfirmOrderHeadView.h"

@implementation MailConfirmOrderHeadView


- (void)awakeFromNib {
    
    [super awakeFromNib];
    

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(queryUserIdcardList) name:kHTShenFenJobUpDate object:nil];
    
    
}

- (void)setModel:(UserShipingAddressModel *)model {
 
    _model = model;
    
    self.nameLabel.hidden = NO;
    self.phoneLabel.hidden = NO;
    self.addressLabel.hidden = NO;
    self.nullAddressLabel.hidden = YES;
    
    self.nameLabel.text = model.name;
    self.phoneLabel.text = model.phone;
    self.addressLabel.text = [NSString stringWithFormat:@"%@%@%@%@",model.province,model.city,model.area,model.address];
    
    
}
    
    
- (void)setPtModel:(UserShoppingOrderDetailsModel *)ptModel {
        
    _ptModel = ptModel;
    self.nameLabel.hidden = NO;
    self.phoneLabel.hidden = NO;
    self.addressLabel.hidden = NO;
    self.nullAddressLabel.hidden = YES;

    self.nameLabel.text = ptModel.add_name;
    self.phoneLabel.text = ptModel.add_phone;
    self.addressLabel.text = [NSString stringWithFormat:@"%@%@%@%@",ptModel.add_province,ptModel.add_city,ptModel.add_area,ptModel.add_address];
}


- (void)setHtModel:(HTShenFenModel *)htModel {
 
    _htModel = htModel;
    
    self.htLabel.text = [NSString stringWithFormat:@"%@  %@",htModel.idcard_name,htModel.idcard_no];
    
}
    
    
- (void)queryUserIdcardList {
    
    [NetRequestTool postUrl:@"user/queryUserIdcardList" parameters:@{@"page":@(1),@"limit":@"10"} success:^(id responseObject) {
        
        NSArray *modelArray = [HTShenFenModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]];
        if (modelArray.count != 0) {
            self.htModel = modelArray[0];
        }
        
    } failure:^(NSInteger statusCode) {
       
    } finish:^{
        
    }];
    
}

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

@end
