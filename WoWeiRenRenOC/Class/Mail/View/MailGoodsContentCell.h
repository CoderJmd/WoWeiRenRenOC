//
//  MailGoodsContentCell.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/12.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MailGoodsDetailsModel.h"
#import <WebKit/WebKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^RefreshWebViewHeight)(CGFloat);
@interface MailGoodsContentCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (strong, nonatomic) WKWebView *webView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webViewHeight;
@property (strong, nonatomic)MailGoodsDetailsModel *model;
@property (copy, nonatomic)RefreshWebViewHeight refreshWebViewHeight;
@end

NS_ASSUME_NONNULL_END
