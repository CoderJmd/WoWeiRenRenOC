//
//  MailDetailsAddBulkView.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/17.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "MailDetailsAddBulkView.h"

@implementation MailDetailsAddBulkView

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    CAGradientLayer *gl = [CAGradientLayer layer];
    gl.frame = self.addBtn.bounds;
    gl.startPoint = CGPointMake(0.22, 0.36);
    gl.endPoint = CGPointMake(0.85, 0.72);
    gl.colors = @[(__bridge id)[UIColor colorWithRed:221/255.0 green:169/255.0 blue:72/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:189/255.0 green:135/255.0 blue:35/255.0 alpha:1.0].CGColor];
    gl.locations = @[@(0), @(1.0f)];
    self.addBtn.layer.cornerRadius = 6;
    self.addBtn.clipsToBounds = YES;
    [self.addBtn.layer insertSublayer:gl atIndex:0];
    self.bgView.layer.cornerRadius = 12;
    self.bgView.clipsToBounds = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap)];
    [self.bgView addGestureRecognizer:tap];
    self.oneImgView.layer.cornerRadius = 25;
    self.oneImgView.clipsToBounds = YES;
}

- (void)tap {
    
}

- (void)setModel:(pt_group_listModel *)model {
    
    _model = model;
    NSString *nameCountStr = [NSString stringWithFormat:@"参与%@的团",model.name];
    NSMutableAttributedString *attriName = [[NSMutableAttributedString alloc] initWithString:nameCountStr];
    [attriName addAttribute:NSForegroundColorAttributeName value: kRGBAColor(221, 169, 72, 1) range:NSMakeRange(2, model.name.length)];
    self.oneLabel.attributedText = attriName;
    
    int count = [model.tg_group_ct_rs intValue] - [model.tg_group_pt_rs intValue];
    NSString *countStr = [NSString stringWithFormat:@"%d",count];
    NSString *ptCountStr = [NSString stringWithFormat:@"还差%d人",count];
    NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:ptCountStr];
    [attri addAttribute:NSForegroundColorAttributeName value: [UIColor redColor] range:NSMakeRange(2, countStr.length + 1)];
    self.twoLabel.attributedText =attri;
    
    
    NSString *timeCountStr = [NSString stringWithFormat:@"%@后结束",model.tg_group_end_time];
    NSMutableAttributedString *attriTime = [[NSMutableAttributedString alloc] initWithString:timeCountStr];
    [attriTime addAttribute:NSForegroundColorAttributeName value: [UIColor blackColor] range:NSMakeRange(0, model.tg_group_end_time.length)];
    self.threeLabel.attributedText = attriTime;
    
    SD_SETIMAGE_PlaceHolder(self.oneImgView, model.head_img_path, @"defHead");

    
}

@end
