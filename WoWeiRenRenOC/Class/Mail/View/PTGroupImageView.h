//
//  PTGroupImageView.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/7/2.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PTGroupImageView : UIView

+ (instancetype)getView;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UIImageView *huangGuanImgView;


@end

NS_ASSUME_NONNULL_END
