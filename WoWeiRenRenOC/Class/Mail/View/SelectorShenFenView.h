//
//  SelectorShenFenView.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/7/31.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HTShenFenModel.h"
NS_ASSUME_NONNULL_BEGIN


@interface SelectorShenFenView : UIView
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UITextField *nameTF;
@property (weak, nonatomic) IBOutlet UITextField *codeTF;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (strong, nonatomic)HTShenFenModel *htModel;
+ (instancetype)showView;
- (void)cancelView;

@end

NS_ASSUME_NONNULL_END
