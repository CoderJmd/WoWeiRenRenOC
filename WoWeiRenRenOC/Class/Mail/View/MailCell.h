//
//  MailCell.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/12.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MailGoodsModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MailCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (strong, nonatomic)MailGoodsModel *model;
@property (weak, nonatomic) IBOutlet UILabel *vipPriceLabel;

@property (weak, nonatomic) IBOutlet UIImageView *goodsTypeImgView;

@property (weak, nonatomic) IBOutlet UILabel *goodsTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *usYinKaLabel;

@property (assign, nonatomic)BOOL isShowType;
@end

NS_ASSUME_NONNULL_END
