//
//  PropertyHeader.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/9/18.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PropertyHeader : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UILabel *headName;

@end

NS_ASSUME_NONNULL_END
