//
//  MailConfirmOrderHeadView.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/12.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserShipingAddressModel.h"
#import "UserShoppingOrderDetailsModel.h"
#import "HTShenFenModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MailConfirmOrderHeadView : UIView
@property (strong, nonatomic)UserShipingAddressModel *model;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *nullAddressLabel;
@property (weak, nonatomic) IBOutlet UIImageView *addressImgViiew;
@property (strong, nonatomic)UserShoppingOrderDetailsModel *ptModel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *shenfenHeight;
@property (weak, nonatomic) IBOutlet UIView *addressBgView;

@property (weak, nonatomic) IBOutlet UIView *shenfenBgBiew;
@property (strong, nonatomic)HTShenFenModel *htModel;
@property (weak, nonatomic) IBOutlet UILabel *htLabel;
- (void)queryUserIdcardList;
@end

NS_ASSUME_NONNULL_END
