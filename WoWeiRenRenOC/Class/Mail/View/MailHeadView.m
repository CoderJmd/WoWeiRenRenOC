//
//  MailHeadView.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/12.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "MailHeadView.h"
#import <UIButton+WebCache.h>
#import "VerticalButton.h"
#import "HomeSearchVC.h"

@interface MailHeadView () <SDCycleScrollViewDelegate,UITextFieldDelegate>

@property (unsafe_unretained, nonatomic) IBOutlet NSLayoutConstraint *topMargin;
@property (unsafe_unretained, nonatomic) IBOutlet NSLayoutConstraint *colorBgH;

@property (unsafe_unretained, nonatomic) IBOutlet UIView *colorBgView;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *bannerBgView;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *iconBgView;

@property (unsafe_unretained, nonatomic) IBOutlet UIView *numberBgView;
@property (weak, nonatomic) IBOutlet UILabel *bg1LastNum;
@property (weak, nonatomic) IBOutlet UILabel *bg1TotalNum;

@property (weak, nonatomic) IBOutlet UIView *bg2;
@property (weak, nonatomic) IBOutlet UILabel *bg2LastNum;
@property (weak, nonatomic) IBOutlet UILabel *bg2totalNum;
@property (weak, nonatomic) IBOutlet UILabel *bg2BusinessNum;

@property (nonatomic, strong) SDCycleScrollView *cycleScrollView;

@end

@implementation MailHeadView

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    if (NeediPhoneXiPhoneXR) {
        self.topMargin.constant += 22;
        self.colorBgH.constant += 22;
    }
    self.bg2.hidden = ![[NSUserDefaults standardUserDefaults] boolForKey:@"kUserIsMerchants"];
    
    // gradient
    CAGradientLayer *gl = [CAGradientLayer layer];
    gl.frame = CGRectMake(0, 0, kScreenW, self.colorBgH.constant);
    gl.startPoint = CGPointMake(0.5, 0);
    gl.endPoint = CGPointMake(0.5, 0.89);
    gl.colors = @[(__bridge id)[UIColor colorWithRed:206/255.0 green:160/255.0 blue:9/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:246/255.0 green:246/255.0 blue:246/255.0 alpha:1.0].CGColor];
    gl.locations = @[@(0), @(1.0f)];
    [self.colorBgView.layer insertSublayer:gl atIndex:0];
    
    self.numberBgView.layer.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0].CGColor;
    self.numberBgView.layer.cornerRadius = 6;
    self.numberBgView.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.1].CGColor;
    self.numberBgView.layer.shadowOffset = CGSizeMake(0,0);
    self.numberBgView.layer.shadowOpacity = 1;
    self.numberBgView.layer.shadowRadius = 4;
    
    self.iconBgView.layer.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0].CGColor;
    self.iconBgView.layer.cornerRadius = 6;
    self.iconBgView.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.1].CGColor;
    self.iconBgView.layer.shadowOffset = CGSizeMake(0,0);
    self.iconBgView.layer.shadowOpacity = 1;
    self.iconBgView.layer.shadowRadius = 4;
    
    // bannerView
    self.cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(15, 0, KSCREEN_WIDTH-30, self.bannerBgView.height) delegate:self placeholderImage: kImageNamed(@"default_image_square")];
    self.cycleScrollView.layer.cornerRadius = 5;
    self.cycleScrollView.clipsToBounds = YES;
    self.cycleScrollView.autoScrollTimeInterval = 2;
    self.cycleScrollView.autoScroll = YES;
    self.cycleScrollView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
    self.cycleScrollView.showPageControl = YES;
    [self.bannerBgView addSubview:self.cycleScrollView];
    
    self.cycleScrollView.localizationImageNamesGroup = @[@"default_image_square",@"default_image_square"];
    
    self.searcheBgView.layer.cornerRadius = 15;
    self.searcheBgView.clipsToBounds = YES;
    self.searchTF.delegate = self;
    self.searchTF.returnKeyType = UIReturnKeySearch;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(jumpSearchVC)];
    [self.searcheBgView addGestureRecognizer:tap];

}

- (void)jumpSearchVC {
    
    HomeSearchVC *homeVC = [[HomeSearchVC alloc]init];
    [[CommonTTool getCurrentVC].navigationController pushViewController:homeVC animated:YES];
   
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.searchImgView.hidden = YES;
    self.searchLabel.hidden = YES;
    NSLog(@"2");// 准备开始输入  文本字段将成为第一响应者
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    NSLog(@"3");//文本彻底结束编辑时调用
    if (textField.text.length == 0) {
        self.searchImgView.hidden = NO;
        self.searchLabel.hidden = NO;
    }
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    if (![CommonTTool isStrNull:textField.text]) {
        
        if (self.searchName) {
            self.searchName(textField.text);
        }
        
    }else {
        SVP(@"请输入需要搜索的内容");
    }
    
    return YES;
}

- (void)setIconArray:(NSArray *)iconArray
{
    _iconArray = iconArray;
    
    [self.iconBgView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    NSInteger cNum = 5;
    CGFloat margin = 15.0;
    CGFloat iconW = (self.iconBgView.width - (cNum+1)*margin) / cNum;
    CGFloat iconH = 90.0;
    
    for (int i=0; i<iconArray.count; i++) {
        HomeIconModel *m = iconArray[i];
        
        VerticalButton *iconBtn = [VerticalButton buttonWithType:UIButtonTypeCustom];
        iconBtn.frame = CGRectMake(margin+(i%cNum)*(iconW+margin), (i/cNum)*iconH, iconW, iconH);
        iconBtn.titleLabel.font = [UIFont systemFontOfSize:12.0];
        iconBtn.tag = i;
        [iconBtn setTitleColor:kRGBColor(105, 105, 105) forState:UIControlStateNormal];
        [iconBtn setTitle:m.classify_name forState:UIControlStateNormal];
        [iconBtn sd_setImageWithURL:[NSURL URLWithString:m.img_path] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"jiazhenfuwu"]];
        [iconBtn addTarget:self action:@selector(iconButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.iconBgView addSubview:iconBtn];
    }
}

- (void)setBannerArray:(NSArray *)bannerArray
{
    _bannerArray = bannerArray;
    
    NSMutableArray *pathArray = [NSMutableArray array];
    for (BannerModel *model in self.bannerArray) {
        if (model.img_path) {
            [pathArray addObject:model.img_path];
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        self.cycleScrollView.imageURLStringsGroup = pathArray;
    });
}

- (void)setNumDict:(NSDictionary *)numDict
{
    _numDict = numDict;
    int testerdayTotal = [numDict[@"testerday_login_count"] intValue] + [numDict[@"xn_testerday_login_count"] intValue];
    self.bg1LastNum.text = [NSString stringWithFormat:@"%d", testerdayTotal];
    
    int user_countTotal = [numDict[@"user_count"] intValue] + [numDict[@"xn_user_count"] intValue];
    self.bg1TotalNum.text = [NSString stringWithFormat:@"%d", user_countTotal];
    
    self.bg2LastNum.text = self.bg1LastNum.text;
    self.bg2totalNum.text = self.bg1TotalNum.text;
    
    int business_countTotal = [numDict[@"business_count"] intValue] + [numDict[@"xn_business_count"] intValue];
    self.bg1TotalNum.text = [NSString stringWithFormat:@"%d", user_countTotal];
    self.bg2BusinessNum.text = [NSString stringWithFormat:@"%d", business_countTotal];
}


/** 点击图片回调 */
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index
{
    if ([self.delegate respondsToSelector:@selector(MailHeadViewBannerClickAtIndex:model:)]) {
        [self.delegate MailHeadViewBannerClickAtIndex:index model:self.bannerArray[index]];
    }
}

@end

