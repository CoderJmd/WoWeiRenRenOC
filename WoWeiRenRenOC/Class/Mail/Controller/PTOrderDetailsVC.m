//
//  PTOrderDetailsVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/7/2.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "PTOrderDetailsVC.h"
#import "ShoppingCartHeadView.h"
#import "AbulkCell.h"
#import "ShoppingCartGroupHeadView.h"
#import "ShoppingCartCell.h"
#import "ShoppingCartBottomView.h"
#import "UserOrderChildGroupView.h"
#import "UserOrderChildCell.h"
#import "UserOrderDetailsVC.h"
#import "AbulkDetailsGoodsCell.h"
#import "AbulkDetailsGoodsFooterView.h"
#import "AbulkDetailsInfoCell.h"
#import "AbulkDetailsRulesCell.h"
#import "MailConfirmOrderHeadView.h"
#import "PagingViewController.h"
#import "PtListModel.h"
#import "MailGoodsDetailsModel.h"
#import "UserModel.h"
#import "ShareView.h"
#import "PTOrderDetailsGoodsCell.h"



extern UserModel *userModel;
@interface PTOrderDetailsVC ()
@property (nonatomic,strong) MailConfirmOrderHeadView *headView;
@property (strong, nonatomic)NSMutableArray *ptList;
@property (strong, nonatomic)pt_group_listModel *ptModel;
@property (strong, nonatomic)MailGoodsDetailsModel *goodsDetailsModel;

@end

@implementation PTOrderDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.defaultStartPage = 1;
    self.title = @"参团详情";
    [self creatTableViewStyle:UITableViewStyleGrouped cellIdentity:@"PTOrderDetailsGoodsCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"AbulkDetailsInfoCell" bundle:nil] forCellReuseIdentifier:@"AbulkDetailsInfoCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"AbulkDetailsRulesCell" bundle:nil] forCellReuseIdentifier:@"AbulkDetailsRulesCell"];
    
    self.tableView.backgroundColor = kRGBColor(246, 246, 246);
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.frame = CGRectMake(0, NavBarHeight, kScreenW, kScreenH-NavBarHeight -NeedStatusViewHeight);
    self.tableView.tableHeaderView = self.headView;
    
    [self requestData];
    
}

- (void)requestData
{
    SVPShowInternetWaiting
   // 请求列表
    [NetRequestTool postUrl:@"skuOrder/querySkuOrderInfo" parameters:@{@"order_code":self.order_code} success:^(id responseObject) {
        
        self.model = [UserShoppingOrderDetailsModel mj_objectWithKeyValues:responseObject[@"object"]];
        self.headView.ptModel = self.model;
        [self.tableView reloadData];
        [self requestPtListData];
        [self queryPtGroup];
        [self querySkuInfo];
        
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        
    }];
    
}

- (void)querySkuInfo {
    
    NSString *url = @"sku/queryGoodsInfo";
    NSMutableDictionary *para = [NSMutableDictionary dictionary];
    MailGoodsDetailsModel *model = self.model.sku_order_detail_list[0];
    [para setObject:model.sku_id forKey:@"sku_id"];
    
    if ([url isEqualToString:@"sku/queryGoodsInfo"]) {
        [para setObject:@(2) forKey:@"is_pt"];
    }
    
    
    WeakSelf(ws)
    [NetRequestTool postUrl:url parameters:para success:^(id responseObject) {
        ws.goodsDetailsModel = [MailGoodsDetailsModel mj_objectWithKeyValues:responseObject[@"object"]];
        
        
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        SVPDismiss
        [ws endRefreshNoMoreData];
    }];
    
}

- (void)queryPtGroup
{
    SVPShowInternetWaiting
   // 请求列表
    [NetRequestTool postUrl:@"sku/queryPtGroup" parameters:@{@"tg_group_id":self.model.tg_group_id} success:^(id responseObject) {
        
        self.ptModel = [pt_group_listModel mj_objectWithKeyValues:responseObject[@"object"]];
        
        [self.tableView reloadData];
        
        
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        
    }];
    
}


- (void)requestPtListData
{
    SVPShowInternetWaiting
   // 请求列表
    [NetRequestTool postUrl:@"sku/queryPtGroupInfo" parameters:@{@"tg_group_id":self.model.tg_group_id} success:^(id responseObject) {
        
        self.ptList = [UserModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]];
        
        [self.tableView reloadData];
        
        
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        
    }];
    
}


- (MailConfirmOrderHeadView *)headView {
 
    if (!_headView) {
       
        _headView = [[NSBundle mainBundle] loadNibNamed:@"MailConfirmOrderHeadView" owner:self options:nil].lastObject;
        _headView.frame = CGRectMake(0, 0, kScreenW, 70);
        
        
    }
    
    return _headView;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        PTOrderDetailsGoodsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PTOrderDetailsGoodsCell"];
        cell.model = self.model.sku_order_detail_list[indexPath.row];
        if (self.ptModel != nil) {
            cell.ptModel = self.ptModel;
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        tableView.rowHeight = 130;
        return cell;
    }else if (indexPath.section == 1)  {
        
        AbulkDetailsInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AbulkDetailsInfoCell"];
        if (self.ptModel != nil) {
            cell.model = self.ptModel;
        }
        if (self.ptList != nil) {
            cell.ptList = self.ptList;
        }
        [cell.addBtn addTarget:self action:@selector(shareTapClick) forControlEvents:UIControlEventTouchUpInside];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        tableView.rowHeight = 225;
        if (cell.model.tg_group_state != 1) {
            tableView.rowHeight = 185;
        }
        return cell;
    }else {
        
        AbulkDetailsRulesCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AbulkDetailsRulesCell"];
        //cell.model = self.dataArray[indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        tableView.rowHeight = 50;
        return cell;
        
    }
    
}

#pragma mark - 分享
- (void)shareTapClick {
    
    ShareView *share =  [ShareView showView];
    share.buttonClick = ^(NSInteger tag) {
        switch (tag) {
            case 101:
            {
                [self shareType:0];
            }
                break;
            case 102:
            {
                 [self shareType:1];
            }
                break;
            case 103:
            {
                [self weiboShare];
            }
                break;
            case 104:
            {
                UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];

                pasteboard.string = [self getShareURL];

                SVP(@"链接已复制");
                 
            }
                break;
            default:
                break;
        }
        
    };
    
}

- (void)shareType:(int)scene {
    WXMediaMessage * message = [WXMediaMessage message];
    message.title = self.goodsDetailsModel.sku_name;
    message.description = self.goodsDetailsModel.att_title;
    [message setThumbImage:[UIImage imageNamed:@"logo2"]];
    
    
    WXWebpageObject * webPageObject = [WXWebpageObject object];
    webPageObject.webpageUrl =  [self getShareURL];;
    message.mediaObject = webPageObject;
    
    SendMessageToWXReq *req1 = [[SendMessageToWXReq alloc]init];
    req1.bText = NO;
    req1.message = message;
    //设置分享到朋友圈(WXSceneTimeline)、好友回话(WXSceneSession)、收藏(WXSceneFavorite)
    req1.scene = scene;
    [WXApi sendReq:req1 completion:^(BOOL success) {
        
    }];
}

- (void)weiboShare {
    
    // 微博分享、需要授权
    WBAuthorizeRequest *request = [WBAuthorizeRequest request];
    request.redirectURI = @"https://api.weibo.com/oauth2/default.html";
    request.scope = @"all";
    request.userInfo = @{@"SSO_From": @"SendMessageToWeiboViewController",
                       @"Other_Info_1": [NSNumber numberWithInt:123],
                       @"Other_Info_2": @[@"obj1", @"obj2"],
                       @"Other_Info_3": @{@"key1": @"obj1", @"key2": @"obj2"}};

    WBMessageObject *message = [WBMessageObject message];

    // 链接
    WBWebpageObject *webpage = [WBWebpageObject object];
    webpage.objectID = @"identifier1";
    webpage.title = self.goodsDetailsModel.sku_name;
    webpage.description = self.goodsDetailsModel.att_title;
    webpage.thumbnailData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"logo2" ofType:@"png"]];
    
    
    webpage.webpageUrl = [self getShareURL];

    message.mediaObject = webpage;

    WBSendMessageToWeiboRequest *weiboRequest = [WBSendMessageToWeiboRequest requestWithMessage:message
                                                                                     authInfo:request
                                                                                 access_token:nil];
    //    WBSendMessageToWeiboRequest *weiboRequest = [WBSendMessageToWeiboRequest requestWithMessage:message];
    weiboRequest.userInfo = nil;
    BOOL isSuccess =  [WeiboSDK sendRequest:weiboRequest];
    NSLog(@"分享是否成功 %d",isSuccess);
    
}

- (NSString *)getShareURL {
    
    NSString *details = @"";
    if (self.goodsDetailsModel.sku_type == 1 || self.goodsDetailsModel.sku_type == 3) {
        details = [NSString stringWithFormat:@"type=1&sku_id=%@&code=%@&tg_group_id=%@",self.goodsDetailsModel.sku_id,userModel.my_code,self.ptModel.tg_group_id];
    }
    if (self.goodsDetailsModel.sku_type == 1 && self.goodsDetailsModel.is_pt == 2) {
        details = [NSString stringWithFormat:@"type=3&sku_id=%@&code=%@&tg_group_id=%@",self.goodsDetailsModel.sku_id,userModel.my_code,self.ptModel.tg_group_id];
    }
    
    if (self.goodsDetailsModel.sku_type == 2 || self.goodsDetailsModel.sku_type == 9) {
        details = [NSString stringWithFormat:@"type=2&sku_id=%@&code=%@&tg_group_id=%@",self.goodsDetailsModel.sku_id,userModel.my_code,self.ptModel.tg_group_id];
    }
    
    return [NSString stringWithFormat:@"%@%@",SHARE_Detils_URL,details];
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        UserOrderChildGroupView *view = [[NSBundle mainBundle] loadNibNamed:@"UserOrderChildGroupView" owner:self options:nil].lastObject;
        view.nameLabel.text = self.model.business_name;
        view.statusLabel.hidden = YES;
        view.jumpBtn.tag = section;
        [view.jumpBtn addTarget:self action:@selector(headVieTapClick:) forControlEvents:UIControlEventTouchUpInside];
        view.tag = section;
        
        return view;
    }else {
        
        UIView *view = [[UIView alloc]init];
        view.backgroundColor = [UIColor clearColor];

        return view;;
        
    }
    
   
}

- (void)headVieTapClick:(UIButton *)view {
    PagingViewController *vc= [[PagingViewController alloc]init];
    vc.business_id = self.model.business_id;
    [self.navigationController pushViewController:vc animated:YES];
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        
        return 60;
    }
    
    return 10;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = [UIColor clearColor];

    return view;;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 0.01f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section  {
    if (self.model == nil) {
        return 0;
    }
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 3;
}
    


@end

