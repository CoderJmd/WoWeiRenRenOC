//
//  OrderPayVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/29.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "OrderPayVC.h"
extern UserModel *userModel;
@interface OrderPayVC ()
@property (weak, nonatomic) IBOutlet WRBaseButton *confirmBtn;
@property (weak, nonatomic) IBOutlet UIButton *zfbBtn;
@property (weak, nonatomic) IBOutlet UIButton *wxBtn;

@property (nonatomic, assign) BOOL isWx;
@property (strong, nonatomic)NSString *order_code;
@end

@implementation OrderPayVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"支付";
    [self setUi];
}

- (void)setUi {
    
    
    if (NeediPhoneXiPhoneXR) {
        self.top.constant = NEW_StatusBarHeight;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(noticePaySuccess) name:NoticePaySuccess object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(noticePayFailure) name:NoticePayFailure object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(noticePayCancel) name:NoticePayCancel object:nil];
    
    if (self.is_ht == 2) {
        self.bgView.hidden = NO;
    }
    
    
}

- (void)noticePaySuccess {
    
    SVP(@"支付成功！");
    [self.navigationController popToRootViewControllerAnimated:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"" object:nil];
}

- (void)noticePayFailure {
    
    SVP(@"支付失败！");
    [self.navigationController popToRootViewControllerAnimated:YES];

}

- (void)noticePayCancel {
    
    SVP(@"已取消支付");
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}




- (void)getZFBPay {
    
    SVPShowInternetWaiting
    [NetManager isGetAliPayInfoParameters:@{@"order_res":@(self.type),@"parent_order_code":self.parent_order_code} success:^(NSDictionary * _Nonnull dic) {
        //应用注册scheme,在AliSDKDemo-Info.plist定义URL types
        NSString *appScheme = @"woweirenren";
        
        // NOTE: 调用支付结果开始支付
        [[AlipaySDK defaultService] payOrder:dic[@"object"] fromScheme:appScheme callback:^(NSDictionary *resultDic) {
            NSLog(@"reslut = %@",resultDic);
        }];
        
    } finish:^{
        
    }];
    
}

- (void)getWXPay {
    
    
    SVPShowInternetWaiting
    
    [NetManager isGetPreReqParamsModelParameters:@{@"order_res":@(self.type),@"parent_order_code":self.parent_order_code,@"t":@"APP"} success:^(NSDictionary * _Nonnull dic) {
        NSDictionary *dict = dic[@"object"];
        
        PayReq* req = [[PayReq alloc] init];
        req.partnerId   = dict[@"partnerid"];
        req.prepayId    = dict[@"prepayid"];
        req.nonceStr    = dict[@"nonceStr"];
        UInt32 num;
        sscanf([dict[@"timeStamp"] UTF8String], "%u", &num);
        req.timeStamp = num;
        req.package     = dict[@"packageStr"];
        req.sign = dict[@"paySign"];
        [WXApi sendReq:req completion:^(BOOL success) {
            
        }];
        
        
    } finish:^{
        
    }];
    
}


//- (void)back {
//
//
//    UIAlertController *alterController2 = [UIAlertController alertControllerWithTitle:nil message:@"是否确定放弃支付开通会员？" preferredStyle:UIAlertControllerStyleAlert];
//    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"狠心放弃" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//        if (self.type == 1) {
//            [self.navigationController popViewControllerAnimated:YES];
//        }else {
//            [self.navigationController popToRootViewControllerAnimated:YES];
//        }
//
//    }];
//
//    UIAlertAction *answerAction = [UIAlertAction actionWithTitle:@"再想想" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//
//    }];
//    [alterController2 addAction:cancelAction];
//    [alterController2 addAction:answerAction];
//
//    [self presentViewController:alterController2 animated:YES completion:nil];
//
//
//
//}


- (IBAction)nextBtnClcil:(id)sender {
    
    if (self.isWx) {
               
       [self getWXPay];
       
    }else {
           
       [self getZFBPay];
       
    }
    
    
}




- (IBAction)zfbClick:(id)sender {
    
    
    self.zfbBtn.selected = YES;
    self.wxBtn.selected = NO;
    self.isWx = NO;
    
    
}
- (IBAction)wxClick:(id)sender {
    
    self.zfbBtn.selected = NO;
    self.wxBtn.selected = YES;
    self.isWx = YES;
    
}

- (void)back {
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
