//
//  OrderPayVC.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/29.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface OrderPayVC : BaseViewController
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top;
@property (assign, nonatomic)int type; //订单类型 1混合订单 3虚拟订单 4拼团订单
@property (strong, nonatomic)NSString *parent_order_code;
@property (assign, nonatomic)int is_ht; //

@property (weak, nonatomic) IBOutlet UIView *bgView;

@end

NS_ASSUME_NONNULL_END
