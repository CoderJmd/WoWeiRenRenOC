//
//  MailConfirmOrderVC.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/12.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseTableViewController.h"
#import "UserShipingAddressModel.h"
#import "ShoppingCartModel.h"
#import "SelectorShenFenView.h"

NS_ASSUME_NONNULL_BEGIN

@interface MailConfirmOrderVC : BaseTableViewController
@property (strong, nonatomic)NSArray *modelArray;
@property (strong, nonatomic)UserShipingAddressModel *addressModel;
@property (assign, nonatomic)int order_type; //1混合订单 3虚拟订单 4拼团订单
@property (assign, nonatomic)int isAddPt;
@property (strong, nonatomic)NSString *tg_group_id;
@property (strong, nonatomic)NSString *shopping_car_ids;
@property (assign, nonatomic)int is_ht;

@end

NS_ASSUME_NONNULL_END
