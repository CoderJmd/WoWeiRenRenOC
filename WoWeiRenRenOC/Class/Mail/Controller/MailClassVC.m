//
//  MailClassVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/12.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "MailClassVC.h"
#import "BusinessModel.h"
#import "HomeIconModel.h"
#import "WebVC.h"
#import "RegisterPayVC.h"
#import "MailCell.h"
#import "BusinessDetailedVC.h"
#import "MailClassHeadView.h"
#import "MailDetailsVC.h"


extern UserModel *userModel;
@interface MailClassVC ()

@property (nonatomic,strong) MailClassHeadView *headView;

@property (strong, nonatomic)NSMutableArray *classArray;
@property (strong, nonatomic)NSMutableArray *classButtonArray;
@property (strong, nonatomic)NSString *serachName;
@end

@implementation MailClassVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.defaultStartPage = 1;
    
    [self creatTableViewWithCellIdentity:@"MailCell"];
    self.tableView.backgroundColor = kRGBColor(246, 246, 246);
    self.tableView.rowHeight = 135;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.frame = CGRectMake(0, NavBarHeight + 90, kScreenW, kScreenH-NavBarHeight -NeedStatusViewHeight-90);
    
    [self setRefresh];
    self.requstClassId = @"";
    [self requestData];
    
    MailClassHeadView  *headView = [[NSBundle mainBundle] loadNibNamed:@"MailClassHeadView" owner:self options:nil].lastObject;
    headView.frame = CGRectMake(0, NavBarHeight, kScreenW, 90);
    WeakSelf(ws);
    headView.searchName = ^(NSString * name) {
        ws.serachName = name;
        ws.page = 1;
        [ws requestData];
    };
    [self.view addSubview:headView];
    self.headView = headView;
    [self querySkuClassList];
   
}


- (void)querySkuClassList {
    
    HomeIconModel *model = [[HomeIconModel alloc]init];
    model.classify_id = @"";
    model.classify_name = @"全部";
    self.classArray = [NSMutableArray array];
    [self.classArray addObject:model];
    
    [NetRequestTool postUrl:@"sku/querySkuClassList" parameters:@{@"pid":self.model.classify_id} success:^(id responseObject) {
        
       NSArray *modelArray = [HomeIconModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]];
        [self.classArray addObjectsFromArray:modelArray];
        [self setHeadViewUi];

    } failure:^(NSInteger statusCode) {
        [self setHeadViewUi];

    } finish:^{
    }];
    
    
}

- (void)setHeadViewUi {
    
    UIScrollView *scrollView = [[UIScrollView alloc]init];
    scrollView.frame = CGRectMake(0, 0, kScreenW - 30, 30);
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    self.classButtonArray = [NSMutableArray array];
    int w = 0;
    
    for (int i = 0; i < self.classArray.count; i++) {
        
        HomeIconModel*childModel = self.classArray[i];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];

        button.tag = i;
        [button addTarget:self action:@selector(handleClick:) forControlEvents:UIControlEventTouchUpInside];
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:10]};
        CGFloat length = [childModel.classify_name boundingRectWithSize:CGSizeMake(kScreenW - 30, 2000) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size.width;
        button.titleLabel.font = kPRFont(12);
        button.layer.cornerRadius = 2;
        button.clipsToBounds = YES;
        //为button赋值
        [button setTitle:childModel.classify_name forState:UIControlStateNormal];
        //设置button的frame
        button.frame = CGRectMake(w, 0, length + 25 , 30);
        w = button.frame.size.width + button.frame.origin.x + 8;
        [scrollView addSubview:button];
        if (i == 0) {
            button.backgroundColor = kRGBAColor(206, 160, 9, 1);
            [button setTitleColor:WhiteColor forState:UIControlStateNormal];
        }else {
            button.backgroundColor = WhiteColor;
            [button setTitleColor:kRGBAColor(105, 105, 105, 1) forState:UIControlStateNormal];
        }
        [self.classButtonArray addObject:button];

    }
    scrollView.contentSize = CGSizeMake(w, 30);
    
    [self.headView.buttonBgView addSubview:scrollView];
    
    
}

#pragma mark - 分类点击事件
- (void)handleClick:(UIButton *)btn {
    
    int tag = btn.tag;
    for (UIButton *button in self.classButtonArray) {
        button.backgroundColor = WhiteColor;
        [button setTitleColor:kRGBAColor(105, 105, 105, 1) forState:UIControlStateNormal];
    }
    btn.backgroundColor = kRGBAColor(206, 160, 9, 1);
    [btn setTitleColor:WhiteColor forState:UIControlStateNormal];
    HomeIconModel *model = self.classArray[tag];
    self.requstClassId = model.classify_id;
    [self updateModel];
    
    
    
}

- (void)updateModel {
    
    self.page = 1;
    [self requestData];
    
}


- (void)requestData
{
    
    self.serachName = self.headView.searchTF.text;
    if (self.serachName == nil) {
        self.serachName = @"";
    }
    SVPShowInternetWaiting
   // 请求列表
    WeakSelf(ws)
    [NetRequestTool postUrl:@"sku/queryGoodsList" parameters:@{@"page":@(self.page), @"limit":@"10", @"classify_one_id":self.model.classify_id,@"classify_two_id":self.requstClassId,@"sku_name":self.serachName} success:^(id responseObject) {
        NSMutableArray *arr = [MailGoodsModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]];
        if (self.page==self.defaultStartPage) {
            ws.dataArray = arr;
        }
        else {
            [ws.dataArray addObjectsFromArray:arr];
        }
        [ws.tableView reloadData];
        
        // 控制页数
        NSDictionary *page = [responseObject objectForKey:@"page"];
        if ([page isKindOfClass:NSDictionary.class]) {
            NSInteger totalP = [page[@"total_page"] integerValue];
            if (totalP > self.page) {
                self.page++;
                [self endRefresh];
            }
            else {
                [self endRefreshNoMoreData];
            }
        }
        else {
            [self endRefreshNoMoreData];
        }
    } failure:^(NSInteger statusCode) {
        [ws endRefreshNoMoreData];
    } finish:^{
        SVPDismiss
    }];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MailCell"];
    cell.model = self.dataArray[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MailDetailsVC *vc = [[MailDetailsVC alloc] init];
    vc.goodsModel = self.dataArray[indexPath.row];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
