//
//  MailDetailsVC.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/12.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseTableViewController.h"
#import "MailGoodsModel.h"
#import "MailGoodsDetailsModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface MailDetailsVC : BaseTableViewController
@property (strong, nonatomic)MailGoodsModel *goodsModel;
@property (strong, nonatomic)MailGoodsDetailsModel *goodsDetailsModel;
@property (strong, nonatomic)NSString *tg_group_id;

@end

NS_ASSUME_NONNULL_END
