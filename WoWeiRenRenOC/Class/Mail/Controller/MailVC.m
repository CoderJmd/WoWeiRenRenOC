//
//  MailVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/12.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "MailVC.h"
#import "HomeCell.h"
#import "MailHeadView.h"
#import "HomeIconModel.h"
#import "BusinessListVC.h"
#import "BusinessDetailedVC.h"
#import "AddressSelectVC.h"
#import "WebVC.h"
#import "RegisterPayVC.h"
#import "MailCell.h"
#import "MailClassVC.h"
#import "MailGoodsModel.h"
#import "MailDetailsVC.h"
#import "PagingViewController.h"



extern UserModel *userModel;
@interface MailVC () <MailHeadViewDelegata>
@property (strong, nonatomic)NSString *serachName;
@property (nonatomic, strong) MailHeadView *header;

@end

@implementation MailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.defaultStartPage = 1;
    
    [self creatTableViewWithCellIdentity:@"MailCell"];
    self.tableView.backgroundColor = kRGBColor(246, 246, 246);
    self.tableView.rowHeight = 135;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.frame = CGRectMake(0, -StatusViewHeight, kScreenW, kScreenH-TabBarHeight+StatusViewHeight);
    self.tableView.tableHeaderView = self.header;
    [self setRefresh];
    
    SVPShowInternetWaiting
    [self requestData];
    [self requestDataHome];
}

- (void)requestDataHome
{
    [NetManager isBannerinfoListWithParameters:@{@"position":@"SHOP_HOME"} success:^(NSDictionary * _Nonnull dic) {
        
        self.header.bannerArray = [BannerModel mj_objectArrayWithKeyValuesArray:dic[@"object"]];
        
    } finish:^{
        
    }];


}

- (void)requestData
{
    
    self.serachName = self.header.searchTF.text;
    if (self.serachName == nil) {
        self.serachName = @"";
    }
    SVPShowInternetWaiting
   // 请求列表
    WeakSelf(ws)
    [NetRequestTool postUrl:@"sku/queryGoodsList" parameters:@{@"page":@(self.page), @"limit":@"10", @"is_hot":@"2",@"sku_name":self.serachName} success:^(id responseObject) {
        NSMutableArray *arr = [MailGoodsModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]];
        if (self.page==self.defaultStartPage) {
            ws.dataArray = arr;
        }
        else {
            [ws.dataArray addObjectsFromArray:arr];
        }
        [ws.tableView reloadData];
        
        // 控制页数
        NSDictionary *page = [responseObject objectForKey:@"page"];
        if ([page isKindOfClass:NSDictionary.class]) {
            NSInteger totalP = [page[@"total_page"] integerValue];
            if (totalP > self.page) {
                self.page++;
                [self endRefresh];
            }
            else {
                [self endRefreshNoMoreData];
            }
        }
        else {
            [self endRefreshNoMoreData];
        }
    } failure:^(NSInteger statusCode) {
        [ws endRefreshNoMoreData];
    } finish:^{
        SVPDismiss
    }];
}

- (void)MailHeadViewBannerClickAtIndex:(NSInteger)index model:(BannerModel *)model
{
   if ([model.jump_type isEqualToString:@"0"]) {
        WebVC *vc = [[WebVC alloc]init];
        vc.webtitle  = model.title;
        vc.linkPath = model.jump_path;
        vc.htmlString = model.depict;
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
    
    if ([model.jump_type isEqualToString:@"1"]) {

        switch (model.jump_module) {
            case 1:
                {
                    MailDetailsVC *vc = [[MailDetailsVC alloc] init];
                    MailGoodsModel *goodsModel = [[MailGoodsModel alloc]init];
                    goodsModel.sku_id = model.jump_module_param;
                    goodsModel.sku_type = 1;
                    vc.goodsModel = goodsModel;
                    vc.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:vc animated:YES];
                }
                break;
            case 2:
                {
                   PagingViewController *vc= [[PagingViewController alloc]init];
                   vc.business_id = model.jump_module_param;
                   [self.navigationController pushViewController:vc animated:YES];
                }
                break;
            case 3:
            {
                MailDetailsVC *vc = [[MailDetailsVC alloc] init];
                MailGoodsModel *goodsModel = [[MailGoodsModel alloc]init];
                goodsModel.sku_id = model.jump_module_param;
                goodsModel.sku_type = 3;
                vc.goodsModel = goodsModel;
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }
                break;
            case 4:
            {
               MailDetailsVC *vc = [[MailDetailsVC alloc] init];
               MailGoodsModel *goodsModel = [[MailGoodsModel alloc]init];
               goodsModel.sku_id = model.jump_module_param;
               goodsModel.sku_type = 4;
               vc.goodsModel = goodsModel;
               vc.hidesBottomBarWhenPushed = YES;
               [self.navigationController pushViewController:vc animated:YES];
            }
            break;
                
            default:
                break;
        }
    }
    
}





- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MailCell"];
    cell.model = self.dataArray[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    

    MailDetailsVC *vc = [[MailDetailsVC alloc] init];
    vc.goodsModel = self.dataArray[indexPath.row];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (MailHeadView *)header
{
    if (!_header) {
        _header = [[NSBundle mainBundle] loadNibNamed:@"MailHeadView" owner:self options:nil].lastObject;
        _header.frame = CGRectMake(0, 0, kScreenW, 280);
        _header.delegate = self;
        if (NeediPhoneXiPhoneXR) {
            _header.height += 22.0;
            _header.searchTop.constant += 22;
        }
        WeakSelf(ws);
        _header.searchName = ^(NSString * name) {
               ws.serachName = name;
               ws.page = 1;
               [ws requestData];
        };
    }
    return _header;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    [CommonTTool isVipShow:YES];
    
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
