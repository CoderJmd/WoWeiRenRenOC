//
//  MailYinKaVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/27.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "MailYinKaVC.h"
#import "BusinessModel.h"
#import "HomeIconModel.h"
#import "WebVC.h"
#import "RegisterPayVC.h"
#import "MailCell.h"
#import "BusinessDetailedVC.h"
#import "MailClassHeadView.h"
#import "MailDetailsVC.h"
#import "AbulkHeadSearchView.h"
#import "AbulkCell.h"
#import "MailGoodsModel.h"
#import "YinKaMailClassHeadView.h"

extern UserModel *userModel;
@interface MailYinKaVC ()
@property (strong, nonatomic)NSString *requstClassId;
@property (strong, nonatomic)NSString *requstTwoClassId;

@property (nonatomic,strong) YinKaMailClassHeadView *headView;
@property (strong, nonatomic)NSString *serachName;

@property (strong, nonatomic)NSMutableArray *classArray;
@property (strong, nonatomic)NSMutableArray *classButtonArray;

@property (strong, nonatomic)NSMutableArray *classTwoArray;
@property (strong, nonatomic)NSMutableArray *classTWoButtonArray;
@end

@implementation MailYinKaVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"银卡专区";
    self.defaultStartPage = 1;
    [self creatTableViewWithCellIdentity:@"MailCell"];
    self.tableView.backgroundColor = kRGBColor(246, 246, 246);
    self.tableView.rowHeight = 135;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.frame = CGRectMake(0, NavBarHeight + 130, kScreenW, kScreenH-NavBarHeight -NeedStatusViewHeight-130);
    
    
    [self setRefresh];
    
    SVPShowInternetWaiting
    [self requestData];
    
    YinKaMailClassHeadView  *headView = [[NSBundle mainBundle] loadNibNamed:@"YinKaMailClassHeadView" owner:self options:nil].lastObject;
    self.headView = headView;
    headView.frame = CGRectMake(0, NavBarHeight, kScreenW, 130);
    headView.searchName = ^(NSString * name) {
        self.page = 1;
        [self requestData];
    };
    
    [self.view addSubview:headView];
    [self querySkuClassList];

}

- (void)querySkuClassList {
    
    self.classArray = [NSMutableArray array];
    [NetRequestTool postUrl:@"sku/querySkuClassList" parameters:@{@"pid":@"0",@"show_yk":@"1"} success:^(id responseObject) {
        
       NSArray *modelArray = [HomeIconModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]];
        [self.classArray addObjectsFromArray:modelArray];
        [self setHeadViewUi];

    } failure:^(NSInteger statusCode) {
        [self setHeadViewUi];

    } finish:^{
    }];
    
    
}

- (void)setHeadViewUi {
    
    UIScrollView *scrollView = [[UIScrollView alloc]init];
    scrollView.frame = CGRectMake(0, 0, kScreenW - 30, 30);
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    self.classButtonArray = [NSMutableArray array];
    int w = 0;
    
    for (int i = 0; i < self.classArray.count; i++) {
       
        HomeIconModel*childModel = self.classArray[i];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        if (i == 0) {
            self.requstClassId = childModel.classify_id;
        }
        button.tag = i;
        [button addTarget:self action:@selector(handleClick:) forControlEvents:UIControlEventTouchUpInside];
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:10]};
        CGFloat length = [childModel.classify_name boundingRectWithSize:CGSizeMake(kScreenW - 30, 2000) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size.width;
        button.titleLabel.font = kPRFont(12);
        button.layer.cornerRadius = 2;
        button.clipsToBounds = YES;
        //为button赋值
        [button setTitle:childModel.classify_name forState:UIControlStateNormal];
        //设置button的frame
        button.frame = CGRectMake(w, 0, length + 25 , 30);
        w = button.frame.size.width + button.frame.origin.x + 8;
        [scrollView addSubview:button];
        if (i == 0) {
            button.backgroundColor = kRGBAColor(206, 160, 9, 1);
            [button setTitleColor:WhiteColor forState:UIControlStateNormal];
        }else {
            button.backgroundColor = WhiteColor;
            [button setTitleColor:kRGBAColor(105, 105, 105, 1) forState:UIControlStateNormal];
        }
        [self.classButtonArray addObject:button];

    }
    scrollView.contentSize = CGSizeMake(w, 30);
    
    [self.headView.buttonBgView addSubview:scrollView];
    
    
    [self querySkuTwoClassList];
    
}

- (void)querySkuTwoClassList {
    
    HomeIconModel *model = [[HomeIconModel alloc]init];
    model.classify_id = @"";
    model.classify_name = @"全部";
    self.classTwoArray = [NSMutableArray array];
    [self.classTwoArray addObject:model];
    
    [NetRequestTool postUrl:@"sku/querySkuClassList" parameters:@{@"pid":self.requstClassId,@"show_yk":@"1"} success:^(id responseObject) {
        
       NSArray *modelArray = [HomeIconModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]];
        [self.classTwoArray addObjectsFromArray:modelArray];
        [self setHeadTwoViewUi];

    } failure:^(NSInteger statusCode) {
        [self setHeadViewUi];

    } finish:^{
    }];
    
    
}

- (void)setHeadTwoViewUi {
    
    UIScrollView *scrollView = [[UIScrollView alloc]init];
    scrollView.frame = CGRectMake(0, 0, kScreenW - 30, 30);
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    self.requstTwoClassId = @"";
    self.classTWoButtonArray = [NSMutableArray array];
    int w = 0;
    [self.headView.twoButtonBgView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    for (int i = 0; i < self.classTwoArray.count; i++) {
        
        HomeIconModel*childModel = self.classTwoArray[i];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];

        button.tag = i;
        [button addTarget:self action:@selector(handleTwoClick:) forControlEvents:UIControlEventTouchUpInside];
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:10]};
        CGFloat length = [childModel.classify_name boundingRectWithSize:CGSizeMake(kScreenW - 30, 2000) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size.width;
        button.titleLabel.font = kPRFont(12);
        button.layer.cornerRadius = 2;
        button.clipsToBounds = YES;
        //为button赋值
        [button setTitle:childModel.classify_name forState:UIControlStateNormal];
        //设置button的frame
        button.frame = CGRectMake(w, 0, length + 25 , 30);
        w = button.frame.size.width + button.frame.origin.x + 8;
        [scrollView addSubview:button];
        if (i == 0) {
            button.backgroundColor = kRGBAColor(206, 160, 9, 1);
            [button setTitleColor:WhiteColor forState:UIControlStateNormal];
        }else {
            button.backgroundColor = WhiteColor;
            [button setTitleColor:kRGBAColor(105, 105, 105, 1) forState:UIControlStateNormal];
        }
        [self.classTWoButtonArray addObject:button];

    }
    scrollView.contentSize = CGSizeMake(w, 30);
    
    [self.headView.twoButtonBgView addSubview:scrollView];
    
    [self updateModel];
}

#pragma mark - 分类点击事件
- (void)handleClick:(UIButton *)btn {
    
    int tag = btn.tag;
    for (UIButton *button in self.classButtonArray) {
        button.backgroundColor = WhiteColor;
        [button setTitleColor:kRGBAColor(105, 105, 105, 1) forState:UIControlStateNormal];
    }
    btn.backgroundColor = kRGBAColor(206, 160, 9, 1);
    [btn setTitleColor:WhiteColor forState:UIControlStateNormal];
    HomeIconModel *model = self.classArray[tag];
    self.requstClassId = model.classify_id;
    [self updateTwoClass];
    
}

#pragma mark - 分类点击事件
- (void)handleTwoClick:(UIButton *)btn {
    
    int tag = btn.tag;
    for (UIButton *button in self.classTWoButtonArray) {
        button.backgroundColor = WhiteColor;
        [button setTitleColor:kRGBAColor(105, 105, 105, 1) forState:UIControlStateNormal];
    }
    btn.backgroundColor = kRGBAColor(206, 160, 9, 1);
    [btn setTitleColor:WhiteColor forState:UIControlStateNormal];
    HomeIconModel *model = self.classTwoArray[tag];
    self.requstTwoClassId = model.classify_id;
    [self updateModel];
    
}

- (void)updateTwoClass {
    
    [self querySkuTwoClassList];
    
}

- (void)updateModel {
    
    self.page = 1;
    [self requestData];
    
}


- (void)requestData
{
    self.serachName = self.headView.searchTF.text;
    if (self.serachName == nil) {
      self.serachName = @"";
    }
    if (self.requstClassId  == nil) {
        self.requstClassId  = @"";
    }
    if (self.requstTwoClassId  == nil) {
        self.requstTwoClassId  = @"";
    }
    SVPShowInternetWaiting
    // 请求列表
     WeakSelf(ws)
    [NetRequestTool postUrl:@"sku/queryYkGoodsList" parameters:@{@"page":@(self.page), @"limit":@"10",@"sku_name":self.serachName,@"classify_one_id":self.requstClassId,@"classify_two_id":self.requstTwoClassId} success:^(id responseObject) {
         NSMutableArray *arr = [MailGoodsModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]];
         if (self.page==self.defaultStartPage) {
             ws.dataArray = arr;
         }
         else {
             [ws.dataArray addObjectsFromArray:arr];
         }
         [ws.tableView reloadData];
         
         // 控制页数
         NSDictionary *page = [responseObject objectForKey:@"page"];
         if ([page isKindOfClass:NSDictionary.class]) {
             NSInteger totalP = [page[@"total_page"] integerValue];
             if (totalP > self.page) {
                 self.page++;
                 [self endRefresh];
             }
             else {
                 [self endRefreshNoMoreData];
             }
         }
         else {
             [self endRefreshNoMoreData];
         }
     } failure:^(NSInteger statusCode) {
         [ws endRefreshNoMoreData];
     } finish:^{
         SVPDismiss
     }];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MailCell"];
    cell.model = self.dataArray[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    MailDetailsVC *vc = [[MailDetailsVC alloc] init];
    vc.goodsModel = self.dataArray[indexPath.row];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}



@end

