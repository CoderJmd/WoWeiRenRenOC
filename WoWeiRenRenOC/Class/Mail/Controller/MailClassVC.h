//
//  MailClassVC.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/12.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseTableViewController.h"
#import "HomeIconModel.h"
#import "MailGoodsModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface MailClassVC : BaseTableViewController
@property (nonatomic, strong) HomeIconModel *model;
@property (strong, nonatomic)NSString *requstClassId;
@end

NS_ASSUME_NONNULL_END
