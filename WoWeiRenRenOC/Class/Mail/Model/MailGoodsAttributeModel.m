//
//  MailGoodsAttributeModel.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/24.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "MailGoodsAttributeModel.h"

@implementation MailGoodsAttributeModel
+ (NSDictionary *)objectClassInArray
{
    return @{@"sku_attribute_value_list": [MailGoodsAttributeChildModel class]};
}
@end


@implementation MailGoodsAttributeChildModel

@end
