//
//  MailGoodsModel.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/23.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MailGoodsModel : BaseModel
@property (strong, nonatomic)NSString *sold;
@property (assign, nonatomic)int is_pt;
@property (strong, nonatomic)NSString *pt_num;
@property (assign, nonatomic)int sku_type;
@property (strong, nonatomic)NSString *pt_ct_rs;
@property (strong, nonatomic)NSString *sku_id;
@property (strong, nonatomic)NSString *pt_start_time;
@property (strong, nonatomic)NSString *img_path;
@property (strong, nonatomic)NSString *is_hot;
@property (strong, nonatomic)NSString *pt_end_time;
@property (assign, nonatomic)float market_price;
@property (strong, nonatomic)NSString *sku_name;
@property (assign, nonatomic)float buy_price;
@property (assign, nonatomic)float vip_price;
@property (assign, nonatomic)int stock;
@property (assign, nonatomic)float pt_price;
@property (strong, nonatomic)NSString *business_id;
@property (strong, nonatomic)NSString *sku_code;
@property (strong, nonatomic)NSString *introduction;
@property (assign, nonatomic)int is_yk;
@property (strong, nonatomic)NSString *att_title;
@property (strong, nonatomic)NSString *end_time;
@property (assign, nonatomic)float yk_price;
@end

NS_ASSUME_NONNULL_END
