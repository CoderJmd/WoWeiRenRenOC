//
//  HTShenFenModel.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/7/31.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HTShenFenModel : BaseModel
@property (strong, nonatomic)NSString *idcard_name;
@property (strong, nonatomic)NSString *user_idcard_id;
@property (strong, nonatomic)NSString *idcard_no;

@end

NS_ASSUME_NONNULL_END
