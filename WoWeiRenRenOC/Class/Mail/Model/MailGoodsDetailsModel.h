//
//  MailGoodsDetailsModel.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/23.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MailGoodsDetailsModel : BaseModel
@property (strong, nonatomic)NSString *att_title;
@property (assign, nonatomic)int num;
@property (assign, nonatomic)int is_pt;
@property (strong, nonatomic)NSString *classify_two_id;
@property (strong, nonatomic)NSString *pt_ct_rs;
@property (strong, nonatomic)NSString *sell_day;
@property (strong, nonatomic)NSString *pt_start_time;
@property (strong, nonatomic)NSString *hot_num;
@property (strong, nonatomic)NSString *is_hot;
@property (strong, nonatomic)NSString *pt_end_time;
@property (strong, nonatomic)NSString *sku_name;
@property (strong, nonatomic)NSString *is_free_shipping;
@property (strong, nonatomic)NSString *state;
@property (assign, nonatomic)int stock;
@property (assign, nonatomic)float pt_price;
@property (assign, nonatomic)float sku_price;
@property (strong, nonatomic)NSString *introduction;
@property (assign, nonatomic)int sold;
@property (strong, nonatomic)NSString *pt_num;
@property (assign, nonatomic)int sku_type;
@property (strong, nonatomic)NSString *sku_id;
@property (strong, nonatomic)NSString *classify_one_id;
@property (strong, nonatomic)NSString *img_path;
@property (assign, nonatomic)float freight_price;
@property (assign, nonatomic)float market_price;
@property (assign, nonatomic)float buy_price;
@property (assign, nonatomic)float yk_price;
@property (assign, nonatomic)int is_yk;
@property (assign, nonatomic)float vip_price;
@property (strong, nonatomic)NSString *gg_info;
@property (strong, nonatomic)NSString *sku_code;
@property (strong, nonatomic)NSString *business_id;
@property (strong, nonatomic)NSString *head_path;
@property (strong, nonatomic)NSString *business_name;
@property (strong, nonatomic)NSArray *banner_list;
@property (strong, nonatomic)NSArray *sku_attribute_list;
@property (strong, nonatomic)NSString *pt_user_count;
@property (strong, nonatomic)NSArray *pt_user_list;
@property (strong, nonatomic)NSArray *pt_group_list;
@property (strong, nonatomic)NSString *end_time;
@property (assign, nonatomic)float webViewHeight;

@property (strong, nonatomic)NSString *busineee_tel;

@property (strong, nonatomic)NSString *sku_order_detail_id;
@property (strong, nonatomic)NSString *dh_code;

@property (assign, nonatomic)float unit_price;
@property (assign, nonatomic)int is_hx; //是否核销 1/否 2/是
@property (strong, nonatomic)NSString *hx_time;//核销时间//标签
@property (assign, nonatomic)int is_ht; //是否海淘 1不是 2是

@property (assign, nonatomic)int is_td;

@property (assign, nonatomic)BOOL isSelector;
@property (strong, nonatomic)NSString *t_num;
@property (strong, nonatomic)NSString *attribute_value_ids;

@end

@interface pt_group_listModel :  BaseModel

@property (strong, nonatomic)NSString *tg_group_sku_id;
@property (strong, nonatomic)NSString *tg_group_end_time;
@property (strong, nonatomic)NSString *tg_group_pt_rs;
@property (strong, nonatomic)NSString *tg_group_ct_rs;
@property (assign, nonatomic)int tg_group_state;
@property (strong, nonatomic)NSString *tg_group_id;
@property (strong, nonatomic)NSString *tg_group_user_id;
@property (strong, nonatomic)NSString *head_img_path;
@property (strong, nonatomic)NSString *tg_group_sku_code;
@property (strong, nonatomic)NSString *name;

@end

//@interface sku_attribute :  BaseModel
//@property (strong, nonatomic)NSString *attribute_id;
//@property (strong, nonatomic)NSString *num;
//@property (strong, nonatomic)NSString *sku_id;
//@property (strong, nonatomic)NSString *attribute_name;
//@property (strong, nonatomic)NSString *attribute_value;
//@property (strong, nonatomic)NSString *attribute_value_id;
//@property (strong, nonatomic)NSString *sku_attribute_value_id;
//
//@end

NS_ASSUME_NONNULL_END
