//
//  MailGoodsAttributeModel.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/24.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MailGoodsAttributeModel : BaseModel
@property (strong, nonatomic)NSString *attribute_id;
@property (strong, nonatomic)NSString *attribute_name;
@property (strong, nonatomic)NSArray *sku_attribute_value_list;

@end
@interface MailGoodsAttributeChildModel : BaseModel
@property (strong, nonatomic)NSString *attribute_value;
@property (strong, nonatomic)NSString *attribute_value_id;
@property (assign, nonatomic)BOOL isSelector;


@end
NS_ASSUME_NONNULL_END
