//
//  PtListModel.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/7/3.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PtListModel : BaseModel
@property (strong, nonatomic)NSString *head_img_path;
@property (strong, nonatomic)NSString *user_id;
@property (strong, nonatomic)NSString *phone;
@property (strong, nonatomic)NSString *name;
@property (strong, nonatomic)NSString *is_pt_type;

@end

NS_ASSUME_NONNULL_END
