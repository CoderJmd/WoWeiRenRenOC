//
//  BusinessApplyMailAndAbulkShowCell.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/18.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BusinessApplyMailAndAbulkShowCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *applyBtn;
@property (weak, nonatomic) IBOutlet UITextField *nameTF;
@property (weak, nonatomic) IBOutlet UITextField *accountLabel;
@property (weak, nonatomic) IBOutlet UITextField *startBankNameLabel;
@property (weak, nonatomic) IBOutlet UITextField *companyTF;
@property (weak, nonatomic) IBOutlet UITextField *selectedTypeTF;
@property (weak, nonatomic) IBOutlet UIButton *selectedTypeBtn;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UILabel *accountOldLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *startHeight;

@property (assign, nonatomic)int type;

@end

NS_ASSUME_NONNULL_END
