//
//  BusinessJobCell.h
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/18.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessJobModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BusinessJobCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *editeBtn;

@property (strong, nonatomic) BusinessJobModel *model;

@end

NS_ASSUME_NONNULL_END
