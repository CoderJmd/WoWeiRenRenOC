//
//  BusinessApplyMailAndAbulkShowCell.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/18.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BusinessApplyMailAndAbulkShowCell.h"

@implementation BusinessApplyMailAndAbulkShowCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    CAGradientLayer *gl = [CAGradientLayer layer];
    gl.frame = CGRectMake(0,0,kScreenW - 30,40);
    gl.startPoint = CGPointMake(0.22, 0.36);
    gl.endPoint = CGPointMake(0.85, 0.72);
    gl.colors = @[(__bridge id)[UIColor colorWithRed:221/255.0 green:169/255.0 blue:72/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:189/255.0 green:135/255.0 blue:35/255.0 alpha:1.0].CGColor];
    gl.locations = @[@(0), @(1.0f)];
    self.applyBtn.layer.cornerRadius = 4;
    self.applyBtn.clipsToBounds = YES;
    [self.applyBtn.layer insertSublayer:gl atIndex:0];
    [self.selectedTypeBtn addTarget:self action:@selector(selectedTypeClick:) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)selectedTypeClick:(UIButton *)btn {
    
    
    [CommonTTool dismisKeyBoard];
    NSArray *sexS = @[@"银行卡对公账户",@"支付宝对公账户"];
    
    LCActionSheet *actionSheet = [LCActionSheet sheetWithTitle:@"" cancelButtonTitle:@"取消" clicked:^(LCActionSheet * _Nonnull actionSheet, NSInteger buttonIndex) {
        
        if (buttonIndex != 0) {
            // 系统提示框
            self.type = buttonIndex - 1;
            [self setUi];
            
        }
        
    } otherButtonTitleArray:sexS];
    actionSheet.buttonHeight = 45;
    actionSheet.buttonColor = BlueColor;
    
    [actionSheet show];
    
}

- (void)setUi {
    
    if (self.type  == 0) {
        
        self.accountOldLabel.text = @"银行账户";
        self.accountLabel.placeholder = @"请输入银行账号";
        self.nameLabel.text = @"银行名称";
        self.nameTF.placeholder = @"请输入银行名称";
        self.startHeight.constant = 50;
        
    }else{
        
        self.accountOldLabel.text = @"支付宝账户";
        self.accountLabel.placeholder = @"请输入支付宝账号";
        self.nameLabel.text = @"支付宝名称";
        self.nameTF.placeholder = @"请输入支付宝名称";
        self.startHeight.constant = 0;
        
        
    }
    
}

@end
