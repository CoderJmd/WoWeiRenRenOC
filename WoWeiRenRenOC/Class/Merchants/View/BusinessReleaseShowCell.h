//
//  BusinessReleaseShowCell.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/7/21.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessJobModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BusinessReleaseShowCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *editeBtn;
@property (weak, nonatomic) IBOutlet UIButton *stateBtn;
@property (strong, nonatomic)BusinessJobModel *model;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@end

NS_ASSUME_NONNULL_END
