//
//  BusinessReleaseShowCell.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/7/21.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BusinessReleaseShowCell.h"

@implementation BusinessReleaseShowCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(BusinessJobModel *)model {
    
    _model = model;
    self.titleLabel.text = model.recruit_name;
    self.contentLabel.text  = model.descriptionText;
    self.timeLabel.text = model.create_time;
    
    if (model.sh_state==1) {
        // 审核中
        [self.stateBtn setTitle:@"审核中" forState:UIControlStateNormal];
        [self.stateBtn setBackgroundImage:[UIImage imageNamed:@"shenhezhong"] forState:UIControlStateNormal];
//        self.editeBtn.hidden = YES;
    }
    else if (model.sh_state==2) {
        // 已通过
        [self.stateBtn setTitle:@"已通过" forState:UIControlStateNormal];
        [self.stateBtn setBackgroundImage:[UIImage imageNamed:@"tiruzhu"] forState:UIControlStateNormal];
        self.editeBtn.hidden = NO;
    }
    else if (model.sh_state==3) {
        // 未通过
        [self.stateBtn setTitle:@"未通过" forState:UIControlStateNormal];
        [self.stateBtn setBackgroundImage:[UIImage imageNamed:@"weitongguo"] forState:UIControlStateNormal];
        self.editeBtn.hidden = NO;
    }
    
}


@end
