//
//  BusinessReleaseDetailsShowCell.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/7/21.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessJobModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BusinessReleaseDetailsShowCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *contnetLabel;
@property (weak, nonatomic) IBOutlet UIImageView *headImgView;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (strong, nonatomic)BusinessJobModel *model;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bgHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top;

@end

NS_ASSUME_NONNULL_END
