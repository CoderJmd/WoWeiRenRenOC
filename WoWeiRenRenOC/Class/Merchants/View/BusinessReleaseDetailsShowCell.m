//
//  BusinessReleaseDetailsShowCell.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/7/21.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BusinessReleaseDetailsShowCell.h"
#import "SDPhotoBrowser.h"
@interface BusinessReleaseDetailsShowCell()<SDPhotoBrowserDelegate>
@property (nonatomic,strong) NSMutableArray *selectImagePathArray;
@end

@implementation BusinessReleaseDetailsShowCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)setModel:(BusinessJobModel *)model {
    
    _model = model;
    self.nameLabel.text = model.business_name;
    SD_SETIMAGE_PlaceHolder(self.headImgView, model.head_path, @"default_image_square")
    NSString *time = [model.create_time  substringToIndex:10];
    self.timeLabel.text = time;
    self.titleLabel.text = model.recruit_name;
    self.contnetLabel.text = model.descriptionText;
    
    NSDictionary *titleAttributes = @{NSFontAttributeName:kPMFont(16)};
    CGFloat titleHeight = [model.recruit_name boundingRectWithSize:CGSizeMake(kScreenW - 60, 2000) options:NSStringDrawingUsesLineFragmentOrigin attributes:titleAttributes context:nil].size.height;
    
    
    NSDictionary *attributes = @{NSFontAttributeName:kPRFont(14)};
    CGFloat height = [model.descriptionText boundingRectWithSize:CGSizeMake(kScreenW - 60, 2000) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size.height;
    
    CGFloat y = 15 + titleHeight + height + 30;
    self.selectImagePathArray = [NSMutableArray array];
    for (int i = 0 ; i < model.picture_list.count; i++) {
        NSDictionary *dice = model.picture_list[i];
        
        UIButton *imgView = [UIButton buttonWithType:UIButtonTypeCustom];
        imgView.frame = CGRectMake(15, y, kScreenW - 60, 167);
        imgView.layer.cornerRadius = 4;
        imgView.clipsToBounds = YES;
        imgView.imageView.contentMode = UIViewContentModeScaleAspectFill;
        [self.bgView addSubview:imgView];
        [imgView sd_setImageWithURL:[NSURL URLWithString:dice[@"path"]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"default_image_square"]];
        [self.selectImagePathArray addObject:dice[@"path"]];
        imgView.tag = i;
        [imgView addTarget:self action:@selector(selectedImageClick:) forControlEvents:UIControlEventTouchUpInside];
        y += 182;
        
    }
    
    if (iPhoneXR) {
        self.top.constant += 22;
        self.bgHeight.constant += 22;
    }
    
    
}

- (void)selectedImageClick:(UIButton *)btn {
    
    SDPhotoBrowser *browser = [[SDPhotoBrowser alloc] init];
    browser.currentImageIndex = btn.tag;

    browser.imageCount = self.selectImagePathArray.count;
    browser.delegate = self;
    [browser show];
    
}


#pragma mark - SDPhotoBrowserDelegate
- (NSURL *)photoBrowser:(SDPhotoBrowser *)browser highQualityImageURLForIndex:(NSInteger)index {
    
    return [NSURL URLWithString:self.selectImagePathArray[index]];
}

@end
