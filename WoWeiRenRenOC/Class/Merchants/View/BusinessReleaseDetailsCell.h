//
//  BusinessReleaseDetailsCell.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/7/21.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessJobModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface BusinessReleaseDetailsCell : UITableViewCell
@property (strong, nonatomic)BusinessJobModel *model;
@property (weak, nonatomic) IBOutlet UITextField *titleTF;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UITextView *contentTF;
@property (strong, nonatomic)NSMutableArray *selectImagePathArray;
@end

NS_ASSUME_NONNULL_END
