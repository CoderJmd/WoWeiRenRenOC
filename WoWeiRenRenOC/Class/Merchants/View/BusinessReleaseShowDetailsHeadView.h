//
//  BusinessReleaseShowDetailsHeadView.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/7/21.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BusinessReleaseShowDetailsHeadView : UIView
@property (weak, nonatomic) IBOutlet UIButton *backBtn;

@end

NS_ASSUME_NONNULL_END
