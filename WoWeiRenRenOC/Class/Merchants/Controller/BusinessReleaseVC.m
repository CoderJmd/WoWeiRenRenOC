//
//  BusinessReleaseVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/7/21.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BusinessReleaseVC.h"
#import "BusinessReleaseShowCell.h"
#import "AddBusinessJobVC.h"
#import "BusinessJobDetailC.h"
#import "BusinessReleaseDetailsVC.h"
#import "BusinessReleaseShowDetailsVC.h"

@interface BusinessReleaseVC ()

@end

@implementation BusinessReleaseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"企业发布";
    self.defaultStartPage = 1;
    
    UIBarButtonItem *addBtn = [[UIBarButtonItem alloc] initWithTitle:@"新增" style:UIBarButtonItemStyleDone target:self action:@selector(rightBarButtonClick)];
    self.navigationItem.rightBarButtonItem = addBtn;
    
    [self creatTableViewWithCellIdentity:@"BusinessReleaseShowCell"];
    self.tableView.backgroundColor = kRGBColor(246, 246, 246);
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.frame = CGRectMake(0, 0, kScreenW, kScreenH);
    
    [self setRefresh];
    SVPShowInternetWaiting
    [self requestData];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(update) name:kBusinessJobUpDate object:nil];
}

- (void)update {
    
    self.page = 1;
    [self requestData];
    
}

- (void)rightBarButtonClick
{
    BusinessReleaseDetailsVC *vc = [[BusinessReleaseDetailsVC alloc] init];
    vc.title = @"新增企业发布";
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (void)requestData
{
    // 请求列表
    WeakSelf(ws)
    [NetRequestTool postUrl:@"business/queryRecruitList" parameters:@{@"page":@(self.page), @"limit":@"10", @"is_my":@(1)} success:^(id responseObject) {
        NSMutableArray *arr = [BusinessJobModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]];
        if (self.page==self.defaultStartPage) {
            ws.dataArray = arr;
        }
        else {
            [ws.dataArray addObjectsFromArray:arr];
        }
        [ws.tableView reloadData];
        
        // 控制页数
        NSDictionary *page = [responseObject objectForKey:@"page"];
        if ([page isKindOfClass:NSDictionary.class]) {
            NSInteger totalP = [page[@"total_page"] integerValue];
            if (totalP > self.page) {
                self.page++;
                [self endRefresh];
            }
            else {
                [self endRefreshNoMoreData];
            }
        }
        else {
            [self endRefreshNoMoreData];
        }
    } failure:^(NSInteger statusCode) {
        [ws endRefreshNoMoreData];
    } finish:^{
        SVPDismiss
    }];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BusinessReleaseShowCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BusinessReleaseShowCell"];
    cell.model = self.dataArray[indexPath.row];
    cell.editeBtn.tag = indexPath.row;
    [cell.editeBtn addTarget:self action:@selector(cellEditeButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BusinessReleaseShowDetailsVC *vc = [[BusinessReleaseShowDetailsVC alloc] init];
    BusinessJobModel *model = self.dataArray[indexPath.row];
    vc.recruit_id = model.recruit_id;
    [self.navigationController pushViewController:vc animated:YES];
//    BusinessJobModel *model = self.dataArray[indexPath.row];
//    if (model.sh_state==1) {
//        // 审核中
//        SVP(@"审核中无法修改信息");
//    }
//    else if (model.sh_state==2) {
//        // 已通过
//        AddBusinessJobVC *vc = [[AddBusinessJobVC alloc] init];
//        vc.parentsVC = self;
//        vc.model = self.dataArray[indexPath.row];
//        [self.navigationController pushViewController:vc animated:YES];
//    }
//    else if (model.sh_state==3) {
//        // 未通过
//       AddBusinessJobVC *vc = [[AddBusinessJobVC alloc] init];
//       vc.parentsVC = self;
//       vc.model = self.dataArray[indexPath.row];
//       [self.navigationController pushViewController:vc animated:YES];
//    }
    
}

- (void)cellEditeButtonClick:(UIButton *)sender
{
    BusinessReleaseDetailsVC *vc = [[BusinessReleaseDetailsVC alloc] init];
//    vc.parentsVC = self;
    vc.title = @"修改企业发布";
    BusinessJobModel *model = self.dataArray[sender.tag];
    vc.recruit_id = model.recruit_id;
    [self.navigationController pushViewController:vc animated:YES];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
