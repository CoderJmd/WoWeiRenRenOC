//
//  AddBusinessActivityVC.h
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/18.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseViewController.h"
#import "BaseTableViewController.h"
#import "ActivityModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface AddBusinessActivityVC : BaseViewController

@property (nonatomic, strong) ActivityModel *model;
@property (nonatomic, weak)  BaseTableViewController *parentsVC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *navgationHeight;

@end

NS_ASSUME_NONNULL_END
