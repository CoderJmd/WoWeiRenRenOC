//
//  BusinessApplyMailAndAbulkVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/18.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BusinessApplyMailAndAbulkVC.h"
#import "JQScanViewController.h"
#import "BusinessApplyMailAndAbulkShowCell.h"
@interface BusinessApplyMailAndAbulkVC ()
@property (nonatomic,strong) BusinessApplyMailAndAbulkShowCell *cell;
@end

@implementation BusinessApplyMailAndAbulkVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.defaultStartPage = 1;
    self.title = @"资料提交";
    [self creatTableViewWithCellIdentity:@"BusinessApplyMailAndAbulkShowCell"];

    self.view.backgroundColor = WhiteColor;
    self.tableView.backgroundColor = WhiteColor;
    self.tableView.rowHeight = 500;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.frame = CGRectMake(0, NavBarHeight, kScreenW, kScreenH-NavBarHeight -NeedStatusViewHeight-50);
    
    

}

//- (void)jumpScanVCClick {
//
//    JQScanViewController *vc = [JQScanViewController new];
//    [self.navigationController pushViewController:vc animated:YES];
//
//}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    BusinessApplyMailAndAbulkShowCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BusinessApplyMailAndAbulkShowCell"];
    //cell.model = self.dataArray[indexPath.row];
    [cell.applyBtn addTarget:self action:@selector(submitClick) forControlEvents:UIControlEventTouchUpInside];
    self.cell = cell;
    NSDictionary *dict = [[NSUserDefaults standardUserDefaults] objectForKey:kUserMerchantsInfo];
    cell.nameTF.text = dict[@"bank_name"];
    cell.accountLabel.text = dict[@"bank_no"];
    cell.startBankNameLabel.text = dict[@"kh_bank"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

#pragma mark - 申请成为直销
- (void)submitClick {
    
    NSString *company = self.cell.companyTF.text;
    NSString *bank_name = self.cell.nameTF.text;
    NSString *bank_no = self.cell.accountLabel.text;
    NSString *kh_bank = self.cell.startBankNameLabel.text;
    int type = self.cell.type;
    
    if ([CommonTTool isStrNull:company]) {
        SVP(@"请输入公司名称");
        return;
    }
    
    if ([CommonTTool isStrNull:bank_name]) {
        if (type == 0) {
            SVP(@"请输入银行名称");
        }else {
            SVP(@"请输入支付宝名称");
        }
        return;
    }
    if ([CommonTTool isStrNull:bank_no]) {
        if (type == 0) {
            SVP(@"请输入银行账号");
        }else {
            SVP(@"请输入支付宝账号");
        }
        return;
    }
    NSDictionary *para = nil;
    
    if (type == 0) {
        if ([CommonTTool isStrNull:kh_bank]) {
            SVP(@"请输入开户银行名称");
            return;
        }
       para =  @{@"bank_name":bank_name,
                 @"bank_no":bank_no,
                 @"kh_bank":kh_bank,
                 @"org_name":company,
                 @"zh_type":@(1)
        };
    }else {
        para =  @{@"bank_name":bank_name,
                  @"bank_no":bank_no,
                  @"org_name":company,
                  @"zh_type":@(2)
        };
    }
    
    
    
    
    [NetRequestTool  postUrl:@"/business/sqZxtg" parameters:para success:^(id responseObject) {
        SVP(@"申请成功");
        [self performSelector:@selector(businessInfoUpDate) withObject:self afterDelay:2.0];
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        
    }];
    
    
}

- (void)businessInfoUpDate {
    
    NSDictionary *dict = [[NSUserDefaults standardUserDefaults] objectForKey:kUserMerchantsInfo];
    NSString *business_id = dict[@"business_id"];
    [NetRequestTool postUrl:@"business/getBusinessInfo" parameters:@{@"business_id":business_id} success:^(id responseObject) {
       
       NSDictionary *dict = [responseObject objectForKey:@"object"];
       // 去除空值后 直接存字典
       [[NSUserDefaults standardUserDefaults] setObject:[dict deleteNull] forKey:kUserMerchantsInfo];
        [self.navigationController popViewControllerAnimated:YES];
        
    } failure:^(NSInteger statusCode) {
    //        [ws endRefreshNoMoreData];
    } finish:^{
        SVPDismiss
    }];
    
    
    
    
}


@end
