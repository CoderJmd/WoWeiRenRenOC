//
//  BusinessMineVC.m
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/17.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BusinessMineVC.h"
#import "UserMineHeadShowCell.h"
#import "BusinessMineHeadView.h"
#import "BusinessInfoVC.h"
#import "BusinessActivityListVC.h"
#import "AboutWeVC.h"
#import "UserSetingVC.h"
#import "BusinessJobListVC.h"
#import "CustomActivity.h"
#import "BusinessScanningEliminateOrderVC.h"
#import "BusinessApplyMailAndAbulkVC.h"
#import "ContactServiceVC.h"
#import "BusinessModel.h"
#import "ShareView.h"
#import "BusinessReleaseVC.h"
#import "MyPostersShareView.h"


@interface BusinessMineVC ()

@property (nonatomic, strong) BusinessMineHeadView *header;
@property (nonatomic, strong) UIView *footer;
@end

@implementation BusinessMineVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
//    self.dataArray = @[@{@"name":@"我的信息",@"icon":@"wodexinxi"},
//    @{@"name":@"折扣商品",@"icon":@"zhekoushangping"},
//    @{@"name":@"人员招聘",@"icon":@"renyuanzhaoping"},
//    @{@"name":@"关于我们",@"icon":@"guanyuwomen"},
//    @{@"name":@"联系客服",@"icon":@"fenxiang"},
//    @{@"name":@"设置",@"icon":@"shezhi"}].mutableCopy;
    BusinessModel *dict = [BusinessModel mj_objectWithKeyValues:[[NSUserDefaults standardUserDefaults] objectForKey:kUserMerchantsInfo]];
    if (dict.zxtg_state == 4) {
        self.dataArray = @[@{@"name":@"我的信息",@"icon":@"wodexinxi"},
                           @{@"name":@"扫码核销",@"icon":@"saoyisao"},
                           @{@"name":@"申请成为直销或团购商家",@"icon":@"wodezhekoushangjia"},
                           @{@"name":@"折扣商品",@"icon":@"zhekoushangping"},
                           @{@"name":@"企业发布",@"icon":@"shangjiashangchuan"},
                           @{@"name":@"分享APP",@"icon":@"fenxiang"},
                           @{@"name":@"设置",@"icon":@"shezhi"}].mutableCopy;
    }else {
        self.dataArray = @[@{@"name":@"我的信息",@"icon":@"wodexinxi"},
                           @{@"name":@"申请成为直销或团购商家",@"icon":@"wodezhekoushangjia"},
                           @{@"name":@"折扣商品",@"icon":@"zhekoushangping"},
                           @{@"name":@"企业发布",@"icon":@"shangjiashangchuan"},
                           @{@"name":@"分享APP",@"icon":@"fenxiang"},
                           @{@"name":@"设置",@"icon":@"shezhi"}].mutableCopy;
    }
        
    
    [self creatTableViewWithCellIdentity:@"UserMineHeadShowCell"];
    self.tableView.backgroundColor = kRGBColor(246, 246, 246);
    self.tableView.rowHeight = 50;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.frame = CGRectMake(0, -StatusViewHeight, kScreenW, kScreenH+StatusViewHeight);
    self.tableView.tableHeaderView = self.header;
    if ([dict.user_type isEqualToString:@"1"]) {
        self.tableView.tableFooterView = self.footer;
        
    }else {
        self.dataArray = @[@{@"name":@"我的信息",@"icon":@"wodexinxi"},
        @{@"name":@"扫码核销",@"icon":@"saoyisao"},
        @{@"name":@"分享APP",@"icon":@"fenxiang"},
        @{@"name":@"设置",@"icon":@"shezhi"}].mutableCopy;
        [self.tableView reloadData];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserMineHeadShowCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserMineHeadShowCell"];
    cell.nameLabel.text = self.dataArray[indexPath.row][@"name"];
    cell.iconImageView.image = [UIImage imageNamed:self.dataArray[indexPath.row][@"icon"]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BusinessModel *dict = [BusinessModel mj_objectWithKeyValues:[[NSUserDefaults standardUserDefaults] objectForKey:kUserMerchantsInfo]];
    if ([dict.user_type isEqualToString:@"2"]) {
        switch (indexPath.row) {
            case 0:
            {
                BusinessInfoVC *vc = [[BusinessInfoVC alloc] init];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
                break;
            }
            case 1:
            {
                BusinessScanningEliminateOrderVC *vc = [[BusinessScanningEliminateOrderVC alloc] init];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
                break;
            }
            
                break;
            case 2: {
                [self shareTapClick];
                
            }
                break;
            case 3: {
                UserSetingVC *vc = [[UserSetingVC alloc] init];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
                
            }
                break;
            default:
                break;
        }
        
        return;
    }
    if (dict.zxtg_state == 4) {
        
        switch (indexPath.row) {
            case 0:
            {
                BusinessInfoVC *vc = [[BusinessInfoVC alloc] init];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
                break;
            }
            case 1:
            {
                BusinessScanningEliminateOrderVC *vc = [[BusinessScanningEliminateOrderVC alloc] init];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
                break;
            }
            case 2:
            {
                NSDictionary *dict = [[NSUserDefaults standardUserDefaults] objectForKey:kUserMerchantsInfo];

                BusinessModel *model = [BusinessModel mj_objectWithKeyValues:dict];
                if (model.zxtg_state == 2) {
                        
                        // 系统提示框
                        UIAlertController *alterController = [UIAlertController alertControllerWithTitle:nil message:@"正在审核中，请耐心等待!" preferredStyle:UIAlertControllerStyleAlert];
                
                        [alterController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            
                        }]];
                        
                        [[CommonTTool getCurrentVC] presentViewController:alterController animated:YES completion:nil];
                        
                        
                        return;
                    }
                if (model.zxtg_state == 4) {
                    
                    // 系统提示框
                    UIAlertController *alterController = [UIAlertController alertControllerWithTitle:nil message:@"您已是直销或团购商家!" preferredStyle:UIAlertControllerStyleAlert];
            
                    [alterController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        
                    }]];
                    
                    [[CommonTTool getCurrentVC] presentViewController:alterController animated:YES completion:nil];
                    
                    
                    return;
                }
                BusinessApplyMailAndAbulkVC *vc = [[BusinessApplyMailAndAbulkVC alloc] init];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
                break;
            }
            case 3:
            {
                BusinessActivityListVC *vc = [[BusinessActivityListVC alloc] init];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
                break;
                
            }
                break;
            case 4:
            {
               BusinessReleaseVC *vc = [[BusinessReleaseVC alloc]init];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
                break;
                break;
                
            }
                break;
            case 5: {
                [self shareTapClick];
                
            }
                break;
            case 6: {
                UserSetingVC *vc = [[UserSetingVC alloc] init];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
                
            }
                break;
            default:
                break;
        }
        
    }else {
        
        switch (indexPath.row) {
            case 0:
            {
                BusinessInfoVC *vc = [[BusinessInfoVC alloc] init];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
                break;
            }
            case 1:
            {
                NSDictionary *dict = [[NSUserDefaults standardUserDefaults] objectForKey:kUserMerchantsInfo];

                BusinessModel *model = [BusinessModel mj_objectWithKeyValues:dict];
                if (model.zxtg_state == 4) {
                    
                    // 系统提示框
                    UIAlertController *alterController = [UIAlertController alertControllerWithTitle:nil message:@"您已是直销或团购商家!" preferredStyle:UIAlertControllerStyleAlert];
            
                    [alterController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        
                    }]];
                    
                    [[CommonTTool getCurrentVC] presentViewController:alterController animated:YES completion:nil];
                    
                    
                    return;
                }
                BusinessApplyMailAndAbulkVC *vc = [[BusinessApplyMailAndAbulkVC alloc] init];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
                break;
            }
            case 2:
            {
                BusinessActivityListVC *vc = [[BusinessActivityListVC alloc] init];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
                break;
                
            }
                break;
            case 3:
            {
//                BusinessJobListVC *vc = [[BusinessJobListVC alloc] init];
//                vc.hidesBottomBarWhenPushed = YES;
//                [self.navigationController pushViewController:vc animated:YES];
                BusinessReleaseVC *vc = [[BusinessReleaseVC alloc]init];
                [self.navigationController pushViewController:vc animated:YES];
                break;
                
            }
                break;
            case 4: {
                [self shareTapClick];
                
            }
                break;
            case 5: {
                UserSetingVC *vc = [[UserSetingVC alloc] init];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
                
            }
                break;
            default:
                break;
        }
        
    }
    
   
     
     
}

- (BusinessMineHeadView *)header
{
    if (!_header) {
        _header = [[NSBundle mainBundle] loadNibNamed:@"BusinessMineHeadView" owner:self options:nil].lastObject;
        _header.frame = CGRectMake(0, 0, kScreenW, 266);
    }
    return _header;
}

- (UIView *)footer
{
    if (!_footer) {
        _footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, 60)];
        _footer.backgroundColor = kRGBColor(246, 246, 246);
        
        UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kScreenW, 40)];
        lab.textColor = kRGBColor(189, 135, 35);
        lab.font = kFont(14.0);
        lab.textAlignment = NSTextAlignmentCenter;
        NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:kUserMerchantsInfo];
        lab.text = [NSString stringWithFormat:@"入驻有效期至%@", userInfo[@"effective_time"]];
        [_footer addSubview:lab];
        NSDictionary *dict = [[NSUserDefaults standardUserDefaults] objectForKey:kUserMerchantsInfo];
        SD_SETIMAGE_PlaceHolder(_header.topImage, dict[@"head_path"], @"shangjia");
    }
    return _footer;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHidden:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setHidden:NO];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - 分享
- (void)shareTapClick {
    
    ShareView *share =  [ShareView showCodeView];
    share.buttonClick = ^(NSInteger tag) {
        switch (tag) {
            case 101:
            {
                [self shareType:0];
            }
                break;
            case 102:
            {
                 [self shareType:1];
            }
                break;
            case 103:
            {
                [self weiboShare];
            }
                break;
            case 104:
            {
                [MyPostersShareView showView];
                 
            }
                break;
            case 105:
            {
                UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];

                pasteboard.string = SHARE_HOME_URL;

                SVP(@"链接已复制");
                 
            }
                break;
            default:
                break;
        }
        
    };
    
    
    
}

- (void)shareType:(int)scene {
    WXMediaMessage * message = [WXMediaMessage message];
    message.title = SHARE_TITLE;
    message.description = SHARE_DES;
    [message setThumbImage:[UIImage imageNamed:@"logo2"]];
    
    WXWebpageObject * webPageObject = [WXWebpageObject object];
    webPageObject.webpageUrl = SHARE_HOME_URL;
    message.mediaObject = webPageObject;
    
    SendMessageToWXReq *req1 = [[SendMessageToWXReq alloc]init];
    req1.bText = NO;
    req1.message = message;
    //设置分享到朋友圈(WXSceneTimeline)、好友回话(WXSceneSession)、收藏(WXSceneFavorite)
    req1.scene = scene;
    [WXApi sendReq:req1 completion:^(BOOL success) {
        
    }];
}

- (void)weiboShare {
    
    // 微博分享、需要授权
    WBAuthorizeRequest *request = [WBAuthorizeRequest request];
    request.redirectURI = @"https://api.weibo.com/oauth2/default.html";
    request.scope = @"all";
    request.userInfo = @{@"SSO_From": @"SendMessageToWeiboViewController",
                       @"Other_Info_1": [NSNumber numberWithInt:123],
                       @"Other_Info_2": @[@"obj1", @"obj2"],
                       @"Other_Info_3": @{@"key1": @"obj1", @"key2": @"obj2"}};

    WBMessageObject *message = [WBMessageObject message];

    // 链接
    WBWebpageObject *webpage = [WBWebpageObject object];
    webpage.objectID = @"identifier1";
    webpage.title = SHARE_TITLE;
    webpage.description = SHARE_DES;
    webpage.thumbnailData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"logo2" ofType:@"png"]];
    webpage.webpageUrl = SHARE_HOME_URL;

    message.mediaObject = webpage;

    WBSendMessageToWeiboRequest *weiboRequest = [WBSendMessageToWeiboRequest requestWithMessage:message
                                                                                     authInfo:request
                                                                                 access_token:nil];
    //    WBSendMessageToWeiboRequest *weiboRequest = [WBSendMessageToWeiboRequest requestWithMessage:message];
    weiboRequest.userInfo = nil;
    BOOL isSuccess =  [WeiboSDK sendRequest:weiboRequest];
    NSLog(@"分享是否成功 %d",isSuccess);
    
}



@end
