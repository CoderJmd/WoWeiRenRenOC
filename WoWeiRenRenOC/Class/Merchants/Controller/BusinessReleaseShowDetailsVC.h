//
//  BusinessReleaseShowDetailsVC.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/7/21.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseTableViewController.h"
#import "BusinessJobModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BusinessReleaseShowDetailsVC : BaseTableViewController
@property (strong, nonatomic)BusinessJobModel *model;
@property (strong, nonatomic)NSString *recruit_id;
@end

NS_ASSUME_NONNULL_END
