//
//  BusinessReleaseShowDetailsVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/7/21.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BusinessReleaseShowDetailsVC.h"
#import "BusinessReleaseDetailsShowCell.h"
#import "BusinessReleaseShowDetailsHeadView.h"


@interface BusinessReleaseShowDetailsVC ()

@end

@implementation BusinessReleaseShowDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self creatTableViewWithCellIdentity:@"BusinessReleaseDetailsShowCell"];
    self.tableView.backgroundColor = kRGBColor(246, 246, 246);
    self.tableView.rowHeight = kScreenH- NavBarHeight;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.frame = CGRectMake(0, -StatusViewHeight, kScreenW, kScreenH + StatusViewHeight);
    BusinessReleaseShowDetailsHeadView *headView = [[NSBundle mainBundle] loadNibNamed:@"BusinessReleaseShowDetailsHeadView" owner:nil options:nil].lastObject;
    headView.frame = CGRectMake(0, StatusViewHeight, kScreenW, 49);
    [self.view addSubview:headView];
    [headView.backBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self requestData];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BusinessReleaseDetailsShowCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BusinessReleaseDetailsShowCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.model = self.model;
    NSDictionary *titleAttributes = @{NSFontAttributeName:kPMFont(16)};
       CGFloat titleHeight = [self.model.recruit_name boundingRectWithSize:CGSizeMake(kScreenW - 60, 2000) options:NSStringDrawingUsesLineFragmentOrigin attributes:titleAttributes context:nil].size.height;

    NSDictionary *attributes = @{NSFontAttributeName:kPRFont(14)};
    CGFloat height = [self.model.descriptionText boundingRectWithSize:CGSizeMake(kScreenW - 60, 2000) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size.height;

    CGFloat y = 15 + titleHeight + height + 30;
    for (int i = 0 ; i < self.model.picture_list.count; i++) {
       y += 182;
    }
    if (iPhoneXR) {
        y += 22;
    }
    tableView.rowHeight = y + 144;
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.model == nil) {
        return 0;
    }
    return 1;
    
}

- (void)requestData
{
    // 请求列表
    [NetRequestTool postUrl:@"business/queryRecruitInfo" parameters:@{@"recruit_id":self.recruit_id, } success:^(id responseObject) {
        self.model = [BusinessJobModel mj_objectWithKeyValues:responseObject[@"object"]];
        [self.tableView reloadData];
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        SVPDismiss
    }];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [CommonTTool isVipShow:YES];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    
    
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}



@end
