//
//  BusinessReleaseDetailsVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/7/21.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BusinessReleaseDetailsVC.h"
#import "BusinessReleaseDetailsCell.h"


@interface BusinessReleaseDetailsVC ()
@property (nonatomic,strong) BusinessReleaseDetailsCell *cell;
@property (strong, nonatomic)UIButton *addBtn;
@end

@implementation BusinessReleaseDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.title.length == 0) {
        self.title = @"企业发布";
    }
    self.defaultStartPage = 1;
    
    
    
    [self creatTableViewWithCellIdentity:@"BusinessReleaseDetailsCell"];
    self.tableView.backgroundColor = kRGBColor(246, 246, 246);
    self.tableView.rowHeight = kScreenH- NavBarHeight;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.frame = CGRectMake(0, 0, kScreenW, kScreenH);
    if (self.recruit_id != nil) {
        [self requestData];
    }
    
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(15, KSCREEN_HEIGHT - 50 - NeedStatusViewHeight, KSCREEN_WIDTH - 30, 40)];
    button.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 16];
    [button setTitle:@"发布" forState:UIControlStateNormal];
    [button setTitleColor:kRGBColor(255, 246, 203) forState:UIControlStateNormal];
    CAGradientLayer *gl = [CAGradientLayer layer];
    gl.frame = CGRectMake(0,0,KSCREEN_WIDTH - 30,40);
    gl.startPoint = CGPointMake(0.22, 0.36);
    gl.endPoint = CGPointMake(0.85, 0.72);
    gl.colors = @[(__bridge id)[UIColor colorWithRed:221/255.0 green:169/255.0 blue:72/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:189/255.0 green:135/255.0 blue:35/255.0 alpha:1.0].CGColor];
    gl.locations = @[@(0), @(1.0f)];
    button.layer.cornerRadius = 4;
    button.clipsToBounds = YES;
    [button.layer insertSublayer:gl atIndex:0];
    [self.view addSubview:button];
    [button  addTarget:self action:@selector(addAddressClick) forControlEvents:UIControlEventTouchUpInside];
    self.addBtn = button;
}

- (void)addAddressClick {
    
    if ([CommonTTool isStrNull:self.cell.titleTF.text]) {
        SVP(@"请输入标题");
        return;
    }
    
    if ([CommonTTool isStrNull:self.cell.contentTF.text]) {
        SVP(@"请输入内容");
        return;
    }
    
    self.addBtn.userInteractionEnabled = NO;
    
    NSMutableDictionary *para = [NSMutableDictionary dictionary];
    [para setObject:self.cell.titleTF.text forKey:@"recruit_name"];
    [para setObject:self.cell.contentTF.text forKey:@"description"];
    NSMutableArray *picture_list = [NSMutableArray array];
    int i = 0;
    for (NSString *pic in self.cell.selectImagePathArray) {
        NSDictionary *dict = @{@"path":pic,@"num":@(i)};
        i++;
        [picture_list addObject:dict];
    }
    
    if (picture_list.count > 0) {
        NSData *dataFriends = [NSJSONSerialization dataWithJSONObject:picture_list options:NSJSONWritingPrettyPrinted error:nil];

        NSString *jsonString = [[NSString alloc] initWithData:dataFriends encoding:NSUTF8StringEncoding];
        [para setObject:jsonString forKey:@"picture_list"];
    }
    
    NSString *url = @"business/saveRecruit";
    if (self.recruit_id != nil) {
        url = @"business/updateRecruit";
        [para setObject:self.recruit_id forKey:@"recruit_id"];
    }
    
    SVPShowInternetWaiting;
    [NetRequestTool postUrl:url parameters:para success:^(id responseObject) {
        
        if (self.recruit_id != nil) {
            SVP(@"修改成功");
        }else {
            SVP(@"发布成功");
        }
        [self performSelector:@selector(back) withObject:self afterDelay:2];
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        self.addBtn.userInteractionEnabled = YES;

    }];
    
    
    
    
}

- (void)back {
    [self.navigationController popViewControllerAnimated:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:kBusinessJobUpDate object:nil];
    
}

- (void)requestData
{
    // 请求列表
    [NetRequestTool postUrl:@"business/queryRecruitInfo" parameters:@{@"recruit_id":self.recruit_id, } success:^(id responseObject) {
        self.model = [BusinessJobModel mj_objectWithKeyValues:responseObject[@"object"]];
        [self.tableView reloadData];
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        SVPDismiss
    }];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BusinessReleaseDetailsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BusinessReleaseDetailsCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (self.model != nil) {
        cell.model = self.model;
    }
    self.cell = cell;
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}





@end
