//
//  AddBusinessJobVC.h
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/18.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseViewController.h"
#import "BusinessJobModel.h"
#import "BaseTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface AddBusinessJobVC : BaseViewController

@property (nonatomic, strong) BusinessJobModel *model;
@property (nonatomic, weak)  BaseTableViewController *parentsVC;

@end

NS_ASSUME_NONNULL_END
