//
//  BusinessActivityListVC.m
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/18.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BusinessActivityListVC.h"
#import "BusinessActivityListCell.h"
#import "AddBusinessActivityVC.h"

@interface BusinessActivityListVC ()

@end

@implementation BusinessActivityListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"特价活动";
    self.defaultStartPage = 1;
    
    UIBarButtonItem *addBtn = [[UIBarButtonItem alloc] initWithTitle:@"新增" style:UIBarButtonItemStyleDone target:self action:@selector(rightBarButtonClick)];
    self.navigationItem.rightBarButtonItem = addBtn;
    
    [self creatTableViewWithCellIdentity:@"BusinessActivityListCell"];
    self.tableView.backgroundColor = kRGBColor(246, 246, 246);
    self.tableView.rowHeight = 115;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.frame = CGRectMake(0, 0, kScreenW, kScreenH);
    
    [self setRefresh];
    SVPShowInternetWaiting
    [self requestData];
}

- (void)rightBarButtonClick
{
    AddBusinessActivityVC *vc = [[AddBusinessActivityVC alloc] init];
    vc.parentsVC = self;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)requestData
{
    // 请求列表
    WeakSelf(ws)
    [NetRequestTool postUrl:@"business/queryBusinessActivity" parameters:@{@"is_my":@(1)} success:^(id responseObject) {
        ws.dataArray = [ActivityModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]];
        [ws.tableView reloadData];
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        SVPDismiss
        [ws endRefreshNoMoreData];
    }];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BusinessActivityListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BusinessActivityListCell"];
    cell.model = self.dataArray[indexPath.row];
    cell.editeBtn.tag = indexPath.row;
    [cell.editeBtn addTarget:self action:@selector(cellEditeButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (void)cellEditeButtonClick:(UIButton *)sender
{
    AddBusinessActivityVC *vc = [[AddBusinessActivityVC alloc] init];
    vc.parentsVC = self;
    vc.model = self.dataArray[sender.tag];
    [self.navigationController pushViewController:vc animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
