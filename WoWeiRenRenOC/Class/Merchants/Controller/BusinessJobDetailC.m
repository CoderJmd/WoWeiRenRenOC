//
//  BusinessJobDetailC.m
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/19.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BusinessJobDetailC.h"
#import "BusinessDetailedHeaderView.h"
#import "BusinessJobDetailFooter.h"

@interface BusinessJobDetailC ()

@property (nonatomic, strong) BusinessDetailedHeaderView *header;

@property (nonatomic, strong) BusinessJobDetailFooter *footer;

@end

@implementation BusinessJobDetailC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self creatTableViewWithCellIdentity:@"Cell"];
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.frame = CGRectMake(0, -StatusViewHeight, kScreenW, kScreenH+StatusViewHeight);
    
    self.footer.jobTitle.text = self.model.recruit_name;
    self.footer.salaryLab.text = self.model.wages;
    self.footer.jobDes.text = self.model.descriptionText;
    CGFloat textH = [CommonTTool getLabelHeightWithText:self.model.descriptionText width:(kScreenW-30) font:[UIFont systemFontOfSize:14.0]];
    self.footer.height = textH + 135;
    self.tableView.tableFooterView = self.footer;
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(10, StatusViewHeight+10, 50, 20);
    [backBtn setImage:[UIImage imageNamed:@"biaoqian"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    
    SVPShowInternetWaiting
    [self requestData];
}

- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)requestData
{
    [NetRequestTool postUrl:@"business/getBusinessInfo" parameters:@{@"business_id":@(self.model.business_id)} success:^(id responseObject) {
        BusinessModel *m = [BusinessModel mj_objectWithKeyValues:responseObject[@"object"]];
        self.header.model = m;
        self.header.bottomH.constant = 10.0;
        self.header.height = self.header.headerH - 40;
        self.tableView.tableHeaderView = self.header;
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        SVPDismiss
    }];
}

- (BusinessDetailedHeaderView *)header
{
    if (!_header) {
        _header = [[NSBundle mainBundle] loadNibNamed:@"BusinessDetailedHeaderView" owner:self options:nil].lastObject;
        _header.frame = CGRectMake(0, 0, kScreenW, kScreenW+160+84);
    }
    return _header;
}

- (BusinessJobDetailFooter *)footer
{
    if (!_footer) {
        _footer = [[NSBundle mainBundle] loadNibNamed:@"BusinessJobDetailFooter" owner:self options:nil].lastObject;
        _footer.frame = CGRectMake(0, 0, kScreenW, 200);
    }
    return _footer;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
