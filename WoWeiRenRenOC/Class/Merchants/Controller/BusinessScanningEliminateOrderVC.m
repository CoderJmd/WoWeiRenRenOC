//
//  BusinessScanningEliminateOrderVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/18.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BusinessScanningEliminateOrderVC.h"
#import "JQScanViewController.h"
#import "BusinessScanningEliminateOrderShowCell.h"
#import "UserShoppingOrderModel.h"
#import "UserOrderChildCell.h"
#import "BusinessScanningEliminateOrderHeadViewl.h"
#import "BusinessScanningEliminateOrderDetailsVC.h"

@interface BusinessScanningEliminateOrderVC ()

@end

@implementation BusinessScanningEliminateOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.defaultStartPage = 1;
    self.title = @"扫码核销";
    [self creatTableViewStyle:UITableViewStyleGrouped cellIdentity:@"UserOrderChildCell"];

    self.view.backgroundColor = kRGBColor(246, 246, 246);
    self.tableView.backgroundColor = kRGBColor(246, 246, 246);
    self.tableView.rowHeight = 108;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.frame = CGRectMake(0, NavBarHeight, kScreenW, kScreenH-NavBarHeight -NeedStatusViewHeight-50);
    
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(15, KSCREEN_HEIGHT - 50 - NeedStatusViewHeight, KSCREEN_WIDTH - 30, 40)];
    button.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 16];
    [button setTitle:@"扫码" forState:UIControlStateNormal];
    [button setTitleColor:kRGBColor(255, 246, 203) forState:UIControlStateNormal];
    CAGradientLayer *gl = [CAGradientLayer layer];
    gl.frame = CGRectMake(0,0,KSCREEN_WIDTH - 30,40);
    gl.startPoint = CGPointMake(0.22, 0.36);
    gl.endPoint = CGPointMake(0.85, 0.72);
    gl.colors = @[(__bridge id)[UIColor colorWithRed:221/255.0 green:169/255.0 blue:72/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:189/255.0 green:135/255.0 blue:35/255.0 alpha:1.0].CGColor];
    gl.locations = @[@(0), @(1.0f)];
    button.layer.cornerRadius = 4;
    button.clipsToBounds = YES;
    [button.layer insertSublayer:gl atIndex:0];
    [self.view addSubview:button];
    [button  addTarget:self action:@selector(jumpScanVCClick) forControlEvents:UIControlEventTouchUpInside];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateRequestData) name:kUserOrderDetailsHxTableUpDate object:nil];
    [self setRefresh];
    
    [self requestData];
    
}



- (void)jumpScanVCClick {
    
    JQScanViewController *vc = [JQScanViewController new];
    [self.navigationController pushViewController:vc animated:YES];
    
}

#pragma mark - 刷新
- (void)updateRequestData {
    
    self.page = 1;
    [self requestData];
    
}

- (void)requestData
{
    SVPShowInternetWaiting
    WeakSelf(ws)
    [NetRequestTool postUrl:@"skuOrder/querySkuOrderList" parameters:@{@"page":@(self.page), @"limit":@"10", @"order_state":@"5",@"order_type":@"3"} success:^(id responseObject) {
        NSMutableArray *arr = [UserShoppingOrderModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]];
        if (self.page==self.defaultStartPage) {
            ws.dataArray = arr;
        }
        else {
            [ws.dataArray addObjectsFromArray:arr];
        }
        [ws.tableView reloadData];
        
        // 控制页数
        NSDictionary *page = [responseObject objectForKey:@"page"];
        if ([page isKindOfClass:NSDictionary.class]) {
            NSInteger totalP = [page[@"total_page"] integerValue];
            if (totalP > self.page) {
                self.page++;
                [self endRefresh];
            }
            else {
                [self endRefreshNoMoreData];
            }
        }
        else {
            [self endRefreshNoMoreData];
        }
    } failure:^(NSInteger statusCode) {
        [ws endRefreshNoMoreData];
    } finish:^{
        SVPDismiss
        [self endRefresh];
    }];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserOrderChildCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserOrderChildCell"];
    UserShoppingOrderModel *model = self.dataArray[indexPath.section];
    cell.model = model.sku_order_detail_list[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BusinessScanningEliminateOrderDetailsVC *vc = [[BusinessScanningEliminateOrderDetailsVC alloc]init];
    UserShoppingOrderModel *model = self.dataArray[indexPath.section];
    vc.order_code = model.order_code;
    [self.navigationController pushViewController:vc animated:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    BusinessScanningEliminateOrderHeadViewl *view = [BusinessScanningEliminateOrderHeadViewl getView];
    UserShoppingOrderModel *model = self.dataArray[section];
    MailGoodsDetailsModel *goodsModel = model.sku_order_detail_list[0];

    NSString *time =  [CommonTTool time_timestamp:goodsModel.hx_time andDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    view.timeLabel.text = time;
    if (goodsModel.is_hx == 1) {
        view.statusLabel.text = @"未核销";
    }else {
        view.statusLabel.text = @"已核销";
    }
    
    
    return view;
}




- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 60.0f;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake( 0, 0, KSCREEN_WIDTH, 10)];
    view.backgroundColor = VCBgcolor;
    
    return view;;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 10;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section  {
    UserShoppingOrderModel *model = self.dataArray[section];
    
    return model.sku_order_detail_list.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return self.dataArray.count;
}
    



@end
