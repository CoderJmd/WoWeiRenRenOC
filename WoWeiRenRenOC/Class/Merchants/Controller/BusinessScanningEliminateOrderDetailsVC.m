//
//  BusinessScanningEliminateOrderDetailsVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/18.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BusinessScanningEliminateOrderDetailsVC.h"
#import "MailConfirmOrderVC.h"
#import "MailConfirmOrderHeadView.h"
#import "MailConfirmOrderGoodsCell.h"
#import "MailConfirmOrderFooterView.h"
#import "MailConfirmOrderBottomView.h"
#import "UserOrderDetailsHeadView.h"
#import "UserOrderChildCell.h"
#import "UserOrderDetailsShowCell.h"
#import "UserOrderDetailsBottomView.h"
#import "UserShoppingOrderDetailsModel.h"
#import "UserShoppingOrderDetailsLogisticsView.h"
#import "OrderPayVC.h"
#import "PagingViewController.h"
#import "UserOrderChildGroupView.h"
#import "UserOrderDetailsQRCodeCell.h"


@interface BusinessScanningEliminateOrderDetailsVC ()

@property (strong, nonatomic)UserOrderDetailsBottomView *bottomView;

@property (strong, nonatomic)NSMutableArray *cellContentArray;

@property (strong, nonatomic)UserShoppingOrderDetailsModel *model;
@end

@implementation BusinessScanningEliminateOrderDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
        
    
    self.defaultStartPage = 1;
    self.title = @"订单详情";
    
    
//    [self creatTableViewWithCellIdentity:@"UserOrderChildCell"];
    [self creatTableViewStyle:UITableViewStyleGrouped cellIdentity:@"UserOrderChildCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"UserOrderDetailsShowCell" bundle:nil] forCellReuseIdentifier:@"UserOrderDetailsShowCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"UserOrderDetailsQRCodeCell" bundle:nil] forCellReuseIdentifier:@"UserOrderDetailsQRCodeCell"];
    
    self.view.backgroundColor = WhiteColor;
    self.tableView.backgroundColor = WhiteColor;
    self.tableView.rowHeight = 110;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.frame = CGRectMake(0, NavBarHeight, kScreenW, kScreenH-NavBarHeight -NeedStatusViewHeight);
    
    [self.view addSubview:self.bottomView];
    [self requestData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestData) name:kUserOrderDetilsUpDate object:nil];
    
}
- (void)requestData
{
    SVPShowInternetWaiting
   // 请求列表
    [NetRequestTool postUrl:@"skuOrder/querySkuOrderInfo" parameters:@{@"order_code":self.order_code} success:^(id responseObject) {
        
        self.model = [UserShoppingOrderDetailsModel mj_objectWithKeyValues:responseObject[@"object"]];
        self.cellContentArray = [NSMutableArray array];
        
        
        [self.cellContentArray addObject:@{@"title":@"商品金额",@"content":[NSString stringWithFormat:@"￥%@",[CommonTTool  getPrice:self.model.sku_price]]}];
 
        [self.cellContentArray addObject:@{@"title":@"付款金额",@"content":[NSString stringWithFormat:@"￥%@",[CommonTTool  getPrice:self.model.pay_price]]}];
        if (self.model.order_state == 3 || self.model.order_state == 4 || self.model.order_state == 5) {
            NSString *pay_type = self.model.pay_type == 1 ? @"支付宝支付":@"微信支付";
            [self.cellContentArray addObject:@{@"title":@"支付方式",@"content":pay_type}];
            [self.cellContentArray addObject:@{@"title":@"创建时间",@"content":self.model.create_time}];
            [self.cellContentArray addObject:@{@"title":@"付款时间",@"content":self.model.pay_time}];
        }else {
            [self.cellContentArray addObject:@{@"title":@"创建时间",@"content":self.model.create_time}];
        }
        MailGoodsDetailsModel *goodsModel = self.model.sku_order_detail_list[0];
        if (goodsModel.is_hx == 1) {
            [self.cellContentArray addObject:@{@"title":@"核销状态",@"content":@"未核销"}];
            self.bottomView.hidden = NO;
            [self.bottomView.paymentBtn setTitle:@"确认核销" forState:UIControlStateNormal];
            self.bottomView.paymentBtn.hidden = NO;
            [self.bottomView.paymentBtn addTarget:self action:@selector(hxClick) forControlEvents:UIControlEventTouchUpInside];
        }else {
            [self.cellContentArray addObject:@{@"title":@"核销状态",@"content":@"已核销"}];
            NSString *time =  [CommonTTool time_timestamp:goodsModel.hx_time andDateFormat:@"yyyy-MM-dd hh:mm:ss"];
            [self.cellContentArray addObject:@{@"title":@"核销时间",@"content":time}];
             self.bottomView.hidden = YES;
        }
       
        [self.cellContentArray addObject:@{@"title":@"订单编号",@"content":self.model.order_code}];
        if (![CommonTTool isStrNull:self.model.remark]) {
            [self.cellContentArray addObject:@{@"title":@"订单备注",@"content":self.model.remark}];
        }
                
        [self.tableView reloadData];
        
        
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        
    }];
    
}

#pragma mark - 核销
- (void)hxClick {
    
    // 系统提示框
    UIAlertController *alterController = [UIAlertController alertControllerWithTitle:nil message:@"是否核销该订单？" preferredStyle:UIAlertControllerStyleAlert];
    [alterController addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
        
    }]];
    
    [alterController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        MailGoodsDetailsModel *goodsModel = self.model.sku_order_detail_list[0];
        SVPShowInternetWaiting;
        [NetRequestTool postUrl:@"skuOrder/hxOrderDetail" parameters:@{@"dh_code":goodsModel.dh_code} success:^(id responseObject) {
            SVP(@"核销成功");
            [self requestData];
            [[NSNotificationCenter defaultCenter] postNotificationName:kUserOrderDetailsHxTableUpDate object:nil];
            
        } failure:^(NSInteger statusCode) {
            
        } finish:^{
            
        }];
        
    }]];
    
    [[CommonTTool getCurrentVC] presentViewController:alterController animated:YES completion:nil];
    
    
    
    
}



- (UserOrderDetailsBottomView *)bottomView {
    
    if (!_bottomView) {
        
        _bottomView = [[NSBundle mainBundle] loadNibNamed:@"UserOrderDetailsBottomView" owner:self options:nil].lastObject;
        _bottomView.frame = CGRectMake(0, KSCREEN_HEIGHT -70 -NeedStatusViewHeight, kScreenW, 40);
    }
    
    return _bottomView;
}


    
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0) {
        
        UserOrderChildCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserOrderChildCell"];
        cell.model = self.model.sku_order_detail_list[indexPath.row];
        tableView.rowHeight = 110;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        
    }else {
        
        UserOrderDetailsShowCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserOrderDetailsShowCell"];
               //cell.model = self.dataArray[indexPath.row];
        NSDictionary *model = self.cellContentArray[indexPath.row];
        cell.nameLabel.text = model[@"title"];
        cell.contentLabel.text = model[@"content"];
        
        if (indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2) {
            cell.contentLabel.textColor = kRGBAColor(198, 1, 34, 1);
        }else if ([cell.nameLabel.text isEqualToString:@"核销状态"]) {
            cell.contentLabel.textColor = kRGBAColor(206, 160, 9, 1);
        }else {
            cell.contentLabel.textColor = [UIColor darkGrayColor];
        }
        
        
        tableView.rowHeight = 50;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        
    }
    
}


- (void)copyOrderCode {
    
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];

    pasteboard.string = self.cellContentArray[self.cellContentArray.count - 1][@"content"];;

    SVP(@"订单编号已复制");
    
    
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section == 1) {
        
        return self.cellContentArray.count;
    }
    
    
    return self.model.sku_order_detail_list.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    
    return 2;
    
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        
         UserOrderChildGroupView *view = [[NSBundle mainBundle] loadNibNamed:@"UserOrderChildGroupView" owner:self options:nil].lastObject;
         view.nameLabel.text = self.model.business_name;

         view.jumpBtn.tag = section;
         [view.jumpBtn addTarget:self action:@selector(headVieTapClick:) forControlEvents:UIControlEventTouchUpInside];
         view.tag = section;
         
         return view;
        
    }else {
        
        UIView *view = [[UIView alloc]init];
        UIView *one = [[UIView alloc]init];
        one.frame = CGRectMake(0, 10, kScreenW, 10);
        [view addSubview:one];
        one.backgroundColor = [UIColor whiteColor];
        view.backgroundColor = kRGBColor(246, 246, 246);
        view.clipsToBounds = YES;
        
        return view;
        
    }
 
}

- (void)headVieTapClick:(UIButton *)view {
    
    PagingViewController *vc= [[PagingViewController alloc]init];
    vc.business_id = self.model.business_id;
    [self.navigationController pushViewController:vc animated:YES];
}




- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
         return 60;
    }else {
        return 0.01f;
    }
   
}

- (void)back {
    
    [self.navigationController popToViewController:self.navigationController.childViewControllers[1] animated:YES];
}

@end
