//
//
//
//
//  Created by 韩俊强 on 15/10/21.
//  Copyright © 2015年 韩俊强. All rights reserved.
//

#import "JQScanViewController.h"
#import "JQScanResult.h"
#import "JQScanWrapper.h"
#import "MyQRViewController.h"
#import "JQScanResultViewController.h"
#import "JXQrCodeHeadView.h"
#import "JXQrCodeBottomView.h"
#import "BusinessScanningEliminateOrderDetailsVC.h"
@interface JQScanViewController ()
@property (nonatomic,strong) JXQrCodeHeadView *headView;
@property (strong, nonatomic)JXQrCodeBottomView *bottomView;
@end

@implementation JQScanViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    JQScanViewStyle *style = [[JQScanViewStyle alloc]init];
    style.anmiationStyle = LBXScanViewAnimationStyle_LineMove;
    style.animationImage = [UIImage imageNamed:@"CodeScan.bundle/qrcode_scan_light_green"];
    style.colorAngle = kRGBAColor(206, 160, 9, 1);
    style.isNeedShowRetangle = NO;
    style.photoframeAngleH = 60;
    style.photoframeAngleW = 60;
    style.photoframeLineW = 3;
    self.style = style;
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    self.view.backgroundColor = [UIColor blackColor];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.view addSubview:self.headView];
    [self.view addSubview:self.bottomView];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (JXQrCodeHeadView *)headView {
    
    if (!_headView) {
        _headView = [[NSBundle mainBundle] loadNibNamed:@"JXQrCodeHeadView" owner:self options:nil].lastObject;
        
        _headView.frame = CGRectMake(0, StatusViewHeight, kScreenW, 49);
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(back)];
        [_headView.fanhuiBtn addGestureRecognizer:tap];
        
    }
    
    
    return _headView;
    
}


- (JXQrCodeBottomView *)bottomView {
    
    if (!_bottomView) {
        _bottomView = [[NSBundle mainBundle] loadNibNamed:@"JXQrCodeBottomView" owner:self options:nil].lastObject;
        _bottomView.frame = CGRectMake(0, kScreenH - 200 - NeedStatusViewHeight,kScreenW, 160);
        [_bottomView.flashBtn addTarget:self action:@selector(openOrCloseFlash) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _bottomView;
    
}



- (void)back {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}




- (void)scanResultWithArray:(NSArray<JQScanResult*>*)array
{
    
    if (array.count < 1){
        [self popAlertMsgWithScanResult:nil];

        return;
    }
//    //经测试，可以同时识别2个二维码，不能同时识别二维码和条形码
//    for (JQScanResult *result in array) {
//
//        NSLog(@"scanResult:%@",result.strScanned);
//    }

//
//    self.scanImage = scanResult.imgScanned;
//

//    //震动提醒
//    [JQScanWrapper systemVibrate];
//    //声音提醒
//    [JQScanWrapper systemSound];
    JQScanResult *scanResult = array[0];
    NSString*strResult = scanResult.strScanned;

    if (!strResult) {

        [self popAlertMsgWithScanResult:nil];

        return;
    }
    [self showNextVCWithScanResult:scanResult];
   
}

#pragma mark - 识别失败
- (void)popAlertMsgWithScanResult:(NSString*)strResult
{
        // 系统提示框
       UIAlertController *alterController = [UIAlertController alertControllerWithTitle:nil message:@"识别失败，请重试！" preferredStyle:UIAlertControllerStyleAlert];
    
       [alterController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
           
           [self reStartDevice];
       }]];
       
       [[CommonTTool getCurrentVC] presentViewController:alterController animated:YES completion:nil];
    
}

#pragma mark ------ 识别结果 ------
- (void)showNextVCWithScanResult:(JQScanResult*)strResult
{
    
    NSArray *resultArray = [strResult.strScanned componentsSeparatedByString:@","];
    if (resultArray.count != 2) {
        
        UIAlertController *alterController = [UIAlertController alertControllerWithTitle:nil message:@"识别失败，请重试！" preferredStyle:UIAlertControllerStyleAlert];

        [alterController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
           
           [self reStartDevice];

        }]];

        [[CommonTTool getCurrentVC] presentViewController:alterController animated:YES completion:nil];
        
        return;
    }
    
    BusinessModel *model = [BusinessModel mj_objectWithKeyValues:[[NSUserDefaults standardUserDefaults] objectForKey:kUserMerchantsInfo]];
    NSString *business_id = resultArray[1];
    if (model.business_id != [business_id integerValue]) {
        UIAlertController *alterController = [UIAlertController alertControllerWithTitle:nil message:@"该核销商品非本店商品，请重试！" preferredStyle:UIAlertControllerStyleAlert];

        [alterController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
          
          [self reStartDevice];

        }]];

        [[CommonTTool getCurrentVC] presentViewController:alterController animated:YES completion:nil];

        return;
    }
    
    
    
    BusinessScanningEliminateOrderDetailsVC *vc = [[BusinessScanningEliminateOrderDetailsVC alloc]init];
    vc.order_code = resultArray[0];
    [self.navigationController pushViewController:vc animated:YES];
    
}

#pragma mark -底部功能项
//打开相册
- (void)openPhoto
{
    if ([JQScanWrapper isGetPhotoPermission])
        [self openLocalPhoto];
    else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"请到设置->隐私中开启本程序相册权限" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            
        }];
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

//开关闪光灯
- (void)openOrCloseFlash
{
    [super openOrCloseFlash];

    if (self.isOpenFlash){
//        [_btnFlash setImage:[UIImage imageNamed:@"CodeScan.bundle/qrcode_scan_btn_flash_down"] forState:UIControlStateNormal];
        self.bottomView.titleLabel.text = @"轻点关闭";
    }else{
//        [_btnFlash setImage:[UIImage imageNamed:@"CodeScan.bundle/qrcode_scan_btn_flash_nor"] forState:UIControlStateNormal];
        self.bottomView.titleLabel.text = @"轻点照亮";
    }
}
#pragma mark -底部功能项
- (void)myQRCode
{
    MyQRViewController *vc = [MyQRViewController new];
    [self.navigationController pushViewController:vc animated:YES];
}
@end
