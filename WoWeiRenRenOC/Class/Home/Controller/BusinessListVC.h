//
//  BusinessListVC.h
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/15.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseTableViewController.h"
#import "HomeIconModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BusinessListVC : BaseTableViewController

@property (nonatomic, strong) HomeIconModel *model;

@end

NS_ASSUME_NONNULL_END
