//
//  BusinessDetailedVC.m
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/15.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BusinessDetailedVC.h"
#import "BusinessDetailedHeaderView.h"
#import "BusinessDetailedCell.h"
#import "ActivityModel.h"
#import "PagingViewController.h"


@interface BusinessDetailedVC ()

@property (nonatomic, strong) BusinessDetailedHeaderView *header;

@end

@implementation BusinessDetailedVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self creatTableViewWithCellIdentity:@"BusinessDetailedCell"];
    self.tableView.backgroundColor = kRGBColor(246, 246, 246);
    self.tableView.rowHeight = 115;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.frame = CGRectMake(0, -StatusViewHeight, kScreenW, kScreenH+StatusViewHeight);
    self.header.model = self.model;
    self.header.height = self.header.headerH;
    self.tableView.tableHeaderView = self.header;
    [self setRefresh];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(10, StatusViewHeight+10, 32, 32);
    [backBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    
    SVPShowInternetWaiting
    [self requestData];
    if (self.model.zxtg_state == 4 && ![[NSUserDefaults standardUserDefaults] boolForKey:@"kUserIsMerchants"]) {
        UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(15, KSCREEN_HEIGHT - 50 - NeedStatusViewHeight, KSCREEN_WIDTH - 30, 40)];
           button.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size: 16];
           [button setTitle:@"查看店铺" forState:UIControlStateNormal];
           [button setTitleColor:kRGBColor(255, 246, 203) forState:UIControlStateNormal];
           CAGradientLayer *gl = [CAGradientLayer layer];
           gl.frame = CGRectMake(0,0,KSCREEN_WIDTH - 30,40);
           gl.startPoint = CGPointMake(0.22, 0.36);
           gl.endPoint = CGPointMake(0.85, 0.72);
           gl.colors = @[(__bridge id)[UIColor colorWithRed:221/255.0 green:169/255.0 blue:72/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:189/255.0 green:135/255.0 blue:35/255.0 alpha:1.0].CGColor];
           gl.locations = @[@(0), @(1.0f)];
           button.layer.cornerRadius = 4;
           button.clipsToBounds = YES;
           [button.layer insertSublayer:gl atIndex:0];
           [self.view addSubview:button];
           [button  addTarget:self action:@selector(junpDetailsClick) forControlEvents:UIControlEventTouchUpInside];
           
    }
}

- (void)junpDetailsClick {
    
    PagingViewController *vc= [[PagingViewController alloc]init];
    vc.business_id = [NSString stringWithFormat:@"%ld",(long)self.model.business_id];
    [[CommonTTool getCurrentVC].navigationController pushViewController:vc animated:YES];
    
    
}


- (void)requestData
{
    WeakSelf(ws)
    [NetRequestTool postUrl:@"business/queryBusinessActivity" parameters:@{@"business_id":@(self.model.business_id)} success:^(id responseObject) {
        ws.dataArray = [ActivityModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]];
        [ws.tableView reloadData];
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        SVPDismiss
        [ws endRefreshNoMoreData];
    }];
}

- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BusinessDetailedCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BusinessDetailedCell"];
    ActivityModel *m = self.dataArray[indexPath.row];
    SD_SETIMAGE_PlaceHolder(cell.imgView, m.img_path, @"default_image_square")
    cell.titleLab.text = m.activity_name;
    cell.model = m;
    return cell;
}

- (BusinessDetailedHeaderView *)header
{
    if (!_header) {
        _header = [[NSBundle mainBundle] loadNibNamed:@"BusinessDetailedHeaderView" owner:self options:nil].lastObject;
        _header.frame = CGRectMake(0, 0, kScreenW, kScreenW+160+84);
    }
    return _header;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

@end
