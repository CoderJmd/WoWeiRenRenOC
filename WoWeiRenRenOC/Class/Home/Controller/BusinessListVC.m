//
//  BusinessListVC.m
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/15.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BusinessListVC.h"
#import "HomeCell.h"
#import "BusinessDetailedVC.h"
#import "PagingViewController.h"
#import "BenDiBusinessSearchView.h"

@interface BusinessListVC ()<UITextFieldDelegate>
@property (nonatomic,weak) BenDiBusinessSearchView *headView;
@property (strong, nonatomic)NSString *searchName;
@end

@implementation BusinessListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = self.model.classify_name;
    self.defaultStartPage = 1;
    
    [self creatTableViewWithCellIdentity:@"HomeCell"];
    self.headView = [[NSBundle mainBundle] loadNibNamed:@"BenDiBusinessSearchView" owner:self options:nil].lastObject;
    self.headView.frame = CGRectMake(0, NavBarHeight, kScreenW, 50);
    self.headView.searchTF.delegate = self;
    self.headView.searchTF.returnKeyType = UIReturnKeySearch;
    [self.view addSubview:self.headView];
    self.view.backgroundColor = kRGBColor(246, 246, 246);
    self.tableView.backgroundColor = kRGBColor(246, 246, 246);
    self.tableView.rowHeight = 135;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.frame = CGRectMake(0, NavBarHeight + 50, kScreenW, kScreenH - NavBarHeight);
    
    [self setRefresh];
    SVPShowInternetWaiting
    [self requestData];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    if (![CommonTTool isStrNull:textField.text]) {
        self.searchName = textField.text;
        self.page = 1;
        [self requestData];
        
    }else {
        self.searchName = @"";
        self.page = 1;
        [self requestData];
    }
    
    return YES;
}

- (void)requestData
{
    if (self.searchName == nil) {
        self.searchName = @"";
    }
    // 请求列表
    WeakSelf(ws)
    [NetRequestTool postUrl:@"business/queryBusinessList" parameters:@{@"page":@(self.page), @"limit":@"10", @"classification_id":self.model.classify_id,@"business_name":self.searchName} success:^(id responseObject) {
        NSMutableArray *arr = [BusinessModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]];
        if (self.page==self.defaultStartPage) {
            ws.dataArray = arr;
        }
        else {
            [ws.dataArray addObjectsFromArray:arr];
        }
        [ws.tableView reloadData];
        
        // 控制页数
        NSDictionary *page = [responseObject objectForKey:@"page"];
        if ([page isKindOfClass:NSDictionary.class]) {
            NSInteger totalP = [page[@"total_page"] integerValue];
            if (totalP > self.page) {
                self.page++;
                [self endRefresh];
            }
            else {
                [self endRefreshNoMoreData];
            }
        }
        else {
            [self endRefreshNoMoreData];
        }
    } failure:^(NSInteger statusCode) {
        [ws endRefreshNoMoreData];
    } finish:^{
        SVPDismiss
    }];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HomeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HomeCell"];
    cell.model = self.dataArray[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BusinessDetailedVC *vc = [[BusinessDetailedVC alloc] init];
    vc.model = self.dataArray[indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
