//
//  OCExampleViewController.h
//  JXPagingView
//
//  Created by jiaxin on 2018/8/27.
//  Copyright © 2018年 jiaxin. All rights reserved.
//

#import "BaseViewController.h"
#import <UIKit/UIKit.h>
#import "JXPagerView.h"
#import "NewBusinessDetailedHeaderView.h"
#import "ListViewController.h"
#import "JXCategoryTitleView.h"

static CGFloat JXTableHeaderViewHeight = 500;
static const CGFloat JXheightForHeaderInSection = 50;

@interface PagingViewController : BaseViewController <JXPagerViewDelegate, JXPagerMainTableViewGestureDelegate>

@property (nonatomic, strong) JXPagerView *pagerView;
@property (nonatomic, strong) NewBusinessDetailedHeaderView *header;
@property (nonatomic, strong, readonly) JXCategoryTitleView *categoryView;
@property (nonatomic, assign) BOOL isNeedFooter;
@property (nonatomic, assign) BOOL isNeedHeader;
- (JXPagerView *)preferredPagingView;
@property (strong, nonatomic)NSString *business_id;
@end
