//
//  BenDiTableVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/7/6.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BenDiTableVC.h"
#import "HomeCell.h"
#import "HomeHeaderView.h"
#import "HomeIconModel.h"
#import "BusinessListVC.h"
#import "BusinessDetailedVC.h"
#import "AddressSelectVC.h"
#import "WebVC.h"
#import "RegisterPayVC.h"
#import "HomeClassSkuModel.h"
#import "MailCell.h"
#import "AbulkCell.h"
#import "HomeClassSkuHeadView.h"
#import "MailDetailsVC.h"
#import "PagingViewController.h"
#import "BenDiTableHeadView.h"

extern UserModel *userModel;
@interface BenDiTableVC () <BenDiTableHeadViewDelegata>

@property (nonatomic, strong) BenDiTableHeadView *header;
@property (strong, nonatomic)NSArray *hotBusinessArray; //热门商家

@end

@implementation BenDiTableVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.defaultStartPage = 1;
    self.title = @"本地生活";
    [self creatTableViewWithCellIdentity:@"HomeCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"MailCell" bundle:nil] forCellReuseIdentifier:@"MailCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"AbulkCell" bundle:nil] forCellReuseIdentifier:@"AbulkCell"];
    
    self.tableView.backgroundColor = kRGBColor(246, 246, 246);
    self.tableView.rowHeight = 135;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.frame = CGRectMake(0, -StatusViewHeight, kScreenW, kScreenH-TabBarHeight+StatusViewHeight);
    self.tableView.tableHeaderView = self.header;
    [self setRefresh];
    
    SVPShowInternetWaiting
    [self requestData];
}

- (void)requestData
{
    [NetManager isBannerinfoListWithParameters:@{@"position":@"BDSH_HOME"} success:^(NSDictionary * _Nonnull dic) {
        
        self.header.bannerArray = [BannerModel mj_objectArrayWithKeyValuesArray:dic[@"object"]];
        
    } finish:^{
        
    }];
    WeakSelf(ws)
    // 请求首页数据
    [NetRequestTool postUrl:@"index/queryIndex" parameters:@{} success:^(id responseObject) {
        NSDictionary *dict = [responseObject objectForKey:@"object"];
//        ws.header.bannerArray = [BannerModel mj_objectArrayWithKeyValuesArray:dict[@"bannerList"]];
        ws.header.iconArray = [HomeIconModel mj_objectArrayWithKeyValuesArray:dict[@"classifyList"]];
        
        [ws.tableView reloadData];
        
    } failure:^(NSInteger statusCode) {

    } finish:nil];

//    // 请求登录数据
//    [NetRequestTool postUrl:@"index/queryTj" parameters:@{} success:^(id responseObject) {
//        ws.header.numDict = [responseObject objectForKey:@"object"];
//    } failure:^(NSInteger statusCode) {
//
//    } finish:nil];
    
    // 请求热门商家
    [NetRequestTool postUrl:@"business/queryBusinessList" parameters:@{@"page":@"1", @"limit":@"10", @"is_hot":@"1"} success:^(id responseObject) {
        ws.hotBusinessArray = [BusinessModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]];
        [ws.tableView reloadData];
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        SVPDismiss
        [ws endRefreshNoMoreData];
    }];
}

- (void)HeaderViewBannerClickAtIndex:(NSInteger)index model:(BannerModel *)model
{
    [CommonTTool jumpBannerWithModel:model];
}

- (void)HeaderViewIconClickAtIndex:(NSInteger)index model:(HomeIconModel *)model
{
    BusinessListVC *vc = [[BusinessListVC alloc] init];
    vc.model = model;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HomeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HomeCell"];
    cell.model = self.hotBusinessArray[indexPath.row];
    return cell;
   
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    if (![CommonTTool isJumpLoginVC:YES]) {
       return;
    }
    
    BusinessDetailedVC *vc = [[BusinessDetailedVC alloc] init];
    vc.model = self.hotBusinessArray[indexPath.row];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (BenDiTableHeadView *)header
{
    if (!_header) {
        @try {
            _header = [[NSBundle mainBundle] loadNibNamed:@"BenDiTableHeadView" owner:self options:nil].lastObject;

        }@catch (NSException *exception){
            NSLog(@"exception = %@", exception);

        }@finally {
            
        }

        _header.frame = CGRectMake(0, 0, kScreenW, 430);
        _header.delegate = self;
        if (NeediPhoneXiPhoneXR) {
            _header.height += 22.0;
        }
        
        
    }
    return _header;
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 3;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    HomeClassSkuHeadView *headView = [[NSBundle mainBundle] loadNibNamed:@"HomeClassSkuHeadView" owner:self options:nil].lastObject;
    headView.nameLabel.text = @"热门商家";
                                    
    return headView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 38;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];

    
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];

}



@end
