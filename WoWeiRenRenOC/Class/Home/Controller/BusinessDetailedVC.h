//
//  BusinessDetailedVC.h
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/15.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseTableViewController.h"
#import "BusinessModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BusinessDetailedVC : BaseTableViewController

@property (nonatomic, strong) BusinessModel *model;

@end

NS_ASSUME_NONNULL_END
