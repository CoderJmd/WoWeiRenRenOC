//
//  HomeVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/14.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "HomeVC.h"
#import "HomeCell.h"
#import "HomeHeaderView.h"
#import "HomeIconModel.h"
#import "BusinessListVC.h"
#import "BusinessDetailedVC.h"
#import "AddressSelectVC.h"
#import "WebVC.h"
#import "RegisterPayVC.h"
#import "HomeClassSkuModel.h"
#import "MailCell.h"
#import "AbulkCell.h"
#import "HomeClassSkuHeadView.h"
#import "MailDetailsVC.h"
#import "PagingViewController.h"

extern UserModel *userModel;
@interface HomeVC () <HomeHeaderViewDelegata>

@property (nonatomic, strong) HomeHeaderView *header;
@property (strong, nonatomic)NSArray *classSkuArray; //分类商品
@property (strong, nonatomic)NSArray *hotBusinessArray; //热门商家

@end

@implementation HomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.defaultStartPage = 1;
    
    [self creatTableViewWithCellIdentity:@"HomeCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"MailCell" bundle:nil] forCellReuseIdentifier:@"MailCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"AbulkCell" bundle:nil] forCellReuseIdentifier:@"AbulkCell"];
    
    self.tableView.backgroundColor = kRGBColor(246, 246, 246);
    self.tableView.rowHeight = 135;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.frame = CGRectMake(0, -StatusViewHeight, kScreenW, kScreenH-TabBarHeight+StatusViewHeight);
    self.tableView.tableHeaderView = self.header;
    [self setRefresh];
    
    SVPShowInternetWaiting
    [self requestData];
    
}

- (void)requestData
{
    WeakSelf(ws)
    // 请求首页数据
    [NetRequestTool postUrl:@"index/queryIndex" parameters:@{} success:^(id responseObject) {
        NSDictionary *dict = [responseObject objectForKey:@"object"];
        ws.header.bannerArray = [BannerModel mj_objectArrayWithKeyValuesArray:dict[@"bannerList"]];
        ws.header.iconArray = [HomeIconModel mj_objectArrayWithKeyValuesArray:dict[@"classifyList"]];
        ws.classSkuArray = [HomeClassSkuModel mj_objectArrayWithKeyValuesArray:dict[@"classify_sku"]];
        [ws.tableView reloadData];
        
    } failure:^(NSInteger statusCode) {

    } finish:nil];

//    // 请求登录数据
//    [NetRequestTool postUrl:@"index/queryTj" parameters:@{} success:^(id responseObject) {
//        ws.header.numDict = [responseObject objectForKey:@"object"];
//    } failure:^(NSInteger statusCode) {
//
//    } finish:nil];
    
    // 请求热门商家
    [NetRequestTool postUrl:@"business/queryBusinessList" parameters:@{@"page":@"1", @"limit":@"10", @"is_hot":@"1"} success:^(id responseObject) {
        ws.hotBusinessArray = [BusinessModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]];
        [ws.tableView reloadData];
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        SVPDismiss
        [ws endRefreshNoMoreData];
    }];
}

- (void)HeaderViewBannerClickAtIndex:(NSInteger)index model:(BannerModel *)model
{
    NSLog(@"index = %zd", index);
    if ([model.jump_type isEqualToString:@"0"]) {
        WebVC *vc = [[WebVC alloc]init];
        vc.webtitle  = model.title;
        vc.linkPath = model.jump_path;
        vc.htmlString = model.depict;
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)HeaderViewIconClickAtIndex:(NSInteger)index model:(HomeIconModel *)model
{
    BusinessListVC *vc = [[BusinessListVC alloc] init];
    vc.model = model;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section < self.classSkuArray.count) {
        HomeClassSkuModel *model = self.classSkuArray[indexPath.section];
        MailGoodsModel *goodsModel = model.sku_list[indexPath.row];
        if (goodsModel.sku_type == 2 || goodsModel.sku_type == 9) {
            AbulkCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AbulkCell"];
            cell.model = goodsModel;
            tableView.rowHeight = 150;
            return cell;
            
        }else {
            MailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MailCell"];
            cell.model = goodsModel;
            tableView.rowHeight = 135;

            return cell;
            
        }
        
    }else {
        HomeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HomeCell"];
        cell.model = self.hotBusinessArray[indexPath.row];
        tableView.rowHeight = 135;

        return cell;
    }
   
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{


    
    if (indexPath.section < self.classSkuArray.count) {
       HomeClassSkuModel *model = self.classSkuArray[indexPath.section];
       MailDetailsVC *vc = [[MailDetailsVC alloc] init];
       vc.goodsModel = model.sku_list[indexPath.row];
       vc.hidesBottomBarWhenPushed = YES;
       [self.navigationController pushViewController:vc animated:YES];
    }else {
       BusinessDetailedVC *vc = [[BusinessDetailedVC alloc] init];
       vc.model = self.hotBusinessArray[indexPath.row];
       vc.hidesBottomBarWhenPushed = YES;
       [self.navigationController pushViewController:vc animated:YES];
       
    }
    
}

- (HomeHeaderView *)header
{
    if (!_header) {
        _header = [[NSBundle mainBundle] loadNibNamed:@"HomeHeaderView" owner:self options:nil].lastObject;
        _header.frame = CGRectMake(0, 0, kScreenW, 490);
        _header.delegate = self;
        if (NeediPhoneXiPhoneXR) {
            _header.height += 22.0;
        }
    }
    return _header;
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return self.classSkuArray.count + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section < self.classSkuArray.count) {
        HomeClassSkuModel *model = self.classSkuArray[section];
        return model.sku_list.count;
    }else {
        return self.hotBusinessArray.count;
    }
    
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    HomeClassSkuHeadView *headView = [[NSBundle mainBundle] loadNibNamed:@"HomeClassSkuHeadView" owner:self options:nil].lastObject;
    if (section < self.classSkuArray.count) {
        HomeClassSkuModel *model = self.classSkuArray[section];
        headView.nameLabel.text = model.classify_name;
    }else {
        headView.nameLabel.text = @"热门商家";
    }
                                    
    return headView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 38;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:animated];

}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}



@end
