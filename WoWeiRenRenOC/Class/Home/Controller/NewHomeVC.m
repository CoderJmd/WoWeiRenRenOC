//
//  NewHomeVC.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/7/6.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "NewHomeVC.h"
#import "HomeCell.h"
#import "HomeHeaderView.h"
#import "HomeIconModel.h"
#import "BusinessListVC.h"
#import "BusinessDetailedVC.h"
#import "AddressSelectVC.h"
#import "WebVC.h"
#import "RegisterPayVC.h"
#import "HomeClassSkuModel.h"
#import "MailCell.h"
#import "AbulkCell.h"
#import "HomeClassSkuHeadView.h"
#import "MailDetailsVC.h"
#import "PagingViewController.h"
#import "MailClassVC.h"
#import "PagingViewController.h"
#import "HomeVC.h"
#import "BenDiTableVC.h"
#import "GiftBagView.h"


extern UserModel *userModel;
@interface NewHomeVC () <HomeHeaderViewDelegata>

@property (nonatomic, strong) HomeHeaderView *header;
@property (strong, nonatomic)NSArray *classSkuArray; //分类商品
@property (strong, nonatomic)NSArray *hotTgArray; //热门团购
@property (strong, nonatomic)NSArray *hotBusinessArray; //热门商家
@property (strong, nonatomic)MailGoodsDetailsModel *LBMailModel;//礼包
@property (weak, nonatomic)GiftBagView *giftBagView;

@end

@implementation NewHomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.defaultStartPage = 1;
    
//    [self creatTableViewWithCellIdentity:@"HomeCell"];
    [self creatTableViewStyle:UITableViewStyleGrouped cellIdentity:@"HomeCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"MailCell" bundle:nil] forCellReuseIdentifier:@"MailCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"AbulkCell" bundle:nil] forCellReuseIdentifier:@"AbulkCell"];
    
    self.tableView.backgroundColor = kRGBColor(246, 246, 246);
    self.tableView.rowHeight = 135;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.frame = CGRectMake(0, -StatusViewHeight, kScreenW, kScreenH-TabBarHeight+StatusViewHeight);
    self.tableView.tableHeaderView = self.header;
    [self setRefresh];
    
    SVPShowInternetWaiting
    [self requestData];
    BOOL isLogin = [CommonTTool  isJumpLoginVC:NO];
    if (isLogin) {
        [self queryLbInfoList];
    }
    [CommonTTool queryNewVersionInfo];
}

- (void)requestData
{
    WeakSelf(ws)
    // 请求首页数据
    [NetRequestTool postUrl:@"index/queryIndex" parameters:@{} success:^(id responseObject) {
        NSDictionary *dict = [responseObject objectForKey:@"object"];
        ws.header.bannerArray = [BannerModel mj_objectArrayWithKeyValuesArray:dict[@"bannerList"]];
//        ws.header.iconArray = [HomeIconModel mj_objectArrayWithKeyValuesArray:dict[@"classifyList"]];
        ws.classSkuArray = [HomeClassSkuModel mj_objectArrayWithKeyValuesArray:dict[@"classify_sku"]];
        ws.hotTgArray = [MailGoodsModel mj_objectArrayWithKeyValuesArray:dict[@"hot_tg_list"]];
        [ws.tableView reloadData];
        
    } failure:^(NSInteger statusCode) {

    } finish:^{
        SVPDismiss
        [ws endRefreshNoMoreData];
    }];
    
    // 请求首页数据
    [NetRequestTool postUrl:@"index/queryShopIndex" parameters:@{} success:^(id responseObject) {
        NSDictionary *dict = [responseObject objectForKey:@"object"];
        NSMutableArray *array = [HomeIconModel mj_objectArrayWithKeyValuesArray:dict[@"classify_goods_list"]];
//        HomeIconModel *model = [[HomeIconModel alloc]init];
//        model.classify_name = @"本地生活";
//        model.classify_id = @"-1";
//        [array addObject:model];
        ws.header.iconArray = array;
    } failure:^(NSInteger statusCode) {

    } finish:^{
        SVPDismiss
        [ws endRefreshNoMoreData];
    }];
    
    // 请求热门商家
    [NetRequestTool postUrl:@"business/queryBusinessList" parameters:@{@"page":@"1", @"limit":@"10", @"is_hot":@"1"} success:^(id responseObject) {
        ws.hotBusinessArray = [BusinessModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]];
        [ws.tableView reloadData];
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        SVPDismiss
        [ws endRefreshNoMoreData];
    }];
    
}

- (void)HeaderViewBannerClickAtIndex:(NSInteger)index model:(BannerModel *)model
{
    NSLog(@"index = %zd", index);
    
    [CommonTTool jumpBannerWithModel:model];
    
    
}

- (void)HeaderViewIconClickAtIndex:(NSInteger)index model:(HomeIconModel *)model
{
    if ([model.classify_id isEqualToString:@"-1"]) {
        
        BenDiTableVC *vc = [[BenDiTableVC alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
        
        
    }else {
        
        MailClassVC *vc = [[MailClassVC alloc]init];
        vc.title = model.classify_name;
        vc.model = model;
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
        
    }
   
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        
        MailGoodsModel *model = self.hotTgArray[indexPath.row];
        AbulkCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AbulkCell"];
        tableView.rowHeight = 150;

        cell.model = model;
        return cell;
    }else if(indexPath.section == self.classSkuArray.count + 1){
        
        HomeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HomeCell"];
        cell.model = self.hotBusinessArray[indexPath.row];
        cell.discountBtn.hidden= YES;
        tableView.rowHeight = 135;
        return cell;
        
    }else {
        HomeClassSkuModel *model = self.classSkuArray[indexPath.section - 1];
        MailGoodsModel *goodsModel = model.sku_list[indexPath.row];
        MailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MailCell"];
        cell.model = goodsModel;
        tableView.rowHeight = 135;

        return cell;
    }
    
    
   
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{


    
    if (indexPath.section == 0) {
       MailGoodsModel *model = self.hotTgArray[indexPath.row];
       MailDetailsVC *vc = [[MailDetailsVC alloc] init];
       vc.goodsModel = model;
       vc.hidesBottomBarWhenPushed = YES;
       [self.navigationController pushViewController:vc animated:YES];
    }else if(indexPath.section == self.classSkuArray.count + 1){
        
        BusinessModel *model = self.hotBusinessArray[indexPath.row];
                    
        if (model.zxtg_state == 4) {
           PagingViewController *vc= [[PagingViewController alloc]init];
           vc.business_id = [NSString stringWithFormat:@"%ld",(long)model.business_id];
           [self.navigationController pushViewController:vc animated:YES];
        }else {
           BusinessDetailedVC *vc = [[BusinessDetailedVC alloc] init];
           vc.model = self.hotBusinessArray[indexPath.row];

           [self.navigationController pushViewController:vc animated:YES];
        }
        
    }else {
        HomeClassSkuModel *model = self.classSkuArray[indexPath.section - 1];
        MailDetailsVC *vc = [[MailDetailsVC alloc] init];
        vc.goodsModel = model.sku_list[indexPath.row];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
       
    }
    
}

- (HomeHeaderView *)header
{
    if (!_header) {
        _header = [[NSBundle mainBundle] loadNibNamed:@"HomeHeaderView" owner:self options:nil].lastObject;
        _header.frame = CGRectMake(0, 0, kScreenW, 515);
        _header.delegate = self;
        if (NeediPhoneXiPhoneXR) {
            _header.height += 22.0;
        }
    }
    return _header;
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return self.classSkuArray.count + 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section == 0) {
       
        return self.hotTgArray.count;
    }else if(section == self.classSkuArray.count + 1){
        if(self.hotBusinessArray.count > 3) {
            return 3;
        }
        return self.hotBusinessArray.count;
    }else {
        HomeClassSkuModel *model = self.classSkuArray[section - 1];
        return model.sku_list.count;
    }
    
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    HomeClassSkuHeadView *headView = [[NSBundle mainBundle] loadNibNamed:@"HomeClassSkuHeadView" owner:self options:nil].lastObject;
    if (section == 0) {
        headView.nameLabel.text = @"热门团购";
    }else if(section == self.classSkuArray.count + 1){
        
        headView.nameLabel.text = @"热门商家";
    }else {
        HomeClassSkuModel *model = self.classSkuArray[section - 1];
        headView.nameLabel.text = model.classify_name;
    }
                                    
    return headView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (section == 0 && self.hotTgArray.count == 0) {
        
        return 0;
    }
    
    return 38;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [CommonTTool isVipShow:YES];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    if (self.giftBagView.isShow) {
        self.giftBagView.hidden = NO;
    }
    
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

#pragma mark - 查询礼包
- (void)queryLbInfo {
    
    
    [NetRequestTool postUrl:@"index/queryLbInfo" parameters:@{} success:^(id responseObject) {
        
        self.LBMailModel = [MailGoodsDetailsModel mj_objectWithKeyValues:responseObject[@"object"]];
        
        if (![CommonTTool isStrNull:responseObject[@"object"]]) {
            if ([CommonTTool getCurrentVC] == self) {
                self.giftBagView = [GiftBagView showView];
                self.giftBagView.LBMailModel = self.LBMailModel;
            }
            
            
        }
        
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        
    }];
    
    
}

- (void)queryLbInfoList {
    
    
    [NetRequestTool postUrl:@"index/queryLbInfoList" parameters:@{} success:^(id responseObject) {
        
        NSArray *array = [MailGoodsDetailsModel mj_objectArrayWithKeyValuesArray:responseObject[@"object"]];
        
        if (![CommonTTool isStrNull:responseObject[@"object"]] && array.count != 0) {
            if ([CommonTTool getCurrentVC] == self) {
                self.giftBagView = [GiftBagView showView];
                self.giftBagView.LBarray = array;
            }
            
            
        }
        
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        
    }];
    
    
}




@end

