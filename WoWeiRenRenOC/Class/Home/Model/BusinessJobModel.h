//
//  BusinessJobModel.h
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/18.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BusinessJobModel : NSObject

/** 招聘名称 **/
@property (nonatomic, copy) NSString *recruit_name;

/** 招聘金额 **/
@property (nonatomic, copy) NSString *wages;

/** 招聘ID  **/
@property (nonatomic, strong) NSString *recruit_id;

/** 商家ID  **/
@property (nonatomic, assign) NSInteger business_id;

/** 审核状态 1待审核 2审核通过 3审核不通过  **/
@property (nonatomic, assign) NSInteger sh_state;

@property (nonatomic, copy) NSString *update_time;
@property (nonatomic, copy) NSString *create_time;


/** 审核备注  **/
@property (nonatomic, copy) NSString *sh_remark;

/** 描述  **/
@property (nonatomic, copy) NSString *descriptionText;

/** 是否删除 1正常 2已删除 **/
@property (nonatomic, assign) NSInteger is_delete;

/** 商家名称  **/
@property (nonatomic, copy) NSString *business_name;

/** 门头照  **/
@property (nonatomic, copy) NSString *head_path;

/** 商家电话  **/
@property (nonatomic, copy) NSString *busineee_tel;

/** 商家地址  **/
@property (nonatomic, copy) NSString *address;

/** 商家描述  **/
@property (nonatomic, copy) NSString *busineee_description;
@property (strong, nonatomic)NSArray *picture_list;



@end

NS_ASSUME_NONNULL_END
