//
//  BusinessModel.h
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/15.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BusinessModel : NSObject

/** 有效期  **/
@property (nonatomic, copy) NSString *effective_time;
/** 有效期  **/
@property (nonatomic, copy) NSString *business_name;
/** 地址  **/
@property (nonatomic, copy) NSString *address;
/** 创建时间  **/
@property (nonatomic, assign) NSInteger create_time;
/** 热门推荐时间  **/
@property (nonatomic, assign) NSInteger hot_time;
/**审核状态 1待审核 2审核通过 3审核不通过   **/
@property (nonatomic, assign) NSInteger sh_state;
/** 登陆密码  **/
@property (nonatomic, copy) NSString *password;
/** 修改时间  **/
@property (nonatomic, assign) NSInteger update_time;
/** 用户ID  **/
@property (nonatomic, assign) NSInteger tj_user_id;
/** 手机号  **/
@property (nonatomic, copy) NSString *phone;
/** 营业执照号  **/
@property (nonatomic, copy) NSString *business_license_no;
/** 是否热门 1热门 2不热门  **/
@property (nonatomic, assign) NSInteger is_hot;
/** 门头照  **/
@property (nonatomic, copy) NSString *head_path;
/** 审核备注  **/
@property (nonatomic, copy) NSString *sh_remark;
/** 分类ID  **/
@property (nonatomic, assign) NSInteger classification_id;
/** 折扣名称  **/
@property (nonatomic, copy) NSString *name;
/** 状态 1正常 2禁用  **/
@property (nonatomic, assign) NSInteger state;
/** 商家ID **/
@property (nonatomic, assign) NSInteger business_id;
/** 折扣值  **/
@property (nonatomic, copy) NSString *value;
/** 折扣ID  **/
@property (nonatomic, assign) NSInteger discount_id;
/** 商家名称   **/
@property (nonatomic, copy) NSString *busineee_tel;
/** 联系人  **/
@property (nonatomic, copy) NSString *contacts;
/** 营业执照图片  **/
@property (nonatomic, copy) NSString *business_license_path;
/** 描述  **/
@property (nonatomic, copy) NSString *descriptionText;
/** 门牌号  **/
@property (nonatomic, copy) NSString *house_number;

@property (strong, nonatomic)NSArray *picture_list;

@property (assign, nonatomic)int zxtg_state;
@property (nonatomic, copy) NSString *business_user_name;
@property (nonatomic, copy) NSString *user_type;
@property (nonatomic, copy) NSString *my_code;

@end

NS_ASSUME_NONNULL_END
