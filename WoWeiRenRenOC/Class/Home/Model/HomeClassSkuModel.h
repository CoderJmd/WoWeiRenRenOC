//
//  HomeClassSkuModel.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/27.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeClassSkuModel : BaseModel
@property (strong, nonatomic)NSString *classify_name;
@property (strong, nonatomic)NSString *classify_id;
@property (strong, nonatomic)NSArray *sku_list;

@end

NS_ASSUME_NONNULL_END
