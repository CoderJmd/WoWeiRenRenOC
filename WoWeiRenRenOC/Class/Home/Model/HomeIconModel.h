//
//  HomeIconModel.h
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/15.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeIconModel : NSObject

@property (nonatomic, copy) NSString *img_path;
@property (nonatomic, copy) NSString *classify_name;
@property (nonatomic, copy) NSString *classify_id;

@end

NS_ASSUME_NONNULL_END
