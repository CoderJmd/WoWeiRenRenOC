//
//  UserModel.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/23.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserModel : BaseModel
@property (strong, nonatomic)NSString *total_pro_number;
@property (strong, nonatomic)NSString *head_img_path;
@property (strong, nonatomic)NSString *education;
@property (strong, nonatomic)NSString *vip_end_time;
@property (strong, nonatomic)NSString *is_vip;
@property (strong, nonatomic)NSString *sex;
@property (strong, nonatomic)NSString *type;
@property (strong, nonatomic)NSString *access_token;
@property (assign, nonatomic)float balance;
@property (strong, nonatomic)NSString *phone;
@property (strong, nonatomic)NSString *idcard;
@property (strong, nonatomic)NSString *name;
@property (strong, nonatomic)NSString *pro_number;
@property (strong, nonatomic)NSString *email;
@property (strong, nonatomic)NSString *frist_number;
@property (strong, nonatomic)NSString *use_code;
@property (strong, nonatomic)NSString *my_code;
@property (strong, nonatomic)NSString *org_name;
@property (strong, nonatomic)NSString *address;
@property (strong, nonatomic)NSString *sh_state;
@property (strong, nonatomic)NSString *sh_remark;
@property (assign, nonatomic)int vip_type;
@property (strong, nonatomic)NSString *constellation;
@property (strong, nonatomic)NSString *birthday;
@property (strong, nonatomic)NSString *province;
@property (strong, nonatomic)NSString *city;
@property (strong, nonatomic)NSString *area;
@property (strong, nonatomic)NSString *alipay_account;
@property (strong, nonatomic)NSString *alipay_name;
@property (strong, nonatomic)NSString *user_id;


@end

NS_ASSUME_NONNULL_END
