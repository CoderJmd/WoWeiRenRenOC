//
//  ActivityModel.h
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/17.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ActivityModel : NSObject

/** 活动名称 **/
@property (nonatomic, copy) NSString *activity_name;

/** 活动ID  **/
@property (nonatomic, assign) NSInteger business_activity_id ;

/** 商家ID  **/
@property (nonatomic, assign) NSInteger business_id;

/** 审核状态 1待审核 2审核通过 3审核不通过  **/
@property (nonatomic, assign) NSInteger sh_state;

/** 修改时间  **/
@property (nonatomic, assign) NSInteger update_time;

/** 创建时间  **/
@property (nonatomic, assign) NSInteger create_time;

/** 审核备注  **/
@property (nonatomic, copy) NSString *sh_remark;

/** 图片  **/
@property (nonatomic, copy) NSString *img_path;

/** 折扣  **/
@property (nonatomic, copy) NSString *discount;

/** 描述  **/
@property (nonatomic, copy) NSString *descriptionText;

/** 是否删除 1正常 2已删除 **/
@property (nonatomic, assign) NSInteger is_delete;

@end

NS_ASSUME_NONNULL_END
