//
//  HomeClassSkuModel.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/27.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "HomeClassSkuModel.h"
#import "MailGoodsModel.h"

@implementation HomeClassSkuModel
+ (NSDictionary *)mj_objectClassInArray {
    
    return @{@"sku_list":[MailGoodsModel class]};
}

@end
