//
//  HomeHeaderView.m
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/14.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "HomeHeaderView.h"
#import <UIButton+WebCache.h>
#import "VerticalButton.h"
#import "MailYinKaVC.h"
#import "HomeSearchVC.h"

@interface HomeHeaderView () <SDCycleScrollViewDelegate>



@property (unsafe_unretained, nonatomic) IBOutlet UIView *colorBgView;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *bannerBgView;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *iconBgView;



@property (nonatomic, strong) SDCycleScrollView *cycleScrollView;

@end

@implementation HomeHeaderView

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    if (NeediPhoneXiPhoneXR) {
        self.topMargin.constant += 22;
        self.colorBgH.constant += 22;
    }
    
    // gradient
    CAGradientLayer *gl = [CAGradientLayer layer];
    gl.frame = CGRectMake(0, 0, kScreenW, self.colorBgH.constant);
    gl.startPoint = CGPointMake(0.5, 0);
    gl.endPoint = CGPointMake(0.5, 0.89);
    gl.colors = @[(__bridge id)[UIColor colorWithRed:206/255.0 green:160/255.0 blue:9/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:246/255.0 green:246/255.0 blue:246/255.0 alpha:1.0].CGColor];
    gl.locations = @[@(0), @(1.0f)];
    [self.colorBgView.layer addSublayer:gl];
    
   
    self.iconBgView.layer.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0].CGColor;
    self.iconBgView.layer.cornerRadius = 6;
    self.iconBgView.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.1].CGColor;
    self.iconBgView.layer.shadowOffset = CGSizeMake(0,0);
    self.iconBgView.layer.shadowOpacity = 1;
    self.iconBgView.layer.shadowRadius = 4;
    
    // bannerView
    self.cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(15, 0, KSCREEN_WIDTH-30, self.bannerBgView.height) delegate:self placeholderImage: kImageNamed(@"default_image_square")];
    self.cycleScrollView.layer.cornerRadius = 5;
    self.cycleScrollView.clipsToBounds = YES;
    self.cycleScrollView.autoScrollTimeInterval = 2;
    self.cycleScrollView.autoScroll = YES;
    self.cycleScrollView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
    self.cycleScrollView.showPageControl = YES;
    [self.bannerBgView addSubview:self.cycleScrollView];
    self.cycleScrollView.bannerImageViewContentMode = UIViewContentModeScaleToFill;
    
    self.cycleScrollView.localizationImageNamesGroup = @[@"default_image_square",@"default_image_square"];
    
    UITapGestureRecognizer *yinKaTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(yinKaTapClick)];
    
    [self.yinKaImgView addGestureRecognizer:yinKaTap];
    
    self.searchBgView.layer.cornerRadius = 15;
    self.searchBgView.clipsToBounds = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(jumpSearchVC)];
    [self.searchBgView addGestureRecognizer:tap];
    
    
}

- (void)jumpSearchVC {
    
    HomeSearchVC *homeVC = [[HomeSearchVC alloc]init];
    [[CommonTTool getCurrentVC].navigationController pushViewController:homeVC animated:YES];
    
}



- (void)yinKaTapClick {
    
    MailYinKaVC *vc = [[MailYinKaVC alloc]init];
    [[CommonTTool getCurrentVC].navigationController pushViewController:vc animated:YES];
    
}

- (void)setIconArray:(NSArray *)iconArray
{
    _iconArray = iconArray;
    
    [self.iconBgView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    NSInteger cNum = 4;
    CGFloat margin = 15.0;
    CGFloat iconW = (self.iconBgView.width - (cNum+1)*margin) / cNum;
    CGFloat iconH = 90.0;
    
    for (int i=0; i<iconArray.count; i++) {
        HomeIconModel *m = iconArray[i];
        
        VerticalButton *iconBtn = [VerticalButton buttonWithType:UIButtonTypeCustom];
        iconBtn.frame = CGRectMake(margin+(i%cNum)*(iconW+margin), (i/cNum)*iconH, iconW, iconH);
        iconBtn.titleLabel.font = [UIFont systemFontOfSize:12.0];
        iconBtn.tag = i;
        [iconBtn setTitleColor:kRGBColor(105, 105, 105) forState:UIControlStateNormal];
        [iconBtn setTitle:m.classify_name forState:UIControlStateNormal];
        if ([m.classify_id isEqualToString:@"-1"]) {
            [iconBtn setImage:[UIImage imageNamed:@"bendi"] forState:UIControlStateNormal];
        }else {
            [iconBtn sd_setImageWithURL:[NSURL URLWithString:m.img_path] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"jiazhenfuwu"]];
        }
//        iconBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;


        
        [iconBtn addTarget:self action:@selector(iconButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.iconBgView addSubview:iconBtn];
    }
}

- (void)setBannerArray:(NSArray *)bannerArray
{
    _bannerArray = bannerArray;
    
    NSMutableArray *pathArray = [NSMutableArray array];
    for (BannerModel *model in self.bannerArray) {
        if (model.img_path) {
            [pathArray addObject:model.img_path];
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        self.cycleScrollView.imageURLStringsGroup = pathArray;
    });
}



- (void)iconButtonClick:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(HeaderViewIconClickAtIndex:model:)]) {
        NSInteger index = sender.tag;
        [self.delegate HeaderViewIconClickAtIndex:index model:self.iconArray[index]];
    }
}

/** 点击图片回调 */
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index
{
    if ([self.delegate respondsToSelector:@selector(HeaderViewBannerClickAtIndex:model:)]) {
        [self.delegate HeaderViewBannerClickAtIndex:index model:self.bannerArray[index]];
    }
}

@end
