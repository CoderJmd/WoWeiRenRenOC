//
//  HomeCell.m
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/14.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "HomeCell.h"
#import "SDPhotoBrowser.h"

@interface HomeCell ()<SDPhotoBrowserDelegate>

@property (unsafe_unretained, nonatomic) IBOutlet UIView *bgView;

@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *imgView;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *titleLab;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *addressBtn;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *phoneBtn;


@end

@implementation HomeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.bgView.layer.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0].CGColor;
    self.bgView.layer.cornerRadius = 6;
    self.bgView.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.1].CGColor;
    self.bgView.layer.shadowOffset = CGSizeMake(0,0);
    self.bgView.layer.shadowOpacity = 1;
    self.bgView.layer.shadowRadius = 4;
    
    self.imgView.layer.cornerRadius = 3;
    self.imgView.clipsToBounds = YES;
    
    
}

- (void)setModel:(BusinessModel *)model
{
    _model = model;
    
    SD_SETIMAGE_PlaceHolder(self.imgView, model.head_path, @"default_image_square")
    
    self.titleLab.text = model.business_name;
    
    [self.addressBtn setTitle:[NSString stringWithFormat:@"  %@", model.address] forState:UIControlStateNormal];
    
    [self.phoneBtn setTitle:[NSString stringWithFormat:@"  %@", model.busineee_tel] forState:UIControlStateNormal];
    
    if ([model.value isEqualToString:@"0"]) {
//         [self.discountBtn setTitle:[NSString stringWithFormat:@"无折扣"] forState:UIControlStateNormal];
        self.discountBtn.hidden = YES;
    }else {
         [self.discountBtn setTitle:[NSString stringWithFormat:@"%@折", model.value] forState:UIControlStateNormal];
        self.discountBtn.hidden = NO;
    }
    
}




- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
