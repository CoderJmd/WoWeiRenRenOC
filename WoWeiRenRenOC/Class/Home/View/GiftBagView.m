//
//  GiftBagView.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/7/29.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "GiftBagView.h"
#import "UserShippingAddressVC.h"

@implementation GiftBagView
- (void)awakeFromNib {
    
    [super awakeFromNib];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cancelView)];
    [self addGestureRecognizer:tap];
    UITapGestureRecognizer *tapTwo = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap)];
    [self.bgView addGestureRecognizer:tapTwo];
    self.bgView.layer.cornerRadius = 5;
    self.bgView.clipsToBounds = YES;
    [self queryDefAddress];
    UITapGestureRecognizer *addressTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(selectorAddressModel)];
    [self.addressBgView addGestureRecognizer:addressTap];
    
}


+ (instancetype)showView
{
    UIWindow *window = UIApplication.sharedApplication.keyWindow;

    GiftBagView *view = [[NSBundle mainBundle] loadNibNamed:@"GiftBagView" owner:self options:nil].lastObject;
    view.frame = window.bounds;
    view.backgroundColor =  [[UIColor blackColor] colorWithAlphaComponent:0.7];
    
    [window addSubview:view];

    
    
    return view;
}




- (void)cancelView {
    
    [UIView animateWithDuration:0.5 animations:^{
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)tap {
    
    
}


- (void)queryDefAddress {
    
    [NetRequestTool postUrl:@"address/queryDefAddress" parameters:@{} success:^(id responseObject) {
        
        self.addressModel = [UserShipingAddressModel mj_objectWithKeyValues:responseObject[@"object"]];
        
        
        
    } failure:^(NSInteger statusCode) {
       
    } finish:^{
        
    }];
    
}

- (void)setAddressModel:(UserShipingAddressModel *)addressModel{
    
    _addressModel = addressModel;
    
    if ([CommonTTool isStrNull:addressModel.address_id]) {
        
        self.nameLabel.hidden = YES;
        self.phoneLabel.hidden = YES;
        self.detailsLabel.hidden = YES;
        self.nullAddressLabel.hidden = NO;
        
    }else {
        
        self.nameLabel.hidden = NO;
        self.phoneLabel.hidden = NO;
        self.detailsLabel.hidden = NO;
        self.nullAddressLabel.hidden = YES;
        
        self.nameLabel.text = addressModel.name;
        self.phoneLabel.text = addressModel.phone;
        self.detailsLabel.text = [NSString stringWithFormat:@"%@%@%@%@",addressModel.province,addressModel.city,addressModel.area,addressModel.address];
        
    }
    
    
}

-(void)setLBMailModel:(MailGoodsDetailsModel *)LBMailModel{
    
    _LBMailModel = LBMailModel;
    SD_SETIMAGE_PlaceHolder(self.imfView, LBMailModel.img_path, @"default_image_square")

}


- (IBAction)cancelBtnClick:(id)sender {
    [self cancelView];
}
- (IBAction)receiveBtnClick:(id)sender {
    
    if ([CommonTTool isStrNull:self.addressModel.address_id]) {
        SVP(@"请选择收货地址");
        return;
    }
    MailGoodsDetailsModel *model = self.LBarray[self.selectIndex];
    
    [NetRequestTool postUrl:@"skuOrder/saveLbOrder" parameters:@{@"address_id":self.addressModel.address_id,@"sku_id":model.sku_id} success:^(id responseObject) {
        [self cancelView];
        SVP(@"礼包领取成功");
    } failure:^(NSInteger statusCode) {
        
    } finish:^{
        
    }];
}

#pragma mark - 选择地址
- (void)selectorAddressModel {
    
    self.isShow = YES;
    self.hidden = YES;

    UserShippingAddressVC *vc = [[UserShippingAddressVC alloc]init];
    vc.selectorAddressModel = ^(UserShipingAddressModel *model) {
        

        self.addressModel = model;

    };
    
    [[CommonTTool getCurrentVC].navigationController pushViewController:vc animated:YES];
    
}


- (void)setLBarray:(NSArray *)LBarray {
    
    _LBarray = LBarray;
    for (int i = 0; i< LBarray.count; i++) {
        MailGoodsDetailsModel *model = LBarray[i];
        if (i  == 0) {
            
            SD_SETIMAGE_PlaceHolder(self.onImageView, model.img_path, @"default_image_square")
            self.oneNameView.text = model.sku_name;
            self.oneContentView.text = model.att_title;
            self.oneSelectButton.selected = YES;
            self.bg1.hidden = NO;
        }
        
        if (i  == 1) {
            
            SD_SETIMAGE_PlaceHolder(self.twoImageView, model.img_path, @"default_image_square")
            self.twoNameView.text = model.sku_name;
            self.twoContentView.text = model.att_title;
            self.bg2.hidden = NO;

        }
        
        if (i  == 2) {
            
            SD_SETIMAGE_PlaceHolder(self.threeImageView, model.img_path, @"default_image_square")
            self.threeNameView.text = model.sku_name;
            self.threeContentView.text = model.att_title;
            self.bg3.hidden = NO;

        }
        
        
        
    }
    
    [self.oneSelectButton addTarget:self action:@selector(selectedLbClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.twoSelectButton addTarget:self action:@selector(selectedLbClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.threeSelectButton addTarget:self action:@selector(selectedLbClick:) forControlEvents:UIControlEventTouchUpInside];
    self.buttonArray = @[self.oneSelectButton,self.twoSelectButton,self.threeSelectButton];
    
}

- (void)selectedLbClick:(UIButton *)btn {
    
    self.selectIndex = btn.tag;
    for (UIButton *a in self.buttonArray) {
        a.selected = NO;
    }
    btn.selected  = YES;
    
    
}


@end
