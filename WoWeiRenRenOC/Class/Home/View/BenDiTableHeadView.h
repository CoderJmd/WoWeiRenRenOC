//
//  BenDiTableHeadView.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/7/21.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeIconModel.h"
#import "BannerModel.h"

NS_ASSUME_NONNULL_BEGIN
@protocol BenDiTableHeadViewDelegata <NSObject>

@optional
- (void)HeaderViewBannerClickAtIndex:(NSInteger)index model:(BannerModel *_Nullable)model;
- (void)HeaderViewIconClickAtIndex:(NSInteger)index model:(HomeIconModel *_Nullable)model;

@end
@interface BenDiTableHeadView : UIView
@property (nonatomic, strong) NSArray *iconArray;
@property (nonatomic, strong) NSArray *bannerArray;
@property (weak, nonatomic) IBOutlet UIView *searcheBgView;
@property (weak, nonatomic) IBOutlet UIImageView *searchImgView;
@property (weak, nonatomic) IBOutlet UILabel *searchLabel;
@property (weak, nonatomic) IBOutlet UITextField *searchTF;

@property (unsafe_unretained, nonatomic) IBOutlet NSLayoutConstraint *colorBgH;
@property (nonatomic, weak) id<BenDiTableHeadViewDelegata> delegate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topMargin;

@end

NS_ASSUME_NONNULL_END
