//
//  NewBusinessDetailedHeaderView.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/16.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface NewBusinessDetailedHeaderView : UIView
@property (nonatomic, strong) BusinessModel *model;
@property (weak, nonatomic) IBOutlet UIView *bannerView;

@property (nonatomic, assign) CGFloat headerH;
@end

NS_ASSUME_NONNULL_END
