//
//  BusinessDetailedCell.m
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/15.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BusinessDetailedCell.h"
#import "SDPhotoBrowser.h"
@interface BusinessDetailedCell()<SDPhotoBrowserDelegate>

@end;
@implementation BusinessDetailedCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.bgView.layer.backgroundColor = [UIColor colorWithRed:248/255.0 green:248/255.0 blue:248/255.0 alpha:1.0].CGColor;
    self.bgView.layer.cornerRadius = 6;
    self.imgView.layer.cornerRadius = 6;
    self.imgView.clipsToBounds = YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(ActivityModel *)model {
    
    _model = model;
    self.imgView.userInteractionEnabled = YES;
       UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(selectedImageCiew)];
    [self.imgView addGestureRecognizer:tap];
    
}

- (void)selectedImageCiew {
    
    SDPhotoBrowser *browser = [[SDPhotoBrowser alloc] init];
    browser.currentImageIndex = 0;

    browser.imageCount = 1;
    browser.delegate = self;
    [browser show];
    
}


#pragma mark - SDPhotoBrowserDelegate
- (NSURL *)photoBrowser:(SDPhotoBrowser *)browser highQualityImageURLForIndex:(NSInteger)index {
    
    return [NSURL URLWithString:self.model.img_path];
}


@end
