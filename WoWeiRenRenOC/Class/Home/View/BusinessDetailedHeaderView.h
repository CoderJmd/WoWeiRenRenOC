//
//  BusinessDetailedHeaderView.h
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/15.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BusinessDetailedHeaderView : UIView

@property (nonatomic, strong) BusinessModel *model;

@property (nonatomic, assign) CGFloat headerH;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomH;

@end

NS_ASSUME_NONNULL_END
