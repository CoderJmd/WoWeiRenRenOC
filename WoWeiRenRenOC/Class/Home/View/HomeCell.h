//
//  HomeCell.h
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/14.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeCell : UITableViewCell

@property (nonatomic, strong) BusinessModel *model;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *discountBtn;
@end

NS_ASSUME_NONNULL_END
