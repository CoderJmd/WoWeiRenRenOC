//
//  GiftBagView.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/7/29.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserShipingAddressModel.h"
#import "MailGoodsDetailsModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface GiftBagView : UIView
@property (strong, nonatomic)UIWindow *window;
@property (weak, nonatomic) IBOutlet UIView *bgView;

@property (weak, nonatomic) IBOutlet UIImageView *imfView;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;

@property (weak, nonatomic) IBOutlet UIButton *receiveBtn;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;

@property (weak, nonatomic) IBOutlet UILabel *detailsLabel;
@property (weak, nonatomic) IBOutlet UILabel *nullAddressLabel;
@property (strong, nonatomic)UserShipingAddressModel *addressModel;
@property (strong, nonatomic)MailGoodsDetailsModel *LBMailModel;
+ (instancetype)showView;
- (void)cancelView;
@property (weak, nonatomic) IBOutlet UIView *addressBgView;
@property (assign, nonatomic)BOOL isShow;
@property (strong, nonatomic)NSArray *LBarray;
@property (weak, nonatomic) IBOutlet UIImageView *onImageView;
@property (weak, nonatomic) IBOutlet UILabel *oneNameView;
@property (weak, nonatomic) IBOutlet UILabel *oneContentView;

@property (weak, nonatomic) IBOutlet UIButton *oneSelectButton;

@property (weak, nonatomic) IBOutlet UIImageView *twoImageView;
@property (weak, nonatomic) IBOutlet UILabel *twoNameView;
@property (weak, nonatomic) IBOutlet UILabel *twoContentView;

@property (weak, nonatomic) IBOutlet UIButton *twoSelectButton;


@property (weak, nonatomic) IBOutlet UIImageView *threeImageView;
@property (weak, nonatomic) IBOutlet UILabel *threeNameView;
@property (weak, nonatomic) IBOutlet UILabel *threeContentView;

@property (weak, nonatomic) IBOutlet UIButton *threeSelectButton;
@property (strong, nonatomic)NSArray *buttonArray;
@property (assign, nonatomic)int selectIndex;
@property (weak, nonatomic) IBOutlet UIView *bg1;
@property (weak, nonatomic) IBOutlet UIView *bg2;
@property (weak, nonatomic) IBOutlet UIView *bg3;

@end

NS_ASSUME_NONNULL_END
