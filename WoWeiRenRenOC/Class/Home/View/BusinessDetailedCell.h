//
//  BusinessDetailedCell.h
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/15.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActivityModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BusinessDetailedCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (strong, nonatomic)ActivityModel *model;


@end

NS_ASSUME_NONNULL_END
