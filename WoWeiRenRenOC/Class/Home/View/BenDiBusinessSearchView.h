//
//  BenDiBusinessSearchView.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/8/20.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BenDiBusinessSearchView : UIView
@property (weak, nonatomic) IBOutlet UITextField *searchTF;

@end

NS_ASSUME_NONNULL_END
