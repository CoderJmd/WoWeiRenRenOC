//
//  BusinessDetailedHeaderView.m
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/15.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "BusinessDetailedHeaderView.h"

@interface BusinessDetailedHeaderView ()
@property (weak, nonatomic) IBOutlet UIImageView *topImag;

@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UIButton *disCountBtn;
@property (weak, nonatomic) IBOutlet UIButton *addressBtn;
@property (weak, nonatomic) IBOutlet UIButton *phoneBtn;
@property (weak, nonatomic) IBOutlet UILabel *desLab;

@end

@implementation BusinessDetailedHeaderView

- (void)setModel:(BusinessModel *)model
{
    _model = model;
    
    SD_SETIMAGE_PlaceHolder(self.topImag, model.head_path, @"default_image_square")
    
    self.nameLab.text = model.business_name;
    
    [self.addressBtn setTitle:[NSString stringWithFormat:@"  %@", model.address] forState:UIControlStateNormal];
    
    [self.phoneBtn setTitle:[NSString stringWithFormat:@"  %@", model.busineee_tel] forState:UIControlStateNormal];
    if ([model.value isEqualToString:@"0"]) {
        self.disCountBtn.hidden = YES;
    }else {
        [self.disCountBtn setTitle:[NSString stringWithFormat:@"%@折", model.value] forState:UIControlStateNormal];
    }
    

    
    self.desLab.text = model.descriptionText;
    
    CGFloat textH = [CommonTTool getLabelHeightWithText:model.descriptionText width:(kScreenW-30) font:[UIFont systemFontOfSize:14.0]];
    self.headerH = textH + (kScreenW * 0.75) + 160;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
