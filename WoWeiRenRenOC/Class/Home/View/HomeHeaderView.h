//
//  HomeHeaderView.h
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/14.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeIconModel.h"
#import "BannerModel.h"

@protocol HomeHeaderViewDelegata <NSObject>

@optional
- (void)HeaderViewBannerClickAtIndex:(NSInteger)index model:(BannerModel *_Nullable)model;
- (void)HeaderViewIconClickAtIndex:(NSInteger)index model:(HomeIconModel *_Nullable)model;
@end

NS_ASSUME_NONNULL_BEGIN

@interface HomeHeaderView : UIView

@property (nonatomic, strong) NSArray *iconArray;
@property (nonatomic, strong) NSArray *bannerArray;

@property (unsafe_unretained, nonatomic) IBOutlet NSLayoutConstraint *topMargin;
@property (unsafe_unretained, nonatomic) IBOutlet NSLayoutConstraint *colorBgH;
@property (nonatomic, weak) id<HomeHeaderViewDelegata> delegate;
@property (weak, nonatomic) IBOutlet UIImageView *yinKaImgView;
@property (weak, nonatomic) IBOutlet UIView *searchBgView;

@end

NS_ASSUME_NONNULL_END
