//
//  NewBusinessDetailedHeaderView.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/6/16.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "NewBusinessDetailedHeaderView.h"
#import "SDCycleScrollView.h"
@interface NewBusinessDetailedHeaderView ()
@property (weak, nonatomic) IBOutlet UIImageView *topImag;

@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UIButton *disCountBtn;
@property (weak, nonatomic) IBOutlet UIButton *addressBtn;
@property (weak, nonatomic) IBOutlet UIButton *phoneBtn;
@property (weak, nonatomic) IBOutlet UILabel *desLab;
@property (nonatomic, strong) SDCycleScrollView *cycleScrollView;
@end

@implementation NewBusinessDetailedHeaderView

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    // bannerView
    self.cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0,kScreenW,(kScreenW*0.75)) delegate:self placeholderImage: kImageNamed(@"default_image_square")];
    self.cycleScrollView.layer.cornerRadius = 5;
    self.cycleScrollView.clipsToBounds = YES;
    self.cycleScrollView.autoScrollTimeInterval = 2;
    self.cycleScrollView.autoScroll = YES;
    self.cycleScrollView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
    self.cycleScrollView.showPageControl = YES;
    [self.bannerView addSubview:self.cycleScrollView];

    self.cycleScrollView.localizationImageNamesGroup = @[@"default_image_square",@"default_image_square"];
    
}

- (void)setModel:(BusinessModel *)model
{
    _model = model;
    
    SD_SETIMAGE_PlaceHolder(self.topImag, model.head_path, @"default_image_square")
    
    self.nameLab.text = model.business_name;
    
    [self.addressBtn setTitle:[NSString stringWithFormat:@"  %@", model.address] forState:UIControlStateNormal];
    
    [self.phoneBtn setTitle:[NSString stringWithFormat:@"  %@", model.busineee_tel] forState:UIControlStateNormal];
    
    [self.disCountBtn setTitle:[NSString stringWithFormat:@"%@折", model.value] forState:UIControlStateNormal];
    
    self.desLab.text = model.descriptionText;
    
    CGFloat textH = [CommonTTool getLabelHeightWithText:model.descriptionText width:(kScreenW-30) font:[UIFont systemFontOfSize:14.0]];
    self.headerH = textH + (kScreenW*0.75) + 130;
    NSMutableArray *pathArray = [NSMutableArray array];
    for (NSDictionary *dic in model.picture_list) {
        if (![CommonTTool isStrNull:dic[@"path"]]) {
            [pathArray addObject:dic[@"path"]];

        }
    }
    if (pathArray.count == 0) {
        [pathArray addObject:model.head_path];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        self.cycleScrollView.imageURLStringsGroup = pathArray;
    });
    
    
}

/** 点击图片回调 */
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index
{
    
}
@end
