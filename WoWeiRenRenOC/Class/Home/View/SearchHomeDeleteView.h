//
//  SearchHomeDeleteView.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/7/8.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SearchHomeDeleteView : UIView
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;

@end

NS_ASSUME_NONNULL_END
