//
//  AppDelegate.h
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/3/13.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;

@end

