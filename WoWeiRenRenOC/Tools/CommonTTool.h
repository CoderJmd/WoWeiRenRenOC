//
//  CommonTTool.h
//  OCDRemind
//
//  Created by 姜孟东 on 2017/4/7.
//  Copyright © 2017年 Znzkj. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "BannerModel.h"

@interface CommonTTool : NSObject



#pragma mark - 计算label高度
+ (CGFloat)getLabelHeightWithText:(NSString *)text width:(CGFloat)width font: (UIFont *)font;


#pragma mark - 计算label宽度
+ (CGFloat)getWidthWithText:(NSString *)text height:(CGFloat)height font:(CGFloat)font;

/**
 验证手机号码是否合法

 @param mobile 手机号
 @return BOOL值
 */
+ (BOOL)valiMobile:(NSString *)mobile;


#pragma mark - 判断固定电话是否合法
+ (BOOL)validateContactNumber:(NSString *)mobileNum;


/**
 验证邮箱是否合法

 @param email 邮箱
 @return 返回值
 */
+ (BOOL)validateEmail:(NSString *)email;

//正则去除网络标签
+ (BOOL)validateHttpwithString:(NSString *)string;


/**
 验证身份证是否正确
 */
+ (BOOL)chk18PaperId:(NSString *)sPaperId;


/**
 快速设置本地缓存

 @param obj Obj
 @param key Key
 */
+ (void)setUserDefaults:(id)obj key:(NSString *)key;


/**
 快速获取本地缓存

 @param key key
 @return obj
 */
+ (id)getUserDefaults:(NSString *)key;


/**
 收起键盘
 */
+ (void)dismisKeyBoard;


/**
 改变站位字体颜色

 @param TF 需要改变的TF
 @param color 颜色
 */
+ (void)changeTextFiledPPaceholderTF:(UITextField *)TF Text:(NSString *)text color:(UIColor *)color font:(UIFont *)font;

/**
 图片切圆

 @param oldImage 以前的图片
 @return 返回圆形图片
 */
+ (UIImage *)circleImage:(UIImage *)oldImage;

/**
 获得当前显示的ViewController
 
 @return VC
 */
+ (UIViewController *)getCurrentVC;
+ (BOOL)checkEmptyString:(NSString *)string;

/**
 处理时间

 @param str 需要处理的时间
 @return 返回经过处理的时间
 */
+ (NSString *)compareCurrentTime:(NSString *)str;


/**
 获取时间戳

 @return 时间戳
 */
+ (NSString *)getTimestempString;

#pragma mark - 时间戳 转 时间
+ (NSString *)time_timestamp:(NSString *)timestampstr andDateFormat:(NSString *)dateFormat;
/**
 获得价格的字符串

 @param Price 价格
 @return zifc
 */
+ (NSString *)getPriceDouble:(int )Price;


/**
 获得视频第一帧

 @param path URL
 @return 图片
 */
+ (UIImage *)getVideoPreViewImage:(NSURL *)path;


/**
 判断是否全是数字

 @return 数字
 */
+ (BOOL)isPureInt:(NSString *)string;


/**
 * 加文字随意@param logoImage 需要加文字的图片@param watemarkText 文字描述@returns 加好文字的图片
 */
+ (UIImage *)addWatemarkTextAfteriOS7_WithLogoImage:(UIImage *)logoImage watemarkText:(NSString *)watemarkText;


/**
 图片压缩

 @param lodImage 需要压缩是图片
 @return 返回的图片
 */
+ (UIImage *)imageCompression:(UIImage *)lodImage;


/**
 清除视频缓存
 */
+ (void)removeSandboxFile;


#pragma mark -  判断string是否为空
+ (BOOL)isBlankString:(NSString*)string;


/**
 * 去除两端空格
 */
+ (NSString *)trimmingString:(NSString *)string;


/**
 首次启动页

 @return 返回判断
 */
+ (BOOL)isShowUserGuideLoad;


/**
 只获取日期

 @param oldDate 老日期
 @return 新日期
 */
+ (NSString *)getDateNoHours:(NSString *)oldDate;


/**
 距离字符串处理

 @param mi 距离
 @return 字符串
 */
+ (NSString *)distanceWithString:(double)mi;


#pragma mark - 金额正则表达式
+ (BOOL)validateMoney:(NSString *)money;


#pragma mark - 将某个时间转化成 时间戳
+ (int)timeSwitchTimestamp:(NSString *)formatTime andFormatter:(NSString *)format;


#pragma mark -  判断对象是否是空
+ (BOOL)isStrNull:(id)str;


#pragma mark - 判断是否是有效的中文名
+ (BOOL)isVaildRealName:(NSString *)realName;


#pragma mark - 行间距
+ (NSMutableAttributedString *)replaceStringToAttributedString:(NSString *)replaceS initWithWidth:(CGFloat)The_Width andLineSpacing:(CGFloat)LineSpacing;


#pragma mark - 不可变字符串宽高
+ (CGSize)sizeWithString:(NSString *)string font:(UIFont *)font;

/*根据传过来的文字内容、字体大小、宽度和最大尺寸动态计算文字所占用的size
 * text 文本内容
 * fontSize 字体大小
 * maxSize  size（宽度，1000）
 * return  size （计算的size）
 */
+ (CGSize)labelAutoCalculateRectWith:(NSString*)text Font:(UIFont *)font MaxSize:(CGSize)maxSize;

#pragma mark - 银行卡号正则表达式
+ (BOOL)isBankCard:(NSString *)cardNumber;

#pragma mark - 判断是否为网址
+ (BOOL)isUrlAddress:(NSString*)url;

#pragma mark - 每隔4个长度加一个空格
+ (NSString *)formatterBankCardNum:(NSString *)string;


#pragma mark - 车牌号格式校验(粤A8888澳)
+ (BOOL)checkCarID:(NSString *)carID;

#pragma mark - 转换时间格式为2018/12/05
+ (NSString *)changeDateFormatter:(NSString *)time;


#pragma mark - 转换时间格式为后台yyyy-MM-dd HH:mm:ss
+ (NSString *)changeBackDateFormatter:(NSString *)time;

#pragma mark - 如果用户输入没有http等抬头，自动替补全
+ (BOOL)isUrlWithUrl:(NSString *)urlStr;

#pragma mark - 通过银行卡号判断开户行
+ (NSString *)returnBankName:(NSString *)idCard;

#pragma mark - 根据出生日期返回年龄的方法
+ (NSString *)dateToOld:(NSDate *)bornDate;

#pragma mark - 根据出生日期返回详细的年龄(精确到天)
+ (NSString *)dateToDetailOld:(NSDate *)bornDate;

#pragma mark - 获取当前的时间
+ (NSDate *)getCurrentTimesWithFormat:(NSString *)format;

#pragma mark - 获取当前的时间字符串
    + (NSString *)getCurrentTimesStrWithFormat:(NSString *)format;

#pragma mark - 获取当前时间戳有两种方法(以秒为单位)
+ (NSString *)getNowTimeTimestamp;

+ (NSString *)getNowTimeTimestamp2;

#pragma mark - 获取当前时间戳  （以毫秒为单位）
+ (NSString *)getNowTimeTimestamp3;

#pragma mark - 截取两个字符串的中间字符串
+ (NSString *)getMiddleString:(NSString *)jieshu beginString:(NSString *)bString endString:(NSString *)eString;

#pragma mark - 时间大小比较
+ (int)compareOneDay:(NSDate *)oneDay withAnotherDay:(NSDate *)anotherDay andDateForm:(NSString *)dateForm;

#pragma mark - 字符串转时间
+ (NSDate *)nsstringConversionNSDate:(NSString *)dateStr;

/**
 获取App名称
 
 @return App名称
 */
+ (NSString *)getMyApplicationName;

#pragma mark - 全局替换图片
+ (void)setImageWithURL:(UIImageView *)imageView url:(NSString *)url placeholderImage:(NSString *)placeholderImage;

#pragma mark - 根据活跃度获取皇冠等级
+ (NSDictionary *)getHgRankAndImageWithActivityNum:(NSString *)activityNum isy:(BOOL)y;

#pragma mark - 处理评论和阅读量
+ (NSString *)commentNumWithHandle:(NSString *)numStr;


+ (void)serviceCallPhone;


+ (NSString *)getPrice:(float)str;

+ (NSString *)getGoodsSpecifications:(NSString *)str;
/**
*  获取当天的字符串
*  @return 格式为年-月-日 时分秒
*/
+ (NSString *)getCurrentTimeyyyymmdd;

/**
 *  获取时间差值  截止时间-当前时间
 *  nowDateStr : 当前时间
 *  deadlineStr : 截止时间
 *  @return 时间戳差值
 */
+ (NSInteger)getDateDifferenceWithNowDateStr:(NSString*)nowDateStr deadlineStr:(NSString*)deadlineStr;

//判断是否登录
+ (BOOL)isJumpLoginVC:(BOOL)isShowAlert;

//判断是否是银卡
+ (void)isYinKaGoodsBuy;

+ (BOOL)isVipShow:(BOOL)isShowAlert;

//Bnner跳转
+ (void)jumpBannerWithModel:(BannerModel *)model;

+ (NSInteger)compareVersion2:(NSString *)v1 to:(NSString *)v2;

+ (void)queryNewVersionInfo;
@end
