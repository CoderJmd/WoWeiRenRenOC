//
//  NSDictionary+NoNull.m
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/18.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "NSDictionary+NoNull.h"

@implementation NSDictionary (NoNull)

- (NSDictionary *)deleteNull
{
    NSMutableDictionary *mutableDic = [[NSMutableDictionary alloc] init];
    NSArray *keys = self.allKeys;
    for (NSString *keyStr in keys) {
        if ([[self objectForKey:keyStr] isEqual:[NSNull null]]) {
            [mutableDic setObject:@"" forKey:keyStr];
        }
        else{
            [mutableDic setObject:[self objectForKey:keyStr] forKey:keyStr];
        }
    }
    return mutableDic;
}

@end
