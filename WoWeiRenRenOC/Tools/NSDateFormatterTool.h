//
//  NSDateFormatterTool.h
//  SchoolApp
//
//  Created by 姜孟东 on 2019/7/5.
//  Copyright © 2019 znz. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSDateFormatterTool : NSObject
@property (strong, nonatomic)NSDateFormatter *formatter;
+ (instancetype)shareManager;
@end

NS_ASSUME_NONNULL_END
