//
//  NSDateFormatterTool.m
//  SchoolApp
//
//  Created by 姜孟东 on 2019/7/5.
//  Copyright © 2019 znz. All rights reserved.
//

#import "NSDateFormatterTool.h"
static NSDateFormatterTool *manager = nil;



@implementation NSDateFormatterTool
+ (instancetype)shareManager{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[self alloc] init];
    });
    return manager;
}


- (NSDateFormatter *)formatter {
    
    if (_formatter == nil) {
        
        _formatter = [[NSDateFormatter alloc] init];
        
    }
    
    return _formatter;
}


@end
