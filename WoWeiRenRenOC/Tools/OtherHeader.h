//
//  OtherHeader.h
//  XiaoMiFeng
//
//  Created by 杨涛 on 17/8/31.
//  Copyright © 2017年 杨涛. All rights reserved.
//

#ifndef OtherHeader_h
#define OtherHeader_h

#ifdef __OBJC__
#import <UIKit/UIKit.h>      // 这个是你预编译的文件举例。
#endif


#pragma mark - 屏幕高度/宽度
#define kScreenH [UIScreen mainScreen].bounds.size.height
#define kScreenW [UIScreen mainScreen].bounds.size.width
#pragma mark - TabBar选项卡高度
#define TabBarHeight (NeediPhoneXiPhoneXR ? 83.0 : 49.0)
#pragma mark - 状态栏/导航条高度
#define StatusViewHeight (NeediPhoneXiPhoneXR ? 44.0 : 20.0)
#define NavBarHeight (44.0+StatusViewHeight)
#define NeedStatusViewHeight (NeediPhoneXiPhoneXR ? 24.0 : 0)
//默认矩形
#define kDefaultRect CGRectMake(0, NavBarHeight, ScreenWidth, ScreenHeight-NavBarHeight)


#define kFont(font) [UIFont systemFontOfSize:font] // 字体
// 自定义字体名称
#define kFontNamePingFangSC(weightName) [NSString stringWithFormat:@"PingFang-SC-%@",(weightName)]
#define kFontNamePingFangSCRegular  kFontNamePingFangSC(@"Regular")
#define kFontNamePingFangSCMedium   kFontNamePingFangSC(@"Medium")
#define kFontNamePingFangSCSemibold kFontNamePingFangSC(@"Semibold")
#define kPRFont(font) [UIFont fontWithName:@"PingFangSC-Regular" size:font]
#define kPMFont(font) [UIFont fontWithName:@"PingFangSC-Medium" size:font]
#define kPLFont(font) [UIFont fontWithName:@"PingFangSC-Light" size:font]
#define kPSFont(font) [UIFont fontWithName:@"PingFangSC-Semibold" size:font]
#define KSCREEN_WIDTH ([[UIScreen mainScreen]bounds].size.width) // 屏幕宽
#define KSCREEN_HEIGHT ([[UIScreen mainScreen]bounds].size.height) // 屏幕高
#define RATIO_WIDTH ([[UIScreen mainScreen]bounds].size.width) / 375.0 // 屏幕宽比
#define RATIO_HEIGHT ([[UIScreen mainScreen]bounds].size.height) / 667.0 // 屏幕高比
#define RATIOFont_HEIGHT ([[UIScreen mainScreen]bounds].size.height) / 896.0 // 屏幕高比

#define kImageNamed(_pointer) [UIImage imageNamed:_pointer] // 定义UIImage对象
#define UserDefaults [UserDefault alloc] // UserDefault

#define ZNZDefaults  [NSUserDefaults standardUserDefaults]

#define ZNZNotificationCenter   [NSNotificationCenter defaultCenter]

/**设置frame*/
#define kFrame(x,y,w,h) CGRectMake(x, y, w, h)

#define tagValue 101

#define iPhone5 ([UIScreen mainScreen].bounds.size.height == 568 ? YES : NO)
#define iPhone6 ([UIScreen mainScreen].bounds.size.height == 667 ? YES : NO)
#define iPhone6_plus ([UIScreen mainScreen].bounds.size.height == 736 ? YES : NO)
#define NeediPhoneXiPhoneXR (iPhoneX||iPhoneXR)
#define iPhoneX ([UIScreen mainScreen].bounds.size.height == 812 ? YES : NO)
#define iPhoneXR ([UIScreen mainScreen].bounds.size.height == 896 ? YES : NO)
#define systemVersion11 ([[UIDevice currentDevice].systemVersion floatValue] >= 11.0 ? YES : NO)
#define systemVersion10 ([[UIDevice currentDevice].systemVersion floatValue] >= 10.0 ? YES : NO)
#define equalsystemVersion10 ([[UIDevice currentDevice].systemVersion intValue] == 10.0 ? YES : NO)
#define belowsystemVersion11 ([[UIDevice currentDevice].systemVersion floatValue] < 11.0 ? YES : NO)
#define systemVersion9     ([[UIDevice currentDevice].systemVersion floatValue]<10.0 ? YES : NO)


// 常量
#define RXInternetLoading @"请求中..."
#define RXInternetError @"服务器异常"

#define kUserMerchantsInfo @"kUserMerchantsDict_kUserMerchantsInfo"
#define kUserInfo @"kUser_kUserInfo"
#define kisLogin @"kisLogin"
//* SVHUD 提示
#define SVP(msg) [SVProgressHUD showInfoWithStatus:msg];
#define SVPShowInternetWaiting [SVProgressHUD showWithStatus:RXInternetLoading];
#define SVPDismiss [SVProgressHUD dismiss];


//// 开发的时候打印，但是发布的时候不打印的NSLog
//#ifdef DEBUG
//#define NSLog(...) NSLog(@"%s,%s 第%d行 \n %@\n\n",__FILE__,__func__,__LINE__,[NSString stringWithFormat:__VA_ARGS__])
//#else
//#define NSLog(...)
//#endif

#ifdef DEBUG

#define NSLog(FORMAT, ...) fprintf(stderr, "%s:%d\t%s\n", [[[NSString stringWithUTF8String: __FILE__] lastPathComponent] UTF8String], __LINE__, [[NSString stringWithFormat: FORMAT, ## __VA_ARGS__] UTF8String]);

#else

#define NSLog(FORMAT, ...) nil

#endif

// sd设置imgView图片
//#define KSD_SetImgViewImg_PlaceHolder(imgView, imgUrl, imgPlaceHolder) [imgView sd_setImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:[UIImage imageNamed:imgPlaceHolder]];
#define KSD_SetImgViewImg_PlaceHolder(imgView, imgUrl, imgPlaceHolder) [CommonTTool setImageWithURL:imgView url:imgUrl placeholderImage:imgPlaceHolder];


// sd设置btn图片
#define KSD_SetBtnImge_PlaceHolder(btn, imgUrl, imgPlaceHolder) [btn sd_setImageWithURL:[NSURL URLWithString:imgUrl] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:imgPlaceHolder]];

// 新版导航栏高度
#define NEW_NavHeight ([[UIApplication sharedApplication] statusBarFrame].size.height + 44)

// 新版状态栏高度
#define NEW_StatusBarHeight [[UIApplication sharedApplication] statusBarFrame].size.height

//新版tabbar高度 适配iphone X XS XR XSMax等所有机型
#define New_TabbarHeight ([[UIApplication sharedApplication] statusBarFrame].size.height>20?83:49)


// 强引用和弱引用的用法
#ifndef weakify
#if DEBUG
#if __has_feature(objc_arc)
#define weakify(object) autoreleasepool{} __weak __typeof__(object) weak##_##object = object;
#else
#define weakify(object) autoreleasepool{} __block __typeof__(object) block##_##object = object;
#endif
#else
#if __has_feature(objc_arc)
#define weakify(object) try{} @finally{} {} __weak __typeof__(object) weak##_##object = object;
#else
#define weakify(object) try{} @finally{} {} __block __typeof__(object) block##_##object = object;
#endif
#endif
#endif

#ifndef strongify
#if DEBUG
#if __has_feature(objc_arc)
#define strongify(object) autoreleasepool{} __typeof__(object) object = weak##_##object;
#else
#define strongify(object) autoreleasepool{} __typeof__(object) object = block##_##object;
#endif
#else
#if __has_feature(objc_arc)
#define strongify(object) try{} @finally{} __typeof__(object) object = weak##_##object;
#else
#define strongify(object) try{} @finally{} __typeof__(object) object = block##_##object;
#endif
#endif
#endif

#define WeakSelf(weakSelf)  __weak __typeof(self) weakSelf = self;
#define NAVH (MAX(Device_Width, Device_Height)  == 812 ? 88 : 64)
#define TABBARH 49
#define Device_Width  [[UIScreen mainScreen] bounds].size.width//获取屏幕宽高
#define Device_Height [[UIScreen mainScreen] bounds].size.height

#pragma mark - 设置网络加载图片
#define SD_SETIMAGE_PlaceHolder(imageView, imageUrl, imagePlaceHolder) [imageView sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:imagePlaceHolder]];

#endif /* ControllerHeader_h */
