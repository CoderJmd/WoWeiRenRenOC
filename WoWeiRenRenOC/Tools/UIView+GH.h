//
//  UIView+GH.h
//  LaborUnion
//
//  Created by BPTC on 15/7/29.
//  Copyright (c) 2015年 BPTC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (GH)

@property (nonatomic, assign) CGFloat x;
@property (nonatomic, assign) CGFloat y;
@property (nonatomic, assign) CGFloat centerX;
@property (nonatomic, assign) CGFloat centerY;
@property (nonatomic, assign) CGFloat width;
@property (nonatomic, assign) CGFloat height;
@property (nonatomic, assign) CGSize size;
@property (nonatomic, assign) CGPoint origin;

/**
 * Shortcut for frame.origin.x.
 *
 * Sets frame.origin.x = left
 */
@property (nonatomic) CGFloat left;

/**
 * Shortcut for frame.origin.y
 *
 * Sets frame.origin.y = top
 */
@property (nonatomic) CGFloat top;

/**
 * Shortcut for frame.origin.x + frame.size.width
 *
 * Sets frame.origin.x = right - frame.size.width
 */
@property (nonatomic) CGFloat right;

/**
 * Shortcut for frame.origin.y + frame.size.height
 *
 * Sets frame.origin.y = bottom - frame.size.height
 */
@property (nonatomic) CGFloat bottom;


/**
 * The view bounds centerY
 */
@property (nonatomic, readonly) CGFloat boundsCenterY;

/**
 * The view bounds centerY
 */
@property (nonatomic, readonly) CGFloat boundsCenterX;
@end
