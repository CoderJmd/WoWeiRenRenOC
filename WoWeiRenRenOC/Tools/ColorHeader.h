//
//  ColorHeader.h
//  XiaoMiFeng
//
//  Created by 杨涛 on 17/8/31.
//  Copyright © 2017年 杨涛. All rights reserved.
//

#ifndef ColorHeader_h
#define ColorHeader_h


#define kRGBColor(r, g, b) [UIColor colorWithRed:(r/255.0) green:(g/255.0) blue:(b/255.0) alpha:1.0] // 颜色
#define kRGBAColor(r, g, b, a) [UIColor colorWithRed:(r/255.0f) green:(g/255.0f) blue:(b/255.0f) alpha:a] // 颜色+透明度
#define kColor(c) [UIColor colorWithRed:(c)/255.0 green:(c)/255.0 blue:(c)/255.0 alpha:1.0] // 颜色(r,g,b相同）
#define kAColor(c, a) [UIColor colorWithRed:(c)/255.0 green:(c)/255.0 blue:(c)/255.0 alpha:a] // 颜色(r,g,b相同) + 透明度

// 十六进制color  FloatColor(0x333333)
#define FloatColor(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16)) / 255.0 \
green:((float)((rgbValue & 0xFF00) >> 8)) / 255.0 \
blue:((float)(rgbValue & 0xFF)) / 255.0 alpha:1.0]

//自定义颜色
#define kColorHexString(hexString)  [UIColor colorWithHexString:hexString]
#define RandomColor kRGBAColor(arc4random_uniform(256), arc4random_uniform(256), arc4random_uniform(256), arc4random_uniform(256)) // 随机色

#define VCBgcolor   kColor(245)

#define ContentDarkColor kRGBColor(45, 57, 56)  //内容最深的颜色

#define RedColor kRGBColor(255, 110, 141) // 红色
#define OrangeColor kRGBColor( 240, 102, 48) // 橙色
#define YellowColor kRGBColor(244, 172, 43) // 黄色
#define GreenColor kRGBColor(178, 219, 113) // 绿色
#define BlueColor kRGBColor(26, 171, 168) // 蓝色
#define PurpleColor kRGBColor(186, 120, 236) // 紫色

#define LightGreyColor kRGBColor(245, 245, 245) // 浅灰色
#define WhiteColor [UIColor whiteColor] // 白色
#define BlackColor kRGBColor(45, 57, 56) // 黑色
#define BlackColor2  kRGBAColor(45, 57, 56, 0.5) // 半黑色
#define ClearColor [UIColor clearColor] // 透明
#define TextOrangeColor kRGBColor(189, 135, 35) //
#endif /* ColorHeader_h */
