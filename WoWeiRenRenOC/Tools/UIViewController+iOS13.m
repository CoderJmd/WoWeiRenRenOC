//
//  UIViewController+iOS13.m
//  SchoolApp
//
//  Created by 姜孟东 on 2020/2/3.
//  Copyright © 2020 znz. All rights reserved.
//

#import "UIViewController+iOS13.h"




@implementation UIViewController (iOS13)
- (UIModalPresentationStyle)modalPresentationStyle{
    return UIModalPresentationFullScreen;
}
@end
