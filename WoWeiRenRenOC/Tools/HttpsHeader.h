//
//  ControllerHeader.h
//  XiaoMiFeng
//
//  Created by 杨涛 on 17/8/31.
//  Copyright © 2017年 杨涛. All rights reserved.
//

#ifndef HttpsHeader_h
#define HttpsHeader_h

//#import "NetRequestTool.h"//

// 测试环境
//#define BASE_URL @"http://api.dbase.club/xy_api/"
#define BASE_URL @"http://tyapi.znzkj.net/ofaapi/"
// 生产环境
//#define BASE_URL @"http://api.ofajs.cn/ofaapi/"


// 分享标题
#define SHARE_TITLE @"我为人人"
// 分享描述
#define SHARE_DES @"点击链接即可下载我为人人APP。"
//#define SHARE_HOME_URL @"http://tyapi.znzkj.net/ofaapi/index.html"
//#define SHARE_Detils_URL @"http://tyapi.znzkj.net/ofaapi/detail.html?"

#define SHARE_HOME_URL @"http://api.ofajs.cn/ofaapi/index.html"
#define SHARE_Detils_URL @"http://api.ofajs.cn/ofaapi/detail.html?"

#pragma mark - 登录模块
static NSString *const Register = @"user/register"; // 注册
static NSString *const Forget = @"forget"; // 忘记密码
static NSString *const Login = @"user/login"; // 登录
static NSString *const BusinessLogin = @"business/login"; // 登录
static NSString *const BannerinfoList = @"index/queryBannerList"; // banner
static NSString *const updateUserInfo = @"user/updateUserInfo"; // 修改会员资料 
static NSString *const QueryTj = @"index/queryTj"; //查询平台数据
static NSString *const OrderSaveOrder = @"order/saveOrder"; //下单
static NSString *const GetAliPayInfo = @"pay/getAliPayInfo"; //获取支付宝支付参数
static NSString *const GetPreReqParamsModel = @"pay/getPreReqParamsModel"; //获取微信支付参数



#pragma mark - 上传文件
static NSString *const UpdateImageAliyunFile = @"file/uploadQiniuFile"; // 上传文件

#endif /* HttpsHeader_h */
