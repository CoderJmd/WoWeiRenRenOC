//
//  NSDictionary+NoNull.h
//  WoWeiRenRenOC
//
//  Created by 姜伟 on 2020/3/18.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSDictionary (NoNull)

- (NSDictionary *)deleteNull;

@end

NS_ASSUME_NONNULL_END
