//
//  UIScrollView+UITouch.m
//  WoWeiRenRenOC
//
//  Created by 姜孟东 on 2020/9/19.
//  Copyright © 2020 姜孟东. All rights reserved.
//

#import "UIScrollView+UITouch.h"



@implementation UIScrollView (UITouch)

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    // 下面两句任选其一即可
    [super touchesBegan:touches withEvent:event];
     [[self nextResponder] touchesBegan:touches withEvent:event];
}
 
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesMoved:touches withEvent:event];
    [[self nextResponder] touchesMoved:touches withEvent:event];
}
 
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    [[self nextResponder] touchesEnded:touches withEvent:event];
}

@end
